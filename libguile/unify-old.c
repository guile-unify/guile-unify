/*
 	Copyright (C) 2009, 2010 Free Software Foundation, Inc.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#include<libguile.h>
#include<stdio.h>
//#include "libguile/unify.h"

#define DB(X)
#define DS(X)
#define gp_debug0(s)        DB(printf(s)      ; fflush(stdout))
#define gp_debug1(s,a)      DB(printf(s,a)    ; fflush(stdout))
#define gp_debug2(s,a,b)    DB(printf(s,a,b)  ; fflush(stdout))
#define gp_debug3(s,a,b,c)  DB(printf(s,a,b,c); fflush(stdout))
#define gp_debus0(s)        DS(printf(s)      ; fflush(stdout))
#define gp_debus1(s,a)      DS(printf(s,a)    ; fflush(stdout))
#define gp_debus2(s,a,b)    DS(printf(s,a,b)  ; fflush(stdout))
#define gp_debus3(s,a,b,c)  DS(printf(s,a,b,c); fflush(stdout))
/*
   000 - A ref to a new object
   010 - A cons ref
   110 - A cons rest
   100 - A rest

 1x11xx - unbounded
 1x01xx - eq
 0x11xx - val

  unbounded is a null reference aka X & 0b000  == 0
*/

/*
  We will work with three stacks.
  stack  - the variable stack
  cstack - storage stack to undo setted values
  wstack - frame stack in order to do batched backtracking
*/

struct gp_p_statistic
{
  unsigned int n;
  unsigned int nrem;
};

typedef union gp_cstack_t
{
  SCM * id;
  SCM   val;
} gp_cstack_t;

typedef union gp_rstack_t
{
  SCM          *id;
  SCM           val;
  union gp_rstack_t  *jump;
  gp_cstack_t  *branch_id;
} gp_rstack_t;

typedef union gp_wstack_t
{
  gp_cstack_t* ci;
  SCM*      si;
} gp_wstack_t;
  
int  gp_nw      = 20000;
gp_wstack_t SCM_ALIGNED(8) gp_wstack_a[20000];
gp_wstack_t SCM_ALIGNED(8) gp_wstack_b[20000];
gp_wstack_t * gp_wstack = gp_wstack_a;

int gp_nc      = 100000;
gp_cstack_t SCM_ALIGNED(8) gp_cstack_a[100000];
gp_cstack_t SCM_ALIGNED(8) gp_cstack_b[100000];
gp_cstack_t * gp_cstack = gp_cstack_a;

int gp_ns      = 200000;
SCM SCM_ALIGNED(8) gp_stack_a[200000];
SCM SCM_ALIGNED(8) gp_stack_b[200000];
SCM * gp_stack = gp_stack_a;

SCM* gp_si;
SCM* gp_si_b;
SCM* gp_si_a;

gp_wstack_t *gp_wi;
gp_wstack_t *gp_wi_a;
gp_wstack_t *gp_wi_b;

gp_cstack_t *gp_ci;
gp_cstack_t *gp_ci_a;
gp_cstack_t *gp_ci_b;

gp_cstack_t *gp_nnc;
gp_cstack_t *gp_nnc_a;
gp_cstack_t *gp_nnc_b;

SCM *gp_nns;
SCM *gp_nns_a;
SCM *gp_nns_b;

gp_wstack_t *gp_nnw;
gp_wstack_t *gp_nnw_a;
gp_wstack_t *gp_nnw_b;

scm_t_bits gp_smob_t;

int closure_alloc_context = 0;

static inline SCM alloc_unify_words (scm_t_bits car, scm_t_uint16 n_words)
{
  SCM z = SCM_PACK((scm_t_bits) gp_si);
  if(gp_si + n_words > gp_nns) goto er;
  
  *gp_si = SCM_PACK(car);
  gp_si = gp_si + n_words;

  return z;

 er:
  scm_out_of_range(NULL, SCM_I_MAKINUM(gp_nc));
}
// Assumes stack_a and stack_b are allocated in order they appear
#define GP(scm) ( !(0x7 & SCM_UNPACK(scm))                     &&     \
		  (SCM_UNPACK(scm) >= (scm_t_bits) gp_stack_a) &&     \
		  (SCM_UNPACK(scm) <  (scm_t_bits) gp_nns_b))

#define B(c) ((scm_t_bits) c)
#define GP_FF (~B(0))
#define GPM_UNBOUND B(0x2c)
#define GPI_CONS    B(2)
#define GPM_EQ      B(0x24)
#define GPM_ATTR    B(0x4)
#define GPM_VAL     B(0xc)
#define GPM_CONS    (~ B(2))
#define GPM_POINTER B(0x7)
#define GP_TP_MASK   B(0x2e)
#define SET_FR(id,x)    (((x) & (~0x3f)) | ((id) & GP_TP_MASK))
#define GP_UNBOUND(ref) (( SCM_UNPACK(ref) & GP_TP_MASK) == GPM_UNBOUND)
#define GP_POINTER(ref) ( (SCM_UNPACK(ref) & GPM_POINTER) == 0)
#define GP_CONS(ref)    (  SCM_UNPACK(ref) & GPI_CONS    )
#define GP_EQ(ref)      ( (SCM_UNPACK(ref) & GP_TP_MASK) == GPM_EQ)
#define GP_ATTR(ref)    ( (SCM_UNPACK(ref) & GP_TP_MASK) == GPM_ATTR)
#define GP_VAL(ref)     ( (SCM_UNPACK(ref) & GP_TP_MASK) == GPM_VAL)
#define GP_MK_FRAME_VAL(fr)  ((fr) & GPM_VAL)
#define GP_MK_FRAME_EQ(fr)   ((fr) & GPM_EQ)
#define GP_MK_FRAME_ATTR(fr) ((fr) & GPM_ATTR)
#define GP_MK_FRAME_CONS(fr) ((fr) | GPI_CONS)
#define GP_UNMASK_CONSBIT(ref) SCM_PACK( (SCM_UNPACK(ref)) & GPM_CONS)

#define NIMP_SYMBOL(ref) (SCM_TYP7 (ref) == scm_tc7_symbol)
#define NIMP_CONSP(x)    ((1 & SCM_CELL_TYPE (x)) == 0)
#define SCM_TO_FI(x) ( SCM_UNPACK(x) - 2 )
#define FI_TO_SCM(x) SCM_PACK((x) + 2)
//#define FR_TO_WI(x)  (gp_wstack + (((x) & (~0x3f)) >> 4))
#define FR_TO_WI(x)  (gp_wstack + (((x) & (~0x3f)) >> 5))
scm_t_bits gp_frame_id   = GPM_UNBOUND;
scm_t_bits gp_frame_id_b = GPM_UNBOUND;
scm_t_bits gp_frame_id_a = GPM_UNBOUND;

inline void swap_to_b()
{
  gp_si_a = gp_si;
  gp_si   = gp_si_b;

  gp_wi_a = gp_wi;
  gp_wi   = gp_wi_b;

  gp_ci_a = gp_ci;
  gp_ci   = gp_ci_b;

  gp_nnc_a = gp_nnc;
  gp_nnc   = gp_nnc_b;

  gp_nns_a = gp_nns;
  gp_nns   = gp_nns_b;

  gp_nnw_a = gp_nnw;
  gp_nnw   = gp_nnw_b;

  gp_frame_id_a = gp_frame_id;
  gp_frame_id   = gp_frame_id_b;

  gp_wstack = gp_wstack_b;
  gp_cstack = gp_cstack_b;
  gp_stack  = gp_stack_b;
}

inline void swap_to_a()
{
  gp_si_b = gp_si;
  gp_si   = gp_si_a;

  gp_wi_b = gp_wi;
  gp_wi   = gp_wi_a;

  gp_ci_b = gp_ci;
  gp_ci   = gp_ci_a;

  gp_nnc_b = gp_nnc;
  gp_nnc   = gp_nnc_a;

  gp_nns_b = gp_nns;
  gp_nns   = gp_nns_a;

  gp_nnw_b = gp_nnw;
  gp_nnw   = gp_nnw_a;

  gp_frame_id_b = gp_frame_id;
  gp_frame_id   = gp_frame_id_a;

  gp_wstack = gp_wstack_a;
  gp_cstack = gp_cstack_a;
  gp_stack  = gp_stack_a;
}

/*
static inline SCM* GP_GETREF(SCM x)
{
  scm_t_bits up = SCM_UNPACK(x);
  gp_debug1("getref> unpacked %x\n",up);
  return (SCM *) up;
}

static inline SCM GP_UNREF(SCM *x)
{
  scm_t_bits up = (scm_t_bits) x;
  gp_debug1("unref> unpacked %x\n",up);
  return SCM_PACK(up);
  }
*/

#define GP_GETREF(x) ((SCM *) (x))

#define GP_UNREF(x)  ((SCM)   (x))


inline static SCM GP_IT(SCM* id)
{
  return GP_UNREF(id);
}


static SCM* UN_GP(SCM scm)
{
  return GP_GETREF(scm);
}


/*
static inline int GP(SCM scm)
{
  return SCM_UNPACK(scm) & B(2);
}
*/

 /*
#define GP_IT(id) SCM_PACK(SCM_UNPACK(GP_UNREF(id)) + B(2))
#define UN_GP(scm) GP_GETREF(SCM_PACK(SCM_UNPACK(scm) - B(2)))
 */
 //#define GP(scm) (SCM_UNPACK(scm) & B(2))
 /* Moved to unify.h
#define GP(scm) ( !(0x7 & SCM_UNPACK(scm))                   &&	\
		  (SCM_UNPACK(scm) >= (scm_t_bits) gp_stack) &&	\
		  (SCM_UNPACK(scm) <  (scm_t_bits) gp_nns))

 */
SCM gp_unbound_sym;
SCM gp_unbound_str;
SCM gp_unwind_fluid;
SCM gp_cons_sym;
SCM gp_cons_str;


static inline  scm_t_bits gp_get_frame(SCM *id)
{
  SCM ref = *id;
  SCM val = *(id+1);
  scm_t_bits ret;
  if (GP_POINTER(ref))
    ret = SCM_UNPACK(val);
  else 
    ret = (SCM_UNPACK(ref) & ~0x3f) | 0x2c;
  gp_debug1("get_frame> %x\n",(int) ret);
  
  return ret;
}

#define GP_TEST_CSTACK if(gp_ci > gp_nnc) scm_out_of_range(NULL, SCM_I_MAKINUM(gp_nc))

static inline  scm_t_bits gp_store_var_2(SCM *id,scm_t_bits fr)
{
  GP_TEST_CSTACK;
  if(!(GP_CONS(*id)))
    {
      if(1 || gp_frame_id > fr)
	{      
	  (*(gp_ci+0)).id  = id;
	  (*(gp_ci+1)).val = *(id+0);
	  (*(gp_ci+2)).val = *(id+1);
	  gp_ci += 3;
	}
    }
  
  return gp_frame_id;    
}
/*
#define HANDLE_ATTR(id,v)						\
  if(GP_ATTR(id))							\
    if(scm_call_2(*(id+1),GP_UNREF(id),v) == SCM_BOOL_F)		\
    {									\
      scm_misc_error ("set error", "cannot set attribute", scm_list_1 (v)); \
    }
*/
#define HANDLE_ATTR(id,v)

static inline void gp_set_val(SCM *id, SCM v)
{
  scm_t_bits fr;
  SCM val; 
  DS(if(!v) gp_debug0("setting 0 value\n");)
  fr  =  gp_get_frame(id);
  fr  =  gp_store_var_2(id,fr);
  val =  SCM_PACK(GP_MK_FRAME_VAL(fr));
  gp_debug2("set-val> id = %x, val = %x\n",(int) id, (int) SCM_UNPACK(val));
  HANDLE_ATTR(id,v);
  *(id + 0) = val;
  *(id + 1) = v;
}

static inline  void gp_set_ref(SCM *id, SCM ref)
{
  scm_t_bits fr = gp_get_frame(id);
  fr = gp_store_var_2(id,fr);
  gp_debug0("setref>\n");
  HANDLE_ATTR(id,ref);
  *(id + 0) = ref;
  *(id + 1) = SCM_PACK(fr);
}

static inline  void gp_set_eq(SCM *id, SCM v) 
{
  scm_t_bits fr = gp_get_frame(id);
  gp_debug1("got fr %x\n",fr);
    DS(if(!v) gp_debug0("setting 0 value\n");)
  fr = gp_store_var_2(id,fr);
  gp_debug0("handled storing\n");
  HANDLE_ATTR(id,v);
  *(id + 0) = SCM_PACK(GP_MK_FRAME_EQ(fr));
  gp_debug2("set id = %x 0 val = %x\n",id,SCM_UNPACK(*id)) ;
  *(id + 1) = v;
  gp_debug2("set id = %x 1 val = %x\n",id,SCM_UNPACK(v));
}

static inline  void gp_set_attr(SCM *id, SCM attr) 
{
  scm_t_bits fr = gp_get_frame(id);
  gp_debug1("got fr %x\n",fr);
  DS(if(!attr) gp_debug0("setting 0 value\n");)
  fr = gp_store_var_2(id,fr);
  gp_debug0("handled storing\n");
  HANDLE_ATTR(id,attr);
  *(id + 0) = SCM_PACK(GP_MK_FRAME_ATTR(fr));
  gp_debug2("set id = %x 0 val = %x\n",id,SCM_UNPACK(*id)) ;
  *(id + 1) = attr;
  gp_debug2("set id = %x 1 val = %x\n",id,SCM_UNPACK(attr));
}

static inline  void gp_force_attr(SCM *id, SCM attr) 
{
  scm_t_bits fr = gp_get_frame(id);
  gp_debug1("got fr %x\n",fr);
    DS(if(!attr) gp_debug0("setting 0 value\n");)
  fr = gp_store_var_2(id,fr);
  gp_debug0("handled storing\n");
  *(id + 0) = SCM_PACK(GP_MK_FRAME_ATTR(fr));
  gp_debug2("set id = %x 0 val = %x\n",id,SCM_UNPACK(*id)) ;
  *(id + 1) = attr;
  gp_debug2("set id = %x 1 val = %x\n",id,SCM_UNPACK(attr));
}

static inline void gp_set_unbound(SCM *id, SCM v) 
{
  scm_t_bits fr = gp_get_frame(id);
  gp_debug0("set-unbound>\n");
  fr = gp_store_var_2(id,fr);
  HANDLE_ATTR(id,gp_unbound_sym);
  *(id + 0) = SCM_PACK(fr);
  *(id + 1) = v;
}


struct gp_lookup_t
{
  SCM        *id;
  SCM         val;
  SCM         ref;
};

struct gp_lookup_t gp_ret;

/*
static void gp_lookup(SCM *id);

inline static void gp_lookup1(SCM *id)
{
  gp_ret.ref = *id;
  gp_debug1("lookup> id = %x\n",id);
  if(GP_POINTER(gp_ret.ref))
    {
      gp_debug0("lookup> pointer\n");
      gp_lookup(GP_GETREF(gp_ret.ref));
      gp_set_ref(id,GP_UNREF(gp_ret.id));
      return;
    }
  else 
    if(GP_UNBOUND(gp_ret.ref))
      {	
	gp_debug0("lookup> unbound\n");
	gp_ret.val = (SCM) 0;
      }
    else
      gp_ret.val = *(id + 1);
  if(GP_CONS(gp_ret.ref))
    {
      gp_debug0("looked a cons up!\n");
    }
  else
    {
      gp_debug0("looked a value up!\n");
    }
  gp_ret.id = id;
  return;
}

static void gp_lookup(SCM *id)
{
  gp_ret.ref = *id;
  gp_debug1("lookup> id = %x\n",id);
  if(GP_POINTER(gp_ret.ref))
    {
      gp_debug0("lookup> pointer\n");
      gp_lookup1(GP_GETREF(gp_ret.ref));
      gp_set_ref(id,GP_UNREF(gp_ret.id));
      return;
    }
  else 
    if(GP_UNBOUND(gp_ret.ref))
      {	
	gp_debug0("lookup> unbound\n");
	gp_ret.val = (SCM) 0;
      }
    else
      gp_ret.val = *(id + 1);
  if(GP_CONS(gp_ret.ref))
    {
      gp_debug0("looked a cons up!\n");
    }
  else
    {
      gp_debug0("looked a value up!\n");
    }
  gp_ret.id = id;
  return;
}
*/

static void gp_lookup(SCM *id)
{
  SCM ref, *stack[10];
  int sp;
  sp = 0;
  gp_debug0("lookup>\n"); 
 retry:
  ref = *id;  
  if(GP_POINTER(ref))
    {
      if(SCM_LIKELY(sp<10))
	{
	  stack[sp++] = id;
	  id = GP_GETREF(ref);
	  goto retry;
	}
      else
	{
	  gp_debug0("long chain\n");
	  gp_lookup(id);
	  //while(sp>0) gp_set_ref(stack[--sp],GP_UNREF(gp_ret.id));
	  return;
	}
    }

  gp_ret.id  =  id;
  gp_ret.ref =  ref;
  if(GP_UNBOUND(ref))
    {	
      gp_ret.val = (SCM) 0;
    }
  else
    {
      gp_ret.val = *(id + 1);
      DS(if(!gp_ret.val) gp_debug0("looked up a value 0 - prolem!!\n");)
    }

  //while(sp>0) gp_set_ref(stack[--sp],GP_UNREF(id));
  return;
}




#define GP_TEST_WSTACK if(gp_wi > gp_nnw) scm_out_of_range(NULL, SCM_I_MAKINUM(gp_nw))


SCM_DEFINE (gp_gp_newframe, "gp-newframe",0,0,0,(),
	    "Created a prolog frame to backtrack from")
#define FUNC_NAME s_gp_gp_newframe
{
  SCM ret;

  GP_TEST_WSTACK;
  gp_debug0("newframe>\n");
  gp_debug2("newframe>  store fr = %x amd wi = %x\n",gp_frame_id,gp_wi);

  *(gp_wi+0) = (gp_wstack_t) gp_ci;
  *(gp_wi+1) = (gp_wstack_t) gp_si;
  gp_wi += 2;
  ret = FI_TO_SCM(gp_frame_id);
  gp_frame_id +=  ((scm_t_bits) 0x40);
  return ret;
}
#undef FUNC_NAME

static inline SCM gp_newframe()
{
  SCM ret;

  GP_TEST_WSTACK;
  *(gp_wi+0) = (gp_wstack_t) gp_ci;
  *(gp_wi+1) = (gp_wstack_t) gp_si;
  gp_wi += 2;
  ret = FI_TO_SCM(gp_frame_id);
  gp_frame_id +=  ((scm_t_bits) 0x40);
  return ret;
}

#define DB(X) 

#define DDB(X)
/*postpone functionality*/
#define GP_P_SIZE 90000000
#define GP_P_ENDS (GP_P_SIZE - 9)
gp_rstack_t  SCM_ALIGNED(8) gp_postpones[GP_P_SIZE];
gp_rstack_t *gp_p_pt_e   = gp_postpones + GP_P_ENDS;
gp_rstack_t *gp_p_pt     = gp_postpones + 1;
gp_rstack_t *gp_p_pt_end = gp_postpones + GP_P_ENDS;
gp_rstack_t *gp_p_frame  = gp_postpones;
gp_cstack_t *gp_p_next_id;
gp_cstack_t *gp_p_bottom;
gp_rstack_t *gp_p_nextbranch;
SCM          gp_p_token;
int          gp_p_postpone_p = 0;
int          gp_use_token_p  = 0;

#define token       gp_p_token
#define frame       gp_p_frame
#define pt_end      gp_p_pt_end
#define pt_e        gp_p_pt_e
#define next_id     gp_p_next_id
#define bottom      gp_p_bottom
#define nextbranch  gp_p_nextbranch

gp_rstack_t SCM_ALIGNED(8) gp_sestack[10000];
gp_rstack_t *se_pt;

#define P_UNREF(x)  SCM_PACK( (scm_t_bits) (x) | 2)
#define P_GETREF(x) ((gp_rstack_t *) (SCM_UNPACK(x) & ~2))

#define P_BASE(x) ((int) ((scm_t_bits) (x) - (scm_t_bits) gp_postpones))
#define S_BASE(x) ((int) ((scm_t_bits) (x) - (scm_t_bits) gp_sestack))
#define C_BASE(x) ((int) ((scm_t_bits) (x) - (scm_t_bits) gp_cstack))
#define BASE(x)   ((int) ((scm_t_bits) (x) - (scm_t_bits) gp_stack))

#define GP_TEST_P if( (gp_p_pt < pt_e) && (pt_e - gp_p_pt < 10)) \
scm_out_of_range(NULL, SCM_I_MAKINUM(GP_P_SIZE))

void gp_print_p_stack();
static inline void gp_p_clean_branch(gp_rstack_t *pt);

//Depreciated, use a move instead!!
static inline void gp_p_cleanup()
{
  pt_e = frame  - 4;
  if(pt_e < gp_postpones) 
    switch(gp_postpones - frame)
      {
      case 1:
	pt_e = pt_end;
      case 2:
	pt_e = pt_end - 1;
      case 3:
	pt_e = pt_end - 2;
      case 4:
	pt_e = pt_end - 3;
      }
}

static inline void prepare_for_move(gp_rstack_t *pt, scm_t_bits delta);
static inline void gp_p_cleanup_move(SCM start)
{
  gp_rstack_t *s,*e,*ee,*ep;
  scm_t_bits delta;
  DDB(gp_print_p_stack());
  gp_debug0("p cleanup move\n");
  s  = P_GETREF(start) + 1;
  e  = frame           + 1;
  ee = gp_p_pt;
  ep = ee - 1;
  gp_debug1("p cleanup pt = %d\n",(scm_t_bits) gp_p_pt - (scm_t_bits) gp_postpones);

  //move the pointers inside of the tree
  delta = ((scm_t_bits) e - (scm_t_bits) s) / sizeof(gp_rstack_t);
  gp_debug1("p delta = %d\n",delta);
  //move all pointers inside delta
  prepare_for_move(ep,delta);

  //make sure that we move the frame , pt and nextbranch refs!
  frame       = s - 1;
  nextbranch -= delta;
  gp_p_pt    -= delta;

  
  gp_debug1("p cleanup pt = %d\n",(scm_t_bits) gp_p_pt - (scm_t_bits) gp_postpones);
  gp_debug0("p cleanup move move\n");
  //move all data
  for(;e != ee; s++,e++)   *s = *e;  
  DDB(gp_print_p_stack();)
}



//Fr1 nextbr1 next_id1 (*)
static inline void gp_p_newframe_simple()
{
  gp_debug0("p newframe\n");
  GP_TEST_P;
  DDB(gp_print_p_stack());
  gp_p_clean_branch(gp_p_pt - 1);

  (gp_p_pt + 0)->jump      = frame;
  (gp_p_pt + 1)->jump      = nextbranch;
  (gp_p_pt + 2)->branch_id = next_id;
  (gp_p_pt + 3)->branch_id = bottom;
  (gp_p_pt + 4)->val       = SCM_PACK(2);
  (gp_p_pt + 5)->val       = SCM_PACK(1);
  frame = gp_p_pt + 5;
  gp_p_pt += 6;
  
  nextbranch = frame;
  next_id    = gp_cstack - 1;  
  bottom     = next_id;
}

//Fr1 nextbr1 next_id1 token (*)
static inline void gp_p_newframe_token()
{
  gp_debug0("p newframe\n");
  GP_TEST_P;
  DDB(gp_print_p_stack());
  gp_p_clean_branch(gp_p_pt - 1);
  gp_debug0("p newframe cleaned\n");
  (gp_p_pt + 0)->jump      = frame;
  (gp_p_pt + 1)->jump      = nextbranch;
  (gp_p_pt + 2)->branch_id = next_id;
  (gp_p_pt + 3)->branch_id = bottom;
  (gp_p_pt + 4)->val       = token;
  (gp_p_pt + 5)->val       = 0;
  (gp_p_pt + 6)->val       = SCM_PACK(1);
  frame = gp_p_pt + 6;
  gp_p_pt += 7;
  
  nextbranch = frame;
  next_id    = gp_cstack - 1;  
  bottom     = next_id;
  token      = SCM_BOOL_F;
  
  gp_debug0("p newframe exit\n");
}

static inline void gp_p_newframe()
{
  if(gp_use_token_p)    
    gp_p_newframe_token();
  else
    gp_p_newframe_simple();
}


static inline void gp_p_unwind_simple()
{
  gp_debug1("p unwind frame: %x\n", (scm_t_bits) frame);

  bottom     =  (frame-2)->branch_id;
  next_id    =  (frame-3)->branch_id;
  gp_debug0("ni ... ");
  nextbranch =  (frame-4)->jump;
  gp_debug0("nb ... ");
  gp_p_pt    =  frame - 5;
  gp_debug0("pt ... ");
  frame      =  (frame-5)->jump;
  gp_debug0("fr\n");  
}

static inline void gp_p_unwind_token()
{
  gp_debug1("p unwind frame: %x\n", (scm_t_bits) frame);

  token      =  (frame-2)->val;
  bottom     =  (frame-3)->branch_id;
  next_id    =  (frame-4)->branch_id;
  gp_debug0("ni ... ");
  nextbranch =  (frame-5)->jump;
  gp_debug0("nb ... ");
  gp_p_pt    =  frame - 6;
  gp_debug0("pt ... ");
  frame      =  (frame-6)->jump;
  gp_debug0("fr\n");  
}

static inline void gp_p_unwind()
{
  DDB(gp_print_p_stack());
  if(gp_use_token_p)
    gp_p_unwind_token();
  else
    gp_p_unwind_simple();
  DDB(gp_print_p_stack());
}

static inline void gp_p_unmark(SCM *id)
{
  SCM ref = *id;
  gp_debug0("p unmark\n");
  if (GP_POINTER(ref))
    *(id + 1) = SCM_PACK(SCM_UNPACK(*(id+1)) & (~0x10));    
  else 
    *id       = SCM_PACK(SCM_UNPACK(*(id+0)) & (~0x10));     
}
#define unmark gp_p_unmark


static inline int gp_p_marked(SCM *id)
{
  SCM ref = *id;
  int ret;
  if (GP_POINTER(ref))
    ret = SCM_UNPACK(*(id+1)) & 0x10;
  else
    ret = SCM_UNPACK(*(id+0)) & 0x10;
  //gp_debug1("p marked %x\n",ret);
  return ret;

}
#define marked gp_p_marked


static inline void gp_p_mark(SCM *id)
{
  SCM ref = *id;
  if (GP_POINTER(ref))
    *(id + 1) = SCM_PACK(SCM_UNPACK(*(id+1)) | 0x10);    
  else 
    *id       = SCM_PACK(SCM_UNPACK(*(id+0)) | 0x10);     
}

#define mark gp_p_mark


static inline int gp_p_is_jump(gp_rstack_t *pt)
{
  int ret = 
    ( (((scm_t_bits) pt->jump) & 7) == 0)  && 
    ( pt->jump <= pt_end)                  && 
    ( pt->jump >= gp_postpones);

  //gp_debug1("p is jump %d\n",ret);
  return ret;    
}
#define is_jump gp_p_is_jump

static inline int gp_p_at_end(gp_rstack_t *pt)
{
  //gp_debug0("p at_end\n");
  return pt->val == SCM_PACK(1);
}
#define at_end gp_p_at_end



static inline int gp_p_id_at_branch(gp_cstack_t *id)
{
  return id == next_id;
}
#define id_at_branch gp_p_id_at_branch

static inline void gp_p_clean_branch(gp_rstack_t *pt)
{
 retry:
  while(!is_jump(pt) && !at_end(pt))
    {
      if(pt->val == SCM_PACK(3)) { pt--; continue; }
      pt -= 2;      
      unmark(pt->id);      
      pt--;
    }

  if(at_end(pt))
    {
      if(is_jump(pt - 1))
	{
	  pt = (pt - 1)->jump;
	  goto retry;
	}
    }
  else      
    {
      if(gp_use_token_p)
	{
	  if((pt - 1)->val == SCM_EOL)
	    {
	      pt = pt->jump - 1;
	      goto retry;
	    }
	  
	  if(pt->jump->val == SCM_EOL)
	    {
	      pt -= 2;
	      goto retry;
	    }
	}
    }
}

#define clean_branch gp_p_clean_branch

static inline void gp_p_store_branch()
{
  gp_rstack_t *pt;

  gp_debug0("p store branch\n");
  GP_TEST_P;
  gp_debug1("p store check %d\n",(scm_t_bits) nextbranch & 0x7);

  DDB(gp_print_p_stack());

  pt = gp_p_pt - 1;
  clean_branch(pt);
  gp_debug0("cleaned>\n");
  if(gp_use_token_p)
    {
      gp_p_pt->val = token;
      gp_p_pt++;
      gp_p_pt->jump = nextbranch;
      gp_p_pt++;

      nextbranch = (nextbranch + 1)->jump;
      if(nextbranch == frame)
	next_id = gp_cstack - 1;
      else
	next_id  = (nextbranch + 2)->branch_id;
    }
  else
    {
      gp_p_pt->jump = nextbranch;
      gp_p_pt++;
      gp_debug1("nextbranch >%d\n",P_BASE(nextbranch));
      nextbranch = nextbranch->jump;
      gp_debug1("nextbranch >%d\n",P_BASE(nextbranch));
      if(nextbranch == frame)
	next_id = gp_cstack - 1;
      else
	next_id  = (nextbranch + 1)->branch_id;
    }

  DDB(gp_print_p_stack());
}

#define store_branch gp_p_store_branch



static inline void gp_set_frame(SCM *id)
{
  gp_debug1("set-frame id %d\n",(scm_t_bits) id - (scm_t_bits) gp_stack);
  if (GP_POINTER(*id))
    {
      gp_debug0("-pointer-\n");
      *(id + 1) = SCM_PACK((gp_frame_id & ~0x1f) | (SCM_UNPACK(*(id + 1)) & 0x1f));
    }
  else
    {
      *(id + 0) = SCM_PACK((gp_frame_id & ~0x1f) | (SCM_UNPACK(*(id + 0)) & 0x1f));
      gp_debug0("-val-\n");
    }
}


//jump branchid lam .
static inline void gp_p_postpone(SCM lam)
{
  gp_debug0("p postpone\n");
  GP_TEST_P;

  DDB(gp_print_p_stack());
  (frame-1)->val = SCM_PACK(SCM_UNPACK((frame-1)->val) + 2);
  clean_branch(gp_p_pt - 1);
  if(gp_use_token_p) 
    {
      gp_p_pt->val = token;
      token = SCM_BOOL_F;
      
      (gp_p_pt + 1)->jump = nextbranch;  
      nextbranch          = gp_p_pt;
      gp_p_pt+=2;
    }
  else
    {
      gp_p_pt->jump      = nextbranch;  
      nextbranch         = gp_p_pt;
      gp_p_pt++;
    }
  
 
  gp_p_pt->branch_id = bottom; 
  next_id            = bottom;
  gp_p_pt ++;
  bottom        = gp_ci;
 
  gp_p_pt->val  = lam;
  gp_p_pt++;
  
  gp_p_pt->val  = SCM_PACK(1);
  gp_p_pt++;
}

static inline void gp_p_postpone_token(SCM lam, SCM tok)
{
  struct gp_p_statistic *stat;

  gp_debug0("p postpone token\n");
  GP_TEST_P;
  

  DDB(gp_print_p_stack());
  
  stat = (struct gp_p_statistic *) (frame-1);

  //Add statistic: number of active postpones
  stat->n += 1;

  clean_branch(gp_p_pt - 1);

  gp_p_pt->val = token;
  token = tok;

  (gp_p_pt + 1)->jump = nextbranch;  
  nextbranch          = gp_p_pt;
  gp_p_pt+=2;
  
 
  gp_p_pt->branch_id = bottom; 
  next_id            = bottom;
  gp_p_pt ++;
  bottom        = gp_ci;
 
  gp_p_pt->val  = lam;
  gp_p_pt++;
  
  gp_p_pt->val  = SCM_PACK(1);
  gp_p_pt++;

  DDB(gp_print_p_stack());
}

void gp_print_p_stack()
{
  gp_rstack_t *pt, *fr;
  struct gp_p_statistic *stat;

  pt = gp_p_pt - 1;
  fr = frame;

 retry:
  if(fr<gp_postpones)  {printf("fr Error!\n");return;}
  if(pt<gp_postpones)  {printf("pt Error!\n");return;}
  while(!at_end(pt))
    {
      if(pt->val == SCM_PACK(3)) 
	{ 
	  printf("%d empty\n",P_BASE(pt));	  
	  pt--; continue; 
	}
      if(fr<gp_postpones)  {printf("fr Error!\n");return;}
      if(pt<gp_postpones)  {printf("pt Error!\n");return;}

      printf("%d",P_BASE(pt));
      if(is_jump(pt))
	{
	  printf(" jump to %d\n",P_BASE(pt->jump));
	  pt--;
	  if(gp_use_token_p)
	    {
	      printf("%d",P_BASE(pt));
	      if(pt->val == SCM_EOL)
		printf(" erased\n");
	      else
		printf(" token\n");
	      pt --;
	    }
	  continue;
	}
      printf(" val\n");
      pt--;
      printf("%d",P_BASE(pt));
      printf(" val\n");
      pt--;
      printf("%d",P_BASE(pt));
      if(gp_p_marked(pt->id))
	printf(" id = %d - marked\n",BASE(pt->id));
      else
	printf(" id = %d\n",BASE(pt->id));
      pt--;
    }
  printf("%d",P_BASE(pt));
  printf(" .\n");
  if(fr == pt)
    {
      if(fr < gp_postpones + 3) return;

      pt --;
      printf("%d",P_BASE(pt));
      stat = (struct gp_p_statistic *) pt;
      printf(" nlive = %d\n",stat->n   );
      printf(" nrem  = %d\n",stat->nrem);

      if(gp_use_token_p)
	{
	  pt --;
	  printf("%d",P_BASE(pt));
	  printf(" token\n");
	}
      pt --;
      printf("%d",P_BASE(pt));
      printf(" bottom = %d\n", C_BASE(pt->branch_id));
      pt --;
      printf("%d",P_BASE(pt));
      printf(" next_id = %d\n", C_BASE(pt->branch_id));
      pt --;
      printf("%d",P_BASE(pt));
      printf(" nextbranch = %d\n", P_BASE(pt->jump));
      pt --;
      printf("%d",P_BASE(pt));
      printf(" frame = %d\n", P_BASE(pt->jump));	
      fr = pt->jump;
      if(fr < gp_postpones) {printf("Error!\n");return;}
      pt--;
      goto retry;
    }
  else
    {
      pt--;
      printf("%d",P_BASE(pt));
      if(is_jump(pt))
	printf(" continue at %d\n",P_BASE(pt->jump));
      else
	printf(" closure\n");
      pt--;

      printf("%d",P_BASE(pt));
      printf(" nextid = %d\n",C_BASE(pt->branch_id));
      pt--;

      printf("%d",P_BASE(pt));
      printf(" rejump to %d\n",P_BASE(pt->jump));
      pt--;

      if(gp_use_token_p)
	{
	  printf("%d",P_BASE(pt));
	  printf(" token\n");
	  pt --;
	}
          
      goto retry;
    }
}

/* not tested yet
static void gp_gc_mark_rstack()
{
  gp_rstack_t *pt = gp_p_pt;
  while(pt > gp_postpones + 3)
    {
      while(!at_end(pt))
	{
	  if(pt->val == SCM_PACK(3)) {pt--; continue;}
	  if(is_jump(pt))
	    {
	      if(gp_use_token_p)
		{
		  pt--;
		  scm_gc_mark(pt->val);
		}
	      pt--;
	      continue;
	    }
	  
	  if(pt->val && !GP_POINTER((pt-1)->val))
	    {
	      scm_gc_mark(pt->val);
	    }
	  pt -= 3;
	}
      if(frame == pt)
	{
	  if(gp_use_token_p)
	    {
	      pt --;
	      scm_gc_mark(pt->val);	      
	    }
	  pt -= 5;
	}
      else
	{
	  pt--;
	  if(!is_jump(pt)) scm_gc_mark(pt->val);
	  if(gp_use_token_p)
	    pt -= 4;
	  else
	    pt -= 3;
	}
    }
}
*/

 /*
static void gp_p_s_print()
{
  scm_t_bits code;
  gp_rstack_t *pt;
  
  pt = se_pt;
 retry:
  code = SCM_UNPACK((pt-3)->val);
  if(code == 4)
    {
      printf("%d <start>\n",S_BASE(pt-3));
      return;
    }
  printf("%d (%d)",S_BASE(pt-3),(int) code);
  switch(code & 0x3)
    {
    case 0:
      printf(" 0 branch jump %d %d\n",P_BASE((pt-2)->jump),P_BASE((pt-1)->jump));
      break;
    case 1:
      printf(" 1 branch jump %d %d\n",P_BASE((pt-2)->jump),P_BASE((pt-1)->jump));
      break;
    case 2:
      printf(" 2 branches jumps %d %d\n"
	     ,P_BASE((pt-2)->jump),P_BASE((pt-1)->jump));
    }
  pt -= 3;
  goto retry;
}
 */

static void gp_p_search_unwind(SCM s)
{
  gp_rstack_t *i,*ii;
  SCM ltoken;
  scm_t_bits code;
  int remove;
  //struct gp_p_statistic *stat;

  gp_debug0("search unwind\n");
  DB(gp_p_s_print());  

  i   = P_GETREF(s);
  ii  = se_pt - 1;

  ltoken = SCM_EOL;
  remove = 0;
  
  while((se_pt-3)->val  != SCM_PACK(4))
    {
      gp_debug1("se_pt %d ",S_BASE(se_pt - 3));
      gp_debug2("i %p, ii %p\n",(void *) i,(void *) ii);
      //if((scm_t_bits) ii == (scm_t_bits) i) goto out;

      code = SCM_UNPACK((se_pt-3)->val);
      gp_debug1("code %d\n",code);

      if(code & 0x30) 
	{
	  if(code & 0x10)
	    {
	      remove = 1;
	      se_pt -=3;
	    }
	  else
	    {
	      ltoken = (se_pt - 2)->val;
	      se_pt -=3;
	    }
	  continue;;
	}

      switch(code & 0x3)
	{
	  //No branch has been taken
	case 0:
	  {
	    //printf("0\n");
	    //Both of the paths are rejected, move up until we find the next subtree to walk into
	    remove = 0;
	  }
	  break;

	//One branch has been taken
	case 1:
	  {
	    //printf("1\n");
	    if(remove)
	      {
		//get new token
		ltoken = ((se_pt-2)->jump + 1)->val;
	      
		//inhibit branch
		((se_pt-1)->jump + 1)->val = SCM_EOL;
		remove = 0;
	      }
	    else
	      {
		if(ltoken != SCM_EOL)
		  {
		    //Set new token on first branch
		    ((se_pt - 1)->jump + 1)->val = ltoken;

		    //Calculate new token
		    ltoken = scm_call_2(scm_fluid_ref(gp_unwind_fluid),
					ltoken,((se_pt-2)->jump + 1)->val);
		  }
	      }
	  }
	  break;

	case 2:
	  {
	    gp_debug0("2 ... ");
	    if(remove)
	      {
		gp_debug0("remove\n");
		if(code & 0x8)
		  {
		    //inhibit branch
		    ((se_pt-2)->jump + 1)->val = SCM_EOL;
		  }
		else
		  {
		    ltoken = ((se_pt - 1)->jump + 1)->val;
		    ((se_pt  - 2)->jump + 1)->val = SCM_EOL;
		    remove = 0;
		  }
	      }
	    else
	      {
		if(code & 0x8)
		  {
		    gp_debug0("0x8\n");
		    if(ltoken == SCM_EOL)
		      ltoken = ((se_pt  - 2)->jump + 1)->val;
		  }
		else
		  {
		    if(ltoken != SCM_EOL)
		      {
			gp_debug0("!= EOL\n");
			((se_pt - 2)->jump + 1)->val = ltoken;
			
			ltoken = scm_call_2(scm_fluid_ref(gp_unwind_fluid),
					    ltoken,((se_pt-1)->jump + 1)->val);
			
		      }
		    else
		      {
			if(code & 0x4)
			  {
			    gp_debug0("0x4\n");
			    ltoken = scm_call_2(scm_fluid_ref(gp_unwind_fluid),
						((se_pt-1)->jump + 1)->val,
						((se_pt-2)->jump + 1)->val);
			  }
			else
			  {
			    gp_debug0("\n");
			  }
		      }
		  }
	      }
	  }
	} 
      se_pt -= 3;
    }
  
  if(ltoken != SCM_EOL)
    token = ltoken;
  DB(gp_p_s_print());
  }

static inline SCM gp_p_search_a_branch_and_kill(SCM s)
{
  SCM *id;
  scm_t_bits code;
  //Pack up integer masked reference
  gp_rstack_t temp,*i,*ii;
  //struct gp_p_statistic *stat;
  SCM ltoken;
  int remove = 0;

  i  = P_GETREF(s);
  ii = se_pt-1;

  ltoken = SCM_EOL;

  DB(gp_p_s_print());
  
  gp_debug0("p s branch and kill\n");
  gp_debug2("i %d, ii %d\n",S_BASE(se_pt),S_BASE(ii));

  if((se_pt - 3)->val == SCM_PACK(4)) goto skip_se_pt;
 retry:
  
  if((se_pt - 3)->val == SCM_PACK(4)) 
    {
      printf("Error should not back off to start in the loop!");
      goto skip_se_pt;
    }

  code = SCM_UNPACK((se_pt-3)->val);
  gp_debug1("handle code %d\n",code);
  if(code & 0x30) 
    {
      if(code & 0x10)
	{
	  remove = 1;
	  se_pt -=3;	  
	}
      else
	{
	  ltoken = (se_pt - 2)->val;
	  se_pt -=3;
	}    
      goto retry;
    }
  
  switch(code & 0x3)
    {
      //No branch has been taken
    case 0:
      if((se_pt-2)->jump == i)
	{
	  //swap the order
	  temp = *(se_pt  - 1);
	  *(se_pt - 1) = *(se_pt - 2);
	  *(se_pt - 2) = temp;	    
	  (se_pt - 3)->val = SCM_PACK(1);
	}
      else
	{
	  if((se_pt - 1)->jump == i)
	    {
	      (se_pt - 3)->val = SCM_PACK(1);
	    }
	  else
	    {
	      //Both of the paths are rejected, move up until we find the next subtree to walk into
	      se_pt -= 3;
	      remove = 0;
	      goto retry;
	    }
	}
      break;

      //One branch has been taken
    case 1:
      gp_debug2("se_pt id: %d,  id = %d\n",P_BASE((se_pt - 2)->id),P_BASE(i));
      if((se_pt-2)->jump == i)
	{
	  gp_debug0("found id ... ");
	  //We dive into the second branch	  
	  if(remove)
	    {
	      gp_debug0("at remove\n");
	      //We come from a branch that is removed
	      ((se_pt - 1)->jump + 1)->val = SCM_EOL;
	      (se_pt - 3)->val = SCM_PACK(10);
	    }
	  else
	    {	      
	      if(ltoken == SCM_EOL)
		{
		  //token has not been touched
		  gp_debug0("token untouched\n");
		  (se_pt - 3)->val = SCM_PACK(2);
		}
	      else
		{
		  //token has been touched
		  gp_debug0("token touched\n");
		  ((se_pt - 1)->jump + 1)->val = ltoken;
		  (se_pt - 3)->val = SCM_PACK(6);
		}
	    }
	}
      else
	{
	  gp_debug0("transit ... ");
	  if(remove)
	    {
	      gp_debug0("remove\n");
	      //get new token
	      ltoken = ((se_pt-2)->jump + 1)->val;
	      
	      //inhibit branch
	      ((se_pt - 1)->jump + 1)->val = SCM_EOL;
	      se_pt -= 3;
	      remove = 0;
	      goto retry;
	    }
	  
	  if(ltoken == SCM_EOL)
	    {
	      gp_debug0("untouched token\n");
	      se_pt -= 3;
	      goto retry;
	    }

	  gp_debug0("touched token\n");
	  //Set new token on first branch
	  ((se_pt - 1)->jump + 1)->val = ltoken;

	  //Calculate new token
	  ltoken = scm_call_2(scm_fluid_ref(gp_unwind_fluid),
			      ltoken,((se_pt - 2)->jump + 1)->val);

	  se_pt -= 3;
	  goto retry;
	}
      break;

    case 2:
      {
	if(remove)
	  {
	    gp_debug0("2> remove\n");

	    ((se_pt - 2)->jump + 1)->val = SCM_EOL;

	    if(!(code & 0x8))
	      {
		ltoken = ((se_pt - 1)->jump + 1)->val;
		remove = 0;
	      }

	    se_pt -= 3;		
	    goto retry;
	  }

	if(code & 0x8)
	  {
	    gp_debug0("2> 0x8\n");
	    if(ltoken == SCM_EOL)
	      {
		ltoken = ((se_pt - 2)->jump + 1)->val;
	      }
	  }
	else	
	  {
	    if(ltoken != SCM_EOL)
	      {
		gp_debug0("2> != SCM_EOL\n");
		((se_pt - 2)->jump + 1)->val = ltoken;

		ltoken = scm_call_2(scm_fluid_ref(gp_unwind_fluid),
				    ltoken,((se_pt-1)->jump + 1)->val);
	      }
	    else
	      if(code & 0x4)
		{
		  gp_debug0("2> 0x4\n");
		  ltoken = scm_call_2(scm_fluid_ref(gp_unwind_fluid),
				      ((se_pt-1)->jump + 1)->val,
				      ((se_pt-2)->jump + 1)->val);
		}
	  }

	se_pt -= 3;
	goto retry;
      }
    }
  
 skip_se_pt:
  gp_debug1("p search i = %d\n",(scm_t_bits) i - (scm_t_bits) gp_postpones);
  
  gp_debug1("p search spt = %d\n",S_BASE(se_pt));

 rewhile:
  //walk up until branch mark  
  if(S_BASE(se_pt) == 0) return SCM_EOL;
  while(!at_end(i))
    {
      if(i->val == SCM_PACK(3)) { i--; continue; }
      //at a jump we need to take two branches returned in a cons
      if(is_jump(i))
	{
	  gp_rstack_t *car,*cdr;
	  gp_debug0("p search jump\n");	  
	  if(gp_use_token_p)
	    {
	      //Check for deleted branches!
	      if(i->jump->val == SCM_EOL)
		{
		  i -= 2;
		  continue;
		}

	      if((i-1)->val == SCM_EOL)
		{
		  i = i->jump - 1;
		  continue;
		}
		    
	      car = i->jump - 1;	  	      
	      cdr = i - 2;
	      (se_pt + 0)->val  = SCM_PACK(0);
	      (se_pt + 1)->jump = car;
	      (se_pt + 2)->jump = cdr;
	      se_pt += 3;

	      return scm_cons(scm_cons((car + 1)->val,P_UNREF(car))
			      ,scm_cons((cdr + 1)->val,P_UNREF(cdr)));
	    }
	  else
	    {
	      car = i->jump - 1;
	      cdr = i - 1;
	    }
	  return scm_cons(P_UNREF(car),P_UNREF(cdr));
	}

      //We are in a redo mode redo and set frame value
      id        = (i - 2)->id ;
      *(id + 0) = (i - 1)->val;
      *(id + 1) = (i - 0)->val;
      i -= 3;
      gp_debug1("will revert id %d\n", (scm_t_bits) id - (scm_t_bits) gp_stack);
      
      if(id >= gp_si) gp_si = id + 2;
  
      GP_TEST_CSTACK;
      (gp_ci+0)->id  = id;
      (gp_ci+1)->val = *(id + 0);
      (gp_ci+2)->val = *(id + 1);

      gp_ci +=3;

      gp_set_frame(id);
    }
  gp_debug0("p search at lambda\n");
  //Check to see if we are at end
  
  if(i == gp_p_frame+1) return SCM_EOL;
  

  if(SCM_UNPACK((i-1)->val) == 0)
    {
      printf("Error at a deleted branch, should never be here!\n");      
    }
  
  //Check to see if we have a rejump!
  if(is_jump(i-1))
    {
      i = (i - 1)->jump;
      goto rewhile;
    }

  //Fire up some data on the se stack to unroll
  (se_pt + 0)->val  = SCM_PACK(0);
  (se_pt + 1)->val  = SCM_BOOL_F;
  (se_pt + 2)->jump = i - 1;
  se_pt += 3;

  //Give back the lambda closure!!
  return (i-1)->val;
}

static inline SCM gp_p_search_a_branch(SCM s)
{
  SCM *id;
  gp_rstack_t *i;

  i  = P_GETREF(s);

  //Pack up integer masked reference
  gp_debug1("p search i = %d\n",(scm_t_bits) i - (scm_t_bits) gp_postpones);  
  gp_debug0("p search\n");

 rewhile:
  //walk up until branch mark
  while(!at_end(i))
    {
      if(i->val == SCM_PACK(3)) { i--; continue; }
      //at a jump we need to take two branches returned in a cons
      if(is_jump(i))
	{
	  gp_rstack_t *car,*cdr;
	  gp_debug0("p search jump\n");	  
	  if(gp_use_token_p)
	    {
	      if(i->jump->val == SCM_EOL)
		{
		  i += 2;
		  continue;
		}

	      if((i+1)->val == SCM_EOL)
		{
		  i = i->jump - 1;
		  continue;
		}

	      car = i->jump - 1;	  	      
	      cdr = i - 2;
	      return scm_cons(scm_cons((car + 2)->val,P_UNREF(car))
			     ,scm_cons((cdr + 1)->val,P_UNREF(cdr)));
	    }
	  else
	    {
	      car = i->jump - 1;
	      cdr = i - 1;
	    }
	  return scm_cons(P_UNREF(car),P_UNREF(cdr));
	}

      //We are in a redo mode redo and set frame value
      id        = (i - 2)->id ;
      *(id + 0) = (i - 1)->val;
      *(id + 1) = (i - 0)->val;
      i -= 3;
      gp_debug1("will revert id %d\n", (scm_t_bits) id - (scm_t_bits) gp_stack);

      if(id >= gp_si) gp_si = id + 2;
  
      GP_TEST_CSTACK;
      (gp_ci+0)->id  = id;
      (gp_ci+1)->val = *(id + 0);
      (gp_ci+2)->val = *(id + 1);

      gp_ci +=3;

      gp_set_frame(id);
    }
  gp_debug0("p search at lambda\n");
  //Check to see if we are at end
  
  if(i == gp_p_frame+1) return SCM_EOL;

  if(is_jump(i-1))
    {
      i = (i - 1)->jump;
      goto rewhile;
    }
  
  //Give back the lambda closure!!
  return (i-1)->val;
}

static inline void prepare_for_move(gp_rstack_t *pt, scm_t_bits delta)
{
  gp_rstack_t *j;
  gp_debug0("p cleanup move prepare\n");
 retry:
  if(!at_end(pt))
    {
      if(pt->val == SCM_PACK(3)) { pt--; goto retry; }
      if(is_jump(pt))
	{
	  j = pt->jump;
	  if(j >= frame)
	    {
	      pt->jump -= delta;
	    }

	  if(gp_use_token_p)
	    pt -= 2;
	  else
	    pt--;

	  goto retry;
	}
      pt -= 3; 
      goto retry;
    } 
  if(pt == frame) return;
  // ... rejump (token) id lambda . 
  if(gp_use_token_p)
    {
      if(is_jump(pt - 1))
	(pt-1)->jump -= delta;
      pt -= 4;
    }
  else
    pt -= 3;
  if((pt+1)->jump >= frame)
    pt->jump -= delta;
  pt --;
  goto retry;
}

// frame nextbranch next_id bottom (token) .
// rejump (token) branch_id lambda .
static inline void gp_p_mtp()
{
  struct gp_p_statistic *stat,s;
  gp_debug0("p mtp>\n");
  DDB(gp_print_p_stack());
  DB(gp_p_s_print());
  if(gp_use_token_p)
    {
      SCM tok_left;
      gp_rstack_t *pt, *fr;


      if(gp_p_pt != frame + 1)
	gp_p_clean_branch(gp_p_pt - 1);

      //unwind frame data.
      s          = * (struct gp_p_statistic *) (frame-1);
      tok_left   = (frame-2)->val;
      bottom     = (frame-3)->branch_id;
      next_id    = (frame-4)->branch_id;
      nextbranch = (frame-5)->jump;
      pt         = (frame-6);
      fr         = frame;
      frame      = (frame-6)->jump;

      stat = (struct gp_p_statistic *) (frame - 1);

      if(gp_p_pt == fr + 1)
	{
	  //mark remove!
	  (se_pt - 1)->jump->val = SCM_PACK(0);
	  (se_pt - 3)->val = SCM_PACK(0x10);
	  gp_p_pt    =  pt;	  
	  
	  stat->nrem ++;
	}
      else
	{
	  //clear data
	  (pt + 0)->val  = SCM_PACK(3);
	  (pt + 1)->val  = SCM_PACK(3);
	  (pt + 2)->val  = SCM_PACK(3);
	  (pt + 3)->val  = SCM_PACK(3);
	  (pt + 4)->val  = SCM_PACK(3);
	  (pt + 5)->val  = SCM_PACK(3);
	  (pt + 6)->val  = SCM_PACK(3);

	  stat->n    += s.n;
	  stat->nrem += s.nrem + 1;	            

	  //Link structure to subtree
	  (se_pt - 3)->val        = SCM_PACK(0x20);
	  (se_pt - 1)->jump->jump = gp_p_pt - 1;
	  (se_pt - 2)->val        = token;
	}

      token = tok_left;
    }
  else
    {
      //TODO this path is unsuported, needs some love,
      gp_rstack_t *pt;

      //unwind frame data.      
      bottom     = (frame-1)->branch_id;
      next_id    = (frame-2)->branch_id;
      nextbranch = (frame-3)->jump;
      pt         = (frame-4);
      frame      = (frame-4)->jump;

      //clear data
      (pt + 0)->val  = SCM_PACK(3);
      (pt + 1)->val  = SCM_PACK(3);
      (pt + 2)->val  = SCM_PACK(3);
      (pt + 3)->val  = SCM_PACK(3);
      (pt + 4)->val  = SCM_PACK(3);
      
      //combine in a branch
      if(nextbranch != frame)
	store_branch();
    }
  DB(gp_p_s_print());
  DDB(gp_print_p_stack());
  gp_debug0("mtp> END\n");
}

SCM_DEFINE(gp_p_ppf,"gp-postpones-to-prev-frame",0,0,0,(),
	   "")
#define FUNC_NAME s_gp_p_ppf
{
  DDB(gp_print_p_stack());
  gp_p_mtp();
  DDB(gp_print_p_stack());
  return SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(gp_p_search,"gp-search-branch",1,0,0,(SCM s),
"search for a postpone continuation, returns\n(X , Y)  -  A branch\n()       -  No more branches\nLam      -  A continuation closure")
#define FUNC_NAME s_gp_p_search
{
  return gp_p_search_a_branch(s);
}
#undef FUNC_NAME

SCM_DEFINE(gp_p_search_kill,"gp-search-branch-kill",1,0,0,(SCM s),
"search for a postpone continuation, returns\n(X , Y)  -  A branch\n()       -  No more branches\nLam      -  A continuation closure")
#define FUNC_NAME s_gp_p_search_kill
{
  return gp_p_search_a_branch_and_kill(s);
}
#undef FUNC_NAME

SCM_DEFINE(gp_postpone_start,"gp-postpone-start",0,0,0,(),
	   "return start for redo")
#define FUNC_NAME s_gp_postpone_start
{
  return P_UNREF(gp_p_pt - 1);
}
#undef FUNC_NAME

SCM_DEFINE(gp_postpone_search_ptr,"gp-postpone-search-ptr",0,0,0,(),
	   "return search path ptr")
#define FUNC_NAME s_gp_postpone_search_ptr
{
  return P_UNREF(se_pt - 1);
}
#undef FUNC_NAME

SCM_DEFINE(gp_postpone_search_unwind,"gp-postpone-search-unwind",1,0,0,(SCM s),
	   "return search path ptr")
#define FUNC_NAME s_gp_postpone_search_unwind
{
  gp_p_search_unwind(s);
  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_postpone_frame,"gp-postpone-frame",0,0,0,(),
	   "return frame for redo")
#define FUNC_NAME s_gp_postpone_frame
{
    return P_UNREF(frame);
}
#undef FUNC_NAME

SCM_DEFINE(gp_postpone_newframe,"gp-postpone-newframe",0,0,0,(),
	   "prepare for a new postpone frame")
#define FUNC_NAME s_gp_postpone_frame
{
  gp_p_newframe();
  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_postpone_unwind,"gp-postpone-unwind",0,0,0,(),
	   "cleans up a postpone frame")
#define FUNC_NAME s_gp_postpone_frame
{
  gp_p_unwind();
  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_postpone,"gp-postpone",1,0,0,(SCM lam),
"Push closure lam onto a redo tre for later execution at the same \nstate the closure was made.")
#define FUNC_NAME s_gp_postpone
{
  gp_p_postpone(lam);
  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_postpone_token,"gp-postpone-token",2,0,0,(SCM lam, SCM tok),
"Push closure lam onto a redo tre for later execution at the same \nstate the closure was made.")
#define FUNC_NAME s_gp_postpone_token
{
  gp_p_postpone_token(lam,tok);
  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_set_postpone,"gp-set-postpone!",0,0,0,(),
	   "use the postpone branch of unwind to build redo tree")
#define FUNC_NAME s_gp_set_postpone
{
 gp_p_postpone_p = 1;
 return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_unset_postpone,"gp-unset-postpone!",0,0,0,(),
	   "use the standard branch of unwind to not build redo tree")
#define FUNC_NAME s_gp_set_postpone
{
 gp_p_postpone_p = 0;
 return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_use_token,"gp-use-token!",0,0,0,(),
	   "use the postpone branch of unwind to build redo tree")
#define FUNC_NAME s_gp_use_token
{
 gp_use_token_p = 1;
 return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_get_unwind_fluid,"gp-unwind-fluid",0,0,0,(),
  "")
#define FUNC_NAME s_gp_get_unwind_fluid
{
  return gp_unwind_fluid;
}
#undef FUNC_NAME

SCM_DEFINE(gp_get_token,"gp-get-token",0,0,0,(),
  "")
#define FUNC_NAME s_gp_get_token
{
  return token;
}
#undef FUNC_NAME

SCM_DEFINE(gp_use_no_token,"gp-use-no-token!",0,0,0,(),
	   "use the standard branch of unwind to not build redo tree")
#define FUNC_NAME s_gp_use_no_token
{
 gp_use_token_p = 0;
 return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_p_cleanup_move,"gp-cleanup-move",1,0,0,(SCM start),
	   "moves current frame one step")
#define FUNC_NAME s_gp_gp_p_cleanup_move
{
  gp_p_cleanup_move(start);
  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_p_cleanup,"gp-cleanup",0,0,0,(),
	   "frees the beginning of the array, a common idiom needed to compact array space")
#define FUNC_NAME s_gp_gp_p_cleanu
{
  gp_p_cleanup();
  return SCM_BOOL_T;
}
#undef FUNC_NAME


SCM_DEFINE(gp_has_postpones,"gp-has-postpones?",0,0,0,(),
	   "check to see if we postponed stuff")
#define FUNC_NAME s_gp_has_postpones
{
  struct gp_p_statistic *stat = (struct gp_p_statistic *) (frame - 1);
  int r = (frame + 1 == gp_p_pt);
  gp_debug1("has not postpones %d ",r);
  gp_debug2("n = %d    nrem=%d\n",stat->n,stat->nrem);
  return  (r || stat->n == stat->nrem) ? SCM_BOOL_F : SCM_BOOL_T;
}
#undef  FUNC_NAME


#define ppt gp_p_pt



static inline SCM gp_unwind_simple(SCM fr)
{
  gp_cstack_t *ci_old;
  gp_cstack_t *i;   
  
  gp_debug0("unwind>\n");
  if(SCM_UNLIKELY( (scm_t_bits) fr >=gp_frame_id))
    {
      gp_debug2("unwind> got the same fi : %x >= %x\n",fr,gp_frame_id);
      return SCM_BOOL_F;
    }
    
  gp_frame_id = SCM_TO_FI(fr);
  gp_wi       = FR_TO_WI(gp_frame_id);
  
  gp_debug2("unwind> got fr = %x amd wi = %x\n",gp_frame_id,gp_wi);fflush(stdout);
  
  ci_old = gp_ci;
  
  gp_ci =(gp_wi+0)-> ci;
  gp_si =(gp_wi+1)-> si;

  /*
    the postpone algorithm:
    We store a tree containing the redo information and make use of a simple 
    and lightweight compression scheme that at worst produces 2x the optimum 
    amount of variable recalculations.

    We make an array for storing the redo data and it typically looks like
    [0 lam1 . r r r 1.0 id1 lam2 . r r 2.1 id2 lam3 . r 3 (2) r r (1) r r r]
    
    r   r   
    r   r   r
    r   2---3
    r     r
    1---- r
      r
      r
      r

    r         = redo atom
    1,2,3...  = branch id
    (1),...   = pointer to branch id start
    .         = branch separator
    
    After constructed a generation of a redo tree it is traversed starting from 
    the right redoing, then replace variable data until a pointer or end is
    reached. at such a place we first follow the pointer and then continue to 
    the left until we backtrack and then we continue.

    For compression. As we unwind a marked variable has it's frame number set 
    to zero and this will serve as a mark that the variable has been stored 
    for a redo already. When we unwind and have reached just below a branch 
    point we need to make sure to unmark all variables that has been marked 
    further out on the limb. This needs to be done up to secondary branch points
    which already have been cleared. The reason is that we will miss redo 
    information else and the fix in turn for that case is complex. This means 
    that below a branch point every variable that is included somewhere further 
    out on the limb is not marked and will be included the next time it appear 
    hence the risk of doing multiple redos for a variable. But this multiplicity 
    is pretty nice. For no branch we have perfect compression and the more 
    branches we have the less possibilities of duplication in %. Hence the worst 
    case is a binary tree which in turn calculates to at most 2x redo atoms in 
    the tree compared to optimum.
   */

  if(gp_p_postpone_p)
    {
      gp_debug1("postpone algorithm! var-id = %d\n", (scm_t_bits) gp_si - (scm_t_bits) gp_stack);
      gp_debug2("postpone next_id = %d, cur_id = %d!\n"
		,(scm_t_bits) next_id - (scm_t_bits) gp_cstack
                ,(scm_t_bits) gp_ci   - (scm_t_bits) gp_cstack);
      for(i = ci_old-3; i >= gp_ci; i-=3)
	{
	  SCM *id;	  
	  id      = (i+0)->id;	  
	  
	  //gp_debug1("i == %d\n", (scm_t_bits) i  - (scm_t_bits) gp_cstack);
	  //Tree comperssion maximum 2x inneficiency	 
	  if((scm_t_bits) i < (scm_t_bits) bottom && !marked(id))
	    {
	      GP_TEST_P;
	      (ppt + 0)->id  = id;
	      gp_debug1("will mark and store id %d\n",(scm_t_bits) id - (scm_t_bits) gp_stack);
	      (ppt + 1)->val = *(id + 0);
	      (ppt + 2)->val = *(id + 1);
	      ppt += 3;
	    
	      *(id+0) = (i+1)->val;
	      *(id+1) = (i+2)->val;	      	 
	      
	      mark(id);
	    }
	  else
	    {
	      if((scm_t_bits) i < (scm_t_bits) bottom)
		gp_debug2("already marked id = %d bottom = %d\n"
			  ,(scm_t_bits) id - (scm_t_bits) gp_stack
	                  ,(scm_t_bits) i  - (scm_t_bits) bottom  );
	      if(marked(id))
		{
		  *(id+0) = (i+1)->val;
		  *(id+1) = (i+2)->val;	      	 
		  mark(id);
		}
	      else
		{
		  *(id+0) = (i+1)->val;
		  *(id+1) = (i+2)->val;	      	 
		}
	    }

	  if(id_at_branch(i))
	    {
	      SCM left_token = (nextbranch)->val;
	      store_branch();
	      if(gp_use_token_p)
		{
		  token = scm_call_2(scm_fluid_ref(gp_unwind_fluid),
				     left_token,token);
		}
	    }
	}
      bottom = gp_ci<bottom ? gp_ci : bottom;
      gp_debug1("postpone bottom = %d\n",(scm_t_bits) bottom - (scm_t_bits) gp_cstack);
    }
  else
    {
      for(i = ci_old-3; i >= gp_ci; i-=3)
	{
	  SCM *id;	  
	  id      = (i+0)->id;
	  *(id+0) = (i+1)->val;
	  *(id+1) = (i+2)->val;
	}
    }
  
  return SCM_BOOL_T;
}

static inline SCM gp_unwind(SCM fr)
{
  if(SCM_PACK(2) == fr) return SCM_BOOL_T;
  gp_unwind_simple(fr);
  return SCM_BOOL_T;
}


SCM_DEFINE(gp_gp_unwind, "gp-unwind", 1, 0, 0, (SCM fr), 
	   "unwinds the prolog stack till frame refered by the argument")
#define FUNC_NAME s_gp_gp_unwind
{
  return gp_unwind(fr);
}
#undef FUNC_NAME

#define DB(X) 
#undef frame
#undef pt_e
#undef pt_end
#undef ppt
#undef next_id
#undef bottom
#undef nextbranch
#undef mark
#undef unmark
#undef marked
#undef id_at_branch
#undef store_branch
#undef is_jump
#undef at_end

#define GP_TEST_STACK if(gp_si > gp_nns) scm_out_of_range(NULL, SCM_I_MAKINUM(gp_ns))
static inline  SCM* gp_mk_var()
{
  SCM *ret;
  SCM ref;  

  GP_TEST_STACK;
  gp_debug1("test stack handled! %x\n",gp_si);
  ret  = gp_si;
  gp_si += 2;

  (gp_ci + 0)->id  = ret;
  (gp_ci + 1)->val = *(ret+0);
  (gp_ci + 2)->val = *(ret+1);
  gp_ci += 3;

  ref = SCM_PACK(gp_frame_id); 
  *ret = ref;
  *(ret + 1) = 0;

  /*
  */

  gp_debug2("returning from mk_var %x, ref = %x\n",ret,SCM_UNPACK(ref));
  return ret;
}

SCM_DEFINE(gp_swap_a, "gp-swap-to-a", 0, 0, 0, (),
	   "swap to A memory stack bank")
#define FUNC_NAME s_gp_swap_a
{
  swap_to_a();
  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_swap_b, "gp-swap-to-b", 0, 0, 0, (),
	   "swap to B memory stack bank")
#define FUNC_NAME s_gp_swap_b
{
  swap_to_b();
  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_next_budy, "gp-budy", 1, 0, 0, (SCM x),
	   "Assumes that v1 and v2 is allocated consecutively returns v2 when feeded by v1")
#define FUNC_NAME s_gp_next_budy
{
  if(!GP(x)) goto not_a_budy_error;
  SCM *id = UN_GP(x);
  return GP_IT(id + 2);
  
 not_a_budy_error:
  scm_misc_error ("budy error", "cannot budy a non GP element", 
		  scm_list_1 (x));		\
  return SCM_BOOL_F;
}

#undef FUNC_NAME

SCM_DEFINE(gp_mkvar, "gp-var!", 0, 0, 0, (),
	   "makes a unbounded gp variable")
#define FUNC_NAME s_gp_mkvar
{
  return GP_IT(gp_mk_var());
}
#undef FUNC_NAME


static inline SCM gp_mk_cons() 
{
  SCM *ret;
  scm_t_bits fi = GP_MK_FRAME_CONS(gp_frame_id);
  GP_TEST_STACK;
  ret = gp_si;
  gp_si += 6;

  (gp_ci + 0)->id  =   ret + 4;
  (gp_ci + 1)->val = *(ret + 4);
  (gp_ci + 2)->val = *(ret + 5);
  (gp_ci + 3)->id  =   ret + 2;
  (gp_ci + 4)->val = *(ret + 2);
  (gp_ci + 5)->val = *(ret + 3);
  (gp_ci + 6)->id  =   ret + 0;
  (gp_ci + 7)->val = *(ret + 0);
  (gp_ci + 8)->val = *(ret + 1);
  gp_ci+= 9;

  gp_debug1("frame is %x\n",gp_frame_id);

  *(ret+4) = SCM_PACK(fi);
  *(ret+5) = SCM_PACK(2);  
  *(ret+2) = SCM_PACK(gp_frame_id);
  *(ret+0) = SCM_PACK(gp_frame_id);
  *(ret+1) = 0;
  *(ret+3) = 0;

  return GP_UNREF(ret + 4);
}

static inline  SCM mk_gp(SCM data)
{
  return data;
}


SCM_DEFINE(gp_mk, "scm->gp", 1, 0, 0, (SCM data),
	   "creates a gp object from scheme repr")
#define FUNC_NAME s_gp_mk
{
  return mk_gp(data);
}
#undef FUNC_NAME


static int gp_recurent(SCM *id1,SCM *id2)
{
  if(!GP(GP_UNREF(id2))) goto non_gp;

  gp_lookup(id2);  
  gp_debug0("recurent> looked up data\n");
  if(id1 == gp_ret.id  )  
    {
      gp_debug0("recurent> Found a recurence!!\n");
      return 1;  
    }
  
  if(GP_CONS(gp_ret.ref))
    {
      gp_debug0("recurent> got a cons\n");
      id2 = gp_ret.id;
      return gp_recurent(id1,id2-2) || gp_recurent(id1,id2-4);
    }

  if(gp_ret.val && SCM_CONSP(gp_ret.val)) 
    gp_recurent(id1,GP_GETREF(gp_ret.val));
  
  gp_debug0("recurent> atom!\n");
  return 0;

 non_gp:
  if(SCM_CONSP(GP_UNREF(id2)))
    {
      return 
	gp_recurent(id1,GP_GETREF(SCM_CAR(GP_UNREF(id2)))) ||
	gp_recurent(id1,GP_GETREF(SCM_CDR(GP_UNREF(id2))));
    }
  return 0;
}

static SCM smob2scm_gp()
{
  SCM scm;
 
  //gp_debug0("gp2scm> in gp part\n");

  if(gp_ret.val == 0)
    {
      //gp_debug0("gp2scm> unbounded\n");
      return gp_unbound_sym;
    }
  
  //gp_debug2("gp2scm> scm-value %x at id %x\n", SCM_UNPACK(gp_ret.val),gp_ret.id);
  scm = gp_ret.val;
  if(SCM_CONSP(scm))
    {	  
      SCM car = smob2scm(SCM_CAR(scm));
      SCM cdr = smob2scm(SCM_CDR(scm));
      if(car == SCM_CAR(scm) && cdr == SCM_CDR(scm))
	return scm;
      return scm_cons(car,cdr);
    }
  else
    return scm;
}


SCM_DEFINE( smob2scm, "gp->scm", 1, 0, 0, (SCM scm),
	    "creates a scheme representation of a gp object")
#define FUNC_NAME s_smob2scm
{
  //gp_debug0("gp2scm> start!\n");
  if(GP(scm))
    {
      SCM *id;

      //gp_debug0("gp2scm> a gp!\n");
      id = UN_GP(scm);
      //printf("gp2scm> id %d\n",(scm_t_bits) GP_GETREF(id) - (scm_t_bits) gp_stack);
      gp_lookup(id);

      if(GP_CONS(gp_ret.ref))
	{
	  //gp_debug0("gp2scm> cons! at main\n");
	  id = gp_ret.id;
	  return scm_cons(smob2scm(GP_UNREF(id-2)),
			  smob2scm(GP_UNREF(id-4)));
	}      

      return smob2scm_gp();
    }
  else 
    {
      if(SCM_CONSP(scm))
	{	  
	  SCM car = smob2scm(SCM_CAR(scm));
	  SCM cdr = smob2scm(SCM_CDR(scm));
	  if(car == SCM_CAR(scm) && cdr == SCM_CDR(scm))
	    return scm;
	  return scm_cons(car,cdr);
	}
      else
	return scm;
    }
}
#undef FUNC_NAME

#define QCDR(x)   GP_GETREF(SCM_CDR(GP_UNREF(x))) 
#define QCAR(x)   GP_GETREF(SCM_CAR(GP_UNREF(x))) 
#define QCONSP(x) SCM_CONSP(GP_UNREF(x))

/*
#define ATTR_HANDLE(id1,id2)			\
  if(GP_ATTR(id1))				\
    if(scm_call_2(id1,id1,id2) == SCM_BOOL_F)	\
      {						\
	return 0;				\
      }						\
    else					\
      {						\
	U_NEXT;					\
      }						\
*/
#define ATTR_HANDLE(id1,id2)

int gp_plus_unify = 1;
static int gp_unify(SCM *id1, SCM *id2, int raw)
{ 
  SCM * stack[20];
  int   sp;
  struct gp_lookup_t ret2;
  
  raw = !raw;

  sp = 0;
  
#define U_NEXT					\
  {						\
  if(SCM_UNLIKELY(sp==0))			\
    {						\
      return 1;					\
    }						\
  else						\
    {						\
      id2 = stack[--sp];			\
      id1 = stack[--sp];			\
      goto retry;				\
    }						\
}

 retry:
  gp_debug0("unify>\n");  
  if(GP(GP_UNREF(id1)))
    {
      if(GP(GP_UNREF(id2)))
	{
	  gp_debug0("11>\n");
	  gp_lookup(id2);
	  ret2 = gp_ret;
	  gp_lookup(id1);

	  ATTR_HANDLE(gp_ret.id,ret2.id);
	  ATTR_HANDLE(ret2.id,gp_ret.id);

	  if(!GP_CONS(gp_ret.ref) && gp_ret.val && SCM_CONSP(gp_ret.val))
	    {
	      id1 = GP_GETREF(gp_ret.val);
	      if(!GP_CONS(ret2.ref) && ret2.val && SCM_CONSP(ret2.val))
		{
		  id2 = GP_GETREF(ret2.val);
		  goto u00;
		}
	      else
		{
		  id2 = ret2.id;
		  gp_ret = ret2;
		  goto u01;
		}
	    }
	  else
	    {
	      id1 = gp_ret.id;
	      if(!GP_CONS(ret2.ref) && ret2.val && SCM_CONSP(ret2.val))
		{
		  id2 = GP_GETREF(ret2.val);
		  goto u10;
		}
	      id2 = ret2.id;
	    }
	}
      else
	{
	  gp_debug0("10>\n");
	  gp_lookup(id1);

	  ATTR_HANDLE(gp_ret.id,id2);

	  gp_debug0("10> lookup\n");
	  if(GP(gp_ret.val)) scm_out_of_range(NULL, SCM_I_MAKINUM(13));
	  if(gp_ret.val && SCM_CONSP(gp_ret.val))
	    {
	      id1 = GP_GETREF(gp_ret.val);
	      goto u00;
	    }
	  id1 = gp_ret.id;
	  goto u10;
	}
    }
  else
    {
      if(GP(GP_UNREF(id2)))
	{
	  gp_debug0("01>\n");
	  gp_lookup(id2);

	  ATTR_HANDLE(gp_ret.id,id1);

	  if(gp_ret.val && SCM_CONSP(gp_ret.val))
	    {
	      id2 = GP_GETREF(gp_ret.val);
	      goto u00;
	    }
	  id2 = gp_ret.id;
	  goto u01;
	}
      gp_debug0("00>\n");     
      goto u00;
    }

  

  gp_debug0("unify> looked up with u11\n");

  if(gp_plus_unify && gp_ret.val == 0) 
    {
      if(ret2.val == 0 && gp_ret.id == ret2.id)
	{
	  gp_debug0("unify> var == var");
	  U_NEXT;
	}
      else
	goto  unbound1;
    }

  if(gp_plus_unify && ret2.val == 0) 
    {
      if(gp_ret .val == 0 && gp_ret.id == ret2.id)
	{
	  gp_debug0("unify> var == var");
	  U_NEXT;
	}
      else
	goto  unbound2;
    }


  //conses!!
#define DO_CONS						\
  {							\
    gp_debug0("unify> cons\n");				\
    if(SCM_UNLIKELY(sp >= 18))				\
      {							\
	if(!gp_unify(id1 - 2, id2 - 2, raw)) return 0;	\
	id1 = id1 - 4;					\
	id2 = id2 - 4;					\
	goto retry;					\
      }							\
    else						\
      {							\
	stack[sp++] = id1 - 4;				\
	stack[sp++] = id2 - 4;				\
	id1 = id1 - 2;					\
	id2 = id2 - 2;					\
	goto retry;					\
      }							\
  }


  if(GP_CONS(gp_ret.ref))
    {
      if(GP_CONS(ret2.ref))
	{
	  DO_CONS;
	}
      else
	return 0;
    }

  if(GP_CONS(ret2.ref))
    {
      if(GP_CONS(gp_ret.ref))
	{
	  DO_CONS;
	}
      else
	return 0;
    }

  if(GP_EQ(gp_ret.ref) || GP_EQ(ret2.ref))
    {
      gp_debug2("unify> (eq ~x ~x)\n",gp_ret.val,ret2.val);
      if(gp_ret.val == ret2.val)
	{U_NEXT;}
      else
	return 0;
    }
    
  gp_debug2("unify> (equal ~x ~x)\n",gp_ret.val,ret2.val);
  if(scm_is_true(scm_equal_p(gp_ret.val,ret2.val)))
    {U_NEXT;}
  else
    return 0;
 
 unbound1: 
  gp_debug0("unify> unbound1\n");
  if(raw && GP_CONS(ret2.ref) && gp_recurent(id1,id2))  return 0;    
  gp_set_ref(id1,GP_UNREF(id2));
  U_NEXT;

 unbound2: 
  gp_debug0("unify> unbound2\n");
  if(raw && GP_CONS(gp_ret.ref) && gp_recurent(id2,id1)) return 0;    
  gp_set_ref(id2,GP_UNREF(id1));
  U_NEXT;

 u00:
  gp_debug0("unify> looked up with u00\n");

  //conses!!
#define DO_CONS2		       			\
  {							\
    gp_debug0("unify> cons\n");				\
    if(SCM_UNLIKELY(sp >= 18))				\
      {							\
	if(!gp_unify(QCAR(id1),QCAR(id2),raw)) return 0;	\
	id1 = QCDR(id1);				\
	id2 = QCDR(id2);				\
	goto retry;					\
      }							\
    else						\
      {							\
	stack[sp++] = QCDR(id1);			\
	stack[sp++] = QCDR(id2);			\
	id1 = QCAR(id1);				\
	id2 = QCAR(id2);				\
	goto retry;					\
      }							\
  }


  if(QCONSP(id1))
    {
      if(QCONSP(id2))
	{
	  //printf("scm cons unify\n");
	  DO_CONS2;
	}
      else
	return 0;
    }

  if(QCONSP(id2)) return 0;

  if(scm_is_true(scm_equal_p(GP_UNREF(id1),GP_UNREF(id2))))
    {U_NEXT;}
  return 0;
  
 u01:
  {
    SCM *x;
    gp_debug0("unify> looked up with u01>\n");
    x = id1;
    id1 = id2;
    id2 = x;
  }

 u10:
  gp_debug0("unify> looked up with u10>\n");
  if(gp_ret.val == 0) goto  unbound_10;

  //conses!!
#define DO_CONS3						\
  {							\
    gp_debug0("unify> cons\n");				\
    if(SCM_UNLIKELY(sp >= 18))				\
      {							\
	if(!gp_unify(id1 - 2, QCAR(id2),raw)) return 0;	\
	id1 = id1 - 4;					\
	id2 = QCDR(id2);				\
	goto retry;					\
      }							\
    else						\
      {							\
	stack[sp++] = id1 - 4;				\
	stack[sp++] = QCDR(id2);			\
	id1 = id1 - 2;					\
	id2 = QCAR(id2);				\
	goto retry;					\
      }							\
  }


  if(GP_CONS(gp_ret.ref))
    {
      if(QCONSP(id2))
	{
	  DO_CONS3;
	}
      else
	return 0;
    }
  if(QCONSP(id2)) return 0;

  
  if(GP_EQ(gp_ret.ref))
    {
      gp_debug2("unify> (eq ~x ~x)\n",gp_ret.val,ret2.val);
      if((scm_t_bits) gp_ret.val == (scm_t_bits) id2)
	{U_NEXT;}
      else
	return 0;
    }
    
  gp_debug2("unify> (equal ~x ~x)\n",gp_ret.val,ret2.val);
  if(scm_is_true(scm_equal_p(gp_ret.val,GP_UNREF(id2))))
    {U_NEXT;}
  else
    return 0;
 
 unbound_10: 
  gp_debug0("unify> unbound1\n");
  if(raw && QCONSP(id2) && gp_recurent(id1,id2)) return 0;    
  if(GP(GP_UNREF(id2))) scm_out_of_range(NULL, SCM_I_MAKINUM(gp_nw));
  gp_set_eq(id1,GP_UNREF(id2));
  U_NEXT;
}




SCM_DEFINE(gp_gp_unify,"gp-unify!",2,0,0,(SCM v1, SCM v2),
	   "unifies two gp variables")
#define FUNC_NAME s_gp_gp_unify
{
  SCM *vv1, *vv2;
  
  vv1 = UN_GP(v1);
  vv2 = UN_GP(v2);
  return gp_unify(vv1,vv2,0) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_unify_raw,"gp-unify-raw!",2,0,0,(SCM v1, SCM v2),
	   "unifies two gp variables")
#define FUNC_NAME s_gp_gp_unify
{
  SCM *vv1, *vv2;
  
  vv1 = UN_GP(v1);
  vv2 = UN_GP(v2);
  return gp_unify(vv1,vv2,1) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(gp_varp,"gp-var?",1,0,0,(SCM x),
	   "Test for an unbound variable.")
#define FUNC_NAME s_gp_varp
{
  if(GP(x))
    {
      gp_lookup(UN_GP(x));
      return GP_UNBOUND(gp_ret.ref) ? SCM_BOOL_T : SCM_BOOL_F;
    }
  return SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(gp_atomicp,"gp-atomic?",1,0,0,(SCM x),
	   "Test for a atomic gp")
#define FUNC_NAME s_gp_atomicp
{
  gp_lookup(UN_GP(x));
  return (!GP_UNBOUND(gp_ret.ref) && !GP_CONS(gp_ret.ref)) 
    ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_consp,"gp-cons?",1,0,0,(SCM x),
	   "Test for a cons gp")
#define FUNC_NAME s_gp_consp
{
  gp_debug0("gp-cons?>\n");
  gp_lookup(UN_GP(x));
  return GP_CONS(gp_ret.ref) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(gp_clear, "gp-clear", 0, 0, 0, (),
	   "resets the unifyier stack")
#define FUNC_NAME s_gp_clear
{
  gp_si  = gp_stack;
  gp_wi  = gp_wstack;
  gp_ci  = gp_cstack;
  gp_frame_id = GPM_UNBOUND;
  return SCM_BOOL_T;
}
#undef FUNC_NAME



SCM_DEFINE(gp_gp, "gp?", 1, 0, 0, (SCM scm), "")
#define FUNC_NAME s_gp_gp
{
  return GP(scm)?SCM_BOOL_T:SCM_BOOL_F;
}
#undef FUNC_NAME


static inline SCM ggp_set(SCM var, SCM val)
{
  SCM *gvar;
  if(GP(var))
    {
      gvar = GP_GETREF(var);
      gp_lookup(gvar);
      if(GP(val))
	{
	  gp_set_ref(gp_ret.id,val);
	}
      else
	{
	  gp_set_eq(gp_ret.id,val);
	}
      return SCM_BOOL_T;
    }
  return SCM_BOOL_F;
}


SCM_DEFINE(gp_set, "gp-set!", 2, 0, 0, (SCM var, SCM val), 
	   "set gp var var to val")
#define FUNC_NAME s_gp_set
{
  return ggp_set(var,val);
}
#undef FUNC_NAME

SCM_DEFINE(gp_print, "gp-print", 1, 0, 0, (SCM pr), 
	   "print a val")
#define FUNC_NAME s_gp_print
{
  if(GP(pr))
    printf("GP_SCM> %x,%x\n",
	   SCM_UNPACK(*GP_GETREF(pr)),
	   SCM_UNPACK(*(GP_GETREF(pr) + 1)));
  else
    printf("NO GP!\n");

  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_ref_set, "gp-ref-set!", 2, 0, 0, (SCM var, SCM val), 
	   "set gp var reference to val")
#define FUNC_NAME s_gp_ref_set
{
  if(GP(var))
    {
      if(GP(val))
	{
	  gp_set_ref(GP_GETREF(var),val);
	}
      else
	{
	  gp_lookup(GP_GETREF(var));
	  gp_set_eq(gp_ret.id,val);
	}
      return SCM_BOOL_T;
    }
  return SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_set_attr, "gp-set-attr!", 2, 0, 0, (SCM var, SCM val), 
	   "set gp var var to val")
#define FUNC_NAME s_gp_gp_set_attr
{
  if(GP(var))
    {
      gp_set_attr(GP_GETREF(var),val);
      return SCM_BOOL_T;
    }
  return SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_force_attr, "gp-force-attr!", 2, 0, 0, (SCM var, SCM val), 
	   "set gp var var to val")
#define FUNC_NAME s_gp_gp_force_attr
{
  if(GP(val))
    {
      gp_force_attr(GP_GETREF(var),val);
      return SCM_BOOL_T;
    }
  return SCM_BOOL_F;
}
#undef FUNC_NAME


/* new api so that pure ffi will work */
SCM_DEFINE(gp_cons_bang, "gp-cons!", 2, 0, 0, (SCM car, SCM cdr), 
	   "crates a prolog variable cons pair")
#define FUNC_NAME s_gp_cons_bang
{
  SCM *cons;
  gp_debus0("gp-cons>\n");
  cons = GP_GETREF(gp_mk_cons());
  DS(smob2scm(car));
  DS(smob2scm(cdr));
  if(GP(car))    
    { 
      gp_lookup(GP_GETREF(car));      
      gp_set_ref(cons - 2,GP_UNREF(gp_ret.id));
    }
  else
    {
      gp_debus0("atom car>\n");
      gp_set_eq(cons - 2,car);
    }

  if(GP(cdr))    
    { 
      gp_lookup(GP_GETREF(cdr));
      gp_set_ref(cons - 4,GP_UNREF(gp_ret.id));
    }
  else
    {
      gp_debus0("atom cdr>\n");
      gp_set_eq(cons - 4,cdr);
    }

  return GP_UNREF(cons);
}
#undef FUNC_NAME



SCM_DEFINE(gp_pair_bang, "gp-pair!?", 1, 0, 0, (SCM x), 
	   "checks for a prolog or scheme pair and if a prolog variable creates a cons")
#define FUNC_NAME s_gp_pair_bang
{
  SCM * y;
  if(GP(x))
    {
      y = GP_GETREF(x);
      if(GP_UNBOUND(*y))
	{
	  SCM *cons = GP_GETREF(gp_mk_cons());
	  gp_set_ref(y,GP_UNREF(cons));
	  return SCM_BOOL_T;
	}
      else
	if(GP_CONS(*y))
	  {
	    return SCM_BOOL_T;
	  }
      return SCM_BOOL_F;
    }
  return SCM_CONSP(x) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(gp_pair, "gp-pair?", 1, 0, 0, (SCM x), 
	   "checks for a prolog pair or scheme pair")
#define FUNC_NAME s_gp_pair
{
  if(GP(x))
    {
      if(GP_CONS(x))
	{
	  return SCM_BOOL_T;
	}
      return SCM_BOOL_F;
    }
  return SCM_CONSP(x) ? SCM_BOOL_T : SCM_BOOL_F;
}

#undef FUNC_NAME

SCM_DEFINE(gp_null, "gp-null?", 1, 0, 0, (SCM x), 
	   "checks for a prolog pair or scheme pair")
#define FUNC_NAME s_gp_null
{
  if(GP(x))
    {
      if(GP_VAL(x))
	{
	  return SCM_NULLP(*(GP_GETREF(x) + 1)) ? SCM_BOOL_T : SCM_BOOL_F;
	}
      return SCM_BOOL_F;
    }
  return SCM_NULLP(x) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_null_bang, "gp-null!?", 1, 0, 0, (SCM x), 
	   "checks for a prolog pair or scheme pair")
#define FUNC_NAME s_gp_null_bang
{
  SCM * y;
  if(GP(x))
    {
      y = GP_GETREF(x);
      if (GP_UNBOUND(*y))
	{
	  gp_set_val(y,SCM_EOL);
	  return SCM_BOOL_T;
	}
      else 
	if(GP_VAL(*y))
	  {
	    return SCM_NULLP(*(y + 1)) ? SCM_BOOL_T : SCM_BOOL_F;
	  }
      return SCM_BOOL_F;
    }
  return SCM_NULLP(x) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_lookup, "gp-lookup", 1, 0, 0, (SCM x), 
	   "lookup a  chain fro a prolog variable")
#define FUNC_NAME s_gp_gp_null_bang
{
  if(GP(x))
    {
      gp_lookup(GP_GETREF(x));
      return GP_UNREF(gp_ret.id);
    }
  return x;
}

#undef FUNC_NAME

SCM_DEFINE(gp_m_unify, "gp-m-unify!", 2, 0, 0, (SCM x, SCM y), 
	   "checks for a prolog pair or scheme pair")
#define FUNC_NAME s_gp_m_unify
{
  int ret;
  //Todo this is a ugly hack.
  gp_plus_unify  =  0;
  ret = gp_unify(GP_GETREF(x),GP_GETREF(y),1);
  gp_plus_unify  =  1;
  return ret ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(gp_car, "gp-car", 1, 0, 0, (SCM x), 
	   "takes car a prolog pair or scheme pair")
#define FUNC_NAME s_gp_car
{
  if(GP(x))
    {
      return GP_UNREF(GP_GETREF(x) - 2);
    }
  else
    {
      return SCM_CAR(x);
    }
}
#undef FUNC_NAME


SCM_DEFINE(gp_gp_cdr, "gp-cdr", 1, 0, 0, (SCM x), 
	   "takes cdr a prolog pair or scheme pair")
#define FUNC_NAME s_gp_gp_cdr
{
  if(GP(x))
    {
      return GP_UNREF(GP_GETREF(x) - 4);
    }
  else
    return SCM_CDR(x);
}
#undef FUNC_NAME















/*
  Smob that make sure the gc will mark out scm objects
 */

SCM unify_env_mark(SCM w)
{
  int i;
  for(i=0;i < gp_ci_a - gp_cstack_a; i+=3)
    {
      if(!(GP_EQ(gp_cstack_a[i+1].val)))
	scm_gc_mark(gp_cstack_a[i+2].val);
    }  

  for(i=0;i < gp_ci_b - gp_cstack_b; i+=3)
    {
      if(!(GP_EQ(gp_cstack_b[i+1].val)))
	scm_gc_mark(gp_cstack_b[i+2].val);
    }

  for(i=0;i < gp_si_a - gp_stack_a; i+=2)
    {
      if(!(GP_EQ(gp_stack_a[i])))
	scm_gc_mark(gp_stack_a[i+1]);
    }  

  for(i=0;i < gp_si_b - gp_stack_b; i+=2)
    {
      if(!(GP_EQ(gp_stack_b[i])))
	scm_gc_mark(gp_stack_b[i+1]);
    }
  return SCM_BOOL_T;
}


SCM unify_env_smob;
scm_t_bits unify_env_smob_t;

static void gp_init()
{
  //play nice with GC 
  unify_env_smob_t = scm_make_smob_type ("",0);
  scm_set_smob_mark(unify_env_smob_t, unify_env_mark);
  SCM_NEWSMOB(unify_env_smob, unify_env_smob_t, 0);
  
  /* stack initializations */
  gp_clear();
  gp_postpones[0].val = SCM_PACK(1);
  gp_nns_a = gp_stack_a  + gp_ns - 10;
  gp_nnw_a = gp_wstack_a + gp_nw - 10;
  gp_nnc_a = gp_cstack_a + gp_nc - 10;

  gp_nns = gp_nns_a;
  gp_nnw = gp_nnw_a;
  gp_nnc = gp_nnc_a;

  gp_nns_b = gp_stack_b  + gp_ns - 10;
  gp_nnw_b = gp_wstack_b + gp_nw - 10;
  gp_nnc_b = gp_cstack_b + gp_nc - 10;

  gp_unbound_str = scm_from_locale_string ("<gp>");
  gp_unbound_sym = scm_string_to_symbol (gp_unbound_str);
  gp_unbound_str = scm_from_locale_string ("gp-unwind-token");
  gp_unwind_fluid = scm_make_fluid();
  gp_cons_str    = scm_from_locale_string ("<gp-cons>");
  gp_cons_sym    = scm_string_to_symbol (gp_cons_str);
  se_pt  = gp_sestack;
  se_pt->val  = SCM_PACK(4);
  se_pt += 3;
} 


/*
  Local Variables:
  c-file-style: "gnu"
  End:
*/


