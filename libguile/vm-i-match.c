/*
 	Copyright (C) 2009, 2010 Free Software Foundation, Inc.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#define gp_fi     ((scm_t_uint8) 0)
#define gp_mode   ((scm_t_uint8) 1)
#define gp_next   ((scm_t_uint8) 3)
#define gp_sp     ((scm_t_uint8) 2)
#define gp_m0     2
#define gp_m1     3
#define gp_m2     4

#define STORE_NEXT					\
  {							\
    scm_t_int32 offset;					\
    if(*ip == 34)					\
      {							\
	ip++;						\
	FETCH_OFFSET (offset);				\
	LOCAL_SET(unify_base + gp_next			\
		  ,(SCM) (ip + offset));		\
      }							\
  }

#define GET_MOD(base) ((int) SCM_UNPACK(LOCAL_REF(base + gp_fi)))

#define UNIFY_MODDED(base)				\
  {							\
    if(GET_MOD(base)==2)				\
      LOCAL_SET(base + gp_fi,gp_newframe());	\
  }

#define UNIFY_FAIL(base)				\
  {							\
    if(GET_MOD(base)!=2)				\
      {							\
	gp_unwind(LOCAL_REF(base + gp_fi));		\
	LOCAL_SET(base + gp_fi,SCM_PACK(2));		\
      }							\
    ip = (scm_t_uint8 *) SCM_UNPACK(LOCAL_REF(base + gp_next));	\
    sp = (SCM *)         SCM_UNPACK(LOCAL_REF(base + gp_sp));	\
    STORE_NEXT;						\
    NEXT;						\
  }

VM_DEFINE_INSTRUCTION(119, unify_alloc_context, "unify-alloc-context",0,0,0)
{
  //closure_alloc_context = 1;
  NEXT;
}

VM_DEFINE_INSTRUCTION(118, unify_insert_raw, "unify-insert-raw",0,0,0)
{
  SCM x;
  scm_t_bits mode;
  mode = SCM_UNPACK(LOCAL_REF(unify_base + gp_mode));
  gp_debus0("insert>\n");
  POP(x);

  if(mode == gp_m0)
    {
      if(x == *sp)
	{
	  sp--;
	  NEXT;
	}
      UNIFY_FAIL(unify_base);
    }

  if(mode == gp_m2)
    {
      UNIFY_MODDED(unify_base);  
      if(gp_unify(GP_GETREF(UN_GP(x)),GP_GETREF(UN_GP(*sp)),1,1))
	{
	  sp --;
	  NEXT;
	}
      UNIFY_FAIL(unify_base);
    }
  
  if(mode == gp_m1)
    {      
      if(gp_unify(GP_GETREF(UN_GP(x)),GP_GETREF(UN_GP(*sp)),1,0))
	{
	  sp --;
	  NEXT;
	}
      UNIFY_FAIL(unify_base);
    }
}

VM_DEFINE_INSTRUCTION (117, unify_tail_call, "u-tail-call", 1, -1, 1)
{
  int i,base;

  nargs = FETCH ();

  ip++;
  base = (int) FETCH() + 4;

 u_vm_call:
  program = sp[-nargs];

  VM_HANDLE_INTERRUPTS;

  if (SCM_UNLIKELY (!SCM_PROGRAM_P (program)))
    {
      if (SCM_STRUCTP (program) && SCM_STRUCT_APPLICABLE_P (program))
        {
          sp[-nargs] = SCM_STRUCT_PROCEDURE (program);
          goto u_vm_call;
        }
      else if (SCM_NIMP (program) && SCM_TYP7 (program) == scm_tc7_smob
               && SCM_SMOB_APPLICABLE_P (program))
        {
          SYNC_REGISTER ();
          sp[-nargs] = scm_i_smob_apply_trampoline (program);
          goto u_vm_call;
        }
      else
        goto vm_error_wrong_type_apply;
    }

  CACHE_PROGRAM ();

  //Special "tail call" to save stack space
  //printf("base(%d) ~ ref(%d)\n",base,sp-fp-nargs-3);
  {
    SCM *old_fp = fp;
    
    if(base < sp - fp  - nargs - 3)
      {
	sp -= nargs + 3;
	for(i=0;i<=nargs + 3;i++)
	  {
	    fp[base + i] = *sp; sp++;
	  }
	sp = fp + base + 3 + nargs;
	fp = fp + base + 4;
      }
    else if(base == sp - fp  - nargs - 3)
      {
	fp = sp - nargs + 1;
      }
    else
      printf("WARNING! base(%x) > %d\n",base,(int) (sp-fp-nargs-3));

    ASSERT (SCM_FRAME_DYNAMIC_LINK (fp) == 0);
    ASSERT (SCM_FRAME_RETURN_ADDRESS (fp) == 0);
    ASSERT (SCM_FRAME_MV_RETURN_ADDRESS (fp) == 0);
    SCM_FRAME_SET_DYNAMIC_LINK (fp, old_fp);
    SCM_FRAME_SET_RETURN_ADDRESS (fp, ip);
    SCM_FRAME_SET_MV_RETURN_ADDRESS (fp, 0);
    ip = SCM_C_OBJCODE_BASE (bp);
  }

  APPLY_HOOK ();
  NEXT;
}

VM_DEFINE_INSTRUCTION(116, unify_var, "unify-var!",0,0,1)
{
  sp++;
  *sp = GP_IT(gp_mk_var());
  //printf("mk-var> %d", SCM_UNPACK(*sp) - (scm_t_bits) gp_stack);
  NEXT;
}

VM_DEFINE_INSTRUCTION(115, unify_scm, "unify-scm",0,0,1)
{
  //printf("unify-scm> %d, GP? %d\n", (scm_t_bits) *sp - (scm_t_bits) gp_stack, GP(*sp));
  *sp = smob2scm(*sp);
  NEXT;
}

VM_DEFINE_INSTRUCTION(114, unifyp, "unify!",1,0,0)
{
  int raw = FETCH();
  sp--;

  *sp = gp_unify(GP_GETREF(*sp),GP_GETREF(*(sp+1)),raw,1) 
    ? SCM_BOOL_T : SCM_BOOL_F;
  
  NEXT;
}

// car cdr sp


VM_DEFINE_INSTRUCTION(113, gp_cons, "gp-cons",0,0,0)
{
  SCM *id;
  SCM *cons   = GP_GETREF(gp_mk_cons());
  SCM car,cdr = *sp;
  gp_debus0("gp-cons>\n");
  sp --;
  car = *sp;
  DS(smob2scm(car));
  DS(smob2scm(cdr));
  if(GP(car))    
    { 
      id = gp_lookup(UN_GP(car));      
      gp_set_ref(GP_CAR(cons),GP_UNREF(id));
    }
  else
    {
      gp_debus0("atom car>\n");
      gp_set_val(GP_CAR(cons),car);
    }

  if(GP(cdr))    
    { 
      id = gp_lookup(UN_GP(cdr));
      gp_set_ref(GP_CDR(cons),GP_UNREF(id));
    }
  else
    {
      gp_debus0("atom cdr>\n");
      gp_set_val(GP_CDR(cons),cdr);
    }

  *sp = GP_IT(cons);

  DS(smob2scm(*sp));

  NEXT;
}

#define GGG_PACK(x)   SCM_PACK((((size_t) x)<<2) | 2 )
#define GGG_UNPACK(x) (SCM_UNPACK(x)>>2)
#define GGU_PACK(x)   SCM_PACK(SCM_UNPACK(x) |  2 )
#define GGU_UNPACK(x) SCM_PACK(SCM_UNPACK(x) & ~2 )
//#define DS(X) X
VM_DEFINE_INSTRUCTION(112, unify_prompt, "unify-prompt",0,0,0)
{
  int base;
  gp_debus0("prompt>\n");
  ip++;
  base = (int) FETCH();

  UNIFY_MODDED(base);
 
  /* 
  y        = gp_si; gp_si += 3;
  *(y + 0) = SCM_PACK(base);
  *(y + 1) = GP_UNREF(fp);

  //dynwinds
  *(y + 2) = scm_i_dynwinds();
 
  PUSH(GP_UNREF(y));
  */

  PUSH(scm_cons(scm_i_dynwinds(),
		scm_cons(GGG_PACK(base),
			 GGU_PACK(GP_UNREF(fp)))));

  gp_debus2("fp %x bs %d\n",(int) fp, base );
  gp_debus2("fp %x bs %d\n",(scm_t_bits) GGU_PACK(fp), GGG_PACK(base) );
  gp_debus2("sp %x ip %x\n",(int) sp, ip   );
  gp_debus0("end-prompt>\n");
  NEXT;
}

VM_DEFINE_INSTRUCTION(111, unify_unprompt, "unify-unprompt",0,0,0)
{
  SCM prompt, x, winds, wind_ref;
  int delta,found = 0;

  gp_debus0("un-prompt>\n");

  POP(prompt);
  
  wind_ref   = SCM_CAR(prompt);
  prompt     = SCM_CDR(prompt);

  unify_base = (int) GGG_UNPACK(SCM_CAR(prompt));
  fp         = GP_GETREF(GGU_UNPACK(SCM_CDR(prompt)));

  /*

  ref        = GP_GETREF(prompt);
  unify_base = (int) SCM_UNPACK(*ref);
  fp         = GP_GETREF(*(ref + 1));
  wind_ref   = *(ref + 2);
  */
  for (winds = scm_i_dynwinds (), delta = 0;
       scm_is_pair (winds);
       winds = SCM_CDR (winds), delta++)
    {
      if(winds == wind_ref)
	{
	  found=1;
	  break;
	}
    }

  if(!found) 
    {
      scm_misc_error ("abort", "Abort to unknown prompt", scm_list_1 (wind_ref));
    }
  
  //unwind dynwinds fluid, scm-prompts etc.
  scm_dowinds (winds, delta);
  
  program = SCM_FRAME_PROGRAM (fp);
  CACHE_PROGRAM ();

  sp = GP_GETREF(LOCAL_REF(unify_base + gp_sp));
  ip = (scm_t_uint8 *) LOCAL_REF( unify_base + gp_next );
  gp_debus2("fp %x bs %d\n",(int) fp, unify_base );
  gp_debus2("sp %x ip %x\n",(int) sp, ip         );
  x = LOCAL_REF(unify_base + gp_fi);
  gp_unwind(x);
  LOCAL_SET(unify_base + gp_fi,SCM_PACK(2));

  gp_debus0("un-prompt> END\n");
  STORE_NEXT;
  NEXT;
}
//#define DS(X)

VM_DEFINE_INSTRUCTION(110, unify_rebase, "unify-base",0,0,0)
{  
  ip++;
  unify_base = FETCH();
  //DS(scm_simple_format(SCM_BOOL_T,scm_from_locale_string("sp ~a~%"),*sp));
  gp_debus1("base> %d\n",(int) unify_base);
  gp_debus1("base> sp %x\n",(int) LOCAL_REF(unify_base + gp_sp));
  NEXT;
}

VM_DEFINE_INSTRUCTION(120, unify_ifnext, "unify-ifnext",0,0,0)
{
  SCM x;
  gp_debus0("ifnext>\n");
  POP(x);
  if(!(x == SCM_BOOL_F)) NEXT;

  x = LOCAL_REF(unify_base + gp_fi);
  if(GET_MOD(unify_base)!=2) gp_unwind(x);
  LOCAL_SET(unify_base + gp_fi,SCM_PACK(2));  

  sp = (SCM *)         LOCAL_REF(unify_base + gp_sp);
  ip = (scm_t_uint8 *) LOCAL_REF(unify_base + gp_next);
  
  STORE_NEXT;
  NEXT;
}


VM_DEFINE_INSTRUCTION(127, unify_mode, "unify-mode",1,0,0)
{
  LOCAL_SET(unify_base + gp_mode, (SCM)  FETCH());
  NEXT;
}

VM_DEFINE_INSTRUCTION(126, unify_val, "unify-val",0,0,0)
{
  int mode = (int) LOCAL_REF(unify_base + gp_mode);
  if(mode == gp_m0) NEXT;
  *sp = smob2scm(*sp);
  NEXT;  
}


VM_DEFINE_INSTRUCTION(125, unify_next, "unify-next",0,0,0)
{
  SCM x;
  gp_debus0("unify-next>\n");
  ip++;
  unify_base = FETCH();
  x = LOCAL_REF(unify_base + gp_fi);
  gp_debus1("unify-next> base is %d\n", (int) unify_base);
  if(GET_MOD(unify_base)!=2) gp_unwind(x);
  LOCAL_SET(unify_base + gp_fi,SCM_PACK(2));  

  sp = (SCM *)         LOCAL_REF(unify_base + gp_sp);
  ip = (scm_t_uint8 *) LOCAL_REF(unify_base + gp_next);
  gp_debus1("unnify-next> sp = %x\n", (int) sp);

  STORE_NEXT;
  NEXT;
}


VM_DEFINE_INSTRUCTION(124, unify_init, "unify-init",0,0,0)
{
  ip++;
  unify_base = FETCH();ip++;
  LOCAL_SET(unify_base + gp_fi  , SCM_PACK(2))   ; //initiate frame data
  //LOCAL_SET(unify_base + gp_fi  , gp_newframe())   ; //initiate frame data
  LOCAL_SET(unify_base + gp_mode, (SCM) FETCH()) ; //set mode type
  gp_debus1("mode %d\n",LOCAL_REF(unify_base + gp_mode));
  LOCAL_SET(unify_base + gp_sp  , GP_UNREF(sp));
  //getting the jump for the next 
  //ip: <void> <br> a1 b1 c1 
  gp_debus1("init> sp %x\n",(int) sp);
  gp_debus1("init> sp-val %x\n",(int) SCM_UNPACK(*sp));
  STORE_NEXT;
  NEXT;
}


VM_DEFINE_INSTRUCTION(123, unify_cons, "unify-cons",0,0,0)
{
  SCM *id,val;
  int mode;
  gp_debus1("cons> %x\n",SCM_UNPACK(*sp));
  mode = (int) LOCAL_REF(unify_base + gp_mode);

  if(mode  > gp_m0 && GP(*sp))
    {
      gp_debus0("> m0");
      id = gp_lookup(UN_GP(*sp));
      sp--;
      if(GP_CONS(id))
	{
	  gp_debus0("a gp cons");
	  PUSH(GP_IT(GP_CDR(id)));
	  PUSH(GP_IT(GP_CAR(id)));
	  NEXT;
	}	  
      
      val = GP_SCM(id);
      if(SCM_CONSP(val))
	{
	  gp_debus0("a scm cons");
	  PUSH(SCM_CDR(val));
	  PUSH(SCM_CAR(val));
	  NEXT;
	}
      
      if(mode == gp_m2 && GP_UNBOUND(id)) //variable
	{
	  SCM *ref;
	  gp_debus0("cons new>\n");
	  UNIFY_MODDED(unify_base);
	  ref = GP_GETREF(gp_mk_cons());
	  gp_set_ref(id,GP_UNREF(ref));
	  PUSH(GP_IT(GP_CDR(ref)));
	  PUSH(GP_IT(GP_CAR(ref)));	    
	  NEXT;
	}

      

      gp_debus0("cons fail>");
      UNIFY_FAIL(unify_base);
    }
  
  gp_debus0("cons> got simple mathc\n");
  if(!GP(*sp) && SCM_CONSP(*sp))
    {
      SCM x;
      gp_debus0("cons> it's a pair!!\n");
      x = *sp;
      sp--;
      PUSH(SCM_CDR(x));
      PUSH(SCM_CAR(x));
      NEXT;
    }  
  UNIFY_FAIL(unify_base);
}

VM_DEFINE_INSTRUCTION(211, unify_fp, "unify-f?",0,0,0)
{
  SCM x; 
  POP(x);
  if(x == SCM_BOOL_F)    
    UNIFY_FAIL(unify_base);
  
  NEXT;
}

VM_DEFINE_INSTRUCTION(122, unify_eq, "unify-eq",0,0,0)
{
  SCM x, *id, val;
  int mode;

  mode = (int) LOCAL_REF(unify_base + gp_mode);
  gp_debus0("eq>\n"); 
  POP(x);
  
  if(mode>gp_m0 && GP(*sp))
    {
      gp_debus2("addr %x, val %x\n",SCM_UNPACK(*sp),SCM_UNPACK(*GP_GETREF(*sp)));
      id = gp_lookup(GP_GETREF(*sp));
      sp--;
      
      if(GP_UNBOUND(id))
	{
	  if(mode == gp_m2) 
	    {
	      //Set variable to x
	      UNIFY_MODDED(unify_base);	 
	      gp_set_val(id,x);
	      NEXT;
	    }
	  else
	    UNIFY_FAIL(unify_base);
	}
      else
	{
	  //Check that variable is x
	  val = GP_SCM(id);
	  if(GP_EQ(id))
	    {
	      if (scm_is_eq(val, x))
		{
		  NEXT;
		}
	      else
		UNIFY_FAIL(unify_base);
	    }
	  else
	    {
	      if (scm_is_true(scm_equal_p(val,x)))
		{
		  NEXT;
		}
	      else
		UNIFY_FAIL(unify_base);
	    }
	}
    }

  gp_debus1("eq_sp %x\n",(int) SCM_UNPACK(*sp));
  if(!GP(*sp) && scm_is_true(scm_equal_p(x,*sp)))
    {
      sp --;
      NEXT;
    }
  UNIFY_FAIL(unify_base);  
}
  


VM_DEFINE_INSTRUCTION(121, unify_insert, "unify-insert",0,0,0)
{
  SCM x;
  int mode = (int) LOCAL_REF(unify_base + gp_mode);
  gp_debus0("insert>\n");
  POP(x);

  if(mode == gp_m0)
    {
      if(scm_is_true(scm_equal_p(x,*sp)))
	{
	  sp--;
	  NEXT;
	}
      UNIFY_FAIL(unify_base);
    }

  if(mode == gp_m2)
    {
      UNIFY_MODDED(unify_base);  

      if(gp_unify(GP_GETREF(UN_GP(x)),GP_GETREF(UN_GP(*sp)),0,1))
	{
	  sp --;
	  NEXT;
	}
      UNIFY_FAIL(unify_base);
    }

  if(mode == gp_m1)
    {
      if(gp_unify(GP_GETREF(UN_GP(x)),GP_GETREF(UN_GP(*sp)),1,0))
	{
	  sp --;
	  NEXT;
	}
      UNIFY_FAIL(unify_base);
    }
}

VM_DEFINE_INSTRUCTION(108, unify_modded, "unify-modded",0,0,0)
{
  UNIFY_MODDED(unify_base);
  NEXT;
}

VM_DEFINE_INSTRUCTION(109, unify_set,    "unify-set!",0,0,0)
{
  ARGS2(x,y);
  ggp_set(x,y);
  NEXT;
}


VM_DEFINE_INSTRUCTION(210, over_to_c,    "over-to-c",1,0,0)
{
  int i;
  SCM sf;
  SCM *ssp;

  typedef void (*fast_f)(SCM *, SCM **);
  fast_f f;

  i  = FETCH();

  sf = OBJECT_REF(i); 

  if(!(SCM_UNPACK(sf) & (scm_t_bits) 2)) goto check_for_c_fkn;
  f = (fast_f) (SCM_UNPACK(sf) & ~ (scm_t_bits) 2);
 
  
 run_c_fkn:
  ssp = sp;
  (*f)(fp,&ssp);

  sp = ssp;
  if(!(SCM_UNPACK(*sp) >> 4)) goto c_fkn_return;

  sp --;
  sf = fp[-1];
  if(!(SCM_UNPACK(sf) & 2)) goto check_for_c_fkn;
  f = (fast_f) (SCM_UNPACK(sf) & ~2);
  goto run_c_fkn;


 c_fkn_return:
  sp--;
  if(SCM_UNPACK(sp[1]) == 2) goto vm_return;
  
 check_for_c_fkn:
  {
    if(SCM_SYMBOLP(sf))
      {
	if (!SCM_VARIABLEP (sf)) 
	  {
	    SYNC_REGISTER ();
	    sf = resolve_variable (sf, scm_program_module (program));
	    if (!VARIABLE_BOUNDP (sf))
	      {
		NEXT;
	      }
	  }
	sf = VARIABLE_REF(sf);
	OBJECT_SET(i,sf);
	
	f = (fast_f) (SCM_UNPACK(sf) & ~ (scm_t_bits) 2);
	goto run_c_fkn;
      }
    NEXT;
  }
 
  NEXT;
}
