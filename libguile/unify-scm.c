/*
 	Copyright (C) 2009, 2010 Free Software Foundation, Inc.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
scm version 3 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


#include<libguile.h>
#include<stdio.h>

scm_t_bits gp_type;

#define gp_format0(str)				\
scm_simple_format(SCM_BOOL_T,			\
		  scm_from_locale_string(str),	\
		  SCM_EOL)

#define gp_format1(str,x)			\
scm_simple_format(SCM_BOOL_T,			\
		  scm_from_locale_string(str),	\
		  scm_list_1(x))

#define gp_format2(str,x,y)			\
scm_simple_format(SCM_BOOL_T,			\
		  scm_from_locale_string(str),	\
		  scm_list_2(x,y))



#define DB(X)
#define DS(X)
#define gp_debug0(s)        DB(printf(s)      ; fflush(stdout))
#define gp_debug1(s,a)      DB(printf(s,a)    ; fflush(stdout))
#define gp_debug2(s,a,b)    DB(printf(s,a,b)  ; fflush(stdout))
#define gp_debug3(s,a,b,c)  DB(printf(s,a,b,c); fflush(stdout))
#define gp_debus0(s)        DS(printf(s)      ; fflush(stdout))
#define gp_debus1(s,a)      DS(printf(s,a)    ; fflush(stdout))
#define gp_debus2(s,a,b)    DS(printf(s,a,b)  ; fflush(stdout))
#define gp_debus3(s,a,b,c)  DS(printf(s,a,b,c); fflush(stdout))
/*
   000 - A ref to a new object
   010 - A cons ref
   110 - A cons rest
   100 - A rest

 1x11xx - unbounded
 1x01xx - eq
 0x11xx - val

  unbounded is a null reference aka X & 0b000  == 0
*/

/*
  We will work with three stacks.
  stack  - the variable stack
  cstack - storage stack to undo setted values
  wstack - frame stack in order to do batched backtracking
*/

#define gp_N2   1000000
int gp_nc      = gp_N2;
SCM SCM_ALIGNED(8) gp_cstack_a[gp_N2];
SCM SCM_ALIGNED(8) gp_cstack_b[gp_N2/10];
SCM * gp_cstack = gp_cstack_a;

#define gp_N3   1000000
int gp_ns      = gp_N3;
SCM SCM_ALIGNED(8) gp_stack_a[gp_N3];
SCM SCM_ALIGNED(8) gp_stack_b[gp_N3/10];
SCM * gp_stack = gp_stack_a;

SCM* gp_si;
SCM* gp_si_b;
SCM* gp_si_a;

SCM *gp_ci;
SCM *gp_ci_a;
SCM *gp_ci_b;

SCM *gp_nnc;
SCM *gp_nnc_a;
SCM *gp_nnc_b;

SCM *gp_nns;
SCM *gp_nns_a;
SCM *gp_nns_b;

scm_t_bits gp_smob_t;

int closure_alloc_context = 0;

static inline SCM alloc_unify_words (scm_t_bits car, scm_t_uint16 n_words)
{
  SCM z = SCM_PACK((scm_t_bits) gp_si);
  if(gp_si + n_words > gp_nns) goto er;
  
  *gp_si = SCM_PACK(car);
  gp_si = gp_si + n_words;

  return z;

 er:
  scm_out_of_range(NULL, SCM_I_MAKINUM(gp_nc));
}
// Assumes stack_a and stack_b are allocated in order they appear
#define GP(scm) (SCM_NIMP(scm) && SCM_SMOB_PREDICATE (gp_type, scm))
#define GP_STAR(scmp) (GP(GP_UNREF(scmp)))
/*
  Tagging
  xx00  Pointer
  xx01  cons
  xx10  val
  xx11  unbound
   1    attr flag
  1     eq   flag
  
 */
#define B(c) ((scm_t_bits) c)
#define GPM_BASE    B(0x30000)

#define GPI_PTR     B(0x00000)
#define GPI_CONS    B(0x10000)
#define GPI_VAL     B(0x20000)
#define GPI_UNBD    B(0x30000)

#define GPI_ATTR    B(0x40000)
#define GPI_EQ      B(0x80000)
#define GPQ_EQ      B(0xa0000)

#define GPM_PTR     B(0x0ffff)
#define GPM_CONS    B(0x1ffff)
#define GPM_VAL     B(0x2ffff)
#define GPM_UNBD    B(0x3ffff)
#define GPM_EQ      B(0xaffff)

#define GPI         B(0xf0000)
#define GPM         B(0xfffff)

#define GP_FLAGS(x) ((x)[0])
#define GP_SCM(x)   ((x)[1])

#define GP_IS_EQ(x ) (SCM_IMP(x) || scm_is_true(scm_symbol_p(x)))

#define GP_UNBOUND(ref) ( (SCM_UNPACK(GP_FLAGS(ref)) & GPM_BASE) == GPI_UNBD)
#define GP_POINTER(ref) ( (SCM_UNPACK(GP_FLAGS(ref)) & GPM_BASE) == GPI_PTR)
#define GP_CONS(ref)    ( (SCM_UNPACK(GP_FLAGS(ref)) & GPM_BASE) == GPI_CONS)
#define GP_VAL(ref)     ( (SCM_UNPACK(GP_FLAGS(ref)) & GPM_BASE) == GPI_VAL)

#define GP_EQ(ref)      ( SCM_UNPACK(GP_FLAGS(ref)) & GPI_EQ)
#define GP_ATTR(ref)    ( SCM_UNPACK(GP_FLAGS(ref)) & GPI_ATTR)

#define GP_MK_FRAME_VAL(fr)  ((fr) | GPI_VAL)
#define GP_MK_FRAME_PTR(fr)  ((fr) | GPI_PTR)
#define GP_MK_FRAME_CONS(fr) ((fr) | GPI_CONS)
#define GP_MK_FRAME_UNBD(fr) ((fr) | GPI_UNBD)

#define GP_MK_FRAME_EQ(fr)   ((fr) | GPQ_EQ)

#define SCM_TO_FI(x) SCM_I_INUM(x)
#define FI_TO_SCM(x) SCM_I_MAKINUM(x)

#define PTR2NUM(x) SCM_PACK((((scm_t_bits) x) | 2))
#define NUM2PTR(x) ((SCM *) (SCM_UNPACK(x) & ~2))

#define GP_CAR(id)  ((id) - 2)
#define GP_CDR(id)  ((id) - 4)
#define GP_BUDY(id) ((id) + 2)

static inline void swap_to_b()
{
  gp_si_a = gp_si;
  gp_si   = gp_si_b;

  gp_ci_a = gp_ci;
  gp_ci   = gp_ci_b;

  gp_nnc_a = gp_nnc;
  gp_nnc   = gp_nnc_b;

  gp_nns_a = gp_nns;
  gp_nns   = gp_nns_b;

  gp_cstack = gp_cstack_b;
  gp_stack  = gp_stack_b;
}

static inline void swap_to_a()
{
  gp_si_b = gp_si;
  gp_si   = gp_si_a;

  gp_ci_b = gp_ci;
  gp_ci   = gp_ci_a;

  gp_nnc_b = gp_nnc;
  gp_nnc   = gp_nnc_a;

  gp_nns_b = gp_nns;
  gp_nns   = gp_nns_a;

  gp_cstack = gp_cstack_a;
  gp_stack  = gp_stack_a;
}
/*
static inline SCM* GP_GETREF(SCM x)
{
  scm_t_bits up = SCM_UNPACK(x);
  gp_debug1("getref> unpacked %x\n",up);
  return (SCM *) up;
}

static inline SCM GP_UNREF(SCM *x)
{
  scm_t_bits up = (scm_t_bits) x;
  gp_debug1("unref> unpacked %x\n",up);
  return SCM_PACK(up);
  }
*/

#define GP_GETREF(x) ((SCM *) (x))

#define GP_UNREF(x)  ((SCM)   (x))


inline static SCM GP_IT(SCM* id)
{
  return GP_UNREF(id);
}


static SCM* UN_GP(SCM scm)
{
  return GP_GETREF(scm);
}


/*
static inline int GP(SCM scm)
{
  return SCM_UNPACK(scm) & B(2);
}
*/

 /*
#define GP_IT(id) SCM_PACK(SCM_UNPACK(GP_UNREF(id)) + B(2))
#define UN_GP(scm) GP_GETREF(SCM_PACK(SCM_UNPACK(scm) - B(2)))
 */
 //#define GP(scm) (SCM_UNPACK(scm) & B(2))
 /* Moved to unify.h
#define GP(scm) ( !(0x7 & SCM_UNPACK(scm))                   &&	\
		  (SCM_UNPACK(scm) >= (scm_t_bits) gp_stack) &&	\
		  (SCM_UNPACK(scm) <  (scm_t_bits) gp_nns))

 */
SCM gp_unbound_sym;
SCM gp_unbound_str;
SCM gp_unwind_fluid;
SCM gp_cons_sym;
SCM gp_cons_str;



#define GP_TEST_CSTACK if(gp_ci > gp_nnc) scm_out_of_range(NULL, SCM_I_MAKINUM(gp_nc))

static inline void gp_store_var_2(SCM *id, int simple)
{
  GP_TEST_CSTACK;
  
  if(simple || GP_UNBOUND(id))
    {
      *gp_ci = GP_UNREF(id);
    }
  else
    {
      *gp_ci = scm_cons(GP_UNREF(id),  scm_cons(SCM_I_MAKINUM(id[0]),
						id[1]));
    }
  
  gp_ci += 1;
}



static inline void gp_set_val(SCM *id, SCM v)
{
  SCM val; 
  DS(if(!v) gp_debug0("setting 0 value\n"));

  gp_store_var_2(id,0);

  if(GP_IS_EQ(v))
    val =  SCM_PACK(GP_MK_FRAME_EQ(gp_type));
  else
    val =  SCM_PACK(GP_MK_FRAME_VAL(gp_type));
  

  gp_debug2("set-val> id = %x, val = %x\n",(int) id, (int) SCM_UNPACK(val));

  *(id + 0) = val;
  *(id + 1) = v;
}

static inline void gp_set_val_bang(SCM *id, SCM v)
{
  SCM val; 
  DS(if(!v) gp_debug0("setting 0 value\n"));

  if(GP_IS_EQ(v))
    val =  SCM_PACK(GP_MK_FRAME_EQ(gp_type));
  else
    val =  SCM_PACK(GP_MK_FRAME_VAL(gp_type));
  

  gp_debug2("set-val> id = %x, val = %x\n",(int) id, (int) SCM_UNPACK(val));

  *(id + 0) = val;
  *(id + 1) = v;
}

static inline  void gp_set_ref(SCM *id, SCM ref)
{
  SCM val;

  gp_store_var_2(id,0);
  gp_debug0("setref ...");

  val = SCM_PACK(GP_MK_FRAME_PTR(gp_type));
  gp_debug2(">> id = %x, val = %x\n",(scm_t_bits) id, SCM_UNPACK(val));
  *(id + 0) = val;
  *(id + 1) = ref;
}

static inline  void gp_set_ref_bang(SCM *id, SCM ref)
{
  SCM val;

  gp_debug0("setref ...");

  val = SCM_PACK(GP_MK_FRAME_PTR(gp_type));
  gp_debug2(">> id = %x, val = %x\n",(scm_t_bits) id, SCM_UNPACK(val));
  *(id + 0) = val;
  *(id + 1) = ref;
}

static inline  void gp_set_eq(SCM *id, SCM v) 
{

  DS(if(!v) gp_debug0("setting 0 value\n"););

  gp_store_var_2(id,0);

  gp_debug0("handled storing\n");

  *(id + 0) = SCM_PACK(GP_MK_FRAME_EQ(gp_type));
  gp_debug2("set id = %x 0 val = %x\n",id,SCM_UNPACK(*id)) ;
  *(id + 1) = v;
  gp_debug2("set id = %x 1 val = %x\n",id,SCM_UNPACK(v));
}

static inline  void gp_set_eq_bang(SCM *id, SCM v) 
{

  DS(if(!v) gp_debug0("setting 0 value\n"););

  gp_debug0("handled storing\n");

  *(id + 0) = SCM_PACK(GP_MK_FRAME_EQ(gp_type));
  gp_debug2("set id = %x 0 val = %x\n",id,SCM_UNPACK(*id)) ;
  *(id + 1) = v;
  gp_debug2("set id = %x 1 val = %x\n",id,SCM_UNPACK(v));
}


static inline void gp_set_unbound(SCM *id) 
{

  gp_debug0("set-unbound>\n");

  gp_store_var_2(id,0);

  *(id + 0) = SCM_PACK(GP_MK_FRAME_UNBD(gp_type));
  *(id + 1) = SCM_UNBOUND;
}

static inline void gp_set_unbound_bang(SCM *id) 
{
  gp_debug0("set-unbound>\n");

  *(id + 0) = SCM_PACK(GP_MK_FRAME_UNBD(gp_type));
  *(id + 1) = SCM_UNBOUND;
}

static inline SCM * gp_lookup(SCM *id)
{
  gp_debug0("lookup>\n");
 retry:
  if(GP_POINTER(id))
    {
      id = GP_GETREF(GP_SCM(id));
      goto retry;
    }

  gp_debug2("lookup> %x 0 val = %x\n",id,SCM_UNPACK(*id)) ;
  return id;
}


static inline SCM gp_newframe()
{
  SCM cons = scm_cons(PTR2NUM(gp_ci),PTR2NUM(gp_si));
  return cons;
}

#define GP_TEST_STACK if(gp_si > gp_nns) scm_out_of_range(NULL, SCM_I_MAKINUM(gp_ns))
static inline  SCM* gp_mk_var()
{
  SCM *ret;

  GP_TEST_STACK;
  gp_debug1("test stack handled! %x\n",gp_si);
  ret  = gp_si;
  gp_si += 2;

  *(ret + 0) = SCM_PACK(GP_MK_FRAME_UNBD(gp_type)); 
  *(ret + 1) = SCM_UNBOUND;
  
  gp_debug1("returning from mk_var %x\n",ret);
  return ret;
}



static inline SCM gp_mk_cons() 
{
  SCM *ret;
  scm_t_bits fi; 
  GP_TEST_STACK;
  ret = gp_si;
  gp_si += 6;

  gp_debug0("in cons is %x\n");

  fi = GP_MK_FRAME_CONS(gp_type);
  *(ret+4) = SCM_PACK(fi);
  *(ret+5) = SCM_UNDEFINED;  
  
  //hint that var was undefined before in order to undo correctly
  gp_store_var_2(ret + 4,1);

  fi = GP_MK_FRAME_UNBD(gp_type);
  *(ret+2) = SCM_PACK(fi);
  *(ret+0) = SCM_PACK(fi);

  *(ret+1) = SCM_UNBOUND;
  *(ret+3) = SCM_UNBOUND;

  return GP_UNREF(ret + 4);
}


static int gp_recurent(SCM *id1,SCM *id2)
{  
  SCM scm;

  if(!GP(GP_UNREF(id2))) goto non_gp;

  id2 = gp_lookup(id2);  

  gp_debug0("recurent> looked up data\n");
  if(id1 == id2  )  
    {
      gp_debug0("recurent> Found a recurence!!\n");
      return 1;  
    }
  
  if(GP_CONS(id2))
    {
      gp_debug0("recurent> got a cons\n");
      return gp_recurent(id1,GP_CAR(id2)) || gp_recurent(id1,GP_CDR(id2));
    }

  if(GP_UNBOUND(id2)) return 0;

  scm = GP_SCM(id2);
  if(SCM_CONSP(scm))
    return 
      (gp_recurent(id1,GP_GETREF(SCM_CAR(scm))) ||
       gp_recurent(id1,GP_GETREF(SCM_CDR(scm))));

  
  gp_debug0("recurent> atom!\n");
  return 0;

 non_gp:
  if(SCM_CONSP(GP_UNREF(id2)))
    {
      return 
	gp_recurent(id1,GP_GETREF(SCM_CAR(GP_UNREF(id2)))) ||
	gp_recurent(id1,GP_GETREF(SCM_CDR(GP_UNREF(id2))));
    }
  return 0;
}

static SCM smob2scm_gp(SCM *id)
{
  SCM scm;
 
  if(GP_UNBOUND(id))
    return GP_UNREF(id);
  
  scm = GP_SCM(id);
  if(SCM_CONSP(scm))
    {	  
      SCM car = smob2scm(SCM_CAR(scm));
      SCM cdr = smob2scm(SCM_CDR(scm));
      if(car == SCM_CAR(scm) && cdr == SCM_CDR(scm))
	return scm;
      return scm_cons(car,cdr);
    }
  else
    return scm;
}


SCM_DEFINE( smob2scm, "gp->scm", 1, 0, 0, (SCM scm),
	    "creates a scheme representation of a gp object")
#define FUNC_NAME s_smob2scm
{
  if(GP(scm))
    {
      SCM *id;
      id = UN_GP(scm);
      
      id = gp_lookup(id);

      if(GP_CONS(id))
	{
	  return scm_cons(smob2scm(SCM_PACK(GP_CAR(id))),
			  smob2scm(SCM_PACK(GP_CDR(id))));
	}      
      return smob2scm_gp(id);
    }
  else 
    {
      if(SCM_CONSP(scm))
	{	  
	  SCM car = smob2scm(SCM_CAR(scm));
	  SCM cdr = smob2scm(SCM_CDR(scm));
	  if(car == SCM_CAR(scm) && cdr == SCM_CDR(scm))
	    return scm;
	  return scm_cons(car,cdr);
	}
      else
	return scm;
    }
}
#undef FUNC_NAME

#define QCDR(x)   GP_GETREF(SCM_CDR(GP_UNREF(x))) 
#define QCAR(x)   GP_GETREF(SCM_CAR(GP_UNREF(x))) 
#define QCONSP(x) SCM_CONSP(GP_UNREF(x))

// unify under + means unification - means just match
static int gp_unify(SCM *id1, SCM *id2, int raw, int gp_plus_unify)
{ 
  SCM * stack[20];
  int   sp;
  
  sp = 0;
  
#define U_NEXT					\
  {						\
  if(SCM_UNLIKELY(sp==0))			\
    {						\
      return 1;					\
    }						\
  else						\
    {						\
      id2 = stack[--sp];			\
      id1 = stack[--sp];			\
      goto retry;				\
    }						\
}

 retry:
  gp_debug0("unify>\n");  


  if(GP_STAR(id1))
    {
      if(GP_STAR(id2))
	{
	  gp_debug0("11>\n");
	  id2 = gp_lookup(id2);
	  id1 = gp_lookup(id1);


	  if(SCM_CONSP(GP_SCM(id1)))
	    {
	      id1 = GP_GETREF(GP_SCM(id1));
	      if(SCM_CONSP(GP_SCM(id2)))
		{
		  id2 = GP_GETREF(GP_SCM(id2));
		  goto u00;
		}
	      else
		{
		  goto u01;
		}
	    }
	  else
	    {
	      if(SCM_CONSP(GP_SCM(id2)))
		{
		  id2 = GP_GETREF(GP_SCM(id2));
		  goto u10;
		}
	      //u11 falls through
	    }
	}
      else
	{
	  gp_debug0("10>\n");
	  id1 = gp_lookup(id1);

	  gp_debug0("10> lookup\n");
	  if(SCM_CONSP(GP_SCM(id1)))
	    {
	      id1 = GP_GETREF(GP_SCM(id1));
	      goto u00;
	    }

	  goto u10;
	}
    }
  else
    {
      if(GP(GP_UNREF(id2)))
	{
	  gp_debug0("01>\n");
	  id2 = gp_lookup(id2);

	  if(SCM_CONSP(GP_SCM(id2)))
	    {
	      id2 = GP_GETREF(GP_SCM(id2));
	      goto u00;
	    }

	  goto u01;
	}

      gp_debug0("00>\n");     
      goto u00;
    }
  
  // u11 Has unbounded variables
  gp_debug0("unify> looked up with u11\n");
  if(GP_UNBOUND(id1))
    {
      if(id1 == id2)
	{
	  gp_debug0("unify> var == var");
	  U_NEXT;
	}
      else
	{
	  if(gp_plus_unify) 
	    goto  unbound1;
	  else
	    return 0;
	}
    }

  if(GP_UNBOUND(id2))
    {
      if(id1 == id2)
	{
	  gp_debug0("unify> var == var");
	  U_NEXT;
	}
      else
	{
	  if(gp_plus_unify)
	    goto  unbound2;
	  else
	    return 0;
	}
    }

  //any conses?
#define DO_CONS						\
  {							\
    gp_debug0("unify> cons\n");				\
    if(SCM_UNLIKELY(sp >= 18))				\
      {							\
	if(!gp_unify(GP_CAR(id1), GP_CAR(id2)		\
		     , raw, gp_plus_unify)) return 0;	\
	id1 = GP_CDR(id1);			     	\
	id2 = GP_CDR(id2);				\
	goto retry;					\
      }							\
    else						\
      {							\
	stack[sp++] = GP_CDR(id1);			\
	stack[sp++] = GP_CDR(id2);			\
	id1 = GP_CAR(id1);				\
	id2 = GP_CAR(id2);				\
	goto retry;					\
      }							\
  }

  //Cons check
  if(GP_CONS(id1))
    {
      if(GP_CONS(id2))
	{
	  DO_CONS;
	}
      else
	return 0;
    }

  if(GP_CONS(id1))
    {
      if(GP_CONS(id2))
	{
	  DO_CONS;
	}
      else
	return 0;
    }

  //has to be an equality check
  if(GP_EQ(id1) || GP_EQ(id2))
    {
      gp_debug2("unify> (eq ~x ~x)\n", GP_SCM(id1), GP_SCM(id2));
      if(scm_is_eq(GP_SCM(id1), GP_SCM(id2)))
	{U_NEXT;}
      else
	return 0;
    }
    
  gp_debug2("unify> (equal ~x ~x)\n", GP_SCM(id1), GP_SCM(id2));
  if(scm_is_true(scm_equal_p(GP_SCM(id1), GP_SCM(id2))))
    {U_NEXT;}
  else
    return 0;
 
 unbound1: 
  gp_debug0("unify> unbound1\n");
  if(!raw && GP_CONS(id2) && gp_recurent(id1,id2))  return 0;    
  gp_set_ref(id1,GP_UNREF(gp_lookup(id2)));
  U_NEXT;

 unbound2: 
  gp_debug0("unify> unbound2\n");
  if(!raw && GP_CONS(id1) && gp_recurent(id2,id1)) return 0;    
  gp_set_ref(id2,GP_UNREF(gp_lookup(id1)));
  U_NEXT;

 u00:
  gp_debug0("unify> looked up with u00\n");

  //conses!!
#define DO_CONS2		       			\
  {							\
    gp_debug0("unify> cons\n");				\
    if(SCM_UNLIKELY(sp >= 18))				\
      {							\
	if(!gp_unify(QCAR(id1),QCAR(id2)		\
		     ,raw, gp_plus_unify)) return 0;	\
	id1 = QCDR(id1);				\
	id2 = QCDR(id2);				\
	goto retry;					\
      }							\
    else						\
      {							\
	stack[sp++] = QCDR(id1);			\
	stack[sp++] = QCDR(id2);			\
	id1 = QCAR(id1);				\
	id2 = QCAR(id2);				\
	goto retry;					\
      }							\
  }


  if(QCONSP(id1))
    {
      if(QCONSP(id2))
	{
	  //printf("scm cons unify\n");
	  DO_CONS2;
	}
      else
	return 0;
    }

  if(QCONSP(id2)) return 0;

  if(scm_is_true(scm_equal_p(GP_UNREF(id1),GP_UNREF(id2))))
    {U_NEXT;}
  return 0;
  
 u01:
  {
    SCM *x;
    gp_debug0("unify> looked up with u01>\n");
    x = id1;
    id1 = id2;
    id2 = x;
  }

 u10:
  gp_debug0("unify> looked up with u10>\n");
  if(GP_UNBOUND(id1)) goto  unbound_10;

  //conses!!
#define DO_CONS3					\
  {							\
    gp_debug0("unify> cons\n");				\
    if(SCM_UNLIKELY(sp >= 18))				\
      {							\
	if(!gp_unify(GP_CAR(id1), QCAR(id2)			\
		     ,raw, gp_plus_unify)) return 0;		\
	id1 = GP_CDR(id1);					\
	id2 = QCDR(id2);				\
	goto retry;					\
      }							\
    else						\
      {							\
	stack[sp++] = GP_CDR(id1);			\
	stack[sp++] = QCDR(id2);			\
	id1 = GP_CAR(id1);				\
	id2 = QCAR(id2);				\
	goto retry;					\
      }							\
  }


  if(GP_CONS(id1))
    {
      if(QCONSP(id2))
	{
	  DO_CONS3;
	}
      else
	return 0;
    }
  if(QCONSP(id2)) return 0;

  
  if(GP_EQ(id1))
    {
      if(scm_is_eq(GP_SCM(id1),GP_UNREF(id2)))
	{U_NEXT;}
      else
	return 0;
    }
    
  if(scm_is_true(scm_equal_p(GP_SCM(id1),GP_UNREF(id2))))
    {U_NEXT;}
  else
    return 0;
 
 unbound_10: 
  gp_debug0("unify> unbound1\n");
  if(!raw && QCONSP(id2) && gp_recurent(id1,id2)) return 0;    
  if(GP(GP_UNREF(id2))) 
    scm_misc_error ("unify 01 / 10 error", "unify variable at 0 place", 
		    scm_list_1 (GP_UNREF(id2)));
  if(gp_plus_unify)
    {
      gp_set_val(id1,GP_UNREF(id2));
      U_NEXT;
    }
  return 0;
}




SCM_DEFINE(gp_gp_unify,"gp-unify!",2,0,0,(SCM v1, SCM v2),
	   "unifies two gp variables")
#define FUNC_NAME s_gp_gp_unify
{
  SCM *vv1, *vv2;
  
  vv1 = UN_GP(v1);
  vv2 = UN_GP(v2);
  return gp_unify(vv1,vv2,0,1) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_unify_raw,"gp-unify-raw!",2,0,0,(SCM v1, SCM v2),
	   "unifies two gp variables")
#define FUNC_NAME s_gp_gp_unify
{
  SCM *vv1, *vv2;
  
  vv1 = UN_GP(v1);
  vv2 = UN_GP(v2);
  return gp_unify(vv1,vv2,1,1) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(gp_swap_a, "gp-swap-to-a", 0, 0, 0, (),
	   "swap to A memory stack bank")
#define FUNC_NAME s_gp_swap_a
{
  swap_to_a();
  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_swap_b, "gp-swap-to-b", 0, 0, 0, (),
	   "swap to B memory stack bank")
#define FUNC_NAME s_gp_swap_b
{
  swap_to_b();
  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_next_budy, "gp-budy", 1, 0, 0, (SCM x),
	   "Assumes that v1 and v2 is allocated consecutively returns v2 when feeded by v1")
#define FUNC_NAME s_gp_next_budy
{
  SCM *id;
  if(!GP(x)) goto not_a_budy_error;
  id = UN_GP(x);
  return SCM_PACK(GP_BUDY(id));
  
 not_a_budy_error:
  scm_misc_error ("budy error", "cannot budy a non GP element", 
		  scm_list_1 (x));		\
  return SCM_BOOL_F;
}

#undef FUNC_NAME

SCM_DEFINE(gp_mkvar, "gp-var!", 0, 0, 0, (),
	   "makes a unbounded gp variable")
#define FUNC_NAME s_gp_mkvar
{
  return GP_IT(gp_mk_var());
}
#undef FUNC_NAME

SCM_DEFINE(gp_varp,"gp-var?",1,0,0,(SCM x),
	   "Test for an unbound variable.")
#define FUNC_NAME s_gp_varp
{  
  SCM *id;
  if(GP(x))
    {
      id = gp_lookup(UN_GP(x));
      return GP_UNBOUND(id) ? SCM_BOOL_T : SCM_BOOL_F;
    }
  return SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(gp_atomicp,"gp-atomic?",1,0,0,(SCM x),
	   "Test for a atomic gp")
#define FUNC_NAME s_gp_atomicp
{
  SCM *id;
  if(GP(x))
    {
      id = gp_lookup(UN_GP(x));
      return (GP_VAL(id) && !SCM_CONSP(GP_SCM(id)))
	? SCM_BOOL_T : SCM_BOOL_F;
    }
  return scm_is_pair(x) ? SCM_BOOL_F : SCM_BOOL_T;
}
#undef FUNC_NAME

static inline void gp_unwind0(SCM *ci, SCM *si);

SCM_DEFINE(gp_clear, "gp-clear", 0, 0, 0, (),
	   "resets the unifyier stacks")
#define FUNC_NAME s_gp_clear
{
  gp_unwind0(gp_cstack,gp_stack);
  return SCM_BOOL_T;
}
#undef FUNC_NAME



SCM_DEFINE(gp_gp, "gp?", 1, 0, 0, (SCM scm), "")
#define FUNC_NAME s_gp_gp
{
  return GP(scm) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME


static inline SCM ggp_set(SCM var, SCM val)
{
  SCM *id;
  if(GP(var))
    {
      id = GP_GETREF(var);
      id = gp_lookup(id);
      if(GP(val))
	{
	  gp_set_ref(id,gp_lookup(val));
	}
      else
	{
	  gp_set_val(id,val);
	}
      return SCM_BOOL_T;
    }
  return SCM_BOOL_F;
}

SCM_DEFINE (gp_gp_newframe, "gp-newframe",0,0,0,(),
	    "Created a prolog frame to backtrack from")
#define FUNC_NAME s_gp_gp_newframe
{
  return gp_newframe();
}
#undef FUNC_NAME


SCM_DEFINE(gp_set, "gp-set!", 2, 0, 0, (SCM var, SCM val), 
	   "set gp var var to val")
#define FUNC_NAME s_gp_set
{
  return ggp_set(var,val);
}
#undef FUNC_NAME

SCM_DEFINE(gp_print, "gp-print", 1, 0, 0, (SCM pr), 
	   "print a val")
#define FUNC_NAME s_gp_print
{
  if(GP(pr))
    printf("GP_SCM> %p,%p\n",
	   (void *) SCM_UNPACK(*GP_GETREF(pr)),
	   (void *) SCM_UNPACK(*(GP_GETREF(pr) + 1)));
  else
    printf("NO GP!\n");

  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_ref_set, "gp-ref-set!", 2, 0, 0, (SCM var, SCM val), 
	   "set gp var reference to val")
#define FUNC_NAME s_gp_ref_set
{
  SCM *id;
  if(GP(var))
    {
      if(GP(val))
	{
	  gp_set_ref(GP_GETREF(var),val);
	}
      else
	{
	  id = gp_lookup(GP_GETREF(var));
	  gp_set_val(id,val);
	}
      return SCM_BOOL_T;
    }
  return SCM_BOOL_F;
}
#undef FUNC_NAME

/* new api so that pure ffi will work */
SCM_DEFINE(gp_cons_bang, "gp-cons!", 2, 0, 0, (SCM car, SCM cdr), 
	   "crates a prolog variable cons pair")
#define FUNC_NAME s_gp_cons_bang
{
  SCM *cons;
  SCM *id;
  gp_debus0("gp-cons>\n");
  cons = GP_GETREF(gp_mk_cons());
  DS(smob2scm(car));
  DS(smob2scm(cdr));
  if(GP(car))    
    { 
      
      id = gp_lookup(GP_GETREF(car));      
      gp_set_ref(GP_CAR(cons),GP_UNREF(id));
    }
  else
    {
      gp_debus0("atom car>\n");
      gp_set_val(GP_CAR(cons),car);
    }

  if(GP(cdr))    
    { 
      id = gp_lookup(GP_GETREF(cdr));
      gp_set_ref(GP_CDR(cons),GP_UNREF(id));
    }
  else
    {
      gp_debus0("atom cdr>\n");
      gp_set_val(GP_CDR(cons),cdr);
    }

  return GP_UNREF(cons);
}
#undef FUNC_NAME



SCM_DEFINE(gp_pair_bang, "gp-pair!?", 1, 0, 0, (SCM x), 
	   "checks for a prolog or scheme pair and if a prolog variable creates a cons")
#define FUNC_NAME s_gp_pair_bang
{
  SCM * y;
  if(GP(x))
    {
      y = GP_GETREF(x);
      if(GP_UNBOUND(y))
	{
	  SCM *cons = GP_GETREF(gp_mk_cons());
	  gp_set_ref(y,GP_UNREF(cons));
	  return SCM_BOOL_T;
	}
      else
	{
	  if(GP_CONS(y))
	    {
	      return SCM_BOOL_T;
	    }
	}      
      
      return SCM_BOOL_F;
    }
  return SCM_CONSP(x) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(gp_pair, "gp-pair?", 1, 0, 0, (SCM x), 
	   "checks for a prolog pair or scheme pair")
#define FUNC_NAME s_gp_pair
{
  if(GP(x))
    {
      //printf("pair gp-addr %x, val %x\n",SCM_UNPACK(x),SCM_UNPACK(*GP_GETREF(x)));
      if(GP_CONS(GP_GETREF(x)))
	{
	  return SCM_BOOL_T;
	}
      return SCM_BOOL_F;
    }
  return SCM_CONSP(x) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_null, "gp-null?", 1, 0, 0, (SCM x), 
	   "checks for a prolog pair or scheme pair")
#define FUNC_NAME s_gp_null
{
  if(GP(x))
    {
      SCM *id = GP_GETREF(x);
      if(GP_VAL(id))
	{
	  return SCM_NULLP(GP_SCM(id)) ? SCM_BOOL_T : SCM_BOOL_F;
	}
      return SCM_BOOL_F;
    }
  return SCM_NULLP(x) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_null_bang, "gp-null!?", 1, 0, 0, (SCM x), 
	   "checks for a prolog pair or scheme pair")
#define FUNC_NAME s_gp_null_bang
{
  SCM * y;
  if(GP(x))
    {
      y = GP_GETREF(x);
      if (GP_UNBOUND(y))
	{
	  gp_set_eq(y,SCM_EOL);
	  return SCM_BOOL_T;
	}
      else 
	if(GP_VAL(y))
	  {
	    return SCM_NULLP(GP_SCM(y)) ? SCM_BOOL_T : SCM_BOOL_F;
	  }
      return SCM_BOOL_F;
    }
  return SCM_NULLP(x) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_lookup, "gp-lookup", 1, 0, 0, (SCM x), 
	   "lookup a  chain fro a prolog variable")
#define FUNC_NAME s_gp_gp_null_bang
{
  SCM * id;
  if(GP(x))
    {
      //printf("lookup> gp\n");
      gp_debug0("gp-lookup\n");
      id = gp_lookup(GP_GETREF(x));
      if(GP_UNBOUND(id) || GP_CONS(id))
	return GP_UNREF(id);
      else
	return GP_SCM(id);
    }
  //printf("lookup> scm\n");
  return x;
}

#undef FUNC_NAME

SCM_DEFINE(gp_m_unify, "gp-m-unify!", 2, 0, 0, (SCM x, SCM y), 
	   "checks for a prolog pair or scheme pair")
#define FUNC_NAME s_gp_m_unify
{
  int ret;
  //Todo this is a ugly hack.
  ret = gp_unify(GP_GETREF(x),GP_GETREF(y),1,0);
  return ret ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(gp_car, "gp-car", 1, 0, 0, (SCM x), 
	   "takes car a prolog pair or scheme pair")
#define FUNC_NAME s_gp_car
{
  //printf("gp-car>\n");
  if(GP(x))
    {
      return GP_UNREF(GP_CAR(GP_GETREF(x)));
    }
  else
    {
      return SCM_CAR(x);
    }
}
#undef FUNC_NAME


SCM_DEFINE(gp_gp_cdr, "gp-cdr", 1, 0, 0, (SCM x), 
	   "takes cdr a prolog pair or scheme pair")
#define FUNC_NAME s_gp_gp_cdr
{
  //printf("gp-cdr>\n");
  if(GP(x))
    {
      return GP_UNREF(GP_CDR(GP_GETREF(x)));
    }
  else
    return SCM_CDR(x);
}
#undef FUNC_NAME

SCM_DEFINE(gp_var_number, "gp-var-number", 1, 0, 0, (SCM x), 
	   "calculates var id number")
#define FUNC_NAME s_gp_gp_cdr
{
  if(GP(x))
    return SCM_I_MAKINUM((GP_GETREF(x) - gp_stack) / 2);
  else
    return SCM_I_MAKINUM(-1);
}
#undef FUNC_NAME

SCM gp_save_mark_sym;

#include "unify-undo-redo.c"

static SCM unify_env_mark(SCM w)
{ 
  int i;

  for(i=0;i < gp_ci - gp_cstack_a; i++)
    {
      scm_gc_mark(gp_cstack_a[i]);
    }  

  for(i=0;i < gp_ci - gp_cstack_b; i++)
    {
      scm_gc_mark(gp_cstack_b[i]);
    }

  for(i=0;i < gp_si_a - gp_stack_a; i+=2)
    {
      if(GP_VAL(&(gp_stack_a[i])))
	scm_gc_mark(gp_stack_a[i+1]);
    }  

  for(i=0;i < gp_si_b - gp_stack_b; i+=2)
    {
      if(GP_VAL(&(gp_stack_b[i])))
	scm_gc_mark(gp_stack_b[i+1]);
    }
  return SCM_BOOL_T;
}

static SCM gp_type_mark(SCM obj)
{
  scm_gc_mark(SCM_PACK( (scm_t_bits) SCM_SMOB_DATA (obj)));
  return SCM_BOOL_T;
}

SCM unify_env_smob;
scm_t_bits unify_env_smob_t;

SCM this_module;
static int gp_printer(SCM x, SCM port, scm_print_state *spec)
{
  scm_call_2 (scm_variable_ref  
	      (scm_c_module_lookup 
	       (this_module, "gp-printer")), 
	      port, x);
  return 0;
}

SCM_DEFINE(gp_soft_init, "gp-module-init", 0, 0, 0, (), 
	   "makes sure to record current module")
#define FUNC_NAME s_gp_soft_init
{

  this_module = scm_current_module ();
 
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_make_fluid, "gp-make-fluid", 0, 0, 0, (), 
	   "makes a gp fluid variable")
#define FUNC_NAME s_gp_make_fluid
{
  SCM ret;
  SCM_NEWSMOB(ret,GP_MK_FRAME_UNBD(gp_type),(void *)0);
  gp_set_unbound_bang(GP_GETREF(ret));
  return ret;
}
#undef FUNC_NAME

SCM_DEFINE(gp_fluid_set_bang, "gp-fluid-set!", 2, 0, 0, (SCM f, SCM v), 
	   "set! a gp fluid variable")
#define FUNC_NAME s_gp_fluid_set_bang
{
  SCM *id;
  if(!GP(f))
    scm_misc_error ("gp fluid error", "variable is not a fluid, ~a", 
		    scm_list_1 (f)); 
  
  id = gp_lookup(GP_GETREF(f));
  if(GP(v))
    gp_set_ref_bang(id,v);
  else
    gp_set_val_bang(id,v);

  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_dynwind, "gp-dynwind", 2, 0, 0, (SCM in, SCM out), 
	   "ad a dynwind to the action stack")
#define FUNC_NAME s_gp_dynwind
{
  SCM *id;
  gp_ci[0] = scm_cons(in,out);
  gp_ci ++;
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_copy,"gp-copy",1,0,0, (SCM x),
	   "make a fresh copy of a gp structure")
#define FUNC_NAME s_gp_copy
{
  if(scm_is_true(gp_pair(x)))
    return gp_cons_bang(gp_copy(gp_car(x)),gp_copy(gp_gp_cdr(x)));
  return gp_gp_lookup(x);
}
#undef FUNC_NAME

static void gp_init()
{  

  //play nice with GC 
  unify_env_smob_t = scm_make_smob_type ("gp-stack",0);
  scm_set_smob_mark(unify_env_smob_t, unify_env_mark);
  SCM_NEWSMOB(unify_env_smob, unify_env_smob_t, 0);
  
  /* stack initializations */
  swap_to_b();
  gp_si = gp_stack;
  gp_ci = gp_cstack;

  swap_to_a();
  gp_si = gp_stack;
  gp_ci = gp_cstack;

  
  gp_nns_a = gp_stack_a  + gp_ns - 10;
  gp_nnc_a = gp_cstack_a + gp_nc - 10;

  gp_nns = gp_nns_a;
  gp_nnc = gp_nnc_a;

  gp_nns_b = gp_stack_b  + gp_ns - 10;
  gp_nnc_b = gp_cstack_b + gp_nc - 10;

  gp_unbound_str = scm_from_locale_string ("<gp>");
  gp_save_mark_sym = scm_from_latin1_symbol("mark");
  gp_unbound_sym = scm_string_to_symbol (gp_unbound_str);
  gp_unbound_str = scm_from_locale_string ("gp-unwind-token");
  gp_unwind_fluid = scm_make_fluid();
  scm_c_define("gp-token-fluid", gp_unwind_fluid);

  gp_cons_str    = scm_from_locale_string ("<gp-cons>");
  gp_cons_sym    = scm_string_to_symbol (gp_cons_str);

  gp_type = scm_make_smob_type("unify-variable",0);
  scm_set_smob_print(gp_type,gp_printer);
  scm_set_smob_mark(gp_type,gp_type_mark);
} 


/*
  Local Variables:
  c-file-style: "gnu"
  End:
*/
