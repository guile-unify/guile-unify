#define GP_P_SIZE 90000000
#define GP_P_ENDS (GP_P_SIZE - 9)
gp_rstack_t  SCM_ALIGNED(8) gp_postpones[GP_P_SIZE];
gp_rstack_t *gp_p_pt_e   = gp_postpones + GP_P_ENDS;
gp_rstack_t *gp_p_pt     = gp_postpones + 1;
gp_rstack_t *gp_p_pt_end = gp_postpones + GP_P_ENDS;
gp_rstack_t *gp_p_frame  = gp_postpones;
gp_cstack_t *gp_p_next_id;
gp_cstack_t *gp_p_bottom;
gp_rstack_t *gp_p_nextbranch;
SCM          gp_p_token;
int          gp_p_postpone_p = 0;
int          gp_use_token_p  = 0;

#define token       gp_p_token
#define frame       gp_p_frame
#define pt_end      gp_p_pt_end
#define pt_e        gp_p_pt_e
#define next_id     gp_p_next_id
#define bottom      gp_p_bottom
#define nextbranch  gp_p_nextbranch

gp_rstack_t SCM_ALIGNED(8) gp_sestack[10000];
gp_rstack_t *se_pt;

#define P_UNREF(x)  SCM_PACK( (scm_t_bits) (x) | 2)
#define P_GETREF(x) ((gp_rstack_t *) (SCM_UNPACK(x) & ~2))

#define P_BASE(x) ((int) ((scm_t_bits) (x) - (scm_t_bits) gp_postpones))
#define S_BASE(x) ((int) ((scm_t_bits) (x) - (scm_t_bits) gp_sestack))
#define C_BASE(x) ((int) ((scm_t_bits) (x) - (scm_t_bits) gp_cstack))
#define BASE(x)   ((int) ((scm_t_bits) (x) - (scm_t_bits) gp_stack))

#define GP_TEST_P if( (gp_p_pt < pt_e) && (pt_e - gp_p_pt < 10)) \
scm_out_of_range(NULL, SCM_I_MAKINUM(GP_P_SIZE))

#define DB(X)
#define DDB(X)
#define DDS(X)
void gp_print_p_stack();
static inline void gp_p_clean_branch(gp_rstack_t *pt);

//Depreciated, use a move instead!!
static inline void gp_p_cleanup()
{
  DDS(printf("p cleanup\n"));
  pt_e = frame  - 4;
  if(pt_e < gp_postpones) 
    switch(gp_postpones - frame)
      {
      case 1:
	pt_e = pt_end;
      case 2:
	pt_e = pt_end - 1;
      case 3:
	pt_e = pt_end - 2;
      case 4:
	pt_e = pt_end - 3;
      }
}

static inline void prepare_for_move(gp_rstack_t *pt, scm_t_bits delta);
static inline void gp_p_cleanup_move(SCM start)
{
  gp_rstack_t *s,*e,*ee,*ep;
  scm_t_bits delta;
  DDS(printf("p cleanup move\n"));
  DDB(gp_print_p_stack());
  gp_debug0("p cleanup move\n");
  s  = P_GETREF(start) + 1;
  e  = frame           + 1;
  ee = gp_p_pt;
  ep = ee - 1;
  gp_debug1("p cleanup pt = %d\n",(scm_t_bits) gp_p_pt - (scm_t_bits) gp_postpones);

  //move the pointers inside of the tree
  delta = ((scm_t_bits) e - (scm_t_bits) s) / sizeof(gp_rstack_t);
  gp_debug1("p delta = %d\n",delta);
  //move all pointers inside delta
  prepare_for_move(ep,delta);

  //make sure that we move the frame , pt and nextbranch refs!
  frame       = s - 1;
  nextbranch -= delta;
  gp_p_pt    -= delta;

  
  gp_debug1("p cleanup pt = %d\n",(scm_t_bits) gp_p_pt - (scm_t_bits) gp_postpones);
  gp_debug0("p cleanup move move\n");
  //move all data
  for(;e != ee; s++,e++)   *s = *e;  
  DDB(gp_print_p_stack();)
}



//Fr1 nextbr1 next_id1 (*)
static inline void gp_p_newframe_simple()
{
  DDS(printf("p newframe simple\n"));
  GP_TEST_P;
  DDB(gp_print_p_stack());
  gp_p_clean_branch(gp_p_pt - 1);

  (gp_p_pt + 0)->jump      = frame;
  (gp_p_pt + 1)->jump      = nextbranch;
  (gp_p_pt + 2)->branch_id = next_id;
  (gp_p_pt + 3)->branch_id = bottom;
  (gp_p_pt + 4)->val       = SCM_PACK(2);
  (gp_p_pt + 5)->val       = SCM_PACK(1);
  frame = gp_p_pt + 5;
  gp_p_pt += 6;
  
  nextbranch = frame;
  next_id    = gp_cstack - 1;  
  bottom     = next_id;
}

//Fr1 nextbr1 next_id1 token (*)
static inline void gp_p_newframe_token()
{
  DDS(printf("p newframe\n"));
  GP_TEST_P;
  DDB(gp_print_p_stack());
  gp_p_clean_branch(gp_p_pt - 1);
  gp_debug0("p newframe cleaned\n");
  (gp_p_pt + 0)->jump      = frame;
  (gp_p_pt + 1)->jump      = nextbranch;
  (gp_p_pt + 2)->branch_id = next_id;
  (gp_p_pt + 3)->branch_id = bottom;
  (gp_p_pt + 4)->val       = token;
  (gp_p_pt + 5)->val       = 0;
  (gp_p_pt + 6)->val       = SCM_PACK(1);
  frame = gp_p_pt + 6;
  gp_p_pt += 7;
  
  nextbranch = frame;
  next_id    = gp_cstack - 1;  
  bottom     = next_id;
  token      = SCM_BOOL_F;
  
  gp_debug0("p newframe exit\n");
}

static inline void gp_p_newframe()
{
  DDS(printf("p newframe\n"));
  if(gp_use_token_p)    
    gp_p_newframe_token();
  else
    gp_p_newframe_simple();
}


static inline void gp_p_unwind_simple()
{
  DDS(printf("p unwind frame: %x\n", (scm_t_bits) frame));

  bottom     =  (frame-2)->branch_id;
  next_id    =  (frame-3)->branch_id;
  gp_debug0("ni ... ");
  nextbranch =  (frame-4)->jump;
  gp_debug0("nb ... ");
  gp_p_pt    =  frame - 5;
  gp_debug0("pt ... ");
  frame      =  (frame-5)->jump;
  gp_debug0("fr\n");  
}

static inline void gp_p_unwind_token()
{
  DDS(printf("p unwind frame: %x\n", (scm_t_bits) frame));

  token      =  (frame-2)->val;
  bottom     =  (frame-3)->branch_id;
  next_id    =  (frame-4)->branch_id;
  gp_debug0("ni ... ");
  nextbranch =  (frame-5)->jump;
  gp_debug0("nb ... ");
  gp_p_pt    =  frame - 6;
  gp_debug0("pt ... ");
  frame      =  (frame-6)->jump;
  gp_debug0("fr\n");  
}

static inline void gp_p_unwind()
{
  DDB(gp_print_p_stack());
  if(gp_use_token_p)
    gp_p_unwind_token();
  else
    gp_p_unwind_simple();
  DDB(gp_print_p_stack());
}

static inline void gp_p_unmark(SCM *id)
{
  SCM ref = *id;
  DDS(printf("p unmark\n"));
  *id       = SCM_PACK(SCM_UNPACK(*(id+0)) & (~0x100000));     
}
#define unmark gp_p_unmark


static inline int gp_p_marked(SCM *id)
{
  SCM ref = *id;
  int ret;
  ret = SCM_UNPACK(*(id+0)) & 0x100000;
  //gp_debug1("p marked %x\n",ret);
  return ret;

}
#define marked gp_p_marked


static inline void gp_p_mark(SCM *id)
{
  SCM ref = *id;
  *id       = SCM_PACK(SCM_UNPACK(*(id+0)) | 0x100000);     
}

#define mark gp_p_mark


static inline int gp_p_is_jump(gp_rstack_t *pt)
{
  int ret = 
    ( (((scm_t_bits) pt->jump) & 7) == 0)  && 
    ( pt->jump <= pt_end)                  && 
    ( pt->jump >= gp_postpones);

  //gp_debug1("p is jump %d\n",ret);
  return ret;    
}
#define is_jump gp_p_is_jump

static inline int gp_p_at_end(gp_rstack_t *pt)
{
  //gp_debug0("p at_end\n");
  return pt->val == SCM_PACK(1);
}
#define at_end gp_p_at_end



static inline int gp_p_id_at_branch(gp_cstack_t *id)
{
  return id == next_id;
}
#define id_at_branch gp_p_id_at_branch

static inline void gp_p_clean_branch(gp_rstack_t *pt)
{
  DDS(printf("p clean branch\n"));

 retry:
  while(!is_jump(pt) && !at_end(pt))
    {
      if(pt->val == SCM_PACK(3)) { pt--; continue; }
      pt -= 2;      
      unmark(pt->id);      
      pt--;
    }

  if(at_end(pt))
    {
      if(is_jump(pt - 1))
	{
	  pt = (pt - 1)->jump;
	  goto retry;
	}
    }
  else      
    {
      if(gp_use_token_p)
	{
	  if((pt - 1)->val == SCM_EOL)
	    {
	      pt = pt->jump - 1;
	      goto retry;
	    }
	  
	  if(pt->jump->val == SCM_EOL)
	    {
	      pt -= 2;
	      goto retry;
	    }
	}
    }
}

#define clean_branch gp_p_clean_branch

static inline void gp_p_store_branch()
{
  gp_rstack_t *pt;

  DDS(printf("p store branch\n"));
  GP_TEST_P;
  gp_debug1("p store check %d\n",(scm_t_bits) nextbranch & 0x7);

  DDB(gp_print_p_stack());

  pt = gp_p_pt - 1;
  clean_branch(pt);
  gp_debug0("cleaned>\n");
  if(gp_use_token_p)
    {
      gp_p_pt->val = token;
      gp_p_pt++;
      gp_p_pt->jump = nextbranch;
      gp_p_pt++;

      nextbranch = (nextbranch + 1)->jump;
      if(nextbranch == frame)
	next_id = gp_cstack - 1;
      else
	next_id  = (nextbranch + 2)->branch_id;
    }
  else
    {
      gp_p_pt->jump = nextbranch;
      gp_p_pt++;
      gp_debug1("nextbranch >%d\n",P_BASE(nextbranch));
      nextbranch = nextbranch->jump;
      gp_debug1("nextbranch >%d\n",P_BASE(nextbranch));
      if(nextbranch == frame)
	next_id = gp_cstack - 1;
      else
	next_id  = (nextbranch + 1)->branch_id;
    }

  DDB(gp_print_p_stack());
}

#define store_branch gp_p_store_branch



static inline void gp_set_frame(SCM *id)
{
  gp_debug1("set-frame id %d\n",(scm_t_bits) id - (scm_t_bits) gp_stack);
  *(id + 0) = SCM_PACK((gp_frame_id & ~0x3f0000) 
		       | (SCM_UNPACK(*(id + 0)) & 0x3f0000));
  gp_debug0("-val-\n");  
}


//jump branchid lam .
static inline void gp_p_postpone(SCM lam)
{
  DDS(printf("p postpone\n"));
  GP_TEST_P;

  DDB(gp_print_p_stack());
  (frame-1)->val = SCM_PACK(SCM_UNPACK((frame-1)->val) + 2);
  clean_branch(gp_p_pt - 1);
  if(gp_use_token_p) 
    {
      gp_p_pt->val = token;
      token = SCM_BOOL_F;
      
      (gp_p_pt + 1)->jump = nextbranch;  
      nextbranch          = gp_p_pt;
      gp_p_pt+=2;
    }
  else
    {
      gp_p_pt->jump      = nextbranch;  
      nextbranch         = gp_p_pt;
      gp_p_pt++;
    }
  
 
  gp_p_pt->branch_id = bottom; 
  next_id            = bottom;
  gp_p_pt ++;
  bottom        = gp_ci;
 
  gp_p_pt->val  = lam;
  gp_p_pt++;
  
  gp_p_pt->val  = SCM_PACK(1);
  gp_p_pt++;
}

static inline void gp_p_postpone_token(SCM lam, SCM tok)
{
  struct gp_p_statistic *stat;

  DDS(printf("p postpone token\n"));
  GP_TEST_P;
  

  DDB(gp_print_p_stack());
  
  stat = (struct gp_p_statistic *) (frame-1);

  //Add statistic: number of active postpones
  stat->n += 1;

  clean_branch(gp_p_pt - 1);

  gp_p_pt->val = token;
  token = tok;

  (gp_p_pt + 1)->jump = nextbranch;  
  nextbranch          = gp_p_pt;
  gp_p_pt+=2;
  
 
  gp_p_pt->branch_id = bottom; 
  next_id            = bottom;
  gp_p_pt ++;
  bottom        = gp_ci;
 
  gp_p_pt->val  = lam;
  gp_p_pt++;
  
  gp_p_pt->val  = SCM_PACK(1);
  gp_p_pt++;

  DDB(gp_print_p_stack());
}

void gp_print_p_stack()
{
  gp_rstack_t *pt, *fr;
  struct gp_p_statistic *stat;

  pt = gp_p_pt - 1;
  fr = frame;

 retry:
  if(fr<gp_postpones)  {printf("fr Error!\n");return;}
  if(pt<gp_postpones)  {printf("pt Error!\n");return;}
  while(!at_end(pt))
    {
      if(pt->val == SCM_PACK(3)) 
	{ 
	  printf("%d empty\n",P_BASE(pt));	  
	  pt--; continue; 
	}
      if(fr<gp_postpones)  {printf("fr Error!\n");return;}
      if(pt<gp_postpones)  {printf("pt Error!\n");return;}

      printf("%d",P_BASE(pt));
      if(is_jump(pt))
	{
	  printf(" jump to %d\n",P_BASE(pt->jump));
	  pt--;
	  if(gp_use_token_p)
	    {
	      printf("%d",P_BASE(pt));
	      if(pt->val == SCM_EOL)
		printf(" erased\n");
	      else
		printf(" token\n");
	      pt --;
	    }
	  continue;
	}
      printf(" val\n");
      pt--;
      printf("%d",P_BASE(pt));
      printf(" val\n");
      pt--;
      printf("%d",P_BASE(pt));
      if(gp_p_marked(pt->id))
	printf(" id = %d - marked\n",BASE(pt->id));
      else
	printf(" id = %d\n",BASE(pt->id));
      pt--;
    }
  printf("%d",P_BASE(pt));
  printf(" .\n");
  if(fr == pt)
    {
      if(fr < gp_postpones + 3) return;

      pt --;
      printf("%d",P_BASE(pt));
      stat = (struct gp_p_statistic *) pt;
      printf(" nlive = %d\n",stat->n   );
      printf(" nrem  = %d\n",stat->nrem);

      if(gp_use_token_p)
	{
	  pt --;
	  printf("%d",P_BASE(pt));
	  printf(" token\n");
	}
      pt --;
      printf("%d",P_BASE(pt));
      printf(" bottom = %d\n", C_BASE(pt->branch_id));
      pt --;
      printf("%d",P_BASE(pt));
      printf(" next_id = %d\n", C_BASE(pt->branch_id));
      pt --;
      printf("%d",P_BASE(pt));
      printf(" nextbranch = %d\n", P_BASE(pt->jump));
      pt --;
      printf("%d",P_BASE(pt));
      printf(" frame = %d\n", P_BASE(pt->jump));	
      fr = pt->jump;
      if(fr < gp_postpones) {printf("Error!\n");return;}
      pt--;
      goto retry;
    }
  else
    {
      pt--;
      printf("%d",P_BASE(pt));
      if(is_jump(pt))
	printf(" continue at %d\n",P_BASE(pt->jump));
      else
	printf(" closure\n");
      pt--;

      printf("%d",P_BASE(pt));
      printf(" nextid = %d\n",C_BASE(pt->branch_id));
      pt--;

      printf("%d",P_BASE(pt));
      printf(" rejump to %d\n",P_BASE(pt->jump));
      pt--;

      if(gp_use_token_p)
	{
	  printf("%d",P_BASE(pt));
	  printf(" token\n");
	  pt --;
	}
          
      goto retry;
    }
}

/* not tested yet
static void gp_gc_mark_rstack()
{
  gp_rstack_t *pt = gp_p_pt;
  while(pt > gp_postpones + 3)
    {
      while(!at_end(pt))
	{
	  if(pt->val == SCM_PACK(3)) {pt--; continue;}
	  if(is_jump(pt))
	    {
	      if(gp_use_token_p)
		{
		  pt--;
		  scm_gc_mark(pt->val);
		}
	      pt--;
	      continue;
	    }
	  
	  if(pt->val && !GP_POINTER((pt-1)->val))
	    {
	      scm_gc_mark(pt->val);
	    }
	  pt -= 3;
	}
      if(frame == pt)
	{
	  if(gp_use_token_p)
	    {
	      pt --;
	      scm_gc_mark(pt->val);	      
	    }
	  pt -= 5;
	}
      else
	{
	  pt--;
	  if(!is_jump(pt)) scm_gc_mark(pt->val);
	  if(gp_use_token_p)
	    pt -= 4;
	  else
	    pt -= 3;
	}
    }
}
*/

 /*
static void gp_p_s_print()
{
  scm_t_bits code;
  gp_rstack_t *pt;
  
  pt = se_pt;
 retry:
  code = SCM_UNPACK((pt-3)->val);
  if(code == 4)
    {
      printf("%d <start>\n",S_BASE(pt-3));
      return;
    }
  printf("%d (%d)",S_BASE(pt-3),(int) code);
  switch(code & 0x3)
    {
    case 0:
      printf(" 0 branch jump %d %d\n",P_BASE((pt-2)->jump),P_BASE((pt-1)->jump));
      break;
    case 1:
      printf(" 1 branch jump %d %d\n",P_BASE((pt-2)->jump),P_BASE((pt-1)->jump));
      break;
    case 2:
      printf(" 2 branches jumps %d %d\n"
	     ,P_BASE((pt-2)->jump),P_BASE((pt-1)->jump));
    }
  pt -= 3;
  goto retry;
}
 */

static void gp_p_search_unwind(SCM s)
{
  gp_rstack_t *i,*ii;
  SCM ltoken;
  scm_t_bits code;
  int remove;
  //struct gp_p_statistic *stat;

  DDS(printf("search unwind\n"));
  DB(gp_p_s_print());  

  i   = P_GETREF(s);
  ii  = se_pt - 1;

  ltoken = SCM_EOL;
  remove = 0;
  
  while((se_pt-3)->val  != SCM_PACK(4))
    {
      gp_debug1("se_pt %d ",S_BASE(se_pt - 3));
      gp_debug2("i %p, ii %p\n",(void *) i,(void *) ii);
      //if((scm_t_bits) ii == (scm_t_bits) i) goto out;

      code = SCM_UNPACK((se_pt-3)->val);
      gp_debug1("code %d\n",code);

      if(code & 0x30) 
	{
	  if(code & 0x10)
	    {
	      remove = 1;
	      se_pt -=3;
	    }
	  else
	    {
	      ltoken = (se_pt - 2)->val;
	      se_pt -=3;
	    }
	  continue;;
	}

      switch(code & 0x3)
	{
	  //No branch has been taken
	case 0:
	  {
	    //printf("0\n");
	    //Both of the paths are rejected, move up until we find the next subtree to walk into
	    remove = 0;
	  }
	  break;

	//One branch has been taken
	case 1:
	  {
	    //printf("1\n");
	    if(remove)
	      {
		//get new token
		ltoken = ((se_pt-2)->jump + 1)->val;
	      
		//inhibit branch
		((se_pt-1)->jump + 1)->val = SCM_EOL;
		remove = 0;
	      }
	    else
	      {
		if(ltoken != SCM_EOL)
		  {
		    //Set new token on first branch
		    ((se_pt - 1)->jump + 1)->val = ltoken;

		    //Calculate new token
		    ltoken = scm_call_2(scm_fluid_ref(gp_unwind_fluid),
					ltoken,((se_pt-2)->jump + 1)->val);
		  }
	      }
	  }
	  break;

	case 2:
	  {
	    gp_debug0("2 ... ");
	    if(remove)
	      {
		gp_debug0("remove\n");
		if(code & 0x8)
		  {
		    //inhibit branch
		    ((se_pt-2)->jump + 1)->val = SCM_EOL;
		  }
		else
		  {
		    ltoken = ((se_pt - 1)->jump + 1)->val;
		    ((se_pt  - 2)->jump + 1)->val = SCM_EOL;
		    remove = 0;
		  }
	      }
	    else
	      {
		if(code & 0x8)
		  {
		    gp_debug0("0x8\n");
		    if(ltoken == SCM_EOL)
		      ltoken = ((se_pt  - 2)->jump + 1)->val;
		  }
		else
		  {
		    if(ltoken != SCM_EOL)
		      {
			gp_debug0("!= EOL\n");
			((se_pt - 2)->jump + 1)->val = ltoken;
			
			ltoken = scm_call_2(scm_fluid_ref(gp_unwind_fluid),
					    ltoken,((se_pt-1)->jump + 1)->val);
			
		      }
		    else
		      {
			if(code & 0x4)
			  {
			    gp_debug0("0x4\n");
			    ltoken = scm_call_2(scm_fluid_ref(gp_unwind_fluid),
						((se_pt-1)->jump + 1)->val,
						((se_pt-2)->jump + 1)->val);
			  }
			else
			  {
			    gp_debug0("\n");
			  }
		      }
		  }
	      }
	  }
	} 
      se_pt -= 3;
    }
  
  if(ltoken != SCM_EOL)
    token = ltoken;
  DB(gp_p_s_print());
  }

static inline SCM gp_p_search_a_branch_and_kill(SCM s)
{
  SCM *id;
  scm_t_bits code;
  //Pack up integer masked reference
  gp_rstack_t temp,*i,*ii;
  //struct gp_p_statistic *stat;
  SCM ltoken;
  int remove = 0;

  i  = P_GETREF(s);
  ii = se_pt-1;

  ltoken = SCM_EOL;

  DB(gp_p_s_print());
  
  DDS(printf("p s branch and kill\n"));
  gp_debug2("i %d, ii %d\n",S_BASE(se_pt),S_BASE(ii));

  if((se_pt - 3)->val == SCM_PACK(4)) goto skip_se_pt;
 retry:
  
  if((se_pt - 3)->val == SCM_PACK(4)) 
    {
      printf("Error should not back off to start in the loop!");
      goto skip_se_pt;
    }

  code = SCM_UNPACK((se_pt-3)->val);
  gp_debug1("handle code %d\n",code);
  if(code & 0x30) 
    {
      if(code & 0x10)
	{
	  remove = 1;
	  se_pt -=3;	  
	}
      else
	{
	  ltoken = (se_pt - 2)->val;
	  se_pt -=3;
	}    
      goto retry;
    }
  
  switch(code & 0x3)
    {
      //No branch has been taken
    case 0:
      if((se_pt-2)->jump == i)
	{
	  //swap the order
	  temp = *(se_pt  - 1);
	  *(se_pt - 1) = *(se_pt - 2);
	  *(se_pt - 2) = temp;	    
	  (se_pt - 3)->val = SCM_PACK(1);
	}
      else
	{
	  if((se_pt - 1)->jump == i)
	    {
	      (se_pt - 3)->val = SCM_PACK(1);
	    }
	  else
	    {
	      //Both of the paths are rejected, move up until we find the next subtree to walk into
	      se_pt -= 3;
	      remove = 0;
	      goto retry;
	    }
	}
      break;

      //One branch has been taken
    case 1:
      gp_debug2("se_pt id: %d,  id = %d\n",P_BASE((se_pt - 2)->id),P_BASE(i));
      if((se_pt-2)->jump == i)
	{
	  gp_debug0("found id ... ");
	  //We dive into the second branch	  
	  if(remove)
	    {
	      gp_debug0("at remove\n");
	      //We come from a branch that is removed
	      ((se_pt - 1)->jump + 1)->val = SCM_EOL;
	      (se_pt - 3)->val = SCM_PACK(10);
	    }
	  else
	    {	      
	      if(ltoken == SCM_EOL)
		{
		  //token has not been touched
		  gp_debug0("token untouched\n");
		  (se_pt - 3)->val = SCM_PACK(2);
		}
	      else
		{
		  //token has been touched
		  gp_debug0("token touched\n");
		  ((se_pt - 1)->jump + 1)->val = ltoken;
		  (se_pt - 3)->val = SCM_PACK(6);
		}
	    }
	}
      else
	{
	  gp_debug0("transit ... ");
	  if(remove)
	    {
	      gp_debug0("remove\n");
	      //get new token
	      ltoken = ((se_pt-2)->jump + 1)->val;
	      
	      //inhibit branch
	      ((se_pt - 1)->jump + 1)->val = SCM_EOL;
	      se_pt -= 3;
	      remove = 0;
	      goto retry;
	    }
	  
	  if(ltoken == SCM_EOL)
	    {
	      gp_debug0("untouched token\n");
	      se_pt -= 3;
	      goto retry;
	    }

	  gp_debug0("touched token\n");
	  //Set new token on first branch
	  ((se_pt - 1)->jump + 1)->val = ltoken;

	  //Calculate new token
	  ltoken = scm_call_2(scm_fluid_ref(gp_unwind_fluid),
			      ltoken,((se_pt - 2)->jump + 1)->val);

	  se_pt -= 3;
	  goto retry;
	}
      break;

    case 2:
      {
	if(remove)
	  {
	    gp_debug0("2> remove\n");

	    ((se_pt - 2)->jump + 1)->val = SCM_EOL;

	    if(!(code & 0x8))
	      {
		ltoken = ((se_pt - 1)->jump + 1)->val;
		remove = 0;
	      }

	    se_pt -= 3;		
	    goto retry;
	  }

	if(code & 0x8)
	  {
	    gp_debug0("2> 0x8\n");
	    if(ltoken == SCM_EOL)
	      {
		ltoken = ((se_pt - 2)->jump + 1)->val;
	      }
	  }
	else	
	  {
	    if(ltoken != SCM_EOL)
	      {
		gp_debug0("2> != SCM_EOL\n");
		((se_pt - 2)->jump + 1)->val = ltoken;

		ltoken = scm_call_2(scm_fluid_ref(gp_unwind_fluid),
				    ltoken,((se_pt-1)->jump + 1)->val);
	      }
	    else
	      if(code & 0x4)
		{
		  gp_debug0("2> 0x4\n");
		  ltoken = scm_call_2(scm_fluid_ref(gp_unwind_fluid),
				      ((se_pt-1)->jump + 1)->val,
				      ((se_pt-2)->jump + 1)->val);
		}
	  }

	se_pt -= 3;
	goto retry;
      }
    }
  
 skip_se_pt:
  gp_debug1("p search i = %d\n",(scm_t_bits) i - (scm_t_bits) gp_postpones);
  
  gp_debug1("p search spt = %d\n",S_BASE(se_pt));

 rewhile:
  //walk up until branch mark  
  if(S_BASE(se_pt) == 0) return SCM_EOL;
  while(!at_end(i))
    {
      if(i->val == SCM_PACK(3)) { i--; continue; }
      //at a jump we need to take two branches returned in a cons
      if(is_jump(i))
	{
	  gp_rstack_t *car,*cdr;
	  gp_debug0("p search jump\n");	  
	  if(gp_use_token_p)
	    {
	      //Check for deleted branches!
	      if(i->jump->val == SCM_EOL)
		{
		  i -= 2;
		  continue;
		}

	      if((i-1)->val == SCM_EOL)
		{
		  i = i->jump - 1;
		  continue;
		}
		    
	      car = i->jump - 1;	  	      
	      cdr = i - 2;
	      (se_pt + 0)->val  = SCM_PACK(0);
	      (se_pt + 1)->jump = car;
	      (se_pt + 2)->jump = cdr;
	      se_pt += 3;

	      return scm_cons(scm_cons((car + 1)->val,P_UNREF(car))
			      ,scm_cons((cdr + 1)->val,P_UNREF(cdr)));
	    }
	  else
	    {
	      car = i->jump - 1;
	      cdr = i - 1;
	    }
	  return scm_cons(P_UNREF(car),P_UNREF(cdr));
	}

      //We are in a redo mode redo and set frame value
      id        = (i - 2)->id ;
      *(id + 0) = (i - 1)->val;
      *(id + 1) = (i - 0)->val;
      i -= 3;
      gp_debug1("will revert id %d\n", (scm_t_bits) id - (scm_t_bits) gp_stack);
      
      if(id >= gp_si) gp_si = id + 2;
  
      GP_TEST_CSTACK;
      (gp_ci+0)->id  = id;
      (gp_ci+1)->val = *(id + 0);
      (gp_ci+2)->val = *(id + 1);

      gp_ci +=3;

      gp_set_frame(id);
    }
  gp_debug0("p search at lambda\n");
  //Check to see if we are at end
  
  if(i == gp_p_frame+1) return SCM_EOL;
  

  if(SCM_UNPACK((i-1)->val) == 0)
    {
      printf("Error at a deleted branch, should never be here!\n");      
    }
  
  //Check to see if we have a rejump!
  if(is_jump(i-1))
    {
      i = (i - 1)->jump;
      goto rewhile;
    }

  //Fire up some data on the se stack to unroll
  (se_pt + 0)->val  = SCM_PACK(0);
  (se_pt + 1)->val  = SCM_BOOL_F;
  (se_pt + 2)->jump = i - 1;
  se_pt += 3;

  //Give back the lambda closure!!
  return (i-1)->val;
}

static inline SCM gp_p_search_a_branch(SCM s)
{
  SCM *id;
  gp_rstack_t *i;

  i  = P_GETREF(s);

  //Pack up integer masked reference
  DDS(printf("p search i = %d\n",(scm_t_bits) i - (scm_t_bits) gp_postpones));  

 rewhile:
  //walk up until branch mark
  while(!at_end(i))
    {
      if(i->val == SCM_PACK(3)) { i--; continue; }
      //at a jump we need to take two branches returned in a cons
      if(is_jump(i))
	{
	  gp_rstack_t *car,*cdr;
	  gp_debug0("p search jump\n");	  
	  if(gp_use_token_p)
	    {
	      if(i->jump->val == SCM_EOL)
		{
		  i += 2;
		  continue;
		}

	      if((i+1)->val == SCM_EOL)
		{
		  i = i->jump - 1;
		  continue;
		}

	      car = i->jump - 1;	  	      
	      cdr = i - 2;
	      return scm_cons(scm_cons((car + 2)->val,P_UNREF(car))
			     ,scm_cons((cdr + 1)->val,P_UNREF(cdr)));
	    }
	  else
	    {
	      car = i->jump - 1;
	      cdr = i - 1;
	    }
	  return scm_cons(P_UNREF(car),P_UNREF(cdr));
	}

      //We are in a redo mode redo and set frame value
      id        = (i - 2)->id ;
      *(id + 0) = (i - 1)->val;
      *(id + 1) = (i - 0)->val;
      i -= 3;
      gp_debug1("will revert id %d\n", (scm_t_bits) id - (scm_t_bits) gp_stack);

      if(id >= gp_si) gp_si = id + 2;
  
      GP_TEST_CSTACK;
      (gp_ci+0)->id  = id;
      (gp_ci+1)->val = *(id + 0);
      (gp_ci+2)->val = *(id + 1);

      gp_ci +=3;

      gp_set_frame(id);
    }
  gp_debug0("p search at lambda\n");
  //Check to see if we are at end
  
  if(i == gp_p_frame+1) return SCM_EOL;

  if(is_jump(i-1))
    {
      i = (i - 1)->jump;
      goto rewhile;
    }
  
  //Give back the lambda closure!!
  return (i-1)->val;
}

static inline void prepare_for_move(gp_rstack_t *pt, scm_t_bits delta)
{
  gp_rstack_t *j;
  DDS(printf("p cleanup move prepare\n"));
 retry:
  if(!at_end(pt))
    {
      if(pt->val == SCM_PACK(3)) { pt--; goto retry; }
      if(is_jump(pt))
	{
	  j = pt->jump;
	  if(j >= frame)
	    {
	      pt->jump -= delta;
	    }

	  if(gp_use_token_p)
	    pt -= 2;
	  else
	    pt--;

	  goto retry;
	}
      pt -= 3; 
      goto retry;
    } 
  if(pt == frame) return;
  // ... rejump (token) id lambda . 
  if(gp_use_token_p)
    {
      if(is_jump(pt - 1))
	(pt-1)->jump -= delta;
      pt -= 4;
    }
  else
    pt -= 3;
  if((pt+1)->jump >= frame)
    pt->jump -= delta;
  pt --;
  goto retry;
}

// frame nextbranch next_id bottom (token) .
// rejump (token) branch_id lambda .
static inline void gp_p_mtp()
{
  struct gp_p_statistic *stat,s;
  DDS(printf("p mtp>\n"));
  DDB(gp_print_p_stack());
  DB(gp_p_s_print());
  if(gp_use_token_p)
    {
      SCM tok_left;
      gp_rstack_t *pt, *fr;


      if(gp_p_pt != frame + 1)
	gp_p_clean_branch(gp_p_pt - 1);

      //unwind frame data.
      s          = * (struct gp_p_statistic *) (frame-1);
      tok_left   = (frame-2)->val;
      bottom     = (frame-3)->branch_id;
      next_id    = (frame-4)->branch_id;
      nextbranch = (frame-5)->jump;
      pt         = (frame-6);
      fr         = frame;
      frame      = (frame-6)->jump;

      stat = (struct gp_p_statistic *) (frame - 1);

      if(gp_p_pt == fr + 1)
	{
	  //mark remove!
	  (se_pt - 1)->jump->val = SCM_PACK(0);
	  (se_pt - 3)->val = SCM_PACK(0x10);
	  gp_p_pt    =  pt;	  
	  
	  stat->nrem ++;
	}
      else
	{
	  //clear data
	  (pt + 0)->val  = SCM_PACK(3);
	  (pt + 1)->val  = SCM_PACK(3);
	  (pt + 2)->val  = SCM_PACK(3);
	  (pt + 3)->val  = SCM_PACK(3);
	  (pt + 4)->val  = SCM_PACK(3);
	  (pt + 5)->val  = SCM_PACK(3);
	  (pt + 6)->val  = SCM_PACK(3);

	  stat->n    += s.n;
	  stat->nrem += s.nrem + 1;	            

	  //Link structure to subtree
	  (se_pt - 3)->val        = SCM_PACK(0x20);
	  (se_pt - 1)->jump->jump = gp_p_pt - 1;
	  (se_pt - 2)->val        = token;
	}

      token = tok_left;
    }
  else
    {
      //TODO this path is unsuported, needs some love,
      gp_rstack_t *pt;

      //unwind frame data.      
      bottom     = (frame-1)->branch_id;
      next_id    = (frame-2)->branch_id;
      nextbranch = (frame-3)->jump;
      pt         = (frame-4);
      frame      = (frame-4)->jump;

      //clear data
      (pt + 0)->val  = SCM_PACK(3);
      (pt + 1)->val  = SCM_PACK(3);
      (pt + 2)->val  = SCM_PACK(3);
      (pt + 3)->val  = SCM_PACK(3);
      (pt + 4)->val  = SCM_PACK(3);
      
      //combine in a branch
      if(nextbranch != frame)
	store_branch();
    }
  DB(gp_p_s_print());
  DDB(gp_print_p_stack());
  gp_debug0("mtp> END\n");
}

SCM_DEFINE(gp_p_ppf,"gp-postpones-to-prev-frame",0,0,0,(),
	   "")
#define FUNC_NAME s_gp_p_ppf
{
  DDB(gp_print_p_stack());
  gp_p_mtp();
  DDB(gp_print_p_stack());
  return SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(gp_p_search,"gp-search-branch",1,0,0,(SCM s),
"search for a postpone continuation, returns\n(X , Y)  -  A branch\n()       -  No more branches\nLam      -  A continuation closure")
#define FUNC_NAME s_gp_p_search
{
  return gp_p_search_a_branch(s);
}
#undef FUNC_NAME

SCM_DEFINE(gp_p_search_kill,"gp-search-branch-kill",1,0,0,(SCM s),
"search for a postpone continuation, returns\n(X , Y)  -  A branch\n()       -  No more branches\nLam      -  A continuation closure")
#define FUNC_NAME s_gp_p_search_kill
{
  return gp_p_search_a_branch_and_kill(s);
}
#undef FUNC_NAME

SCM_DEFINE(gp_postpone_start,"gp-postpone-start",0,0,0,(),
	   "return start for redo")
#define FUNC_NAME s_gp_postpone_start
{
  return P_UNREF(gp_p_pt - 1);
}
#undef FUNC_NAME

SCM_DEFINE(gp_postpone_search_ptr,"gp-postpone-search-ptr",0,0,0,(),
	   "return search path ptr")
#define FUNC_NAME s_gp_postpone_search_ptr
{
  return P_UNREF(se_pt - 1);
}
#undef FUNC_NAME

SCM_DEFINE(gp_postpone_search_unwind,"gp-postpone-search-unwind",1,0,0,(SCM s),
	   "return search path ptr")
#define FUNC_NAME s_gp_postpone_search_unwind
{
  gp_p_search_unwind(s);
  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_postpone_frame,"gp-postpone-frame",0,0,0,(),
	   "return frame for redo")
#define FUNC_NAME s_gp_postpone_frame
{
    return P_UNREF(frame);
}
#undef FUNC_NAME

SCM_DEFINE(gp_postpone_newframe,"gp-postpone-newframe",0,0,0,(),
	   "prepare for a new postpone frame")
#define FUNC_NAME s_gp_postpone_frame
{
  gp_p_newframe();
  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_postpone_unwind,"gp-postpone-unwind",0,0,0,(),
	   "cleans up a postpone frame")
#define FUNC_NAME s_gp_postpone_frame
{
  gp_p_unwind();
  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_postpone,"gp-postpone",1,0,0,(SCM lam),
"Push closure lam onto a redo tre for later execution at the same \nstate the closure was made.")
#define FUNC_NAME s_gp_postpone
{
  gp_p_postpone(lam);
  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_postpone_token,"gp-postpone-token",2,0,0,(SCM lam, SCM tok),
"Push closure lam onto a redo tre for later execution at the same \nstate the closure was made.")
#define FUNC_NAME s_gp_postpone_token
{
  gp_p_postpone_token(lam,tok);
  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_set_postpone,"gp-set-postpone!",0,0,0,(),
	   "use the postpone branch of unwind to build redo tree")
#define FUNC_NAME s_gp_set_postpone
{
 gp_p_postpone_p = 1;
 return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_unset_postpone,"gp-unset-postpone!",0,0,0,(),
	   "use the standard branch of unwind to not build redo tree")
#define FUNC_NAME s_gp_set_postpone
{
 gp_p_postpone_p = 0;
 return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_use_token,"gp-use-token!",0,0,0,(),
	   "use the postpone branch of unwind to build redo tree")
#define FUNC_NAME s_gp_use_token
{
 gp_use_token_p = 1;
 return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_get_unwind_fluid,"gp-unwind-fluid",0,0,0,(),
  "")
#define FUNC_NAME s_gp_get_unwind_fluid
{
  return gp_unwind_fluid;
}
#undef FUNC_NAME

SCM_DEFINE(gp_get_token,"gp-get-token",0,0,0,(),
  "")
#define FUNC_NAME s_gp_get_token
{
  return token;
}
#undef FUNC_NAME

SCM_DEFINE(gp_use_no_token,"gp-use-no-token!",0,0,0,(),
	   "use the standard branch of unwind to not build redo tree")
#define FUNC_NAME s_gp_use_no_token
{
 gp_use_token_p = 0;
 return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_p_cleanup_move,"gp-cleanup-move",1,0,0,(SCM start),
	   "moves current frame one step")
#define FUNC_NAME s_gp_gp_p_cleanup_move
{
  gp_p_cleanup_move(start);
  return SCM_BOOL_T;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_p_cleanup,"gp-cleanup",0,0,0,(),
	   "frees the beginning of the array, a common idiom needed to compact array space")
#define FUNC_NAME s_gp_gp_p_cleanu
{
  gp_p_cleanup();
  return SCM_BOOL_T;
}
#undef FUNC_NAME


SCM_DEFINE(gp_has_postpones,"gp-has-postpones?",0,0,0,(),
	   "check to see if we postponed stuff")
#define FUNC_NAME s_gp_has_postpones
{
  struct gp_p_statistic *stat = (struct gp_p_statistic *) (frame - 1);
  int r = (frame + 1 == gp_p_pt);
  gp_debug1("has not postpones %d ",r);
  gp_debug2("n = %d    nrem=%d\n",stat->n,stat->nrem);
  return  (r || stat->n == stat->nrem) ? SCM_BOOL_F : SCM_BOOL_T;
}
#undef  FUNC_NAME


#define ppt gp_p_pt



static inline SCM gp_unwind_simple(SCM fr)
{
  gp_cstack_t *ci_old;
  gp_cstack_t *i;   
  scm_t_bits ffr;
  ffr = SCM_TO_FI(fr);
  DDS(printf("unwind> %x\n",ffr));
  if(SCM_UNLIKELY( (scm_t_bits) ffr >=gp_frame_id))
    {
      gp_debug2("unwind> got the same fi : %x >= %x\n",ffr,gp_frame_id);
      return SCM_BOOL_F;
    }
    
  gp_frame_id = ffr;
  gp_wi       = FR_TO_WI(gp_frame_id);
  
  DDS(printf("unwind> got fr = %x amd wi = %x\n",gp_frame_id,gp_wi);fflush(stdout));
  
  ci_old = gp_ci;
  
  gp_ci =(gp_wi+0)-> ci;
  gp_si =(gp_wi+1)-> si;

  /*
    the postpone algorithm:
    We store a tree containing the redo information and make use of a simple 
    and lightweight compression scheme that at worst produces 2x the optimum 
    amount of variable recalculations.

    We make an array for storing the redo data and it typically looks like
    [0 lam1 . r r r 1.0 id1 lam2 . r r 2.1 id2 lam3 . r 3 (2) r r (1) r r r]
    
    r   r   
    r   r   r
    r   2---3
    r     r
    1---- r
      r
      r
      r

    r         = redo atom
    1,2,3...  = branch id
    (1),...   = pointer to branch id start
    .         = branch separator
    
    After constructed a generation of a redo tree it is traversed starting from 
    the right redoing, then replace variable data until a pointer or end is
    reached. at such a place we first follow the pointer and then continue to 
    the left until we backtrack and then we continue.

    For compression. As we unwind a marked variable has it's frame number set 
    to zero and this will serve as a mark that the variable has been stored 
    for a redo already. When we unwind and have reached just below a branch 
    point we need to make sure to unmark all variables that has been marked 
    further out on the limb. This needs to be done up to secondary branch points
    which already have been cleared. The reason is that we will miss redo 
    information else and the fix in turn for that case is complex. This means 
    that below a branch point every variable that is included somewhere further 
    out on the limb is not marked and will be included the next time it appear 
    hence the risk of doing multiple redos for a variable. But this multiplicity 
    is pretty nice. For no branch we have perfect compression and the more 
    branches we have the less possibilities of duplication in %. Hence the worst 
    case is a binary tree which in turn calculates to at most 2x redo atoms in 
    the tree compared to optimum.
   */

  if(gp_p_postpone_p)
    {
      DDS(printf("postpone algorithm! var-id = %d\n", (scm_t_bits) gp_si - (scm_t_bits) gp_stack));
      DDS(printf("postpone next_id = %d, cur_id = %d!\n"
		,(scm_t_bits) next_id - (scm_t_bits) gp_cstack
		 ,(scm_t_bits) gp_ci   - (scm_t_bits) gp_cstack));
      for(i = ci_old-3; i >= gp_ci; i-=3)
	{
	  SCM *id;	  
	  id      = (i+0)->id;	  
	  
	  //gp_debug1("i == %d\n", (scm_t_bits) i  - (scm_t_bits) gp_cstack);
	  //Tree comperssion maximum 2x inneficiency	 
	  if((scm_t_bits) i < (scm_t_bits) bottom && !marked(id))
	    {
	      GP_TEST_P;
	      (ppt + 0)->id  = id;
	      gp_debug1("will mark and store id %d\n",(scm_t_bits) id - (scm_t_bits) gp_stack);
	      (ppt + 1)->val = *(id + 0);
	      (ppt + 2)->val = *(id + 1);
	      ppt += 3;
	    
	      *(id+0) = (i+1)->val;
	      *(id+1) = (i+2)->val;	      	 
	      
	      mark(id);
	    }
	  else
	    {
	      if((scm_t_bits) i < (scm_t_bits) bottom)
		gp_debug2("already marked id = %d bottom = %d\n"
			  ,(scm_t_bits) id - (scm_t_bits) gp_stack
	                  ,(scm_t_bits) i  - (scm_t_bits) bottom  );
	      if(marked(id))
		{
		  *(id+0) = (i+1)->val;
		  *(id+1) = (i+2)->val;	      	 
		  mark(id);
		}
	      else
		{
		  *(id+0) = (i+1)->val;
		  *(id+1) = (i+2)->val;	      	 
		}
	    }

	  if(id_at_branch(i))
	    {
	      SCM left_token = (nextbranch)->val;
	      store_branch();
	      if(gp_use_token_p)
		{
		  token = scm_call_2(scm_fluid_ref(gp_unwind_fluid),
				     left_token,token);
		}
	    }
	}
      bottom = gp_ci<bottom ? gp_ci : bottom;
      DDS(printf("postpone bottom = %d\n",(scm_t_bits) bottom - (scm_t_bits) gp_cstack));
    }
  else
    {
      for(i = ci_old-3; i >= gp_ci; i-=3)
	{
	  SCM *id;	  
	  id      = (i+0)->id;
	  *(id+0) = (i+1)->val;
	  *(id+1) = (i+2)->val;
	}
    }
  
  return SCM_BOOL_T;
}

static inline SCM gp_unwind(SCM fr)
{
  if(SCM_PACK(2) == fr) return SCM_BOOL_T; //?
  gp_unwind_simple(fr);
  return SCM_BOOL_T;
}

#define DB(X)
#undef frame
#undef pt_e
#undef pt_end
#undef ppt
#undef next_id
#undef bottom
#undef nextbranch
#undef mark
#undef unmark
#undef marked
#undef id_at_branch
#undef store_branch
#undef is_jump
#undef at_end
