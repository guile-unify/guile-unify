#include <libguile.h>
#include <libguile/programs.h>
#define OBJECT_REF(i)		objects[i]
#define OBJECT_SET(i,o)		objects[i] = o

/* For the variable operations, we _must_ obviously avoid function calls to
   `scm_variable_ref ()', `scm_variable_bound_p ()' and friends which do
   nothing more than the corresponding macros.  */

#define VARIABLE_REF(v)		SCM_VARIABLE_REF (v)
#define VARIABLE_SET(v,o)	SCM_VARIABLE_SET (v, o)
#define VARIABLE_BOUNDP(v)      (VARIABLE_REF (v) != SCM_UNDEFINED)

#define FREE_VARIABLE_REF(i)	free[i]

#define TOPLEVEL_REF(objnum) scm_variable_ref(OBJECT_REF(objnum))

#define TOPLEVEL_SET(objnum)						\
{									\
  SCM what;								\
  what = OBJECT_REF (objnum);						\
									\
  if (!SCM_VARIABLEP (what))						\
    {									\
      SYNC_BEFORE_GC ();						\
      what = resolve_variable (what, scm_program_module (program));	\
      OBJECT_SET (objnum, what);					\
    }									\
									\
  VARIABLE_SET (what, *sp);						\
  sp--;									\
}

#define FUNC2(CFUNC,SFUNC)					\
  {								\
  SCM x, y;							\
  sp--;								\
  x = sp[0];							\
  y = sp[1];							\
  if(SCM_LIKELY(SCM_I_INUMP (x) && SCM_I_INUMP (y)))		\
    {								\
      scm_t_int64 n = SCM_I_INUM (x) CFUNC SCM_I_INUM (y);	\
      if (SCM_LIKELY(SCM_FIXABLE (n)))				\
	*sp = (SCM_I_MAKINUM (n));				\
    }								\
  else								\
    *sp = (SFUNC (x, y));					\
}

#define SUB1						\
{							\
  SCM x = sp[0];					\
  if (SCM_LIKELY(SCM_I_INUMP (x)))			\
    {							\
      scm_t_int64 n = SCM_I_INUM (x) - 1;		\
      if(SCM_LIKELY(SCM_FIXABLE (n)))			\
	*sp =  (SCM_I_MAKINUM (n));			\
    }							\
  else							\
    *sp = (scm_difference (x, SCM_I_MAKINUM (1)));	\
}

#define ADD1					\
{						\
  SCM x = *sp;					\
  if(SCM_LIKELY(SCM_I_INUMP (x)))		\
    {						\
      scm_t_int64 n = SCM_I_INUM (x) + 1;	\
      if (SCM_FIXABLE (n))			\
         *sp =  (SCM_I_MAKINUM (n));		\
    }						\
  else						\
    *sp = (scm_sum (x, SCM_I_MAKINUM (1)));	\
}

#define ASH								\
{									\
  sp--;									\
  SCM x = sp[0];							\
  SCM y = sp[1];							\
  if (SCM_LIKELY(SCM_I_INUMP (x) && SCM_I_INUMP (y)))			\
    {									\
      if (SCM_I_INUM (y) < 0)						\
        /* Right shift, will be a fixnum. */				\
        *sp =  (SCM_I_MAKINUM (SCM_I_INUM (x) >> -SCM_I_INUM (y)));	\
      else								\
        /* Left shift. See comments in scm_ash. */			\
        {								\
          long nn, bits_to_shift;					\
									\
          nn = SCM_I_INUM (x);						\
          bits_to_shift = SCM_I_INUM (y);				\
									\
          if (bits_to_shift < SCM_I_FIXNUM_BIT-1			\
              && ((unsigned long)					\
                  (SCM_SRS (nn, (SCM_I_FIXNUM_BIT-1 - bits_to_shift)) + 1) \
                  <= 1))						\
            *sp =  (SCM_I_MAKINUM (nn << bits_to_shift));		\
          /* fall through */						\
        }								\
      /* fall through */						\
    }									\
  else									\
    *sp = (scm_ash (x, y));						\
}

#define REL(crel,srel)						 \
{								 \
  SCM x,y;                                                       \
  sp--;x = sp[0]; y=sp[1];				         \
  if (SCM_LIKELY(SCM_I_INUMP (x) && SCM_I_INUMP (y)))		 \
    *sp = (scm_from_bool (SCM_I_INUM (x) crel SCM_I_INUM (y)));  \
  else								 \
    *sp = (srel (x, y));					 \
}

#define RETURN      				\
{						\
  fp[0]  = *sp;					\
  fp[1]  = 2;					\
  *spp   = fp+1;				\
  return;					\
}

#define MAKE_FIX(i)						\
{								\
 SCM x;								\
 size_t n,len;							\
 x   = LOCAL_REF(i);						\
 len = SCM_PROGRAM_NUM_FREE_VARIABLES (x);			\
 for (n = 0; n < len; n++)					\
     SCM_PROGRAM_FREE_VARIABLE_SET (x, n, sp[-len + 1 + n]);	\
 sp -= len;     						\
}

#define SCM_COMP_STR(op)			\
{						\
  sp--;						\
  if(scm_is_eq(sp[0],sp[1]))			\
    *sp = SCM_BOOL_T;				\
  else if (SCM_IMP(sp[0]) || SCM_IMP(sp[1]))	\
    *sp = SCM_BOOL_F;				\
  else						\
    {						\
      *sp = op(sp[0],sp[1]);			\
    }						\
}

/* move function definition to the beginning put a marker and return */
#define TAIL_CALL(nargs)			\
{						\
  int i;					\
  for(i=-1;i<nargs;i++)				\
    fp[i] = sp[-nargs + 1 + i];			\
  fp[nargs] = nargs;				\
  *spp = fp + nargs;				\
  return;					\
}

#define MAKE_CLOSURE(len)						\
{									\
  size_t n;								\
  SCM closure;								\
									\
  SYNC_BEFORE_GC ();							\
  closure = scm_words (scm_tc7_program | (len<<16), len + 3);		\
  SCM_SET_CELL_OBJECT_1 (closure, SCM_PROGRAM_OBJCODE  (sp[-len]));	\
  SCM_SET_CELL_OBJECT_2 (closure, SCM_PROGRAM_OBJTABLE (sp[-len]));	\
  sp[-len] = closure;							\
  for (n = 0; n < len; n++)						\
    SCM_PROGRAM_FREE_VARIABLE_SET (closure, n, sp[-len + 1 + n]);	\
  sp -= len;								\
}

#define CONS(to,x,y) ((to) = scm_cons(x,y))
