(define-module (ice-9 syntax-matcher)
  #:use-module (ice-9 match-phd-lookup)
  #:export     (match-syntax-hack))


(define (syntax-null?  x  ) (null? (syntax->datum-list x)))
(define (syntax-equal? x y) (equal? (syntax->datum-list x) y))
(define (syntax-id x)
  (define (syntax? x) (not (eq? x (syntax->datum-list x))))
  (if (pair? x) 
      x
      (let ((r (syntax->datum-list x)))
        (if (pair? r)
            (let ((h (if (syntax? (car r)) (car r) (datum->syntax x (car r))))
                  (l (if (syntax? (cdr r)) (cdr r) (datum->syntax x (cdr r)))))
              (cons h l))
            x))))

          
(make-phd-matcher match-syntax-hack ( (car cdr pair? syntax-null? syntax-equal? syntax-id) ()))
