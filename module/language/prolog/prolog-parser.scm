;;; PROLOG COMPILER

;; Copyright (C) 2001,2008,2009,2010 Free Software Foundation, Inc.

;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

;;;; Code:

(use-modules (language prolog umatch))
(use-modules (language prolog umatch-phd))
(use-modules (language prolog parse-util))
(use-modules (srfi srfi-1))
(use-modules (ice-9 readline))
(use-modules (ice-9 match-phd))
(use-modules (ice-9 pretty-print))

;;debug utility
(define (pp x) (pk (pretty-print x)) x)

(define union  (lambda (x y) (lset-union        eq? x y)))
(define diff   (lambda (x y) (lset-difference   eq? x y)))
(define in-all (lambda (x y) (lset-intersection eq? x y))) 
(define in-one (lambda (x y) (lset-xor          eq? x y)))

(define maxmatch '((0 0) ()))

(define (error-larger RC R C)
  (if (> R (car RC))
      #t
      (if (eq? R (car RC))
          (if (> C (cadr RC))
              #t
              #f)
          #f)))
(define *container* (make-fluid))

(define (error-equal RC R C)
  (and (eq? (car RC) R) 
       (<= (cadr RC) C)
       (>= (cadr RC) C)))

(define-syntax ++match-prolog-syntax
  (syntax-rules (-abs)
    ((_ str -abs abs x . code)
     (with-fluids ((*container* `(,str ,x)))
        (match-prolog-syntax -abs abs x . code)))
    ((_ str x . code)
     (with-fluids ((*container* `(,str ,x)))
        (match-prolog-syntax x . code)))))

(define (set-error Str H L)
  (match `(,H ,L)
         (((l1 c1 r1 d1 pth1) (l2 c2 r2 d2 pth2))          
          (if (or (not (eq? l1 l2)) (not (eq? c1 c2)))
              (if (error-larger (car maxmatch) r2 c2)
                  (set! maxmatch `((,r2 ,c2) (,(format "for ~a starting at (~a:~a) error at ~a:~a" 
                                                       Str r1 (floor c1) r2 (floor c2)))))
                  (if (error-equal (car maxmatch) r2 c2)
                      (set! maxmatch `(,(car maxmatch) (,(format "for ~a starting at (~a:~a) error at ~a:~a" 
                                                                 Str r1 (floor c1) r2 (floor c2)) 
                                                        ,@(cadr maxmatch)))))))              
          #f)
         (_ (error "set-error match error"))))

(define (parse-error Str H L)
  (match `(,H ,L)
         (((l1 c1 r1 d1 pth1) (l2 c2 r2 d2 pth2))          
          (error (format " for ~a starting at (~a:~a) error at ~a:~a~%" 
                         Str r1 (floor c1) r2 (floor c2))))))

(define (container-error X)
  (let ((*container* (fluid-ref *container*)))
    (set-error (car *container*) (cadr *container*) X) #f))

(define (parse-container-error X)
  (let ((*container* (fluid-ref *container*)))
  (match `(,(cadr *container*) ,X)
         (((l1 c1 r1 d1 pth1) (l2 c2 r2 d2 pth2))          
          (error (format "for ~a starting at (~a:~a) error at ~a:~a~%" 
                         (car *container*) r1 (floor c1) r2 (floor c2)))))))


(define (shift+ X) (match X ((l c r . u) `(,l ,(+ c 1/2) ,r ,@u))))
(define (container-error+ Str X)
  (let ((*container* (fluid-ref *container*)))
    (set-error (string-append Str " " (car *container*)) (cadr *container*) (shift+ X)) X))

(define-syntax 1-parser
  (syntax-rules ()
    ((_ name op?) 
     (define (name x)
       (let ((f? (lambda (x) (op? (car x)))))
         (match-prolog-syntax x
            (((? f? H) . L) (cons (car H) L))
            (_              (let ((*container* (fluid-ref *container*)))
                              (set-error (car *container*) (cadr *container*) x)))))))))

(set! *pmax* 2000)
(reset-opdata)
(def-operators
  (<fin>   "."   1210 :infix  :right )
  (<:->    ":-"  1200 :infix  :no    )	
  (<-->>   "-->" 1200 :infix  :no    )
  (<p:->   ":-"  1200 :prefix :no    )
  (<semi>  ";"   1100 :infix  :right )
  (<->>    "->"  1050 :infix  :right )
  (<comma> ","   1000 :infix  :right )
  (</+>    "\\+" 900  :prefix :yes   )
  (</.>    "/."  900  :prefix :yes   )
  (<:>     ":"   600  :infix  :right )
  (<pipe>  "|"   600  :infix  :right )

  (((<=>    "="  )
    (</=>  "\\="  )
    (<=..>  "=.." )
    (<==>   "=="  )
    (</==>  "\\==") 
    (<at<>  "@<"  )
    (<at=<> "@=<" )
    (<at>>  "@>"  )
    (<at>=> "@>=" ) 
    (<is>   "is"  )
    (<ref-is> "&is")
    (<attr-is>  "@is")
    (<force-is> "@@is")
    (<=:=>  "=:=" )
    (<=/=>  "=\\=")
    (<<>    "<"   )
    (<=<>   "=<"  )
    (<<=>   "<="  )
    (<>>    ">"   )
    (<>=>   ">="  )
    (<=>>   "=>"  ))
   700  :infix  :no    )
  
  ((( <o+> "+"   )
    ( <o-> "-"   )
    ( <A>  "/\\" )
    ( <V>  "\\/" ))
   500  :infix  :left)

  (((<o*>  "*"  )
    (<o/>  "/"  )
    (<o//> "//" ) 
    (<rem> "rem")
    (<mod> "mod")
    (<<<>  "<<")
    (<>>>  ">>"))
   400  :infix  :left)
  
  (((<**> "**")
    (<^>  "^" ))
   200  :infix  :right)

  (((<p+> "+")
    (<p-> "-")
    (<p/> "\\"))
   200  :prefix :yes))



(define expression-ops '(<=> </=> <=..>))

(define (expression-op? x) (member x expression-ops))


(def-match-str <left>   "(")
(def-match-str <right>  ")")
(def-match-str <bleft>  "[")
(def-match-str <bright> "]")
(def-match-str <bar>    "|")
(def-match-str <:>      ":")
(def-match-str <lam>    "/.")
(def-match-str <dot>    ".")
(def-match-str <!>      "!")
(def-match-str <comma>  ",")
(def-match-str <_>      "_")
(def-match-str <fail>   "fail")
(def-match-str <true>   "true")


(define (sp2? x) (member x '(#\space #\tab #\newline)))
(define (sp?  x) (sp2? (car x)))
(define (<sp> X)
  (match-prolog-syntax X
     ( (#\newline . (- l c r . u)) (cons #\newline `(,l 1 ,(+ r 1) ,@u)))
     ( ((? sp? X) . L            ) (cons (car X)    L ))
     ( _                           #f)))


(define (<spc> X)
  (match-prolog-syntax -abs ((<*>)) X
     ([(<*> <sp>) . L]
      (match-prolog-syntax -abs ((<com>)) L 
        ([<com>     . L] (<spc> L))
        (             L  (cons '<spc> L))))))

(define (<spc+> X)
  (match-prolog-syntax -abs ((<*>)) X
     ([(<+> <sp>) . L]
      (match-prolog-syntax -abs ((<com>)) L 
        ([<com>     . L] (<spc> L))
        (             L  (cons '<spc> L))))))


(1-parser <nw> ttok?)

(define (<stok> X)
  (match-prolog-syntax -abs ((<stokH> <.stokH.>) (<stokL> <.stokL.>))   
     X
     ((<stokH> <stokL> . L)   (cons (mk-var <.stokH.> <.stokL.>) L))
     (_                       (container-error X))))

(define (stk?  x) (member x '(#\space #\tab #\newline #\( #\) #\[ #\])))

(define (sth? x) (and (not (stk? x)) (not (tok? x))))
(1-parser <stokH> sth?)
  
(define (sk? X) (not (stk? X)))
(1-parser <sk> sk?)

(define (<stokL> X)
  (match-prolog-syntax -abs ((<*> <.*.>)) 
                       X
                       (( (<*> <sk>) . L)  (cons <.*.> L))
                       (_                  (container-eror X))))



(1-parser <tk> char-lower-case?)

(define (ttok? x) (or (equal? x #\_) (char-is-both? x)  (char-numeric? x)))
(1-parser <ttk> ttok?         )

(define specials (string->list ".,()[]{}|:'`\""))
(define (ssok? x) (and (not (ttok? x)) (not (sp2? x)) (not (member x specials))))

(1-parser <ssk> ssok?         )

(1-parser <dig> char-numeric? )

(define (<nws> X)
   (match-prolog-syntax -abs ((<*> <.*.>))  X 
     ([(<*> <ttk>) . L] (cons <.*.> L))))



(define (<A> f) (lambda (X) (f X)))


(define (fkn-error Nm H)
  (lambda (X)
    (set-error (format #f "no closing paranthesis to function ~a" Nm) H X) #f))

(define (<fkn> X)
  (++match-prolog-syntax "fkn" 
       -abs ((<tok> <.tok.>) 
             (<var> <.var.>)
             (<A>) 
             (<spc>) 
             (<left>) 
             (<args> <.args.>) 
             (<right>))
     X

     ((<tok> <spc> <left> <args> . (cond (<right> . L)  ((<A> (fkn-error <.tok.> X)))))
      (cons `(<fkn> ,(mk-fkn <.tok.> <.args.>) ,<.args.>) 
            L))

     ((<var> <spc> <left> <args> . (cond (<right> . L)  ((<A> (fkn-error <.var.> X)))))
      (cons `(<fkn> ,<.var.> ,<.args.>) 
            L))


     ((<tok>                             . L)  
      (cons `(<fkn> ,(mk-fkn <.tok.> '()) ())
            L))

     (_ (set-error "fkn" X X))))


(define (mk-fkn F A)  
    (let ((N (length A)))
      (if (> (length A) 0)
	  (string->symbol
	   (string-append 
	    (symbol->string F) "/" 
	    (number->string (length A))))
	  F)))
      

(define (<noteoln> X)
  (match-prolog-syntax X
      ( [(not #\newline) . L]  (cons #t L) )                 
      ( _                       #f)))
  
(define (<com> X)
  (match-prolog-syntax -abs ((<*>)) X
     ([#\% (<*> <noteoln>) . L]  (cons '<com> L))
     (_                          #f)))


(define (<args> X)
  (++match-prolog-syntax "fkn args" -abs ((<spc>) (<eterm> <.eterm.>) (<comma>) (<args> <.args.>)) X
     ([<spc> <eterm> <spc> <comma> <spc> <args> . L]  (cons (cons <.eterm.> <.args.>) L))
     ([<spc> <eterm> <spc>                      . L]  (cons (cons <.eterm.> '()     ) L))
     ([<spc>                                    . L]  (cons '() L))
     (_  #f)))

(define (<econtext> L)
  (if (fluid-ref *expression-context*) (cons #t L) #f))
(define (<tcontext> L)
  (if (fluid-ref *expression-context*) #f (cons #t L)))

(define (<arg> X)
  (++match-prolog-syntax "arg" 
    -abs ((<sexp> <.sexp.>) 
          (<var>  <.var.>) 
          (<fkn>  <.fkn.>) 
          (<lis>  <.lis.>)
          (<spc>)
          (<left>)
          (<right>)
          (<lam>)
          (<stmo> <.stmo.>)
          (<pk>          )
          (<econtext>    )
          (<atm>  <.atm.>) 
          (<_>    <._.>)) 
   
    X
    
     ( [<left> <spc> <lam> . L]
       (match-prolog-syntax 
        -abs ((<left>) (<stmo> <.stmo.>) (<spc>) (<right>))
        X
        ( [<left> <stmo> <spc> <right> . L]
          (cons `(<lam> ,<.stmo.>) L))))

     ( [<left> <stmo> <spc> <right> . L]
       (cons `(:group ,<.stmo.>) L))
     ( [#\s #\c #\m <sexp> . L] (cons `(<scm> ,<.sexp.>) L))
     ( [#\f #\w #\h #\e #\n <sexp> . L]
       (cons `(<fkn> fwhen (,<.sexp.>)) L))
     ( [<fkn>   . L]   (cons  <.fkn.> L)              )
     ( [<var>   . L]   (cons `(<var> ,<.var.>) L) )
     ( [<lis>   . L]   (cons `(<lis> ,<.lis.>) L) )
     ( [<atm>   . L]   (cons `(<atm> ,<.atm.>) L) )
     ( [<_>     . L]   (cons <._.>             L) )
     (  _            (set-error "list arg" X X))))


(define (<sexp> X)
  (++match-prolog-syntax "sexp" 
     -abs ((<spc>) (<left>) (<sexargs> <.sexargs.>) (<spc>) (<right>)) X

     ([<spc> <left> <sexargs> <spc> <right> . L]
      (cons <.sexargs.> L))
     (_  (set-error "sexp" X X))))

(define (<sexargs> X)
  (++match-prolog-syntax "sexp args" 
     -abs ((<spc>) (<right>) (<sterm> <.sterm.>) (<sexargs> <.sexargs.>)) X

     ([<spc> . (and L [<right> . U]) ] (cons '() L))
     ([<spc> <sterm> <sexargs> . L  ] (cons (cons <.sterm.> <.sexargs.>) L))
     (_ (set-error "sexp args" X X))))


(define (<sterm> X)
  (++match-prolog-syntax "sexp arg" 
     -abs ((<sexp> <.sexp.>) (<var> <.var.>) (<stok> <.stok.>) (<int> <.int.>) (<str> <.str.>)) X

     ( (<sexp> . L) (cons <.sexp.>           L))
     ( (<var>  . L) (cons `(u-deref <.var.>) L))
     ( (<stok> . L) (cons <.stok.>           L))     
     ( (<int>  . L) (cons <.int.>            L))
     ( (<str>  . L) (cons <.str.>            L))
     ( _ (set-error "sexp arg" X X))))
      

(define  (mk-var x) (string->symbol (list->string x)))

(1-parser <ucase> char-upper-case?)
(define (<var> X)
  (match-prolog-syntax -abs ((<ucase> <.ucase.>) 
                             (<nws>    <.nws.> )) 
     X
     ([<ucase> <nws> . L]  (cons (mk-var (cons <.ucase.> <.nws.>)) L))
     (_                    #f)))

(define (list-error H)
  (lambda (X)
    (begin (set-error "list does not close at" H X) #f)))


(define (<lis> X)
  (++match-prolog-syntax "list" 
     -abs ((<bleft>) (<bright>) (<spc>) (<lisb> <.lisb.>) (<A>)) X

     ( [<bleft> <spc>              . (cond (<bright> . L) ((<A> (list-error X))))]
       (cons '(:term (<atm> <eoln>)) L))

     ( [<bleft> <spc> <lisb> <spc> . (cond (<bright> . L) ((<A> (list-error X))))]
       (cons <.lisb.> L))
     ( _               (set-error "list" X X))))

(define (<lisb> X)
  (match-prolog-syntax -abs ((<eterm> <.eterm.> <.eterm2.>)
                             (<spc>) 
                             (<spc+>)
                             (<comma>)
                             (<lisb> <.lisb.>)(<bar>)(<:>)) X

     ([ <eterm> <spc> <comma> <spc> <lisb> . L]
      (cons `(cons ,<.eterm.> ,<.lisb.>) L))
     ([ <eterm> <spc+>          <lisb> . L]
      (cons `(cons ,<.eterm.> ,<.lisb.>) L))

     ([ <eterm> <spc> . (cond  [<bar> <spc> <eterm>  . L]
                               [<:>   <spc> <eterm>  . L])]
      (cons `(cons ,<.eterm.> ,<.eterm2.>)  L))
     ([ <eterm> <spc> <:> <spc> <eterm>  . L]
      (cons `(cons ,<.eterm.> ,<.eterm2.>)  L))
     ([ <eterm>                            . L]
      (cons `(cons ,<.eterm.> (:term (<atm> <eoln>))) L))
     ( _   #f)))


(define (<atm> X)
  (match-prolog-syntax -abs ((<quote> <.quote.>)
                             (<top>   <.top.>)
                             (<num>   <.num.>)
                             (<int>   <.int.>)
                             (<str>   <.str.>)
                             (<tok>   <.tok.>)) X

                             
     ([<top>   . L]  (cons `(<top> ,<.top.>) L) )
     ([<quote> . L]  (cons `(<tok> ,<.quote.>) L) )
     ([<num>   . L]  (cons `(<num> ,<.num.>) L))
     ([<int>   . L]  (cons `(<int> ,<.int.>) L))
     ([<str>   . L]  (cons `(<str> ,<.str.>) L))
     ([<tok>   . L]  (cons `(<tok> ,<.tok.>) L))
     (_              #f)))

(def-match-str <qt>     "'")
(def-match-str <tq>     "'")
(1-parser <nq> (lambda (x) (not (eq? x #\'))))
(define (<quote> X) 
  (match-prolog-syntax -abs ((<qt>) (<tq>) (<+> <.+.>)) X
  ((<qt> (<+> <nq>) <tq> . L)
   (cons (mk-var <.+.>) L))
  (_ #f)))

(def-match-str <tp>     "{")
(def-match-str <pt>     "}")
(1-parser <ntop> (lambda (x) (not (eq? x #\}))))
(define (<top> X) 
  (match-prolog-syntax -abs ((<tp>) (<pt>) (<+> <.+.>)) X
  ((<tp> (<+> <ntop>) <pt> . L)
   (cons (mk-var <.+.>) L))
  (_ #f)))


(define (<tok> X)
  (++match-prolog-syntax "token" -abs ((<tk> <.tk.>)(<*> <.*.>) (<+> <.+.>)) X

     ([ <tk> (<*> <ttk>) . L]   (cons (mk-var (cons <.tk.> <.*.>)) L))
     ([ (<+> <ssk>)      . L]   (cons (mk-var  <.+.>)              L))
     (_                         #f)))


(define (mk-num n) (string->number (list->string n)))
(define (<int> X)
  (++match-prolog-syntax "integer" -abs ((<+> <.+.>)) X
     ([    (<+> <dig>) . L]  (cons (mk-num <.+.>           ) L)) 
     ([#\- (<+> <dig>) . L]  (cons (mk-num (cons #\- <.+.>)) L))
     (_                      (set-error "integer" X X))))


(define (<nums> X)
  (match-prolog-syntax -abs ((<+> <.+.>)) X
     (((<+> <dig>) . L)     (cons (list->string <.+.>) L))
     (_   (container-error X))))

(define (<digs+> X)
  (match-prolog-syntax -abs ((<+> <.+.>)) X
     (((<+> <dig>) . L)     (cons (list->string <.+.>) L))
     (_                     (container-error X))))

(define (<+/-> X)
  (match-prolog-syntax X
     ((#\+ . L) (cons "+" L))
     ((#\- . L) (cons "-" L))
     (L         (cons ""  L))))


(define (<exp> X)
  (match-prolog-syntax -abs ((<+/->   <.+/-.>  )  
                             (<digs+> <.digs+.>)) X
     ((#\e <+/-> <digs+> . L)   (cons (string-append "e" <.+/-.> <.digs+.>) L))
     (L                         (cons "" L))))

(define (<num> X)
  (++match-prolog-syntax "float" 
     -abs ((<spc>) (<digs+> <.digs+.>) (<dot>) (<nums> <.nums.>) (<exp> <.exp.>)) X

     ((<digs+> <dot>  <nums> <exp>  . L) (cons (string->number
                                                (string-append <.digs+.> 
                                                               "." 
                                                               <.nums.>
                                                               <.exp.>))
                                               L))
     (_ #f ))) 

(define (<strbody> A)
  (let ((A (car A)))
  (lambda (X)
    (match-prolog-syntax X
      ([(and H (not ,A)) . L] (cons (car H) L))
      (_ (container-error X))))))

(define (<e> n)
  (lambda (X)
    (match X
           ((L c l d path)
            (match path
                   ((a ... fkn file)
                    (error (format #f "error ~a in container ~a, function ~a (row ~a col ~a)"
                                   n file fkn l c)))
                   ((a ... file)
                    (error (format #f "error ~a in container ~a, (row ~a col ~a)"
                                   n file l c)))))
           (_ (error "Bug wrong element format")))))

(define (<str> X)
  (++match-prolog-syntax "string" -abs ((<*> <.*.>) (<e>)) X
     (((and A (cond #\")) (<*> (<strbody> A)) . (cond (,(car A) . L) ((<e> "no ending quote in string"))))
      (cons `(<str> ,(list->string <.*.>)) L))
     (_  #f)))

(define (<stms> X)
  (++match-prolog-syntax "statatements" -abs ((<+> <.+.>) (<spc>)) X
    (((<+> <stm>) <spc>)  <.+.>)
    (_  (set-error "no statements" X X))))

(define (dot-error X) (container-error+ "dot error" X))

(define (<stm> X)
  (++match-prolog-syntax "statement" 
     -abs (( <spc>              ) 
           ( <term>   <.term.>  ) 
           ( <A>                )
           ( <sunk>             )
           ( <tcontext>         )
           ( <dot>              ) 
           ( <xy-op>  <.xy-op.> ) 
           ( <stm>    <.stm.>   )) 
     X

     ((<spc> <term> <spc> . (cond (<dot> . L) ((<A> dot-error)))) 
      (cons  (cons <.term.> '())               L))

     ((<tcontext> <spc> <sunk> <spc> <xy-op> . L)  (=> next)
      (if (and (expression-op? <.xy-op.>) (not (fluid-ref *expression-context*)))
          (<estm> <stm> X)
          (next)))

     ((<spc> <term> <spc> <xy-op> <stm> . L)   (cons `(,<.term.> ,<.xy-op.> ,@<.stm.>)  L))

     ((<spc> <term> <spc> <xy-op>       . L)   (container-error+ "missing term error" L) #f)
     (_ #f)))


(define (<eterm> L)
  (with-fluids ((*expression-context* #t))
               (<term> L)))

(define (<estm> <F> X)
  (++match-prolog-syntax "expression statement" 
                         -abs ((<eterm> <.eterm1.> <.eterm2.>) 
                               (<spc>                        )
                               (<pk>                         )
                               (<xy-op> <.xy-op.>            )) 
                         
                         X                  
                         ((<spc> <eterm> <spc> <xy-op> <spc> <eterm> . L)
                          (let* ((LL (match L ((U N . V) `((#\a ,@U) ,(- N 1) ,@V))))
                                 (R  (<F> LL)))
                            (if R
                                (cons `((:group (,<.xy-op.> ,<.eterm1.>  ,<.eterm2.>)) ,@(cdar R)) (cdr R))
                                #f)))
                         (_ #f)))


(define (<stmo> X)
  (++match-prolog-syntax "grouped statement"
     -abs ((<spc>) 
           (<term> <.term.>) 
           (<right>) 
           (<sunk>)
           (<pk>)
           (<tcontext>)
           (<xy-op> <.xy-op.>) 
           (<stmo> <.stmo.>)) X

     ((<spc> <term> <spc> . (and L (<right> . _)))   
      (cons  (cons <.term.> '()) L))

     ((<tcontext> <spc> <sunk> <spc> <xy-op>  . L) (=> next)
      (if (and (expression-op? <.xy-op.>) (not (fluid-ref *expression-context*)))
          (<estm> <stmo> X)
          (next)))

     ((<spc> <term> <spc> <xy-op> <stmo> . L)
      (cons `(,<.term.> ,<.xy-op.> ,@<.stmo.>) L))

     ((<spc> . (and L (<right> . _)))
      (cons '() L))
     
     (_ (container-error X))))

(define (mk-term X Y C)
  (match `(,X ,Y)
     (((Pr . L)  Pos    )        (mk-term L   Pos `(,Pr ,C)))
     ((()       (Po . L))        (mk-term '() L   `(,Po ,C)))
     ((()       ()      )         C)))

(define (<x-> X)
  (match-prolog-syntax -abs ((<spc>)(<x-op> <.x-op.>)) X
    ((<spc> <x-op> . L) (cons <.x-op.> L))
    (_ #f)))

(define (<-x> X)
  (match-prolog-syntax -abs ((<spc>)(<op-x> <.op-x.>)) X
    ((<spc> <op-x> . L) (cons <.op-x.> L))
    (_ #f)))

(define (prefix-error str H)
  (lambda (X)
    (set-error (format #f "possible prefix (~a) at postfix error" str) H X)
    #f))

(define (postfix-error str H)
  (lambda (X) (parse-error (format #t "prefix (~a) at postfix error" str) H X)))

(define (<sunk> X)
  (if (fluid-ref *expression-context*) 
      #f
      (++match-prolog-syntax "unbalanced parenthesis" -abs ((<left>) (<right>) (<sunk0>) (<eterm>))
                             X
                             ((<left> <sunk0> <right>  . L)  (cons #t L))
                             ((<eterm>                 . L)  (cons #t L))
                             (_ #f))))

(define (<sunk0> X)
  (match-prolog-syntax -abs ((<sunk>)(<sunk0>)(<spc>)(<right>))
       X
       ((<spc> . (cond (<sunk> <sunk0>                     . L)
                       (and (not (<right> . _)) (_ <sunk0> . L))
                       (and      (<right> . _)               L)))
        (cons #t L))
       (_ (parse-container-error X))))
                       

(define (<term> X)
  (++match-prolog-syntax "statement term" 
     -abs ((<fail>             )
           (<true>             )         
           (<*>      <.pre-op.> <.post-op.>) 
           (<A>                ) 
           (<pk>               ) 
           (<x->     <.epre.>  ) 
           (<-x>     <.epost.> )
           (<spc>              )  
           (<arg>    <.arg.>   )  
           (<!>                )                        
           (<stmo>   <.stmo.>  ) 
           (<left>             ) 
           (<right>            )) X           

     ((<fail>                            . L) (cons '(:term <fail>)    L))
     ((<true>                            . L) (cons '(:trem <true>)    L))
     ((<!>                                     . L) (cons '(:term <!>)        L))
     (((<*> <x->) <spc> 
       . (cond (<-x> (<A> (postfix-error <.epost.>)))
               (<arg> (<*> <-x>)  
                      . (cond (and U  (<x-> (<A> (prefix-error <.epre.> U))))
                              L))))
                              
      (cons (mk-term (reverse <.pre-op.>)
                     <.post-op.>
                     `(:term ,<.arg.>))
            L))

     ((<left> <stmo> <spc> <right>             . L) (cons <.stmo.> L))

     (_ (set-error "statement term" X X))))


(define (remove-dupes L Ret)
  (match L
         (()       (reverse Ret))
         ((H . T)  (if (member H Ret)
                       (remove-dupes T Ret)
                       (remove-dupes T (cons H Ret))))))

(define (combine-errors x)
  (if (pair? x)
      (string-append (format #f "~a~%" (car x)) (combine-errors (cdr x)))
      ""))

(define (check-error X)
  (if X X (error (string-append (format #f "parse error:~%") (combine-errors (remove-dupes (cadr maxmatch) '()))))))

(define (treeify X)
  (treeify-ops X))

(define (parse x) 
  (set! maxmatch '((0 0) ()))
  (pretty-print (map treeify (check-error (<stms> `(,(string->list x) 1 1 0 (ram)))))))

(define (ptest a x) 
  (set! maxmatch '((0 0) ()))
  (with-fluids ((*container* '("empty" (() 1 1 0 0))))
        (a `(,(string->list x) 1 1 0 (ram)))))



                                        
                                   
