(export <car> <cdr> <cons> <pair?> <null?> <member> <append> <q> <bq> gp-append bq u-length)

(use-modules (language prolog guile-log-pre))

;;Basic list processing ideoms
(<define> (<car> x ret)
    (<match> (x)
      ((,ret . _) (<cut> <cc>))
      (_          (<cut> <fail>))))

(<define> (<cdr> x ret)
    (<match> (x)
      ((_ . ,ret) (<cut> <cc>))
      (_          (<cut> <fail>))))

(<define> (<cons> x y ret)
    (<match> (ret)
      ((,x . ,y) (<cut> <cc>))
      (_         (<cut> <fail>))))

(<define> (<pair?> x)
    (<match> (x)
      ((_ . _) (<cut> <cc>))
      (_       (<cut> <fail>))))

(<define> (<null?> x)
    (<match> (x)
      (() (<cut> <cc>))
      (_  (<cut> <fail>))))

(<define> (<member> x l)
  (<match> (l)
    (()        (<cut> <fail>))
    ((,x . _)  <cc>)
    ((_  . l)  (<cut> (<member> x l)))
    (_         (error "l not a proper u-list in (<member> x l)"))))
        
(<define> (<append> x y z)    
    (<match> (x z)
      (() ,y     
       <cc>)
      ((xa . xs) (xa . zs)
       (<append> xs y zs))
      (_ _ (<cut> <fail>))))

        
(define-guile-log <q>
  (syntax-rules ()
    ((_ w (x ...) out)
     (parse<> w
       (<match> (out)
         (('x ...) (<cut> <cc>))
         (_        (<cut> <fail>)))))))

(define-syntax tr-bq 
  (lambda (x)
    (syntax-case x (unquote unquote-splicing)
      ((w out (a ... )  ((unquote (unquote (f b ...))) . r))
       #'(parse<> w
           (<match> (out)
             ((a ... x . l)
              (<cut> 
               (f b ... x)
               (tr-bq l () r)))
             (_ (<cut> <fail>)))))

      ((w out (a ... )  ((unquote (unquote-splicing (f b ...))) . r))
       #'(parse<> w
           (<match> (out)
             ((a ... . l)
              (<cut> 
               (<var> (res res2)
                 (f b ... res)                 
                 (tr-bq res2 () r)
                 (<append> res res2 l))))                 
             (_ (<cut> <fail>)))))

      ((w out (a ... )  ((unquote-splicing f) . r))
       #'(parse<> w
           (<match> (out)
             ((a ... . l)
              (<cut> 
               (<var> (res)
                 (tr-bq res () r)
                 (<append> f res2 l))))
             (_ (<cut> <fail>)))))

      ((w out (a ...) ((unquote b) . r))
       #'(tr-bq w out (a ... (unquote b)) r))

      ((w out (a ...) (b . r))
       #'(tr-bq w out (a ... (quote b)) r))

      ((w out (a ...) b)
       #'(parse<> w
           (<match> (out)
             ((a ... . b) (<cut> <cc>))
             (_           (<cut> <fail>))))))))

(define-guile-log <bq>
  (syntax-rules ()
    ((_ w l out) (tr-bq w out () l))))

(define (gp-append a l)
  (umatch #:mode - (a)
    ((x . u)
     (gp-cons! x (gp-append u l)))
    (() l)
    (_ (error "gp-append first argument not a gp-list"))))

(define-syntax bq
  (lambda (x)
    (syntax-case x (unquote-splicing unquote)
      ((_ (unquote-splicing a))
       (if (eq? (syntax->datum #'a) '_)
           #'(gp-var!)
           #'a))
      ((_ (unquote a) . l)
       #'(gp-cons! a (bq . l)))
      
      ((_ (a ...)     . l)
       #'(gp-cons! (bq a ...) (bq . l)))

      ((_ a           . l)
       (if (eq? (syntax->datum #'a) '_)
           #'(gp-cons! (gp-var!) (bq . l))
           #'(gp-cons! 'a         (bq . l))))
      ((_) #''()))))
       

(define (u-length l)
  (let loop ((l l) (n 0))
    (umatch #:mode - (l)
      ((x . l)
       (loop l (+ n 1)))
      (_ n))))
