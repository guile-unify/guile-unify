(define-module (language prolog list-ffi)
  #:use-module (language prolog umatch-ffi)
  #:use-module (language prolog guile-log-ffi))

(include-from-path "language/prolog/list-code.scm")
