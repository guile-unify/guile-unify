(define-module (language prolog umatch-phd)
  #:use-module (ice-9 match-phd)
  #:export     (match-prolog-syntax match-prolog-syntax))

;; (list row column depth path)
(define (*car      x) (match x 
                             (((h . u)   . l) `(,h ,@l))
                             (_                (error (format #t "*car got error arg ~a~%" x)))))

(define (*cdr      x) (match x 
                             (((h . u) c . l) `(,u ,(+ c 1) ,@l))
                             (_                (error (format #t "*cdr got error arg ~a~%" x)))))

(define (*pair?    x) (match x 
                             ((x         . l) (pair? x))
                             (_                (error (format #t "*pair? got error arg ~a~%" x)))))

(define (*null?    x) (match x 
                             ((x         . l) (null? x))
                             (_                (error (format #t "*null? got error arg ~a~%" x)))))

(define (*equal? x y) (match x 
                             ((x         . l) (equal? x y))
                             (_                (error (format #t "*equal? got error arg ~a~%" x)))))


(make-phd-matcher match-prolog-syntax
     ( (*car *cdr *pair? *null? *equal?)
       (  (+ (*car *cdr *pair? *null? *equal?))
          (- ( car  cdr  pair?  null?  equal?)))))



