(export <next> prompt=> <or> <and> <not> <bag-of> <fkn> <cond> <if> <->>
        <with-prolog-environment> <collector> parse<> <eval>
        <cc> <fail> <let> <let*> <var> </.> <ask> <ret> <when>
        <define> <cut> <pp> <pp-dyn> <set-cut>
        bag-of/3 set-of/3 <run> <recur>
        <with-bt> <exec>  <funcall> <take> <*> <lambda>
        <match> <=> <==> *r* ! !! <repl-vars> <unify> <apply>
        <and!> <and!!> <if-only> <succeeds> 
        <format> <tail-code> <code> <ret> <return>
        <def> <<define>> 
        unwind-mute unwind-interval unwind-token 
        let<> mark-once/2 mark/2
        <or!>)

;; Some repl magic.
(define *prompt*    (make-fluid))
(define *chain*     #f)
(define (*finish*) 'finished)
(define *r*         #f)
(fluid-set! *prompt* #f)

#;(if *chain* (if old (old 'change lam)))
                 

(define (!  x) (fluid-ref x))
(define (!! x) (u-scm (! x)))

(define *stall*
  (lambda (pr) 
    (with-fluids ((*prompt* pr))
      (set! *chain* #f)
      (start-repl))))

(define (guile-log-finish-set! f) (set! *finish* f))

(define (mk-stall)
  (let* ((f      *stall*)
         (id     (gensym "id"))
         (end    #t)
         (g      (lambda x
                   (match x
                     (('change q)  
                      (set! end #t)
                      (set! f q))                      

                     (('change-chain q)  
                      (set! end #f)
                      (set! f q))

                     (('f)     f)
                     (('end?)  end)
                     ((p)          
                      ;(pp `(eval f ,id))
                      (f p))))))
    ;(pp `(mk ,id))
    g))

(define *lambda*  #f)

(define (dyn-stall old)
  (if (not *chain*)
      (begin
        (if old (old 'change *stall*))
        (set! *lambda* old))
      (if old
          (if (old 'end?)
              (let ((f (old 'f)))
                (old 'change-chain
                     (lambda (p)
                       (with-fluids ((*prompt* p))
                         (let ((recur #t))
                           (dynamic-wind
                               (lambda ()  
                                 'ok)
                               (lambda ()  
                                 (start-repl-init 
                                  (lambda () 
                                    (let ((fr (gp-newframe)))
                                      (dynamic-wind
                                          (lambda () 'ok)
                                          (lambda () 
                                            (f p) 
                                            (set! *lambda* old)
                                            (set! recur #f)
                                            (old 'change *stall*)
                                            (set! *chain* #f))
                                          (lambda () 
                                            (gp-unwind fr)))))
                                  #f))
                             (lambda ()  
                               (if recur
                                   (dyn-stall old)))))))))))))
                         
(define-syntax with-lambda
  (syntax-rules ()
    ((_ n (g r) code ...)
     (let ((old   *lambda*)
           (first #t      ))
       (dynamic-wind
           (lambda () 
             (if first 
                 (set! first #f)
                 (error (format #f "~a has no reentrant stall") n)))
           (lambda () 
             (if *lambda* (*lambda* 'change r))
             (set! *lambda* g)
             code 
             ...)
           (lambda () (dyn-stall old)))))))

(define-guile-log <repl-vars>
  (syntax-rules ()
    ((_ w (v ...) code ...)
     (let ((v (if (fluid? v) (! v) v)) ...)
       (parse<> w (<and> code ...))))))

;;This is function mungling that is quite ugly but soo fun
(define-syntax <exec>
  (syntax-rules ()
    ((_ (v ...) code ...)
     (begin
       (define v (if (fluid? v) v (make-fluid)))
       ...
       (let ((first #t           )
             (id    (gensym "id"))
             (frame (gp-newframe)))
         (dynamic-wind
             (lambda () 
               ;(format #t "enter ~a~%" id)
               (if first
                   (set! first #f)
                   (error "re ennenterance not supported for guile-log <exec>")))
             (lambda ()
               (with-fluids ((v (u-var!)) ...)
                 
                 (let* ((cc #f)
                        (r (lambda (p)
                             (if p
                                 (parse<> (p last p cc)
                                   (<and> code ...))
                                 (umatch #:tag w-tag #:name '<exec> ()
                                         ((prompt=> (w-prompt w-tag)
                                            (parse<> 
                                                (w-prompt w-tag w-prompt cc)
                                              (<and> code ...))))
                                         ((*finish*))))))

                                         
                        (g (mk-stall)))
                   (set! cc g)
                   (with-lambda '<exec> (g r)
                       (r (fluid-ref *prompt*))))))
             (lambda () 
               ;(format #t "leav ~a~%" id)
               (gp-unwind frame))))))))
  
(define-syntax <*>
  (syntax-rules ()
    ((_ f ...) (<exec> () (f ...)))))

#;
(define-syntax <funcall>
  (syntax-rules ()
    ((_ f a ...)
     (let ((first #t           )
           (frame (gp-newframe)))
       (dynamic-wind
           (lambda () 
             (if first
                 (set! first #f)
                 (error "re entrance not supported for guile-log")))
           
           (lambda ()
             (if (fluid-ref *prompt*)
                 (let* ((cc #f)
                        (r  (lambda (p) (f a ... p cc)))
                        (g  (mk-stall)))
                   (set! cc g)
                   (if (fluid-ref *lambda*)
                       ((fluid-ref *lambda*) 'change r))
                   (with-fluids ((*lambda* g))
                     (r (fluid-ref *prompt*))))
                 (<exec> () (f a ...) (mk-stall) (*finish*))))
           
           (lambda () (gp-unwind frame)))))))
           
                    


(define (pp X) (pretty-print X) X)

(define (<take> n v)
  (catch #t
         (lambda ()
           (if (fluid-ref *prompt*)
               (let* ((ret '())
                      (v    (if (fluid? v) (! v) v))
                      (m    n)
                      (g   (lambda (p)
                             (if (> m 0)
                                 (begin
                                   (set! ret (cons (u-scm v) ret))
                                   (set! m (- m 1))
                                   (u-abort p))
                                 (with-fluids ((*prompt* p))
                                   (set! *chain* #f)
                                   ;(set! *r* (reverse ret))
                                   ;(pp `(*r* = ,*r*))
                                   (start-repl-init #f 
                                                    (lambda () 
                                                      (reverse ret))))))))
    
                 (set! *finish* (lambda () 
                                  (set! *r* (reverse ret))
                                  *r*))
                 (set! *chain* #t)
                 (if *lambda* (*lambda* 'change g))        
                 (g (fluid-ref *prompt*)))
               '()))
         (lambda x (set! *chain* #f))))




(define-syntax <eval> 
  (syntax-rules ()
    ((_ (v ...) code cc fini)
     (let ((first #t)
           (frame (gp-newframe))
           (id    (gensym "id"))
           (f     (lambda ()
                    (let ((v (u-var!)) ...)
                      (umatch #:tag w-tag #:name '<eval> ()
                              ((prompt=> (w-prompt w-tag)
                                 (parse<> (w-prompt w-tag w-prompt cc)
                                   code)))
                              (fini))))))
       (dynamic-wind
           (lambda () 
             ;(format #t "enter ~a~%" id)
             (if first
                 (set! first #f)
                 (begin 
                   (error "re reentrance not supported for guile-log <eval>")
                   f)))
           (lambda () (f))
           (lambda () 
             ;;(format #t "leav ~a~%" id)
             (gp-unwind frame)))))))
       
   
                                        
(define-syntax <run>
  (syntax-rules (*)
    ((_ (v) code ...)
     (let ((ret '()))
       (<eval> (v)
         (<and> code ...)
         (lambda (p)
           (set! ret (cons (u-scm v) ret))
           (u-abort p))
         (reverse ret))))

    ((_ (v ...) code ...)
     (let ((ret '()))
       (<eval> (v ...)
               (<and> code ...)
               (lambda (p)
                 (set! ret (cons (u-scm (list v ...)) ret))
                 (u-abort p))
               (reverse ret))))
    
    ((_ * . l)  (<run> . l))

    ((_ m (v) code ...)
     (let ((n    m)
           (ret '()))
       (<eval> (v)
         (<and> code ...)
         (lambda (p)
           (if (> n 0)
               (begin
                 (set! ret (cons (u-scm v) ret))
                 (set! n (- n 1))
                 (if (= n 0)
                     (reverse ret)
                     (u-abort p)))
               (reverse ret)))
         (reverse ret))))

    ((_ m (v ...) code ...)
     (let ((n m) (ret '()))
       (<eval> (v ...)
               (<and> code ...)
               (lambda (p)
                 (if (> n 0)
                     (begin
                       (set! ret (cons (u-scm (list v ...)) ret))
                       (set! n (- n 1))
                       (if (= n 0)
                           (reverse ret)
                           (u-abort p)))
                     (reverse ret)))
               (reverse ret))))))
  
                       

(define-syntax prompt=>
 (syntax-rules ()
   ((_ (Pr W) Code ...)
    (let ((Pr (u-prompt W)))
      Code ...))))

(define-syntax mk-syms
  (lambda (x)
    (syntax-case x ()
      ((q . l)
       ;(pk (syntax->datum (syntax l)))
       (syntax (mk-syms* . l))))))
       
(define-syntax mk-syms*
  (lambda (x)
    (syntax-case x ()
      ((q 1  (a as ...) rs  l)
       (syntax (mk-syms 10 (as ...) rs l)))
      ((q 10 (a as ...) rs  l)
       (with-syntax ((sym (datum->syntax (syntax q) (gensym "arg"))))
            (syntax (mk-syms 10 (as ...) (sym . rs)  l))))
      ((q 11 (a) rs (f b ...))
       (syntax (f rs b ...)))
      ((q 11 (a as ...) rs  l)
       (with-syntax ((sym (datum->syntax (syntax q) (gensym "pr"))))
            (syntax (mk-syms 11 (as ...) (sym . rs)  l))))
      ((q 2 (a as ...) rs  l)
       (with-syntax ((sym-p (datum->syntax (syntax q) (gensym "pr"))))
            (syntax (mk-syms 2 (as ...) (sym-p . rs)  l))))
      ((q 4 (a as ...) rs  l)
       (with-syntax ((sym-p1 (datum->syntax (syntax q) (gensym "pr" )))
                     (sym-w2 (datum->syntax (syntax q) (gensym "tag")))
                     (sym-p2 (datum->syntax (syntax q) (gensym "pr" ))))
            (syntax (mk-syms 4 (as ...) ((sym-p1 sym-w2 sym-p2) . rs)  l))))
      ((q 2 () rs (f a ...))
       (with-syntax ((sym (datum->syntax (syntax q) (gensym "w"))))
                    (syntax (f w rs a ...))))
      ((q 4 () rs (f a ...))
       (with-syntax ((sym (datum->syntax (syntax q) (gensym "w"))))
                    (syntax (f w rs a ...))))
      ((q n () rs (f a ...))
       (syntax (f rs a ...))))))

(define-syntax %semi-turn%
  (syntax-rules ()
    ((_ w (pr2 ...) (cut fi pr cc) (ee)    (e ...))
     (umatch #:tag w #:name '<or> () 
             ((prompt=> (pr2 w) (parse<> (cut w pr2 cc)  e)))
             ...
             ((parse<> (cut fi pr cc) ee))))
     
    ((_ w prs meta (x . l) (y ...)) 
     (%semi-turn% w prs meta l (y ... x)))))


(define-guile-log  <or>
  (syntax-rules ()
    ((_ meta e1) (parse<> meta e1))
    ((_ meta e1 es ...) 
     (mk-syms 2 (es ...) () (%semi-turn% meta (e1 es ...) ())))))

(define-guile-log <and>
  (lambda (x)
    (syntax-case x (last)
      ((_ w)
       #'(parse<> w <cc>)) 

      ((_ meta       e1       ) 
       #'(parse<> meta e1))

      ((_ (cut fi pr cc) (a b ...) e2 ...)
       (log-code-macro? (syntax->datum (syntax a)))
       #'(a (cut fi pr (parse<> (cut fi pr cc) (<and> e2 ...))) b ...))

      ((_ (cut fi pr cc) e1 e2 ...)  
       #'(let ((ccc (lambda (pr2) (<and> (cut last pr2 cc) e2 ...))))
           (parse<> (cut fi pr ccc) e1))))))

;;This is inspired from kanren
;; (<and!>  a ... ) gives exactly one answer from (and a ....)
;; (<and!!> a ... ) is the same as (and (and! a) ...)
;; this can be modeled by a cut in a less elegant way.

(define-guile-log <and!>
  (syntax-rules ()
    ((_ w) (parse<> w <cc>))
    ((_ (cut fi pr cc) a ...) 
     (let ((ccc (lambda (Pr) (cc pr))))
       (parse<> (cut fi pr ccc) (<and> a ...))))))


  
(define-syntax and!!
  (syntax-rules ()
    ((_ (cut fi pr cc) ccc a)
     (parse<> (cut fi pr (ccc cc)) a))
    ((_ (cut fi pr cc) ccc a as ...)
     (let ((ccc (lambda (pr) (and!! (cut fi pr cc) ccc as ...))))
       (parse<> (cut fi pr ccc) a)))))

(define-guile-log <and!!>
  (syntax-rules ()
    ((_ w) (parse<> w <cc>))
    ((_ (cut fi pr cc) a ...)
     (let ((ccc (lambda (cc) (lambda (Pr) (cc pr)))))
       (and!! w ccc a ...)))))
#|
(<if-only> cond then)       =  (<and> (<and!> cond) then)
(<if-only> cond then else)  =  (<or> (<and> (<and!> cond) then)
                                     (<and> (<not> cond)  else))
|#

;; this will try to make a success and if so reset the state and continue it's a 
;; companion to <not>.

(define-guile-log <succeeds>
  (syntax-rules ()
    ((_ (cut fi pr cc) g)
     (let* ((P   (gp-newframe))
            (ccc (lambda (Pr) (gp-unwind P) (cc pr))))        
       (parse<> (cut fi pr ccc) g)))))



(define-syntax <%fkn%>
  (syntax-rules (last)
    ((_ (cut last pr cc) f a ...)  (f pr cc a ... ))
    ((_ (cut w    pr cc) f a ...)  (u-call  w f pr cc a ... ))))


(define-guile-log <with-fail>
  (syntax-rules ()
    ((_ (cut fi p cc) pp code ...)
     (parse<> (cut fi pp cc) (<and> code ...)))))

(define-guile-log <if>
  (syntax-rules ()
    ((_ meta p a)
     (parse<> meta (<and> (<and!> p) a)))
    ((_ (cut fi p cc) pred a b)     
     (umatch #:tag w #:name '<if> ()
             ((prompt=> (pr w)
                (parse<> (cut w pr cc)
                  (<and> (<and!> pred) (<with-fail> p a)))))
             ((parse<> (cut fi p cc) b))))))
                         

(define-guile-log <cond>
  (syntax-rules ()
    ((_ meta (#t b)) 
     (parse<> meta b))
    ((_ meta (a b))  
     (parse<> meta (<and> a b)))
    ((_ meta (a b) ab ...)     
     (parse<> meta (<if> a b (<cond> ab ...))))))
        
(define-guile-log <var>
  (syntax-rules ()
    ((_ meta (v ...) code ...) 
     (let ((v (u-var!)) ...) (parse<> meta (<and> code ...))))))


(define-syntax <%let%>
  (syntax-rules ()
    ((_ meta (v ...) code ...) (let (v  ...) 
                                 (parse<> meta (<and> code ...))))))

(define-syntax <%let*%>
  (syntax-rules ()
    ((_ meta (v ...) code ...) (let* (v  ...) 
                                 (parse<> meta (<and> code ...))))))

(define-syntax <%cc%>
  (syntax-rules (last parse<>)
    ((_ (cut w    p (parse<> . l))) (parse<> . l))
    ((_ (cut last p cc))   (cc p)           )
    ((_ (cut w    p cc))   (u-call w cc p)  )))

(define-syntax <%cut%>
  (syntax-rules ()
    ((_ (cut w p cc)           )   
     (parse<> (cut last cut cc) <cc>))
    ((_ (cut w p cc) code ...  )   
     (parse<> (cut last cut cc) (<and> code ... )))))

(define-syntax <%not%>
  (syntax-rules ()
    ((_ (cut w p cc) code)
     (umatch #:tag q-not #:name '<not> ()
             ((prompt=> (pr q-not) 
                        (parse<> (pr q-not pr (lambda (x) (u-abort p))) code)))
             ((parse<> (cut w p cc) <cc>))))))
                        

(define-syntax <%fail%>
  (syntax-rules ()
    ((_ _         p)  (u-abort p))
    ((_ (_ _ p _)  )  (u-abort p))))

(define-guile-log <set-cut>
  (syntax-rules ()
    ((_ (cut w p (_ (_ _ _ cc) <cc>))) (parse<> (p w p cc) <cc>))))

(log-code-macro '<set-cut>)

(define-guile-log <when>
  (syntax-rules ()
    ((_ wc p)
     (if p 
         (parse<> wc <cc>) 
         (parse<> wc <fail>)))
    
    ((_ wc p code ...)
     (if p 
         (parse<> wc (<and> code ...))
         (parse<> wc <fail>)))))

(log-code-macro '<when>)

(define-guile-log <pp>
  (syntax-rules ()
    ((_ wc x) (begin (pp (u-scm x))
                  (parse<> wc <cc>)))))
(log-code-macro '<pp>)

(define-guile-log <pp-dyn>
  (syntax-rules ()
    ((_ wc a b)   
     (u-dynwind
      (lambda () (pp (u-scm a)))
      (lambda () (parse<> wc <cc>))
      (lambda () (pp (u-scm b)))))))

(define-guile-log <format>
  (syntax-rules ()
    ((_ wc s str a ...)
     (begin 
       (format s str (u-scm a) ...)
       (parse<> wc <cc>)))))
(log-code-macro '<format>)

(define-guile-log <tail-code>
  (syntax-rules ()
    ((_ (_ _ p cc) (pp ccc) code ...)
     (let ((pp p) (ccc cc)) code ...))))

(define-guile-log <code>
  (syntax-rules ()
    ((_ wc code ...)
     (begin code ... (parse<> wc <cc>)))))
(log-code-macro '<code>)

(define-guile-log <return>
  (syntax-rules ()
    ((_ wc code ...)
     (begin code ...))))

(log-code-macro '<code>)

(define-syntax parse<>
  (lambda (x)
    (syntax-case x ()
      ((_ . l)
       ;(pp `(parse<> ,@(syntax->datum (syntax l))))
       (syntax (parse2<> . l))))))

(define-syntax parse2<>
  (syntax-rules (     <not> if
                      <ret> <fail> <cc> <pp> 
                      <let> <let*> <cut>
                      <repl-vars> last)                       
    ((_ meta (<not>      X   ))   (<%not%>  meta   X   ))    
    ((_ meta (<ret> code)    ) (u-scm code))
    ((_ meta  <fail>         ) (<%fail%> meta))
    ((_ meta (<fail> Pr)     ) (<%fail%> meta Pr))
    ((_ meta  <cc>           ) (<%cc%> meta))
    ((_ meta (if p . l)      ) (<if> meta (<when> p) . l))
    ((_ meta  <cut>          ) (<%cut%>  meta      ))
    ((_ meta (<cut>   l ...) ) (<%cut%>  meta l ...))
    ((_ meta (<let>   l ...) ) (<%let%>  meta l ...))
    ((_ meta (<let*>  l ...) ) (<%let*%> meta l ...))
    ;;Be nice to function calls
    ((_ meta (f ...)         ) (dispatch meta (f ...)))))

(define-syntax dispatch
  (lambda (x)
    (syntax-case x ()
      ((_ w (n . l))       
       ;(pk (syntax->datum (syntax (n w . l))))
       (if (guile-log-macro? (syntax->datum (syntax n)))
           (syntax (n w . l))
           (syntax (<%fkn%> w n . l)))))))

(define-syntax <with-prolog-environment>
  (syntax-rules (last)
    ((_ (      p cc) code) (parse<> (p   last p cc) code))
    ((_ (cut w p cc) code) (parse<> (cut w    p cc) code))))


(define-syntax <collector>
  (syntax-rules (:vars :args :ret)
    ((_ name (:var v ...) (:args as ...) (:ret l) code)
     (define (name  as ... <cut> <cc>)
       (let ((data '()) (v (u-var!)) ...)
         (let ((lam (lambda (pr) 
                      (set! data (cons `(,(u-scm v) ...) data))
                      (u-abort pr))))                            
           (<with-prolog-environment> (<cut> lam)
              (<semi> code
                      (<code> (if (u-unify! l data)
                                  (<cc> <cut>)
                                  (u-abort <cut>)))))))))
    ((_ name (:vars v) (:args as ...) (:ret l) code)
     (define (name  as ... <cut> <cc>)
       (let ((data '()) (v (u-var!)))
         (let ((lam (lambda (pr) 
                      (set! data (cons (u-scm v) data))
                      (u-abort pr))))                            
           (<with-prolog-environment> (<cut> lam)
               (<semi> code
                      (<code> (if (u-unify! l data)
                                  (<cc> <cut>)
                                  (u-abort <cut>)))))))))))


(define <next> (lambda x x))

(define-syntax <ask>
  (syntax-rules ()
    ((_ code)
     (umatch #:tag <next> #:name '<ask> ()
             ((prompt=> (pr <next>)
                        (parse<> (pr <next> pr (lambda x #t)) code)))
             (#f)))))
            
(define-syntax <define>
  (syntax-rules ()
    ((_ (name a ...) code ...)
     (define (name <Cut> <CC> a ...)
       (<with-prolog-environment> 
           (<Cut> last <Cut> <CC>) 
         (<and> code ...))))))

(define *depth* (make-fluid))

(define-guile-log <match>
  (syntax-rules ()
    ((_ wc as ...)
     (match0 (+) wc as ...)
     #;(mk-syms 1 v () (find-last0 v wc (as ...))))))

(define-syntax match0
  (syntax-rules ()
    ((_ (m) wc #:mode m0 . l)
     (match0 (m0) wc . l))
    ((_ meta wc v . l)
     (mk-syms 1 v () (find-last0 v wc meta l)))))

(define-syntax <lambda>
  (syntax-rules ()
    ((_ (a ...) code ...)
     (lambda (<Cut> <CC> a ... )
       (<with-prolog-environment> (<Cut> last <Cut> <CC>) 
         (<and> code ...))))
    ((_ a code ...)
     (lambda (<Cut> <CC> . a)
       (<with-prolog-environment> (<Cut> last <Cut> <CC>) 
         (<and> code ...))))))


(define-guile-log <recur>
  (syntax-rules ()
    ((_ (cut q p cc) n ((w v) ...) code ...)
     (letrec ((n (lambda (pp cccc w ...)
                   (parse<> (pp q pp cccc) 
                     (<and> code ...)))))
       (n p cc v ...)))))


(define-syntax find-last0
  (syntax-rules ()
    ((_ args v wc me as)
     (mk-syms 11 as () (find-last args v () as wc me)))))

(define-guile-log <with-bt>
  (syntax-rules ()
    ((_ (cut q pp cc) (p) code ...)
     (let ((p pp))
       (<with-prolog-environment> (cut q pp cc)
         (<and> code ...))))))

(define-syntax find-last
  (lambda (x)
    (syntax-case x ()
      ((_ . l) 
       ;(pk `(find-last ,@(syntax->datum #'l)))
       (syntax (find-last* . l))))))

(define-syntax find-last*
  (lambda (x)
    (syntax-case x ()      
      ((_  pr       args       v ()   ((a)        ...) wc me)
       (syntax (find-last** me pr args v  ((a) ...)  ()   wc)))

      ((_  pr       args       v ()   ((a as ...) ...) wc me)
       (syntax (find-last* pr       args    v ((a) ...) 
                           ((  as ...) ...) wc me)))
      
      ((_  pr        args      v ((m ...) ...) ((e       ) ...) wc me)
       (syntax (find-last** me pr args v ((m ...) ...) () 
                            ((e) ...) () wc)))

      ((_ pr args v ((b ...  ) ...) ((a as ...) ... ) wc me)
       (syntax (find-last* pr args v ((b ... a) ...) 
                           ((as   ...) ... ) wc me))))))

(define-syntax find-last**
  (syntax-rules ()
    ((_ (m) (pr ...) args v ((a)) ((b) ...) (cut w p cc))
     (umatch #:mode m #:tag <next> #:name '<match> 
             ()
             ((prompt=> (pr <next>)
                        (<with-prolog-environment> (cut <next> pr cc) b)))
             ...
             ((<with-prolog-environment> (cut w p cc) a))))

    ((_ m pr args v (a aa ...) (b ...) wc)
     (find-last** m pr args v (aa ...) (b ... a) wc))


    ((_ (m) (pr ...) args v ((as ...)) ((aas ...)  ...) ((a)) ((b) ...)  
        (cut w p cc))
     (umatch #:mode m #:tag <next> #:name '<match>
             v
             (aas ... (prompt=> (pr <next>)
                        (<with-prolog-environment> (cut <next> pr cc) b)))
             ...
             
             (as ...  (<with-prolog-environment> (cut w p cc) a))))

    ((_ m pr args v ((as ...) (aas ...) ...) ((aass ...)  ...) ((a) (aa) ...) 
        ((b) ...)  wc)
     (find-last** m pr args v ((aas ...) ...) ((aass ...) ... (as ...)) 
                  ((aa) ...) ((b) ... (a)) wc))))

(define-guile-log <bag-of>
  (syntax-rules ()
    ((_ meta X Code L) (parse<> meta (bag-of/3 X (</.> () Code) L)))))

(define (bag-of/3 Cut CC X Lam L )
  (let ((Ret '()))
    (umatch #:tag w #:name 'bag-of () 
            ((prompt=> (pr w) 
               (u-call w Lam pr (lambda (pr2) 
                                  (set! Ret (cons (u-scm X) Ret)) 
                                  (u-abort pr2)))))
            ((begin
               (u-unify! L Ret)
               (CC Cut))))))

(define-guile-log <set-of>
  (syntax-rules ()
    ((_ meta X Code L) (parse<> meta (set-of/3 X (</.> () Code) L)))))

(define (set-of/3 X Lam L Cut CC)
  (let ((Ret '()))
    (umatch #:tag w #:name 'set-of () 
            ((prompt=> (pr w) 
               (u-call w Lam pr (lambda (pr2) 
                                  (set! Ret (lset-adjoin equal? Ret (u-scm X))) 
                                  (u-abort pr2)))))
            ((begin
               (u-unify! L Ret)
               (CC Cut))))))


(define (mark-once/2 X Y Cut CC)
  (let ((XX (u-scm X))
        (YY (u-scm Y)))
    (if (and (symbol? XX) (symbol? YY))
        (if (symbol-property XX YY)
            (u-abort Cut)
            (dynamic-wind
                (lambda x #t)
                (lambda () (set-symbol-property! XX YY #t))
                (lambda () (symbol-property-remove! XX YY))))
        (error "mark-once need two symbols to function correctly"))))

(define (mark/2 X Y Cut CC)
  (let ((XX (u-scm X))
        (YY (u-scm Y)))
    (if (and (symbol? XX) (symbol? YY))
        (if (symbol-property XX YY)
            (CC Cut)
            (dynamic-wind
                (lambda x #t)
                (lambda () (set-symbol-property! XX YY #t))
                (lambda () (symbol-property-remove! XX YY))))
        (error "mark-once need two symbols to function correctly"))))
          
(define-syntax let<>0
  (syntax-rules ()
    ((_ wc (m pat val) code)
     (umatch #:mode m #:name 'let<>/<=>/<==> (val)
             (pat (parse<> wc code  )) 
             (_   (parse<> wc <fail>))))))

(define-guile-log let<>
  (syntax-rules ()
    ((_ w (a b ...) code ...)
     (let<>0 w a (let<> (b ...) code ...)))
    ((_ w () code ...)
     (parse<> w (<and> code ...)))))

(define-syntax letify
  (lambda (x)
    (syntax-case x ()
      ((_ w ((m f) pat val) code ...)
       #`(let<>0 w (m #,(tr-pat #'pat) (f val)) code ...))

      ((_ w (m pat val) code ...)
       #`(let<>0 w (m #,(tr-pat #'pat) val) code ...)))))

(define (tr-pat x)
  (syntax-case x (quote unquote)
    ((quote   _) x)
    ((unquote _) x)
    ((x . l)     #`(#,(tr-pat #'x) #,@(tr-pat #'l)))
    (()          #'())
    ( x          #'(unquote x))))
        

;;TODO, this will unify with a cyclic check!
;;Not possible to use a pure raw form here
(define-guile-log <=>
  (syntax-rules ()
    ((_ wc X Y)
     (<=>q wc (u-unify! +) X Y))))
(log-code-macro '<=>)

(define-guile-log <==>
  (syntax-rules ()
    ((_ wc X Y)
     (<=>q wc (gp-m-unify! -) X Y))))
(log-code-macro '<==>)

(define-guile-log <=>q
  (syntax-rules (unquote quote _)

    ((_ wc q _   x)
     (parse<> wc <cc>))

    ((_ wc q x   _)
     (parse<> wc <cc>))

    ((_ wc q ()  ()) 
     (parse<> wc <cc>))
    
    ((_ wc q ()  X)
     (<=>q wc q '() X))

    ((_ wc q X   ())
     (<=>q wc q '() X))

    ((_ wc q (X) (Y))
     (<=>q wc q X Y))

    ;;unquote logic so that we can evaluate forms in the unification
    ((_ wc (unify m) (unquote X)  (unquote Y))
     (<when> wc (unify X Y) <cc>))

    ((_ wc (unify m) (unquote X)  (quote   Y))
     (<when> wc (unify X (quote Y)) <cc>))

    ((_ wc (u m)     (unquote X)  (Y . L)    )   
     (letify wc (m (Y . L) X) <cc>))

    ((_ wc (unify m) (unquote X)  Y          )
     (<when> wc (unify X Y) <cc>))

    ((_ wc q Y            (unquote X))   
     (<=>q wc q (unquote X) Y))

    ;; quote logic to protect quoted forms from destructioning
    ((_ wc (unify m) (quote X)  (quote Y))
     (<when> wc (unify (quote X) (quote Y)) <cc>))

    ((_ wc (u m)     (quote X)  (Y . L)    ) 
     (letify wc (m (Y . L) (quote X)) <cc>))

    ((_ wc (unify m) (quote X)  Y          )  
     (<when> wc (unify (quote X) Y) <cc>))

    ((_ wc q Y         (quote X))  
     (<=>q wc q (quote X) Y))


    ((_ wc q     (X . Lx) (Y . Ly))
     (parse<> wc (<and> (<=>q q X Y) (<=>q q Lx Ly))))

    ((_ wc (u m)     X        (Y . Ly))
     (letify wc (m (Y . Ly) X) <cc>))

    ((_ wc (u m)     (X . Lx)    Y)
     (letify wc (m (X . Lx) Y) <cc>))

    ((_ wc (unify m) X Y)
     (<when> wc (unify X Y) <cc>))))

(define-guile-log </.>
  (syntax-rules ()
    ((_ code ...) (<lambda> () code ...))))
 

(define (<unify> <Cut> <CC> X Y)
  (if (u-unify! X Y)
      (<CC> <Cut>)
      (u-abort <Cut>)))

(define (<apply> P CC F . L) (apply (u-scm F) `(,P ,CC ,@L)))

(define-syntax <def>
  (syntax-rules ()
    ((_ (f #:mode mode a ...) m ... (me ... codee))
     (<define> (f a ...)
         (<match> #:mode mode (a ...)
           m ... 
           (me ... (<cut> codee)))))

    ((_ (f a ...) m ... (me ... codee))
     (<define> (f a ...)
         (<match> (a ...)
           m ... 
           (me ... (<cut> codee)))))))

(define-syntax <<define>>
  (syntax-rules ()
    ((_ (#:mode mode f a ...) (m ... code) ...)
     (<define> (f a ...)
         (<match> #:mode mode (a ...)
            (m  ... (<cut> code)) ...)))

    ((_ (f a ...) (m ... code) ...)
     (<define> (f a ...)
         (<match> (a ...)
           (m  ... (<cut> code)) ...)))))

(include-from-path "language/prolog/interleave.scm")  			    
  

