(define-module (language prolog guile-log)
  #:use-module (system base compile)
  #:use-module (ice-9 match-phd)
  #:use-module (language prolog umatch)
  #:use-module (language prolog guile-log-pre)
  #:use-module (ice-9 pretty-print)
  #:use-module (srfi srfi-1)
  #:use-module (system repl repl)
  #:re-export  (u-cons u-abort u-var! u-scm u-unify! u-unify-raw!))

(define log-module
  (resolve-module
   '(language prolog guile-log)))

(include-from-path "language/prolog/macros.scm")
