(export gp-clear gp-unify! gp-unify-raw! gp-newframe gp-unwind gp-var! 
        gp->scm scm->gp gp-atom fast-match def 
        gp-print
        gp-budy gp-swap-to-a gp-swap-to-b gp-m-unify!
        fun umatch *gp-fi* g-member g-iright u-prompt
        u-abort u-set! u-var! u-call u-deref gp-atomic? 
        u-context u-modded
        u-unify! u-scm u-unify-raw! u-cons gp-lookup
        gp-var? gp-cons? gp-cons! let-alias gp-set! u-list
        gp-printer gp-var-number
        gp-car gp-cdr
        gp-store-state gp-restore-state
        gp-make-fluid gp-fluid-set! gp-fluid-ref with-gp-fluids u-dynwind
        <umatch> gp-copy)

(gp-module-init)

(use-modules (srfi srfi-11))

(define (get-line x u)
  (if (gp? x)
      (let ((x (gp-lookup x)))
        (if (and (gp? x) (gp-pair? x))
            (get-line (gp-cdr x) (cons (gp-car x) u))
            (if (null? x)
                (values (reverse u) '())
                (values (reverse u) x))))
      (values (reverse u) x)))


(define (gp-printer port x)
  (define (f l d)
    (if (eq? (length l) 1)
        (format port "<#gp {~{~a~}~a}>" l d)
        (format port "<#gp {~a ~{ ~a~}~a}>" (car l) (cdr l) d)))

  (let ((x (gp-lookup x)))
    (if (gp? x)
        (if (gp-pair? x)
            (let-values (((l d) (get-line x '())))              
              (if (null? x)
                  (f l "")
                  (f l (format #f " . ~a" d))))
            (let ((varn (gp-var-number x)))
              (format port "<#~a>" varn)))
        (format port "<#gp ~a>" x))))

(define gp-fluid-ref gp-lookup)

(define-syntax with-gp-fluids
  (syntax-rules ()
    ((_ ((f v) ...) code ...)
     (begin
       (u-set! f v)
       ...
       code 
       ...))))
        

(define-syntax u-dynwind
  (syntax-rules ()
    ((_ pre action post)
     (let ((p pre))
       (p)
       (gp-dynwind p post)
       (action)))))
