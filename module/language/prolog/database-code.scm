(export db-list define-db make-fpattern make-matcher-env <fmatch> <db-lookup>)
(use-modules (ice-9 match))
(use-modules (ice-9 pretty-print))

(define (pp x) (pretty-print (u-scm x)) x)

(define (var->pattern X)
  (define vars '((a a)))
  (define i 0)
  (define (get-name v)
    (let loop ((l vars))
      (umatch #:mode - (l)
        (((,v . n) . _)
         n)
        ((x)          
         (let ((n (string->symbol
                   (string-append 
                    "A" 
                    (number->string i)))))

           (set! i (+ i 1))
           (set-cdr! l (list (cons v n)))
           n))
        
        ((_ . l) 
         (loop l)))))


  (let loop ((X X))
    (umatch #:mode - (X)       
       ((X . L)  
        (cons (loop X) (loop L)))
       
       ((? gp-var? X) 
        (get-name X))
         
       (() '())

       (X        
        `(quote ,(gp-lookup X))))))
        

(define (mk-pat-check pat)
  (let ((pat (var->pattern pat)))
    `(,@pat <cc>)))

(define (mk-pat-del pat)
  (let ((pat (var->pattern pat)))
    `(,@pat (<cut>
             (<and>
              (<code>
               (Prev 'next Next)
               (Next 'prev Prev)
               (set! Prev #f)
               (set! Next #f))             
              <fail>)))))

(define (db-lambda n pats module)

  (define (db patc patd . args)
    (let ((___ (map (lambda x '_) args)))
      `(let ((Prev #f)
             (Next #f))
         (case-lambda
           ((s) 
            (case s
              ((next) Next)
              ((prev) Prev)))
       
           ((s f)
            (case s
              ((next) (set! Next f))
              ((prev) (set! Prev f))))
       
           ((p cc s ,@args)
            (case s
              ((check) 
               (<with-prolog-environment> 
                   (p last p cc) 
                 (<match> ,args
                   ,@patc
                   (,@___ (<cut> (Next 'check ,@args))))))

              ((delete)
               (<with-prolog-environment> 
                   (p last p cc) 
                 (<match> ,args
                   ,@patd
                   (,@___  (<cut> (Next 'delete ,@args))))))))))))

  (define (db* patc patd)
    `(let ((Prev #f)
           (Next #f))
       (case-lambda
         ((s) 
          (case s
            ((next) Next)
            ((prev) Prev)))

         ((s f)
          (case s
            ((next) (set! Next f))
            ((prev) (set! Prev f))))

         ((p cc s . l)
          (case s
            ((check) 
             (<with-prolog-environment> 
                 (p last p cc) 
               (<match> (l)
                 ,@patc
                 ( _    (<cut> (<apply> Next 'check l))))))

            ((delete)
             (<with-prolog-environment> 
                 (p last p cc) 
               (<match> (l)
                 ,@patd
                 (_     (<cut> (<apply> Next 'delete l)))))))))))

  (let ((patc  (map mk-pat-check pats))
        (patd  (map mk-pat-del   pats))
        (patc* (map mk-pat-check (map list pats)))
        (patd* (map mk-pat-del   (map list pats))))
    (compile
     (case n
       ((1) (db patc patd 'x))
       ((2) (db patc patd 'x 'y))
       ((3) (db patc patd 'x 'y 'z))
       ((4) (db patc patd 'x 'y 'z 'u))
       (else (db* patc* patd*)))
     #:env module)))

                      
(define (concat s w)
  (datum->syntax w
                 (string->symbol
                  (string-append                   
                   (symbol->string s)
                   (symbol->string
                    (syntax->datum w))))))
                

(define (mk-edge-lam)
  (let ((Tf #f))
    (case-lambda
      ((x)    Tf)
      ((x f)  (set! Tf f))
      ((p cc s . l)
       (case s
         ((check)  (u-abort p))
         ((delete) (cc p)))))))
         
(define (mk-asserta n Head DB Reset module)
  (define (g reset? p cc DB)
    (let ((fnext (Head 'next))
          (f     (db-lambda n DB module)))
      (f 'next fnext)
      (f 'prev Head)
      (Head 'next f)      
      (fnext 'prev f)
      (if reset? (Reset))
      (cc p)))
  
  (case-lambda 
    ((p cc)     
     (g #t p cc (cdr (reverse (DB)))))
    
    ((p cc . l) 
     (g #f p cc (list l)))))

(define (mk-assertz n Tail DB Reset module)
  (define (g reset? p cc DB)
    (let ((fprev (Tail 'prev))
          (f     (db-lambda n DB module)))
      (f 'next Tail)
      (f 'prev fprev)
      (fprev 'next f)
      (Tail  'prev f)
      (if reset? (Reset))
      (cc p)))

  (case-lambda 
    ((p cc)     
     (g #t p cc (cdr (reverse (DB)))))

    ((p cc . l) 
     (g #f p cc (list l)))))

;;This has to be a toplevel form
(define-syntax define-db
  (lambda (x)
    (syntax-case x ()
      ((_ x)
       #'(define-db x (current-module)))

      ((_ (n a ...) module)
       (with-syntax ((asserta (concat 'asserta- #'n))
                     (assertz (concat 'assertz- #'n))
                     (retract (concat 'retract- #'n))
                     (adda    (concat 'adda-    #'n))
                     (addz    (concat 'addz-    #'n))
                     (getlist (concat 'get-     #'n))
                     (N      (datum->syntax #'n (length #'(a ...)))))

         #'(begin
             (define n       #f)
             (define asserta #f)
             (define assertz #f)
             (define retract #f)
             (define getlist #f)
             (define adda    #f)
             (define addz    #f)
             (let* ((Head (mk-edge-lam))
                   (Tail (mk-edge-lam))
                   (DBA  '(tail))
                   (DBZ   DBA))

               (Head 'next Tail)
               (Tail 'prev Head)

               (set! getlist (lambda () DBZ))
               
               (set! n (lambda (p cc a ...)
                         ((Head 'next) p cc 'check a ...)))
               (set! asserta (mk-asserta 
                              N
                              Head
                              (lambda () DBA)
                              (lambda ()
                                (set! DBA (list 'tail))
                                (set! DBZ DBA))
                              module))

               (set! assertz (mk-assertz 
                              N
                              Tail
                              (lambda () DBA)
                              (lambda ()
                                (set! DBA (list tail))
                                (set! DBZ DBA))
                              module))
                     
                     
               (set! retract 
                     (lambda (p cc a ...)
                       ((Head 'next) p cc 'delete a ...)))
               (set! adda
                     (lambda (p cc . l)
                       (let ((x (list 'tail)))
                         (set-cdr! DBZ x)
                         (set-car! DBZ l)
                         (set! DBZ x)
                         (cc p))))

               (set! addz
                     (lambda (p cc . l)
                       (set! DBA (cons l DBA))
                       (cc p))))))))))


;; ====================================== advanced database interface =======================
;;

(define (make-dlink) #(#f #f 0 #f))


(define (dlink-car! dlink !?)
  (let ((r (vector-ref dlink 0)))
    (if r 
        r
        (if !?
            (let ((r (make-dlink)))
              (vector-set! dlink 0 r)
              r)
            #f))))

(define (dlink-cdr! dlink !?)
  (let ((r (vector-ref dlink 1)))
    (if r 
        r
        (if !?
            (let ((r (make-dlink)))
              (vector-set! dlink 1 r)
              r)
            #f))))
  
(define (dlink-vars dlink)
  (vector-ref dlink 2))

(define (get-nlist-from-atom! a dlink)
  (let* ((r (vector-ref dlink 3)))
    (if r
        (let ((x (assoc a r)))
          (if x
              x
              (let* ((y (cons (cons a 0) r)))
                (vector-set! dlink 3 y)
                (car y))))
        (let ((y (cons (cons a 0) '())))
          (vector-set! dlink 3 y)
          (car y)))))

(define (get-fs-from-atoms a dlink)
  (let ((r (vector-ref dlink 3)))
    (if r (let ((w (assoc a r))) (if w (cdr w) 0)))))
        
           
;; car cdr atoms vars
(define (add-linkage e f dlink)
  (match e
    ((x . l)
     (add-linkage x f (dlink-car! dlink #t))
     (add-linkage l f (dlink-cdr! dlink #t)))

    (() 
     'ok)

    ((? gp-var? v)
     (vector-set! dlink 2 (logior f (dlink-vars dlink))))

    (a
     (let ((w (get-nlist-from-atom! a dlink)))
       (set-cdr! w (logior (cdr w) f))))
    'ok))

(define (make-matcher-env)
  (let ((db (make-dlink))
        (n   1)
        (s  '()))
    (lambda x
      (match x
        (('add a l)
         (set! s (cons l s))
         (add-linkage a n db)
         (set! n (ash n 1))
         (if #f #f))

        (('db) 
         s)
        
        (('make)
         (let ((m (- n 1)))
           (define (g e f db)             
             (if db
                 (match e
                   ((x . l)
                    (logior
                     (g l (g x f (dlink-car! db #f)) (dlink-cdr! db #f))
                     (logand f (vector-ref db 2))))
                     
                    
                   ((? gp-var?)
                    f)

                   (() 
                    f)

                   (a 
                    (logior
                     (logand f (get-fs-from-atoms a db))
                     (logand f (vector-ref db 2)))))
                 f))
           (lambda (a)
             (let ((f (g a m db)))
               (let loop ((k (ash n -1)) (ret '()) (s s))
                 (if (= k 0)
                     ret
                     (if (= 0 (logand k f))                         
                         (loop (ash k -1) ret                (cdr s))
                         (loop (ash k -1) (cons (car s) ret) (cdr s)))))))))))))

(define (make-fpattern x v)
  (define vars '())
  (define (search-vars x)
    (umatch #:mode - (x)
       ((x . l)
        (begin
          (search-vars x)
          (search-vars l)))
       (x (let ((x (gp-lookup x)))
            (if (gp-var? x)
                (let ((a (assoc x vars)))
                  (if (not a)
                      (set! vars (cons (cons x (u-var!)) vars)))))))))
  
  (define (replace x)
    (umatch #:mode - (x)
       ((x . l)
        (cons (replace x) (replace l)))
       (x (let ((x (gp-lookup x)))
            (if (gp-var? x)
                (cdr (assoc x vars))
                x)))))

  (search-vars x)
  (search-vars v)  
  (list (map cdr vars) (replace x) (replace v)))

(define (<fmatch> p cc pat x y)
  (define (clean-up Fr)
    (gp-swap-to-b)
    (gp-unwind Fr)
    (gp-swap-to-a))

  (let ((w (map (lambda (x) (gp-var!)) (car pat))))
    (gp-swap-to-b)
    (let ((Fr (gp-newframe)))
      (let loop ((vs (car pat)) (w w))
        (match vs
          ((v . l)
           (u-set! v (car w))
           (loop l (cdr w)))
          (() 'ok)))
      (gp-swap-to-a)
      (if (gp-unify! (gp-copy (cadr pat)) x)
          (let ((ret (gp-copy (caddr pat))))
            (clean-up Fr)
            (if (gp-unify-raw! y ret)
                (cc p)
                (u-abort p)))
          (begin
            (clean-up Fr)
            (u-abort p))))))

(define (pp X)
  (pretty-print (u-scm X))
  X)

(define (db-list db x)
  (umatch #:mode - (db)
          ((db) (db x))
          (db   (db x))))
                      
(<define> (<db-lookup> db x y)
    (<match> (db)
      ((db)
       (<cut> (db-lookup (db x) x y)))
      (db
       (<cut> (db-lookup (db x) x y)))))

(<define> (db-lookup db x y)
    (<match> (db)
      ((pat   . l)  (<fmatch> pat x y))
      ((_     . l)  (<cut> (db-lookup l x y)))
      (()           (<cut> <fail>))))
