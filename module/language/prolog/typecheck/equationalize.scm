(define-module  (language prolog typecheck equationalize)
  #:use-module (language prolog typecheck register)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (ice-9 pretty-print)  
  #:use-module (ice-9 match-phd)  
  #:use-module (language prolog umatch-ffi)
  #:use-module (ice-9 receive)
  #:export (equationalize check solve-var? gensym-them add-var-points))

;; the main function is equationalize, which takes a format that should be 
;; derived from  tree-il representation with suitable tags to be able to 
;; communicate to the compiler about type-checking conclusions. For know it's 
;; just a toy translator. The idea is to translate this code to a set of 
;; equations that later on is fed into the typechecker and is a good starting 
;; point to see the type structure
;;
;; (equationalize Bindings Equations Code Outer-type)
;; To get the equations of (if 1 1 1) you would write 
;; (recieve (Bind Eq) (equationalize '() '() '(if 1 1 1) 'T-outer) ...)
;;
;; the language have just a few functions implemented for illustration
;; (number? X),(integer? X), (symbol? X), (char? X), (string? X)., (boolean? X).
;; plain simple (if X Y Z) (let Var Val Code) (lambda (x ...) code) is 
;; available as well. functions application should be written as 
;;
;;   (apply symbol? X).
;;
;; and boolean or and not is supported as is because they are special
;; 
;; So currently not all that exciting but it is very instructive to play 
;; with a few expressions an look att the corresponding equations that 
;; results. esiest is to use the check function like (check Code) e.g. 
;;
;;    (check `(if 1 1 1)) which produces the type equation,
;;
;;(if 1 1 1)
;;((or ((= 1 boolean) (= integer Texp))
;;     ((not (= 1 boolean)) (= integer Texp))))

;;Have a play!

#|
Add var points in order to be able to change the type value for
code verifying points
|#
(define (add-var-points x)
  (let ((f (lambda (loop loop2 l)
             (umatch #:mode - (l)
                     ((x . l)
                      (cons (loop x) (loop2 l)))
                     (() '())))))
    (let loop ((x x))    
      (umatch #:mode - (x)
              ((x . l)
               (let ((ret (u-var!)))
                 (u-var!) ;;the budy part
                 (u-set! ret
                         (cons (loop x)
                               (let loop2 ((l l))
                                 (f loop loop2 l))))
                 ret))
              (x (let ((ret (u-var!)))
                   (u-var!)  ;;  The budy part is allocated!
                   (u-set! ret x)
                   ret))))))

(define (pp X)       (begin (pretty-print (u-scm X)) X))
(define union  (lambda (x y) (lset-union eq? x y)))

;;**************************** PRELUDE TO TYPECHECKING ***********************  

(define (solve-var? x)
  (and (symbol? x)
       (char-upper-case? (car (string->list (symbol->string x))))))

;;Takes a spesification and makes a representation of them
(define (gensym-them F X)
  (define pairs '())
  (define (find-gensym Pairs X)
    (match Pairs
       (((,X S). L) S)
       ((_     . L) (find-gensym L X))
       (()          (let ((S (F)))
                      (set! pairs (cons (list X S) pairs))
                      S))))
 
  (define (f X)
    (match X
       ((X . L)          (cons (f X) (f L)))
       ((? solve-var? X) (find-gensym pairs X))
       (X   
             X)))
  (let ((R (f X)))
    (values (cons 'arg (car R)) (cadr R))))

    
(define (mk-lambda-sign P)
  (lambda ()
    (gensym-them (lambda () (u-var!)) P)))
    
(define (compute-type Const Code)  
  (define (type? X) 
    (and (symbol? X) 
         (char-upper-case? 
          (car (string->list (symbol->string X))))))

  (define (gen-map Code Bind Lam)
    (match Code
           ([X . L]     (gen-map X Bind (lambda (Bind) (gen-map L Bind Lam))))
           ((? type? X) (if (member X Bind)
                            (Lam Bind)
                            (let ((T (Const)))
                              (Lam (cons (cons X T) Bind)))))
           (_           (Lam Bind))))

  (define (make-rep X Bind)
    (match X
           ([X . L]  (cons (make-rep X Bind) (make-rep L Bind)))
           ([]       '[])
           (X        (let ((R (assoc-ref Bind X)))
                       (if R R X)))))
           
  (make-rep Code (gen-map Code '() (lambda (x) x))))


#;((Tlam)             (values 2))
#;((SignF)            (values 2))

;;typecheck will create a list of equation that defines the rules for the
;; predicates, the rules are
;; [= A B]           A and B should unify
;; [and A B] [A B]   A holds and B holds
;; [or A B]          A holds or B holds
;; [not A]           A is not true
;; [arg A]           A is an argument variable
;; [res A]           A is the result of a function
;;  e                Accepts everything
(define (equationalize Bind Eq Expr Tp)
  ;(pp `(,Expr : ,Tp))
  
  (define (handle-case-lambda x es ls)
    (match x
      (() 
       (values 
        Bind
        `((= (or ,@(reverse ls)) ,Tp)
          ,@es)))

      ((lam . lams)
       (let-values (((Bind2 Eq2) (equationalize Bind '() lam Tp)))
         (match Eq2
           ((A ('= Lam Qp))
            (handle-case-lambda lams (cons A es) (cons Lam ls))))))))

  (define (handle-letrec Bind2 Defs Code Ds Qs)
    (match Defs
      (((F Lam) . Defs)
       (let-values (((B E) (equationalize Bind2 '() Lam F)))
         
         (match E
           ((D . Q) 
            (handle-letrec  Bind2 Defs Code (cons D Ds) (cons Q Qs))))))
      (()
       (let-values (((B E) (equationalize Bind2 '() Code Tp)))
         (values Bind
                 `(,@E
                   ,@(reverse Ds)
                   ,@(reverse Qs)
                   ,@Eq))))))

  (match Expr
    (['let X V Code]   
     (let ((T (gensym "Let")))
       (let-values (((Bind Eq) (equationalize Bind Eq V T)))         
         (equationalize (cons X Bind) 
                        `((= (def ,X) (res ,T))
                          ,@Eq)
                        Code Tp))))


    ;;to ease deduction lambdas must be typed
    
    (['lambda ((A ': Ta) ...) ': T Code]     
     (let*-values
         (((SignF)            (mk-lambda-sign (list Ta T)))
          ((Ta2 T2)           (gensym-them (lambda () (gensym "A"))
                                           (list Ta T)))
          ((Bind-lam Eq-lam)  (equationalize (append A Bind)
                                             '() Code T2)))
       (values Bind 
               (cons `(,@(map (lambda (x y) 
                                (list '= (list 'def x) y)) 
                              A (cdr Ta2))
                       ,@(reverse Eq-lam)
                       )
                     (cons `(= (lambda ,SignF) ,Tp)
                           Eq)))))

    (['case-lambda Lams]
     (handle-case-lambda Lams Eq '()))

    (['letrec Fs Defs Code]
     (handle-letrec (append Fs Bind) Defs Code '() '()))
 
    (['if P X Y] 
     (let ((F           (equationalize-bool Bind '() P))) 
       (let*-values          
          (((Bind1 Eq1) (F #t))
           ((Bind0 Eq0) (F #f))
           ((Bind1 Eq1) (equationalize Bind1 Eq1 X Tp))
           ((Bind0 Eq0) (equationalize Bind0 Eq0 Y Tp)))

         (values (union Bind0 Bind1) 
                 (append `((or ,(reverse Eq1) ,(reverse Eq0))) Eq)))))


    
    (['apply F A]
     (let*-values
         (((Ta)      (map (lambda x (gensym "Targ")) A))
          ((Tr)      (gensym "Tres"))
          ((Bind Eq) (equationalize Bind Eq F `[def [lambda (arg ,@Ta) ,Tr]]))
          ((Bind Eq) (equationalize-arg Bind Eq A Ta)))
       
       (values Bind (cons `[= [res ,Tr] ,Tp] Eq))))

    (['@@ Pth S]
     (let ((Ts (get-fkn-sign (resolve-module Pth) S)))
       (values Bind
               (cons `(= ,(compute-type 
                           (lambda () (gensym "T")) Ts) ,Tp)
                     Eq))))
    (['begin A]        
     (equationalize Bind Eq A Tp))
    
    (['begin A . L]    
     (let-values (((Bind Eq)  (equationalize Bind Eq A (gensym "A"))))
       (equationalize Bind Eq (cons 'begin L) Tp)))

    (['set! X Y]
     (let ((T (gensym "T")))
       (let-values (((B E)  (equationalize Bind '() Y T)))
         (values Bind
                 `((= [res void] ,Tp)
                   (= ,X [res ,T])
                   ,@E ,@Eq)))))

    (['type Te Code]
     (let ((T (gensym "T")))
       (let-values (((B E) (equationalize Bind Eq Code T)))
         (values Bind
                 `((= [res ,Te] ,Tp)
                   (= [res ,T ] ,Te)
                   ,@E)))))

    ((? symbol? X)
     (if (member X Bind)
         (values Bind (cons `(= [res ,X] ,Tp) Eq))
         (let ((Ts (get-fkn-sign (current-module) X)))
           (if Ts
               (values Bind 
                       (cons `(= ,(compute-type 
                                   (lambda () (gensym "T")) Ts) ,Tp)
                             Eq))
               (values Bind 
                       (cons `(=  e ,Tp) 
                             Eq))))))
         

    (('quote ())
     (values Bind (cons `(= (res nil)       ,Tp)   Eq)))
    (('quote (? symbol?))    
     (values Bind (cons `(= (res symbol)    ,Tp)   Eq)))
    ((? integer?)            
     (values Bind (cons `(= (res integer)   ,Tp)   Eq)))
    ((? boolean?)            
     (values Bind (cons `(= (res boolean)   ,Tp)   Eq)))
    ((? number? )            
     (values Bind (cons `(= (res number)    ,Tp)   Eq)))
    ((? char?   )            
     (values Bind (cons `(= (res character) ,Tp)   Eq)))
    ((? string? )            
     (values Bind (cons `(= (res string)    ,Tp)   Eq)))))

(define (check X Texp)
  (let*-values 
      (((Ba Ea)   (equationalize '() '() X Texp)))
    (if *type-trace* (pp `(check ,X)))
    (let ((res (reverse Ea)))
      (if *type-trace*
          (begin
            (pp '----------------------------------------------------------)
            (pp res)))
      res)))

(define (equationalize-arg Bind Eq Xs Tps)
  (match `(,Xs ,Tps)

         (([X . Xs] [Tp . Tps])   
          (let*-values 
              (((Bind Eq)  (equationalize Bind Eq X (list 'arg Tp))))
            (equationalize-arg Bind Eq Xs Tps)))

         (([]       []        )   (values Bind Eq))))


  

(define (equationalize-bool Bind Eq Expr)
  (define (pred? X)
    (define (f L)
      (if (pair? L)
          (if (eq? X (caar L))
              #t
              (f (cdr L)))
          #f))
    (f *predtypes*))

  (define (type-of-pred X)
    (define (f L)
      (if (pair? L)
          (if (eq? X (caar L))
              ((cadar L))
              (f (cdr L)))
          #f))
    (f *predtypes*))

  ;;(pk Expr)
  (match Expr
    (['and X Y]   
     (let ((F1 (equationalize-bool Bind Eq X))
           (F2 (equationalize-bool Bind Eq Y)))
       (lambda (X) 
         (if X 
             (let*-values
                 (((Bind1 Eq1) (F1 #t))
                  ((Bind2 Eq2) (F2 #t)))
               (values (union Bind1 Bind2) `(and ,Eq1 ,Eq2)))
             (let*-values
                 (((Bind1 Eq1) (F1 #f))
                  ((Bind2 Eq2) (F2 #f)))
               (values (union Bind1 Bind2) `(or  ,Eq1 ,Eq2)))))))



    (['or X Y]    
     (let ((F1 (equationalize-bool Bind Eq X))
           (F2 (equationalize-bool Bind Eq Y)))
       (lambda (X) 
         (if X 
             (let*-values 
                 (((Bind1 Eq1) (F1 #t))
                  ((Bind2 Eq2) (F2 #t)))
               (values (union Bind1 Bind2) `(or ,Eq1 ,Eq2)))
             (let*-values 
                 (((Bind1 Eq1) (F1 #f))
                  ((Bind2 Eq2) (F2 #f)))
               (values (union Bind1 Bind2) `(and  ,Eq1 ,Eq2)))))))



    (['not  X ]   
     (let ((F (equationalize-bool Bind Eq X)))
       (lambda (X)
         (if X (F #f) (F #t)))))

    (['apply (? pred? String?) (X)] 
     (lambda (P)
       (if P 
           (equationalize Bind Eq X 
                          `(validated ,(type-of-pred String?)))
           (equationalize Bind Eq X (gensym "A")
                          #;`(validated (not ,(type-of-pred String?)))))))



    (X                     
     (lambda (P)
       (if P
           (equationalize Bind Eq X 'boolean)
           (equationalize Bind Eq X 'boolean))))))

