(define-module (language prolog  typecheck solver        )
  #:use-module (language prolog  typecheck equationalize )
  #:use-module (language prolog  typecheck register      )
  #:use-module (language prolog  guile-log-ffi               )
  #:use-module (language prolog  umatch-ffi                  )
  #:use-module (language tree-il match-tree-il           )
  #:use-module (srfi     srfi-1                          )
  #:use-module (srfi     srfi-11                         )
  #:use-module (ice-9    pretty-print                    )  
  #:use-module (ice-9    match-phd                       )  
  #:use-module (ice-9    receive                         )
  #:duplicates (first)
  #:export (type-solver code->solver 
                        add-subtype-relation=> 
                        add-subtype-relation<=> 
                        define-parameter-type
                        ))

#|
Type solver utility, 
and - types
or  - types
not - types
subtypes
parameter types
recursive types
|#

;;

(define old-solve-var? solve-var?)
(define (solve-var? x) (old-solve-var? (gp-lookup x)))

(define *res-vars* '())
(define var? (lambda (x) (gp-var? (gp-lookup x))))

;; ******************************** TRACE FUNCTIONALITY ******************
(define (pr . L)
  (define (s M)
    (let loop ((S "") (M M))
      (if (= M 0)
          S
          (loop (string-append " " S) (- M 1)))))
  (if *type-trace*
      (match (my-scm L)
        ((M N X Y)  
         (begin
           (format #t "~a~10a  ~1a ~1a~%~a~a~%~a~a~%~%" 
                   (s M) N "" "" (s M) X (s M) Y)))
        ((M N E X Y)  
         (begin
           (format #t "~a~10a  ~1a ~1a~%~a~a~%~a~a~%~%" 
                   (s M) N "" E (s M) X (s M) Y)))
        ((M N Ex Ey X Y)  
         (begin
           (format #t "~a~10a  ~1a ~1a~%~a~a~%~a~a~%~%" 
                   (s M) N Ex Ey (s M) X (s M) Y))))))
;-----------------------------------------------------------------------

;; => points in the direction of a more general type
(define *sub-type-relation=>* '()) 
(define *sub-type-relation<=* '()) 

(define (homogenize x)
  (let* ((r (gp-newframe))
         (ret (my-scm (sym->var x))))
    (gp-unwind r)
    ret))
    

(define (fasr a b li)
  (match (homogenize
          (match (cons a b)
            (((a . args) . b)   `(,a ,args ,b))
            ((a          . b)   `(,a #f    ,b))))
    ((a arg b)
     (let loop ((x li))
       (match x
         (((,a ,arg . l) . u)
          `((,a ,arg ,b ,@l) ,@u))
         (( x       . u)
          (cons x (loop u)))
         (()
          (list (list a arg b))))))))

(define (add-subtype-relation=> a b)
  (format #t "(~a) is a (~a)~%" a b)
  (set!  *sub-type-relation=>* (fasr a b *sub-type-relation=>*))
  (set!  *sub-type-relation<=* (fasr b a *sub-type-relation<=*)))

(define (add-subtype-relation<=> a b)
  (format #t "(~a) alias (~a)~%" a b)
  (add-subtype-relation=> a b)
  (add-subtype-relation=> b a))

(define (macro-relation-> a b)
  (format #t "(~a) transform into (~a)~%" a b)
  (set!  *sub-type-relation=>* (fasr a b *sub-type-relation=>*))
  (set!  *sub-type-relation<=* (fasr a b *sub-type-relation<=*)))

(define-syntax define-parameter-type
  (syntax-rules ()
    ((_ n v)  (macro-relation-> 'n 'v))))

(define (prepare-subtype)
  (define (clean x s)
    (match x
      (((y . l) . u) 
       (set-symbol-property! y s '())
       (clean u s))
      (() 'ok)))
  
  (define (add x s)
    (match x
      (((x . l) . u)
       (set-symbol-property! x s (sym->var l))
       (add u s))
      (() '())))

  (clean *sub-type-relation=>* '=>)
  (clean *sub-type-relation<=* '<=)
  (add   *sub-type-relation=>* '=>)
  (add   *sub-type-relation<=* '<=))
       
       
(<define> (<forall> Z L N Id Cut)
  ;;(<when> (begin (pp (my-scm `(forall ,N ,Id : ,L))) #t))
  (<match> #:mode - (L)
           ((A . B)  
            (<let> ((Fr (b-frame)))
                   (b-page Fr
                           (</.>
                               (<cut> 
                                (<or> 
                                 (<=> A Z)
                                 (<and> 
                                  (<when> (begin (b-unwind Fr) #t)
                                          (<forall> Z B (+ N 1) Id Cut)))))))))
           (_        
            (<fail> Cut))))


(define (sub* q)
  (lambda (Pr CC X Z Cut)
    (define (f)
      (umatch #:mode - (X)
              ((? var?)  (u-abort Pr))
              ((X . A)   (cons X  A))
              (X         (cons X #f))))
    
    (let* ((x.A (f))
           (x   (car x.A))
           (a   (cdr x.A))
           (F   (lambda (G l L)
                  (umatch #:mode + ((sym->var l))
                          ((,a . U)
                           (add-var-points U))
                          (_ 
                           (G L)))))

           (l   (let loop  ((l (q)))
                  (match l
                    ((X . L)
                     (umatch #:mode - (X)
                             ((,x . l)
                              (F loop l L))
                             (_ (loop L))))

                    ((_ . L)  
                     (loop L))
  
                    (()                 
                     (u-abort Pr))))))
      (<forall> Pr CC Z l 0 (gensym "id") Cut))))

(define (s-t-=>) *sub-type-relation=>*)
(define (s-t-<=) *sub-type-relation<=*)

(define sub=> (sub* s-t-=>))
(define sub<= (sub* s-t-<=))


(<define> (<subtype-unify-ar> F Ex Ey X Y)
    (<match> #:mode - (X Y)
             (('or . L) _  (<cut> (F Ex Ey X Y)))
             (_ ('and . L) (<cut> (F Ex Ey X Y)))
             (_ _          (<cut> (<subtype-unify-ar0> F Ex Ey X Y)))))

(<define> (<subtype-unify-ar0> F Ex Ey X Y)
  (<when> (begin (pr 6 'subtype X Y) #t))
  (<var> (Z)
    (<let> ((Fr (b-frame)))
       (b-page Fr 
         (</.>
             (<with-bt> (Cut)    ;; If sub<= succeeds then a Cut will skip 
                                 ;; the last part in the <or> below
                (<or> (F Ex Ey X Y)        
                      (<and> 
                       (<when> (begin (b-unwind Fr) #t))
                       (sub<= X Z Cut)
                       (<when> (begin 
                                 (pr 6 'subtype<= Z Y)
                                 (if (gp-fluid-ref *validated*) 
                                     (u-set! X Z)) 
                                 #t))
                       (unify-rec
                        (lambda (pr cc x y)
                          (<subtype-unify-ar> 
                           pr cc %a=r% Ex Ey x y))
                        Z 
                        Y))

                      (<and> 
                       (<when> (begin (b-unwind Fr) #t))
                       (sub=> Y Z Cut) 
                       (<when> (begin 
                                 (pr 6 'subtype=> X Z)
                                 (if (gp-fluid-ref *validated*) 
                                     (u-set! Y Z)) 
                                 #t))
                       (unify-rec
                        (lambda (pr cc x y)
                          (<subtype-unify-ar> 
                           pr cc %a=r% Ex Ey x y))
                        X
                        Z)))))))))


;; ***** printing utility ******
(define (my-scm X)
  (define *A* '(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z))
  (define *vars* '())
  (define *syms* '())
    
  (define (get-var X V S)   
    (define (f? Y) (eq? (gp-lookup X) (gp-lookup Y)))

    ;;(pp 'get-var)
    (umatch #:mode - (V)
            (()            (begin
                             (if (null? *A*) 
                                 (set! *A* (map (lambda x (gensym "A")) 
                                                '(1 1 1 1 1 1 1 1 1))))
                             (set! *vars* (cons X *vars*))
                             (set! *syms* (cons (car *A*) *syms*))
                             (set! *A*    (cdr *A*))
                             (car *syms*)))
            ((Y  . L)       (if (f? Y)
                                (car S)
                                (get-var X L (cdr S))))))

  (define (f X)
    ;;(pp `(get-var.f ,X))
    (umatch #:mode - (X)       
       ((X . L)       (cons (f X) (f L)))
       ((? var? X) (get-var X *vars* *syms*))
       (X             (u-scm X))))
  (f X))


;; ************************* B-bank helpers **************
(define *u1* #f)
(define *u2* #f)

(define (swap-u1-u2 Pr CC)
  (u-dynwind
    (lambda () #t)
    (lambda () 
      (let ((r *u2*))
        (set! *u2* *u1*)
        (set! *u1* r)
        (Pr CC)))
    (lambda x
      (let ((r *u2*))
        (set! *u2* *u1*)
        (set! *u1* r)))))

(define (b-frame)
  (gp-swap-to-b)
  (let ((ret (gp-newframe)))
    (gp-swap-to-a)
    ret))

(define (b-unwind K)
  (gp-swap-to-b)
  (gp-unwind K)
  (gp-swap-to-a))
 
(define (b-cons X Y)
  (gp-swap-to-b)
  (let ((r (u-cons X Y)))
    (gp-swap-to-a)
    r))

(define (b-page Pr CC Fr F)
  (u-dynwind
   (lambda x  #t)
   (lambda () (F Pr CC))
   (lambda x  (b-unwind Fr))))

;****************************************************************''
;; *********** Recursive datatypes *****************

(define (sync-budy-data Cold Cnew)
  (define (sync-budy-data-arg Lx Ly)
    (umatch #:mode - (Lx Ly)
      ((X . Lx) (Y . Ly)
       (begin
         (f X Y)
         (sync-budy-data-arg Lx Ly)))
      (() () #t)))

  (define (f Cold Cnew)
    (umatch #:mode - (Cold Cnew)
            ((X . Lx) (Y . Ly)
             (let ((Bx (gp-budy Cold))
                   (By (gp-budy Cnew)))
               (u-set! By Bx)
               (sync-budy-data-arg Lx Ly)))
            
            (()       ()
             #t)
            
            (_ _
               (let ((Bx (gp-budy Cold))
                     (By (gp-budy Cnew)))
                 (u-set! By Bx)
                 #t))))

  (gp-swap-to-b)
  (f Cold Cnew)
  (gp-swap-to-a))

     
#|
There is a special matcher to find rec patterns
(rec r (A) (or (f1 A) (f2 A)))
(rec r (a) (or (f2 a) (f2 a)))
|#


(define (rec-match Pat X)
  (define vars *res-vars*)
  (define (f Pat X)
    ;(pp (my-scm `(match ,Pat ,X)))
    (umatch #:mode - (Pat X)
       ((X . Lx)   (Y . Ly)
        (and (f X Y) (f Lx Ly)))
       ((? var? X) (? var? Y)
        (let ((X (gp-lookup X))
              (Y (gp-lookup Y)))
          (if (member Y vars)
              (eq? X Y)
              (begin
                (if (not (eq? Y X))
                    (u-set! Y X))
                (set! vars (cons X vars))
                #t))))
       (X X #t)
       (_ _ #f)))

  (let* ((Fr  (gp-newframe))
         (Ret (f Pat X)))
    (gp-unwind Fr)
    Ret))
   
                  
(define (equal-match X Y)
  (umatch #:mode - (X Y)
          ((X . Lx) (Y . Ly)
           (and (equal-match X Y)
                (equal-match Lx Ly)))
          (X X #t)
          (_ _ #f)))

(define (add-rec rec A C L CC)  
  (define (f l)
    (umatch #:mode - (l)
            (((R Ar Cr) . l)
             (begin
               (if (equal-match `(,R ,Ar) `(,rec ,A))
                   (begin
                     (sync-budy-data Cr C)
                     (CC C))
                   (begin
                     (f l)))))

            (V 
             (begin
               (gp-swap-to-b)
               (let ((Y (add-var-points 
                         (gp-cp 
                          (list rec A C)))))
                 (u-set! (gp-lookup V)
                         (cons Y (u-var!)))
                 (gp-swap-to-a)
                 (CC C))))))
  (f L))

(define *tag->budy* #f)
(define (budy-it X)
  (umatch #:mode - (X)
          (('or  . _)  X)
          (('and . _)  X)
          (('not _  )  X)
          (_           
           `(budy ,X ,(gp-budy X)))))
                     

(define (gp-cp x)
  (umatch #:mode - (x)
     ((x . l) (u-cons (gp-cp x) (gp-cp l)))
     (x       (gp-lookup x))))
                            
          
;; recursive budies algorithm
(define (unify-rec Pr CC F X Y)
  (define (memberR R A Z)
    (define (f Z)
      ;;(pp (my-scm  `(memberR ,Z)))
      (umatch #:mode - (Z)
              ((R2 A2 . L) 
               (if (equal-match `(,R ,A) `(,R2 ,A2))
                   #t
                   (f L)))

              (V           
               (begin
                 (gp-swap-to-b)
                 (u-set! (gp-lookup V) 
                         (u-cons (gp-cp R) 
                                 (u-cons (gp-cp A) 
                                         (u-var!))))
                 (gp-swap-to-a)
                 #f))))
    (f Z))
  
  (define (budy X Zx)
    (umatch #:mode - ((budy-it Y))
            ((budy ('rec R A C) Zr)
             (begin
               ;;(pp (my-scm `(rec: ,R ,A budy: ,X ,Zx)))
               (u-set! Y C)
               (if (memberR R A Zx) 
                   (CC Pr))
               (add-rec 
                R A C *u2*
                (lambda (C)
                  (unify-rec Pr CC F X C)))))

            (('budy Y Zy)
             (F Pr CC X Y))
            
            (_ 
             (F Pr CC X Y))))
  
  (define (rec R Zr A C)
    (umatch #:mode - ((budy-it Y))
            (('budy ('rec Ry Ay Cy) Zry)
             (begin
               (u-set! Y Cy)
               (if (memberR R A Zry)
                   (CC Pr))
               (if (memberR Ry Ay Zr)
                   (CC Pr))
               
               (add-rec 
                R A C *u1*
                (lambda (C)
                  (add-rec 
                   Ry Ay Cy *u2*
                   (lambda (Cy)
                     (unify-rec Pr CC F 
                                C Cy)))))))

            (('budy Y Zy)
             (begin
               ;;(pp (my-scm `(rec: ,R ,A budy: ,Y ,Zy)))
               (if (memberR R A Zy)
                   (CC Pr))
               (add-rec 
                R A C *u1*
                (lambda (C)
                  (unify-rec Pr CC F C Y)))))
            
            ((H . L) (F Pr CC X Y))))

  (pr 4 'unify-rec X Y)

  (umatch #:mode - ((budy-it X))
            (('budy ('rec R A C) Zr)
             (begin
               (u-set! X C)
               (rec R Zr A C)))

            (('budy X Zx)
             (budy X Zx))
            
            (_ (F Pr CC X Y))))
         




(define (clean-lambda x)
  (define (f x)
    (umatch #:mode - (x)
      (('lambda x) (let ((x (u-scm x)))
                     (if (procedure? x)       
                         (let-values (((Args Res) (x)))
                           (f `(lambda ,Args ,Res)))
                         x)))
      ((x     . l) (cons (f x) (f l)))
      (x           x)))
  (f x))

(define *branch-prompt* (gp-make-fluid))

(define (must-success Pr CC F Error)
  (umatch  #:tag w ()
          ( (prompt=> (Pr2 w) (u-call w F Pr2 (lambda (Pr3) (CC Pr)))) )
          ( (Error) )))

(define *branch-list* '())
(define (id x) x)
(define (branch Pr CC F1 F2)
  ;;br-id get the values of the curent undo list
  (let ((br-id (if (null? *branch-list*)
                   1
                   (let ((r (car *branch-list*)))
                     (set! *branch-list* (cdr *branch-list*))
                     r))))
    (u-dynwind
        (lambda () #f)
        (lambda ()
          (match br-id
            (2  (F2 Pr CC))
            (1  ((id (umatch  #:tag w ()
                              ( (prompt=> (Pr2 w) 
                                  (with-gp-fluids ((*branch-prompt* Pr2))
                                                  (F1 Pr CC))))
                              ( (begin 
                                  (set! br-id 2)
                                  (set! *branch-list* '())
                                  (F2 Pr CC)))))))))
        (lambda ()
          (set! *branch-list* (cons br-id *branch-list*))))))

(<define> (branch2 F1 F2)
    (<or> (F1) (F2)))
    
(define (sym->var Code)
  (define *symbols* '())
  (define *vars*    #f)

  (define (find-var X)
    (match X
           ((X . L)
            (begin 
              (find-var X)
              (find-var L)))

           ((? solve-var? X) 
            (if (not (member X *symbols*))
                (begin
                  (set! *symbols* (cons X        *symbols*))
                  (set! *vars*    (cons (u-var!) *vars*)))))
           
           (_          'ok)))
    
  (define (get-var X S Z)
    (match S
           ((,X . L) (car Z))
           (( _ . L) (get-var X L (cdr Z)))
           (()       (error "did not find var!"))))

  (define (subst X)
    (match X
      ((X . L)           (cons (subst X) (subst L)))
      ((? solve-var? X)  (get-var X *symbols* *vars*))
      (X                 X)))
  
  (find-var Code)
  (subst    Code))

(define (code->solver Code)
  (define *symbols* '())
  (define *vars*    #f)

  (define (find-var X)
    (match X
           ((X . L)
            (begin 
              (find-var X)
              (find-var L)))

           ((? solve-var? X) 
            (if (not (member X *symbols*))
                (set! *symbols* (cons X *symbols*))))

           (_          'ok)))
    
  (define (get-var X S Z)
    (match S
           ((,X . L) (car Z))
           (( _ . L) (get-var X L (cdr Z)))
           (()       (error "did not find var!"))))

  (define (subst X)
    (match X
           ((X . L)           (cons (subst X) (subst L)))
           ((? solve-var? X)  (get-var X *symbols* *vars*))
           (X                 X)))
  
  (set! *branch-list* '())
  
  ;; clear prolog variable stacks and setup environment

  ;; A bank
  (gp-clear)
  (set! *produced-var* (u-var!))

  ;; B bank
  (gp-swap-to-b)
  (gp-clear)
  (set! *u1*           (u-var!))
  (set! *u2*           (u-var!))
  (gp-swap-to-a)

  ;; enter realations into the database to be able to work with parametrized
  ;; and subtypes

  (prepare-subtype)
  
  (let ((A    (check Code 'Tres))
        (ret '()))
    (find-var A)
    (set! *vars* (map (lambda x (u-var!)) *symbols*))
    ;(pp *symbols*)
    ;(pp (my-scm *vars*))
    (let ((ret '()) (i 0))
      (umatch #:tag w ()
              ((prompt=> (Pr w)
                 (gp-fluid-set! *branch-prompt* Pr)
                 (<eval> (r)
                   ;;code
                   (type-solver 1 (add-var-points (subst A)))

                   ;;cc
                   (lambda (Pr)  
                     (if (< i 10000)
                         (begin
                           (set! i   (+ i 1))
                           (set! ret (cons
                                      (my-scm
                                       (clean-lambda
                                        (get-var 'Tres *symbols* *vars*)))
                                      ret))

                           (u-abort (gp-fluid-ref *branch-prompt*)))
                         (cons i ret)))

                   ;;fail
                   (lambda () 
                     (format
                      #t 
                      "to high complexity, check not able to finish~%")
                     (cons i ret)))))
              ((cons i ret))))))
    

(define (pp X)       (begin (pretty-print X) X))
(define union  (lambda (x y) (lset-union        eq? x y)))

;;So this is the initial solver for the code taking care of outer boolean logic
;;and translating it via a backtracking mechanism.


(<define> (type-solver E X)
   ;(<pp> `(type-solver ,E ,X))
   (<match> #:mode - (E X)              
       (-1 ['and     X Y]  (<cut> (branch (</.> (type-solver -1 X))
                                          (</.> (type-solver -1 Y)))))
       ( 1 ['and     X Y]  (<cut> (<and> (type-solver  1 X) 
                                         (type-solver  1 Y))))
       (-1 ['or      X Y]  (<cut> (<and> (type-solver -1 X) 
                                         (type-solver -1 Y))))
       
       ( 1 ['or      X Y]  (<cut> (branch (</.> (type-solver  1 X)) 
                                          (</.> (type-solver  1 Y)))))
       (-1 ['not     X  ]  (<cut> (type-solver  1 X)))
       ( 1 ['not     X  ]  (<cut> (type-solver -1 X)))
       ( E ['=       X Y]  (<cut> (my-unify E X Y)))

       (-1 [X       .  Y]  (<cut> (branch  (</.> (type-solver -1 X))
                                           (</.> (type-solver -1 Y)))))
       ( 1 [X       .  Y]  (<cut> (<and> (type-solver  1 X) 
                                         (type-solver  1 Y))))
       ( E [X]             (<cut> (type-solver E X)))
       ( E []              (<cut> <cc>))
       ( _ _               (<code> (error "type error")))))


;;This technique mans that a unification only matches one time and not
;;Many

(define (the-lambda Pr CC G F A R)
  (<define> (z A AA B BB)
    (<and> (G A AA)
           (G R BB)))
  (let-values (((Arg Res)  ((u-scm F))))
    (z Pr CC 
       A (add-var-points Arg)
       R (add-var-points Res))))

(<define> (my-unify E X Y)
  (must-success   
   (</.> (the-unify E X Y))
   (lambda ()
     (format #t "could not unify:~%")
     (pp (my-scm `(= (,E) ,X ,Y)))
     (error "type-error"))))

(<define> (<the-lam> E X Y)
    (<match> #:mode - (X Y)
             ([F]          [A R]     
              (the-lambda (<lambda> (A B) (the-unify E A B)) F A R))
             ([A R]        [F]       
              (the-lambda (<lambda> (A B) (the-unify E A B)) F A R))
             ([F]          [G]  
              (<code> (error  "lambda f = lambda g")))
             (_ _ (error "the-lam match error"))))


(define *validated* (gp-make-fluid))
(gp-fluid-set! *validated* #f)

(define (validating Pr CC F E X Y)
  (with-gp-fluids ((*validated* #t))
    (F Pr 
       (lambda (Pr)
         (with-gp-fluids ((*validated* #f))
           (CC Pr)))
       E 1 X Y)))


(<define> (the-unify E X Y)
    (<when> (begin (pr 0 'the-unify E X Y) #t))
    (<match> #:mode - (X Y)
        (['lambda X]          ['lambda Y]       (<cut> (<the-lam> E X Y)))     

        (['res ['lambda F]]  ['def ['lambda A R]]
         (<cut> (the-lambda (<lambda> (A B) 
                              (%a=r%1 E 1 A B))
                            F A R)))

        (['validated A]       ['res B]          (<cut> 
                                                 (validating %r=a%1 E A B)))
        (['res B]             ['validated A]    (<cut>
                                                 (validating %a=r%1 E B A)))

        (['arg A]             ['res B]          (<cut> (%a=r%1 E 1 A B)))
        (['res A]             ['arg B]          (<cut> (%r=a%1 E 1 A B)))
        
        (['or . L]            ['def A]          (defor E `(def ,A) L))
        (['def A]             ['arg B]          (<cut> (<=> A B)))
        (['def A]             ['res B]          (<cut> (<=> A B)))
        (['def A]             B                 (<cut> (<=> A B)))

        (['arg A]             ['def B]          (<cut> (<=> A B)))
        (['res A]             ['def B]          (<cut> (<=> A B)))
        (A                    ['def B]          (<cut> (<=> A B)))

        (['res A]             B                 (<cut> (%r=a%1 E 1 A B)))
        (B                    ['res A]          (<cut> (%r=a%1 E 1 A B)))

        (['arg A]             B                 (<cut> (%r=a%1 E 1 B A)))
        (B                    ['arg A]          (<cut> (%r=a%1 E 1 B A)))

        (A                    B                 (<cut> (<=> A B)))
        (A                    B                 (<cut> (%r=a%1 E 1 A B)))))

(define (%r=a%1 Cut CC NotX NotY X Y) (%a=r%1 Cut CC NotY NotX Y X))
(<define> (defor E Def OR)
    (<match> #:mode - (OR)
      ((X . L)  (the-unify E Def X))
      ((_ . L)  (<cut> (defor E Def L)))
      (_        <fail>)))
  
  
;;The B stack bank is alive for the entire unifying construction
;;After it will backtrack and reset during unify temporary settings
(define (store-vars X)
  (define vars '())
  (define (f X)
    (umatch #:mode - (X)
            ((X . L) 
             (begin (f X) (f L)))
            (X 
             (let ((X (gp-lookup X)))
               (if (gp-var? X)
                   (if (member X vars)
                       #t
                       (set! vars (cons X vars)))
                   #t)))))
  (f X)
  (set! *res-vars* vars))

(define (%a=r%1 Pr CC Ex Ey X Y)
  (store-vars Y)
  (u-set! *produced-var* (u-var!))
  (let ((Fr (b-frame)))
    (b-page Pr CC Fr
            (lambda (Pr CC)
              (%a=r% Pr (lambda (Pr)
                          (b-unwind Fr)
                          (CC Pr))
                     Ex Ey X Y)))))

;;taking care of inner boolean logic
(<define> (%a=r% NotX NotY X Y)
    (<when> (begin (pr 2 'a=r NotX NotY X Y) #t))
    (<match> #:mode - (X Y) 
             (['lambda F]          ['lambda A R]     
              (the-lambda (<lambda> (A B) 
                            (%r=a%1 NotX NotY A B)) 
                          F A R))

             (['lambda A R]        ['lambda F]       
              (the-lambda (<lambda> (A B) 
                            (%a=r%1 NotX NotY A B)) 
                          F A R))

             (['lambda F]          ['lambda G]       
              (<code> (error  "lambda f = lambda g")))

             ((? var? X)        Y
              (<cut> (unify%a=r% NotX NotY X Y)))


             (['not ['not   X]] Y        
              (<cut> (%a=r% NotX NotY X Y)))

             (['not ['or  . L]] Y        
              (<cut> (%and% %a=r% (- NotX) NotY L Y)))

             (['not (and A ['and . L])] Y        
              (<cut>  (%or%  %a=r% A (- NotX) NotY L Y)))
                     
             ((and A ['or . L])        Y      
              (<cut> (%or% %a=r% A NotX NotY L Y)))

             (['and . L]        Y        
              (<cut> (%and% %a=r% NotX NotY  L Y)))

             (Y ['not ['not   X]]        
                (<cut> (%a=r% NotX NotY Y X)))

             (Y ['not ['and  . L]]        
                (<cut> (%try% %r=a%1 (- NotY) NotX  L Y)))

             (Y ['not (and A ['or . L])]        
              (<cut> (%or%  %r=a%1 A (- NotY) NotX L Y)))

             (Y              ['or  . L]
              (<cut> (%try% %r=a%1 NotY NotX  L Y)) )

             (Y              (and A ['and . L])
              (<cut> (%or%  %r=a%1 A NotY NotX L Y)))

             ((? var? X)        Y
              (unify%a=r% NotX NotY X Y))
             
             (X             (? var? Y)
              (<cut> (unify%a=r% NotX NotY X Y)))

             (X                 Y        
              (<cut> (unify%a=r% NotX NotY X Y)))

             (_ _ <fail>)))

;; patching in the ability of subtyping
(define (unify%a=r% pr cc ex ey x y)
  (unify-rec pr cc 
             (lambda (pr cc x y)
               (<subtype-unify-ar> 
                pr cc
                unify%a=r%0 
                ex ey x y))
             x y))

(define (res-var Pr CC Ex Ey X Y)
  (if (gp-fluid-ref *validated*)
      (begin 
        (u-set! Y X)
        (CC Pr))
      (u-abort Pr)))

;; unification and list destruction
(<define> (unify%a=r%0 EX EY X Y)
  (<when> (begin (pr 8 'unify-a=r EX EY X Y) #t))
  (<match> #:mode - (EX EY X Y)
       (_ _  ('rec . L) _ 
          (<cut> (unify%a=r% EX EY (cons 'rec  L) Y)))
       (_ _   _ ('rec . L)  
          (<cut> (unify%a=r% EX EY X (cons 'rec  L))))

       ;; lambda expansion
       (NotX NotY ['lambda F]           ['lambda A R]     
        (<cut> 
         (the-lambda (<lambda> (A B) 
                       (%a=r% NotX NotY A B)) 
                     F A R)))

       (NotX NotY ['lambda A R]        ['lambda F]       
        (<cut>
         (the-lambda (<lambda> (A B) 
                       (%a=r% NotX NotY A B)) 
                     F A R)))

       (_ _ ['lambda F]          ['lambda G]       
          (<code> (error "lambda f = lambda g")))
       
       ( E1 E2 ['lambda A1 R1] ['lambda A2 R2]
            (<cut>
             (<and> 
              (swap-u1-u2)
              (unify%a=r%0 E2 E1 A2 A1)
              (swap-u1-u2)
              (%a=r% E1 E2 R1 R2))))

        ;; list destruction
       ( 1  1 [A  . B1]       [A  . B2]    (<cut> (%arg%  %a=r% 1 1 B1 B2)))
       ( 1  1 [_  . _ ]       [_  . _ ]    (<cut> <fail>))
       ( _ -1 [A  . B1]       [A  . B2]    (<cut> <fail>))
       (-1  1 [A  . B1]       [A  . B2]    (<cut> (%arg%  %a=r% -1 1 B1 B2)))
       (-1  1 [A1 . B1]       [A2 . B2]    (<cut> (<cc>)))

       ;;unification
       ( 1  1 (? var? X)     (? var? Y)  (<cut> (<x=> X Y)))
       (-1  1 (? var? X)     (? var? Y)  (<cut> (<x=> X ['not Y])))
       ( 1 -1 (? var? X)     (? var? Y)  (<cut> (<x=> X ['not Y])))
       (-1 -1 (? var? X)     (? var? Y)  (<cut> (<x=> X Y)))
       (Ex Ey  X             (? var? Y)  (<cut> (res-var Ex Ey X Y)))
        
       ( 1  1 (? var? X)     Y           (<cut> (<x=> X  Y)))
       (-1 -1 (? var? X)     Y           (<cut> (<x=> X  Y)))
       (-1  1 (? var? X)     Y           (<cut> (<x=> X  ['not Y])))
       ( 1 -1 (? var? X)     Y           (<cut> (<x=> X  ['not Y])))

       ;; atom treatment
       ( 1  1 A               A          (<cut> <cc>  ))
       (_  -1 _               _          (<cut> <fail>))
       (-1  1 A               A          (<cut> <fail>))
       (-1  1 A               B          (<cut> <fail>))
       (_ _ _ _ (<cut> <fail>))))

(define *produced-var* #f)
(define (qmember X L)
  (umatch #:name 'qmember #:mode - (L)
          ((,X . _) #t)
          ((_  . L) (qmember X L))
          (_        #f)))

(define (qappend X L)
  (umatch #:name 'qappend #:mode - (X)
          ((_  . U) (qappend U L))
          (U        (begin
                      (u-set! U L)
                      #f))))


(define (<x=> Pr CC X Y)
  (define (f Y L CC)
    (umatch #:name '<x=> #:mode - (Y)
            ((X . U) (f X L (lambda (L) (f U L CC))))
            (X       (let ((X (gp-lookup X)))
                       (if (gp-var? X) 
                           (if (or (qmember X L) (qmember X *produced-var*))
                               (CC L)
                               (CC (cons X L)))
                           (CC L))))))

  ;;(pp (my-scm `(x= ,*produced-var*)))
  (let ((X (gp-lookup X))
        (Y (gp-lookup Y)))
    (if (gp-var? X)
        (if (qmember X *produced-var*)
            (if (eq? X Y)
                (begin #;(pp 'matched)   (CC Pr))
                (begin #;(pk 'no-match)  (u-abort Pr)))
            (f Y (u-var!)
               (lambda (L)
                 (qappend *produced-var* L)
                 (if (not (eq? X Y)) (u-set! X Y))                 
                 (CC Pr))))
        (error "<x=> should be called with X a variable"))))

(<define> (%and% F A B X Z)
   (<match> (X)
            ([X]     (<cut> (<apply> F A B X Z)))
            ([X   Y] (<cut> (<and> (<apply> F A B X Z) (<apply> F A B Y Z))))
            ([X . L] (<cut> (<and> (<apply> F A B X Z) (%and% F A B L Z))))
            (_       (error "match error in %and%"))))

(<define> (%try% F A B X Z)
   (<match> (X)
            ([X]     (<cut> (<apply> F A B X Z)))
            ([X   Y] (<cut> (branch2
                             (</.> (<apply> F A B X Z))
                             (</.> (<apply> F A B Y Z)))))
            ([X . L] (<cut> (branch2
                             (</.> (<apply> F A B X Z)) 
                             (</.>  (%try% F A B L Z)))))
            (_       (error "match error in %try%"))))


(define (parenthood Pr CC U X)
  (if (gp-fluid-ref *validated*)
      (u-set! U X))
  (CC Pr))
      

;; Unwinding both A bank and B bank in or construction in order to backtrack
;; B bank = during unifying backtracking.

(<define> (%or% F U A B X Z)
    (<let> ((Fr (b-frame)))
         (b-page Fr
             (</.> (%or%0 F U A B X Z Fr)))))
           

(<define> (%or%0 F U A B X Z Fr)
  (<match> (X)
    ([X]       (<cut> (<and>
                       (<when> (begin (b-unwind Fr) #t))
                       (<apply> F A B X Z)
                       (parenthood U X))))

    ([X   Y]   (<cut> (<or> (<and>
                             (<when> (begin (b-unwind Fr) #t))
                             (<apply> F A B X Z)
                             (parenthood U X))
                            (<and> 
                             (<when> (begin (b-unwind Fr) #t))
                             (<apply> F A B Y Z)
                             (parenthood U Y)))))

    ([X . L]   (<cut> (<or> (<and>
                             (<when> (begin (b-unwind Fr) #t))
                             (<apply> F A B X Z)
                             (parenthood U X))
                            (%or%0 F U A B L Z Fr))))
    (_ (error "match error in %or%"))))

           
(<define> (%arg% F EX EY X Y)
    ;;(<pp> `(arg ,EX ,EY ,X ,Y))
   (<match> #:mode - (EX EY X Y)
          ( _  _ []      []          <cc>)

          ( 1  1 [X]     [A    ]    (<cut> (<apply> F 1 1 X A)))
          ( 1  1 [X . Y] [A . B]    (<cut> 
                                     (<and> 
                                      (<apply> F 1 1 X A) 
                                      (%arg% F 1 1 Y B))))

          (-1 -1 [X]     [A    ]    (<cut> (<apply> F -1 -1 X A)))
          (-1 -1 [X . Y] [A . B]    (<cut> (<and> (<apply> F -1 -1 X A) 
                                           (%arg% F -1 -1 Y B))))

          ( K  L [X]     [A    ]    (<cut> (<apply> F K L X A)))
          ( K  L [X . Y] [A . B]    (<cut> (<or> (<apply> F K L X A) 
                                                 (%arg% F K L Y B))))

          ( A B X Y (begin (pp (u-scm X)) (pp Y) (error "match error in %arg%")))))
