(define-module (language prolog guile-theorem guile-theorem)
  #:use-module (language prolog guile-theorem db)
  #:use-module (language prolog guile-log-ffi)
  #:use-module (language prolog list-ffi)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module (language prolog umatch-ffi)
  #:use-module (language prolog guile-log-pre)
  #:export (check theorem tactic intros intro tapply exact assumption *cc* 
                  left right seq alt orelse pr reset auto trivial idtac ftac
                  *trace* try cut assert))

;; Registration of value -> map

;; A quick type-checker lambda apply let var
(<define> (typecheck-fun H L X Cl Cx)
  (<var> (HH)
    (<match> (L HH Cl)
      (([Y ': A] . Ls) ([Y ': A] . ,H) (A . Cls)       
       (<cut>
        (typecheck-fun HH Ls X Cls Cx)))
      
      ((Y       . Ls) ([Y ': A] . ,H) (A . Cls)
       (<cut>
        (typecheck-fun HH Ls X Cls Cx)))

      (()             _              ()
       (<cut> (typecheck H X Cx))))))

(<define> (typecheck-apply H L Cl Cr C)
  (<match> (L)
    ((X . Ls)
     (<cut>
      (<match> (Cl)
        ((Ca . Cls)
         (<cut>
          (typecheck H X Ca)
          (typecheck-apply H Ls Cls Cr C)))
        (()
         (<cut>(<match> (Cr)
                 (('=> Cl Crr)
                  (typecheck-apply H L Cl Crr C))
                 (_ (error "type-error"))))))))
    (()
     (<cut>
      (<match> (Cl)
        ((_ . _)  
         (<cut> (<=> ('=> Cl Cr) C)))
        (()
         (<cut> (<=> Cr C))))))))


(<define> (typecheck-begin H L C)
  (<match> (L)
    ((X) 
     (<cut>
      (typecheck H X C)))
    ((X . L)
     (<cut>
      (<var> (CC)
        (typecheck H X CC)
        (typecheck H L C))))))

(<define> (typecheck H X C)
  (<match> (X C)
    (('/. L X) ('=> Cl Cx)
     (<cut>
      (typecheck-fun H L X Cl Cx)))
    
    
    (('let X Y Z) C
     (<cut>
      (<var> (C2)
        (typecheck H Y C2)
        (typecheck (bq [,X : ,C2] ,@H) Z C))))

    (('begin . L) C
     (<cut>
      (typecheck-begin H L C)))

    (('quote x) C
     (<cut>
      (<=> C 'symbol)))
    
    ((F . L)  C
     (<cut>
      (<var> (Cl Cr)
        (typecheck H F (bq => ,Cl ,Cr))
        (typecheck-apply H L Cl Cr C))))
    
    (X          C          
     (<let> ((XX (gp-lookup X)))
       (<or>
        (if (number?  XX) (<=> C 'number))
        (if (string?  XX) (<=> C 'string))
        (if (char?    XX) (<=> C 'char))
        (if (boolean? XX) (<=> C 'bool))
        (<member> (bq ,X : ,C) H)
        (if (symbol? XX)
            (<or>
             (<let> ((Sf (catch #t
                           (lambda ()
                             (signature-ref (module-symbol-binding 
                                             (current-module) XX)))
                           (lambda x #f))))
               (if Sf ((Sf 'signature) C)))
             (<let> ((Sf (signature-symbol-ref XX)))
               (if Sf ((Sf 'signature) C)))))
        (<let> ((Sf (signature-ref XX)))
          (if Sf ((Sf 'signature) C))))))


    (_ _ (<cut> <fail>))))
    
(define (check x)
  (<run> 1 (C) (typecheck '() x C)))

;;Some predefined function types
(signature-set! + (make-signature ('=> 'number 'number 'number)))
(signature-set! - (make-signature ('=> 'number 'number 'number)))
(signature-set! * (make-signature ('=> 'number 'number 'number)))
(signature-set! / (make-signature ('=> 'number 'number 'number)))


;; The theorem framework

(define (pp x)
  (pretty-print (u-scm (syntax->datum x)))
  x)

(define tr
  (lambda (x)
    (syntax-case x (_)
      ((x . l)
       (cons (tr #'x) (tr #'l)))
      (() #'())
      (_ #'_)
      (x 
       (if (symbol? (syntax->datum #'x))
           #''x
           #'x)))))

(define (print-and-return r) (pr) r)
(define-syntax theorem
  (lambda (x)
    (syntax-case x (:)
      ((_ nm : tp)
       (with-syntax ((ttp (tr #'tp)))
         #'(begin 
             (set! *cc-hist*  '())             
             (gp-clear)
             (u-set! *idnr* 0)
             (let ((GoalTp (gp-var!)))
               (umatch #:mode + (GoalTp)
                       (ttp  (print-and-return
                              (solve (lambda x 'proof-failed) 
                                     (lambda x 'QED)
                                     '() `[nm : ,GoalTp] '())))))))))))
  
(define *cc* #f)
(define *cc-hist* '())

(define (reset)
  (set! *cc* (car (reverse *cc-hist*)))
  (pr))

(define (tactic f . l)
  (set! *cc-hist* (cons *cc* *cc-hist*))  
  (match *cc*
    ((p cc Hyp Goal Goals s)
     (gp-restore-state s)
     (let ((ret (apply f (lambda () 'tactic-failed) cc Hyp Goal Goals l)))
       (if (not (eq? ret 'QED)) (pr))
       ret))))

(define (print-hyp Hyps)
  (format #t "Hypothesis:~%")
  (map (lambda (Hyp) (pretty-print Hyp)) (u-scm Hyps)))

(define (print-goal Goal)
  (format #t "Goal:~%")
  (pretty-print (u-scm Goal)))


(define *trace* #f)
(define (trace p cc nm H G Gs)  
  (if *trace*
      (begin
        (format #t "--------------------------------~%")
        (print-hyp H)
        (format #t "================================~%")
        (print-goal G)
        (format #t "~%")
        (format #t " ~a more goals ... applies ~a~%~%" (length Gs) nm)
        (format #t "++++++++++++++++++++++++++++++++~%")))
  (cc p))
  
  

(define (pr)  
  (match *cc*
    ((p cc Hyp Goal Goals s)
     (format #t "--------------------------------~%")
     (print-hyp Hyp)
     (format #t "================================~%")
     (print-goal Goal)
     (format #t "--------------------------------~%")
     (format #t " ~a more goals~%~%" (length Goals)))))

(define (set-data . l)
  (set! *cc* l)
  'tactic?)

(<define> (solve Hyp Goal Goals)
  (<match> (Goal)
    (() (<cut>
         (<match> (Goals)
           (([G . H] . Gs)            
            (<cut> (solve H G Gs)))
           (()
            (<cut> (<return> 'QED))))))
    (_
     (<cut>      
      (set-data Hyp Goal Goals (gp-store-state))))))


;; tactics
(define *idnr* (gp-make-fluid))
(u-set! *idnr* 0)
    
(define (make-id n)
  (string->symbol (format #f "H~a" n)))

(<define> (intros-help H L C Gs)
  (<match> (L)
    ((X . Ls)
     (<cut>
      (<let> ((n (gp-lookup *idnr*)))
        (<code> (u-set! *idnr* (+ n 1)))
        (intros-help (bq [,(make-id n) : ,X] ,@H) Ls C Gs))))
    (()
     (<cut> 
      (<match> (C)
        (('=> L CC)
         (<cut> (intros-help H L CC Gs)))
        (_ 
         (<cut> (solve H C Gs))))))))
       
(<define> (intros H Goal Gs)
  (<match> (Goal)
    ([A : (=> L C)]
     (<cut> (intros-help H L C Gs)))
    ((=> L C)
     (<cut> (intros-help H L C Gs)))
    (_ <fail>)))


(<define> (intro-help H S L C Gs)
  (<match> (L S)
    ((X . Ls) (S . Ss)
     (<cut>
      (<var> (HH)
        (<=> HH ([,S ': ,X] . ,H))
        (intro-help HH Ss Ls C Gs))))

    ((X . Ls) ()
     (<cut>
      (<var> (G)
        (<=> G  ('=> L C))
        (solve H G Gs))))

    (() (X . Xs)
     (<match> (C)
       (('=> L CC)
        (<cut>
         (intro-help H S L CC Gs)))
       (_ 
        (<cut> (solve H C Gs)))))
         
     (() ()
      (<cut> (solve H C Gs)))
     
    (_ _ <fail>)))

(define(intro Syms)
  (<lambda> (H G Gs)
    (<match> (G)
      ([A : (=> L C)]
       (<cut> (intro-help H Syms L C Gs)))
      ((=> L C)
       (<cut> (intro-help H Syms L C Gs)))
      (_ <fail>))))


(define (tapply E)
  (<lambda> (H G Gs)
    (<var> (La Ca)
      (<member> (bq ,E : (=> ,La ,Ca)) H)
      (<match> (G)
        (('=> Lb Cb)
         (<cut>
          (apply-help H Gs La Lb Ca Cb)))
        (_
         (<cut> (apply-help H Gs La '() Ca G)))))))

(<define> (apply-help H Gs La Lb Ca Cb)
  (<match> (Ca)
    (('=> Laa Caa)
     (<cut>
      (<var> (Laaa)
        (<append> La Laa Laaa)
        (apply-help H Gs Laaa Lb Caa Cb))))
    (_
     (<cut>
      (<match> (Cb)
        (('=> Lbb Cbb)
         (<cut>
          (<var> (Lbbb)
            (<append> Lb Lbb Lbbb)
            (apply-help H Gs La Lbbb Ca Cbb))))
        (_ 
         (<cut>
          (<=> Ca Cb)
          (<let> ((Na (u-length La))
                  (Nb (u-length Lb)))
            (<when> (<= Nb Na))
            (apply-help2 H Gs La Lb Na Nb '())))))))))

(<define> (apply-help2 H Gs La Lb Na Nb R)
  (if (eq? Na Nb)
      (apply-help3 H Gs La Lb R)
      (<let> ((La (gp-lookup La)))
        (apply-help2 H Gs (gp-cdr La) Lb (- Na 1) Nb (cons (gp-car La) R)))))

(<define> (apply-help3 H Gs La Lb R)
  (<match> (La Lb)
    ((Xa . Las) (Xb . Lbs)
     (<cut>
      (<=> Xa Xb)
      (apply-help3 H Gs Las Lbs R)))
    (() () 
     (<cut> (apply-help4 H Gs R)))
    (_  _ <fail>)))

(<define> (apply-help4 H Gs R)
  (<match> (R)
    (() 
     (<cut> (solve H '() Gs)))
    ((X)
     (<cut> (solve H X Gs)))
    ((X . L)
     (<cut> (apply-help4 H (cons (cons X H) Gs) L)))
    (_ <fail>)))

;; The exact tactic     
(define (exact x)
  (<lambda> (H G Gs)
    (<var> (Q C)
      (typecheck H x C)
      (<=> G C)
      (solve H '() Gs))))

;; The assumption tactic
(<define> (assumption H G Gs)
  (trace 'assumption H G Gs)
  (<member> (bq _ : ,G) H)
  (solve H '() Gs))


;;left or right tactics
(<define> (left H G Gs)
  (<match> (G)
    (('or X Y)
     (<cut> (solve H X Gs)))
    (_ <fail>)))

(<define> (right H G Gs)
  (<match> (G)
    (('or X Y)
     (<cut> (solve H Y Gs)))
    (_ <fail>)))

(define-syntax dialog-on
  (syntax-rules ()
    ((_ code ...)
     (begin 
       code ...))))

(define (dialog-off) #f)


(define (seq-help p cc H G Gs Fs)
  (match Fs
    (() (cons (cons G H) Gs))
    ((F . Fs)
     (if (pair? F)
         (let* ((Fs  (map (lambda (F) (apply seq F Fs)) F))
                (ret (alt-help p cc H G Gs Fs)))
           (case ret
             ('QED           ret)
             ('tactic-failed ret)
             ('tactic?
              (match *cc*
                ((_ _ H G Gs _) Gs)))))

         (let loop ((H H) (G G) (Gs Gs) (R '()))
           (let ((ret (F p cc H G '())))
             (case ret 
               ('tactic-failed ret)
               ('QED
                (match Gs
                  (((G . H) . Gs) (loop H G Gs R))
                  (()             R)))
               ('tactic?
                (match *cc*
                  ((p cc Hyp Goal Goals s)
                   (let ((RR (seq-help p cc Hyp Goal Goals Fs)))
                     (case RR
                       ('tactic-failed RR)
                       (else
                        (match Gs
                          (((G . H) . Gs) (loop H G Gs (append R RR)))
                          (()             (append R RR))))))))))))))))

(define (seq . Fs)
  (lambda (p cc H G Gs)
    (let ((Fr  (gp-newframe))
          (Gs2 (seq-help p cc H G Gs Fs)))
      (case Gs2
        ('tactic-failed 
         (begin
           (gp-unwind Fr)
           (solve p cc H G Gs)
           Gs2))
        ('QED Gs2)
        (else
         (solve p cc H '() Gs2))))))


(define (alt-help p cc H G Gs Fs)
    (let loop ((H H) (G G) (Gs Gs) (Fs Fs) (R '()))
      (if (and (pair? Fs) G)
          (let ((ret ((car Fs) p cc H G '())))
            (case ret 
              ('tactic-failed ret)
              ('QED
               (umatch #:mode - (Gs)
                 (((G . H) . Gs)
                  (loop H    G  Gs (cdr Fs) R))
                 (()
                  (loop H   #f '() (cdr Fs) R))))
              ('tactic?  
               (match *cc*
                 ((p cc Hyp Goal Gss _)
                   (umatch #:mode - (Gs)
                     (()
                      (loop H #f '() (cdr Fs)
                            (append R (cons (cons Goal Hyp) Gss))))
                     (((G2 . H2) . Gs2)
                      (loop H2 G2 Gs2 (cdr Fs) 
                            (append R (cons (cons Goal Hyp) Gss))))))))))
          
          (if G 
              'tactic-failed        
              (solve p cc H '() R)))))

(define (alt . Fs)
  (lambda (p cc H G Gs)
    (let ((Fr  (gp-newframe))
          (ret (alt-help p cc H G Gs Fs)))
      (case ret
        ('tactic-failed
         (begin
           (gp-unwind Fr)
           (solve p cc H G Gs)
           ret))
        ('QED ret)
        (else 'tactic?)))))

           
(define (wrap p cc f . l)
  (let ((ret (apply f p cc l)))
    (case ret
      ('tactic-failed (p))
      ('QED           'QED)
      ('tactic? 
       (match *cc*
         ((p cc . _) (cc p)))))))

(define (continue p cc tactic)
  (match *cc*
    ((p cc H G Gs _)
     (wrap p cc tactic H G Gs))))

(define idtac solve)

(define (auto-help r)
  (<lambda> (H G Gs)
    (<when> (> r 0))
    (<or> (wrap intros H G Gs)
          (wrap assumption H G Gs)
          (<var> (Tag)
            (<member> `(Tag : (=> . _)) H)
            (wrap (apply (gp-lookup Tag)) H G Gs)))
    (continue (seq (auto (- r 1))))))

(define (auto . l)
  (dialog-off)
  (let ((r (match l (() 5) ((n) n))))
    (lambda (p cc H G Gs)
      
      (let ((ret ((auto-help r) 
                  (lambda ()  'tactic-failed)
                  (lambda (p) 'tactic-failed)
                  H G '())))
        (case ret
          ('tactic-failed (dialog-on (idtac p cc H G Gs)))
          ('QED           (dialog-on ret)))))))

(define (trivial-help r)
  (<lambda> (H G Gs)
    (<when> (> r 0))
    (<or> (wrap intros H G Gs)
          (wrap assumption)
          (<var> (Tag)
            (<member> `(Tag : (=> (_) _)) H)
            (wrap (apply (gp-lookup Tag)) H G Gs)))
    (continue (seq (trivial (- r 1))))))

(define (trivial . l)
  (dialog-off)
  (let ((r (match l (() 5) ((n) n))))
    (lambda (p cc H G Gs)
      
      (let ((ret ((trivial-help r) 
                  (lambda ()  'tactic-failed)
                  (lambda (p) 'tactic-failed)
                  H G '())))
        (case ret
          ('tactic-failed (dialog-on (idtac p cc H G Gs)))
          ('QED           (dialog-on ret)))))))


(define (orelse . l)
  (lambda (p cc H G Gs)
    (let ((Fr (gp-newframe)))
      (let loop ((l l))
        (match l
          ((f . l)
           (let ((ret (f p cc H G Gs)))
             (case ret
               ('QED 
                ret)

               ('tactic-failed
                (gp-unwind Fr)
                (loop l))

               ('tactic?
                ret))))
          (() 
           'tactic-failed))))))

(define (ftac . l) 'tactic-failed)
(define (try x)    (orelse x idtac))
(define (cut A)
  (<lambda> (H G Gs)
    (solve H `(=> (,A) ,G) (cons (cons A H) Gs))))

(define (assert A)
  (<lambda> (H G Gs)
    (<match> (A)
      ((H ': B)
       (solve H B (cons (cons G (cons A H)) Gs))))))
