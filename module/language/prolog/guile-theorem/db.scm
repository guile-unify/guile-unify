(define-module (language prolog guile-theorem db)
  #:use-module (language prolog umatch-ffi)
  #:use-module (language prolog guile-log-ffi)
  #:use-module (ice-9 match)
  #:export (signature-ref signature-symbol-ref define-symbol-table 
                          signature-set! signature-symbol-set!
                          mk-hot-signature make-signature))

(define *signatures* (make-weak-key-hash-table 1000))
(define (signature-ref x)
  (hashq-ref *signatures* x))

(define (make-symbol-db) (make-hash-table 100))
(define-syntax define-symbol-table
  (lambda (x)
    (syntax-case x ()      
      ((n) 
       (with-syntax ((nm (datum->syntax #'n 'symbol-table)))
         #'(define nm (make-symbol-db)))))))

(define (signature-symbol-ref x)
  (catch #t
    (lambda ()
      (let ((db (module-symbol-binding (current-module) 'symbol-table)))
        (hashq-ref db x)))
    (lambda x #f)))

(define (signature-set! v sign)
  (hashq-set! *signatures* v sign))

(define (signature-symbol-set! s sign)
  (let ((db (module-symbol-binding (current-module) 'symbol-table)))
    (hashq-set! db s sign)))


(define (mk-hot-signature sign)
  (define *variables* '())
  (define (find-var x)
    (umatch #:mode - (x)
      ((x . l) 
       (begin
         (find-var x)
         (find-var l)))
      (x (let ((x (gp-lookup x)))
           (if (gp-var? x)
               (let ((f (assoc x *variables*)))
                 (if f
                     'ok
                     (set! *variables* 
                           (cons (cons x (make-fluid)) 
                                 *variables*))))
               'ok)))))

  (define (rpl x)
    (umatch #:mode - (x)
      ((x . l)
       (cons (rpl x) (rpl l)))
      (x (let ((x (gp-lookup x)))
           (if (gp-var? x)
               (cdr (assoc x *variables*))
               x)))))

  (find-var sign)
  (let ((vars (map cdr *variables*))
        (rp   (rpl sign)))
    (lambda (x)
      (match x
        ('signature
         (lambda (p cc C)
           (set! *variables* (map (lambda (x) (cons x (gp-var!))) vars))
           (<member> p cc C (rpl rp))))))))
  

(define-syntax make-signature 
  (syntax-rules ()
    ((_ pat1 ...)
     (lambda (x)
       (match x
         ('signature
          (<lambda> (C)
            (<match> (C)
              (pat1 <cc>)
              ...
              (_ (<cut> <fail>))))))))))
              
