(define-module (language prolog database-ffi)
  #:use-module (language prolog guile-log-ffi)
  #:use-module (language prolog umatch-ffi))

(include-from-path "language/prolog/database-code.scm")
