(define-module (language prolog list)
  #:use-module (language prolog umatch)
  #:use-module (language prolog guile-log))

(include-from-path "language/prolog/list-code.scm")
