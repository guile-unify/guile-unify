;; This buffer is for notes you don't want to save, and for Lisp evaluation.
;; If you want to create a file, visit that file with C-x C-f,
;; then enter the text in that file's own buffer.


;; [= [arg X]        [Op A B]]  -> [Op* [= [arg X] A] [= [arg X] B]]
;; [= [arg [Op A B]] X       ]  -> [Op  [= [arg A] X] [= [arg B] X]]
;; [= [res X]        [Op A B]]  -> [Op  [= [res X] A] [= [res X] B]]
;; [= [res [Op A B]] X       ]  -> [Op* [= [res A] X] [= [res B] X]]
;; [= [arg [not x]]         a]  -> [/= a x]
;; [= [arg X]          [not a]  ->
;; This means that we can reduce everyhing to [e1 [= [e2 X] [e3 X]]] where e1,e3 = not/  and e2 = res/arg/ , not/
;;
;; [not A] B
;;


;;replace symbol with unify variables
(define (var-it Code)  (compute-type (lambda () (u-var!)) Code))

;;So solve for the different types
(<def> type-solver
       (['and     X Y]  (<and> (type-solver X) (type-solver Y)))
       (['or      X Y]  (<or>  (type-solver X) (type-solver Y)))
       (['not     X  ]  (<not> (type-solver X)))
       ([=        X Y]  (the-unify X Y))
       ([X          Y]  (<and> (type-solver X) (type-solver Y)))
       ([X        . Y]  (<and> (type-solver X) (type-solver Y)))
       ([X]             (type-solver X)))

(<define> the-unify
          (['= A              ['res ['arg B ]]] (the-unify A B))
          (['= ['validated A] ['res B]]         (%v=r% A B))
          (['= ['arg       A] ['res B]]         (%a=r% A B)))
 
(<define> %r=a%   (NotX NotY X Y   (%a=r% NotY NotX Y X)))

(<define> (%a=r NotX NotY X Y)
    (<match> (X Y) 
             ;;logic
             (['not ['not   X]] Y        (%a=r% NotX NotY X Y))
             (['not ['or  . L]] Y        (%and% (- NotX) NotY  %a=r% L Y))
             (['not ['and . L]] Y        (%or%  (- NotX) NotY  %a=r% L Y))
             (Y ['not ['not   X]]        (%a=r% NotX NotY Y X))
             (Y ['not ['or  . L]]        (%and% (- NotY) NotX  %r=a% L Y))
             (Y ['not ['and . L]]        (%or%  (- NotY) NotX  %r=a% L Y))
             (['or  . L]        Y        (%or%  NotX NotY  %a=r% L Y))
             (['and . L]        Y        (%and% NotX NotY  %a=r% L Y))
             (X              ['or  . L]  (%and% NotY NotX  %r=a% L X)) 
             (Y              ['and . L]  (%or%  NotY NotX  %r=a% L X))
             (X                 Y        (unify%a=r% NotX NotY X Y))))


(<def> unify%a=r% 
        ;;unification
        ( 1  1 (? var? X)     (? var? Y)  (<=> X [res Y]))
        (-1  1 (? var? X)     (? var? Y)  (<=> X [res [not Y]]))
        ( 1 -1 (? var? X)     (? var? Y)  (<=> X [res [not Y]]))
        (-1 -1 (? var? X)     (? var? Y)  (<=> X [res Y]])
        ( _  _  _             (? var? Y)  <fail>)
        
        ( 1  1 (? var? X)     Y           (<=> X [res Y]))
        (-1 -1 (? var? X)     Y           (<=> X [res Y]))
        (-1  1 (? var? X)     Y           (<=> X [res [not Y]]))
        ( 1 -1 (? var? X)     Y           (<=> X [res [not Y]]))

        ;; list destruction
        ( 1  1 [A  . B1]       [A  . B2]    (%arg%  1  1 %r=A% B1 B2))
        (_  -1 [A  . B1]       [A  . B2]    <fail>)
        (-1  1 [A  . B1]       [A  . B2]    (%arg%  K L %r=A% B1 B2))
        (-1  1 [A1 . B1]       [A2 . B2]    <true>)

        ;; atom treatment
        ( 1  1 A               A            <true>)
        ( 1  1 A               B            <fail>)
        (_  -1 _               _            <fail>)
        (-1  1 A               A            <fail>)
        (-1  1 A               B            <true>))


(<def> %and%
       (F A B [X]      Z  (<apply> F A B X Z))
       (F A B [X   Y]  Z  (<and> (<apply> F A B X Z) (<apply> F A B Y Z)))
       (F A B [X . L]  Z  (<and> (<apply> F A B X Z) (%and% F A B L Z))))

(<def> %or%
       (F A B [X]      Z  (<apply> F A B X Z))
       (F A B [X   Y]  Z  (<or> (<apply> F A B X Z) (<apply> F A B Y Z)))
       (F A B [X . L]  Z  (<or> (<apply> F A B X Z) (%and% F A B L Z))))
           
(<define> %arg%
          (F  1  1 [X]     [A    ]    (<apply> F 1 1 X Y))
          (F  1  1 [X . Y] [A . B]    (<and> (<apply> F 1 1 X A) (%arg% F 1 1 Y B)))
          (F -1 -1 [X]     [A    ]    (<apply> F -1 -1 X Y))
          (F -1 -1 [X . Y] [A . B]    (<and> (<apply> F -1 -1 X A) (%arg% F -1 -1 Y B)))
          (F  K  L [X]     [A    ]    (<apply> F K L X Y))
          (F  K  L [X . Y] [A . B]    (<or> (<apply> F K L X A) (%arg% F K L Y B))))

