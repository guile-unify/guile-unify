;;; UNIFY MATCH MATCHER COMPILER

;; Copyright (C) 2001,2008,2009,2010 Free Software Foundation, Inc.

;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, 
;;;; MA 02110-1301 USA

;;; Code:
(define-module (language prolog umatch)
  #:use-module (ice-9 match-phd)
  #:use-module (ice-9 pretty-print)
  #:use-module (ice-9 syntax-matcher)
  #:use-module (srfi srfi-1))


(define-syntax u-list
  (syntax-rules ()
    ((_) '())
    ((_ x y ...)
     (u-cons x (u-list y ...)))))
    

(define-syntax ulet
  (syntax-rules ()
    ((ulet . l) (let . l))))

(define-syntax udef
  (syntax-rules ()
    ((_ n m) 
     (begin 
       (define m (lambda x x))
       (define-syntax n
         (syntax-rules ()
           ((n . a) (<gp> (m . a)))))))))

(define <gp>       (lambda x x))
(define <u-load>   (lambda x x))
(define <u-insert> (lambda x x))
(define <u-var>    (lambda x x))
(define <u-cons>   (lambda x x))
(define <u-and>    (lambda x x))
(define <u-eq>     (lambda x x))
(define <u-skip>   (lambda x x))
(define <code>     (lambda x x))
(define <u-nop>    (lambda x x))
(define <u-f?>     (lambda x x))
(define x          (lambda x x))

(udef  u-prompt     gp-prompt    )
(udef  u-abort      gp-unprompt  )
(udef  u-var!       g-var!       )
(udef  u-call       g-call       )
(udef  u-scm        gp-scm       )
(udef  u-context    g-context    )
(udef  u-set!       g-set!       )
(udef  u-cons       gp-cons      )
(udef  u-modded     g-modded     )
(udef  u-unify!     g-unify!     )
(udef  u-unify-raw! g-unify-raw! )

(load-extension (string-append "libguile-" (effective-version))
                "scm_init_vm")

(include-from-path "language/prolog/umatch-export.scm")

(define-syntax umatch
  (lambda (x)
    (syntax-case x ()
      ((umatch . l) 
       (with-syntax ((w (datum->syntax (syntax l) '*gp-fi*)))
         (syntax (umatch* "anon" w #t * . l)))))))

;;unsyntax construct that works quite ok
(define (pp x) (pretty-print x) x)


(define-syntax umatch* 
  (lambda (x)
    (syntax-case x ()
      ((umatch* nn tt rr mm #:name   n . l)
       (syntax (umatch*  n tt rr mm . l)))

      ((umatch* nn tt rr mm #:tag    t . l)
       (syntax (umatch*  nn t rr mm . l)))

      ((umatch* nn tt rr  mm #:raw     . l)
       (syntax (umatch*  nn tt #f mm . l)))

      ((umatch* nn tt rr mm #:mode   m . l)
       (syntax (umatch*  nn tt rr m . l)))

      ((umatch* n t r m args a a1 ...) 
       (syntax (umatch** 1 a args () (a1 ...) (n t r m)))))))



(define-syntax mksyms
  (lambda (x)
    (syntax-case x ()
      ((mksyms (r ...) (a as ...) . l)
       (with-syntax ((v (datum->syntax (syntax a) (gensym "arg"))))
                    (syntax (mksyms (r ... v) (as ...) . l))))

      ((mksyms r        ()        . l)
       (syntax (umatch** 2 r . l))))))

(define-syntax umatch**
  (syntax-rules ()
    ((umatch** 1 (a) () () (as ...) meta)
     (umatch3 () () () () (a) () () (as ...) () meta))

    ((umatch 1 a args . l)
     (mksyms () args a args . l))

    ((umatch** 2 (v1 vn ...) (a as ...) (arg args ...) () aa meta)
     (let ((v1 arg) (vn args) ...)
       (umatch3 (a) ((<u-load> v1)) () () (as ...) (vn ...) () aa (v1 vn ...) meta)))))

;;simple version with cons and and


(define-syntax umatch3
  (syntax-rules (and cons _ quote unquote ?)
    
    ((umatch3 ((? f? p) . u)      . l)
     (umatch3 ((and (? f?) p) . u) . l))
    
    
    ((umatch3 ((? f?) . u)      (c ...) cc . l)
     (upop    u                 
              (c ... (let ((q (<gp> (<u-nop>))))
                       (<gp> (f? q)) 
                       (<gp> (<u-f?>)))) 
              cc  . l))

    ((umatch3 ((and x) . u)       c     (and cc ...) . l)
     (umatch3 (x . u)             ()    (c and cc ...) . l))
    ((umatch3 ((and x) . u)       c     (cc ...) . l)
     (umatch3 (x . u)             c     (cc ...) . l))
    ((umatch3 ((and x . y) . u)   c     (      cc ...) . l)
     (umatch3 (x (and . y) . u)   ()    (and c cc ...) . l))

    ((umatch3 ((quote x)   . u) (c ...           ) cc  . l)
     (upop    u                 (c ... (<u-eq> (quote x))) cc  . l))

    ((umatch3 ((unquote x) . u) (c ...           ) cc  . l)
     (upop    u                 (c ... (<u-insert> x))  cc . l))
    ((umatch3 ((cons x   y) . u)       (c ...)             (     cc ...) . l)
     (umatch3 (x y          . u)       (c ... (<u-cons>))  (cons cc ...) . l))
    ((umatch3 ((     x . y) . u)       (c ...)             (     cc ...) . l)
     (umatch3 (x y          . u)       (c ... (<u-cons>))  (cons cc ...) . l))
    ;;no more lists
    ((umatch3 (() . u)  (c ...               ) . l)
     (upop          u   (c ... (<u-eq>   '())) . l))

    ((umatch3 (_ . u)   (c ...               ) . l)
     (upop          u   (c ... (<u-skip>)    ) . l))

    ((umatch3 (x  . u)  c cc v . l)
     (check-var x v u  c cc v . l))
    ((umatch3 () . l)
     (upop () . l))))


(define-syntax check-var
  (lambda (x)
    (syntax-case x ()
      ((_ v vs . l)       
       (if (symbol? (syntax->datum (syntax v)))
           (syntax (check-var* v vs . l))
           (syntax (uconst     v    . l)))))))


(define-syntax check-var*
  (lambda (x) 
    (syntax-case x ()
      ((_ v (u . us) x (c ...) . l)
       (if (eq? (syntax->datum (syntax v)) (syntax->datum (syntax u)))
           (syntax (upop x (c ... (<u-insert> v))  . l))
           (syntax (check-var* v us x (c ...) . l))))
      ((_ v ()       x (c ...             ) cc (  w ...) . l)
       (syntax (upop x (c ... (<u-var> v) ) cc (v w ...) . l))))))

(define-syntax uconst
  (syntax-rules ()
    ((uconst v x (c ...           ) . l)
     (upop     x (c ... (<u-eq> v)) . l))))

(define-syntax upop
  (syntax-rules (and cons)
    ((upop     x c  (   and         . cc) . l)
     (umatch3* x () (c  and         . cc) . l))
    ((upop     x c2 (c1 and (c ...) . cc) . l)
     (umatch3* x (c ... (<u-and> (begin . c1) (begin . c2))) cc . l))

    ((upop     x c  (   cons . cc) . l)
     (umatch3* x () (c  cons . cc) . l))
    ((upop     x (c2 ...)              ((c1 ...) cons . cc) . l)
     (umatch3* x (c1 ... c2 ...)       cc                           . l))
    ((upop     x c                     cc                           . l) 
     (umatch3* x c                     cc                           . l)))) 

    
(define-syntax umatch3*
  (lambda (x)
    (syntax-case x ()
      ((_ . l)
       ;(pk `(umatch3* ,@(syntax->datum (syntax l))))
       (syntax (umatch3** . l))))))

(define-syntax umatch3**
  (syntax-rules (cons and)
    ((umatch3* ()  c                     () v (code)     ()             
               (res ...)             (a as ...) args d)
     (umatch3* ()  ()                    () () a         args           
               (res ... (v c code))  (  as ...) args d))
    ((umatch3* () (c ...)                () v (a as ...) (arg args ...) . l)
     (umatch3  (a)  (c ... (<u-load> arg)) () v (as ...)   (args ...)     . l))
    ((umatch3* ()  c                     () v (code)      ()             
               (res ...)       ()         args d)
     (do-the-code (res ... (v c code)) d))
    ((umatch3* ()  (c ...)               (cons (c2 ...) . cc) . l)
     (umatch3* ()  (c2 ... c ...)        cc                   . l))
    ((umatch3* ()  (c2 ...)              ((c1 ...) cons . cc) . l)
     (umatch3* ()  (c1 ... c2 ...)       cc                   . l))
    ((umatch3* ()  (c ...)               (and . cc) . l)
     (umatch3* ()  (c ...)                      cc  . l))
    ((umatch3* ()  (c2 ...)              ((c1 ...) and . cc) . l)
     (umatch3* ()  ((<u-and> (begin c1 ...) (begin c2 ...))) cc          . l))
    ((umatch3* ()  (c ...) ((c2 ...) . cc) . l)
     (umatch3* ()  (c2 ... c ...) cc . l))
    ((umatch3* . l)
     (umatch3  . l))))


(define-syntax mode-it
  (syntax-rules (+ - *)
    ((_ *) 2)
    ((_ -) 3)
    ((_ +) 4)))


(define-syntax do-the-code
  (syntax-rules ()
    ((_ ((() (a ...) c) xx ...) (n t r m))
     (<umatch>
      (let ((t  #f)
            (mo #f)
            (ip #f)
            (sp #f))
        (x a ... (<code> c))
        (do-the-code-next (xx ...) n)
        (mode-it m)
        (begin t mo ip sp))))

    ((_ (((v ...) (a ...)  c) xx ...) (n t r m))
     (<umatch>
      (let ((t  #f)
            (mo #f)
            (ip #f)
            (sp #f))
        (let ((v #f) ...)
          a ... (<code> c))
        (do-the-code-next (xx ...) n)
        (mode-it m)
        (begin t mo ip sp))))))


(define-syntax do-the-code-next
  (lambda (x)
    (syntax-case x ()
      ((_ . l)
       ;(pk `(do-the-code-next ,@(syntax->datum (syntax l))))
       (syntax (do-the-code-next* . l))))))

(define-syntax do-the-code-next*
  (syntax-rules ()
    ((_ () n)
     (error (format #f "no match in ~a" n)))
    ((_ ((() (a ...) c) xx ...) n)
     (begin
       (x a ... (<code> c))
       (do-the-code-next (xx ...) n)))

    ((_ (((v ...) (a ...) c) xx ...) n)
     (begin
       (let ((v #f) ...)
         a ... (<code> c))
       (do-the-code-next (xx ...) n)))))

