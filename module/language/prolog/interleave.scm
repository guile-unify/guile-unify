;; sematically after kanrens all-interleave
(define-guile-log <or!>
  (syntax-rules ()
    ((_ w) 
     (parse<> w <fail>))

    ((_ w a) 
     (parse<> w a))

    ((_ w a ...)
     (parse<> w (interleave (list (</.> a) ...))))))

(define (interleave p cc l)
  (let ((s (gp-store-state)))
    (gp-swap-to-b)
    (let* ((ul (u-var!))
           (ur (u-var!))
           (n  (u-var!))
           (fr (gp-newframe)))
      (u-set! n 0)
      (gp-swap-to-a)
      (letrec ((loop (lambda (l r)
                       (if (null? l)                         
                           (if (null? r)
                               (p)                             
                               (loop (reverse r) '()))
                           (begin
                             (gp-swap-to-b)
                             (u-set! ul l)
                             (u-set! ur r)
                             (gp-swap-to-a)
                             ((car l)))))))
        (define (unwind-if-more-then-one-set)
          (begin
            (gp-swap-to-b)
            (let ((m (gp-lookup n)))
              (if (= m 1)
                  (gp-unwind 2)
                  (u-set! n (+ m 1))))
            (gp-swap-to-a)))

        (loop 
         (map (lambda (a)
                (lambda ()
                  (gp-restore-state s)
                  (a
                   (lambda ()
                     (loop (cdr (gp-lookup ul))
                           (gp-lookup ur)))
                 
                   (lambda (pp)
                     (cc (let* ((s  (gp-store-state))
                                (l  (gp-lookup ul))
                                (r  (gp-lookup ur)))
                           (unwind-if-more-then-one-set)            
                           (lambda ()
                             (loop (cdr l)
                                   (cons (lambda ()
                                           (gp-restore-state s)
                                           (pp))
                                         r)))))))))
              l)

         '())))))

