%Propositional calculus
indirect_proof(P,Qs,QQ):- 
	QQ is [[not,P]|Qs].

ecqs(P,Qs):- 
	member(X,Qs),member([not,X],Qs),!.

andl(P,Qs,QQ):- 
	member([and,X,Y],Qs,[],QQs),!,
	QQ is [X,Y|QQs].

orl(P,Qs,E1,E2):-
	member([or,X,Y],Qs,[],QQs),!,
        E1 is [X|QQs],
	E2 is [Y|QQs].

impl(P,Qs,QQ):- 
	member([impl,X,Y],Qs,[],QQs),!,
	QQ is [[or,X,[not,Y]] | QQs].

ekvi(P,Qs,QQ):- 
	member([ekvi,X,Y],Qs,[],QQs),!,
	QQ is [[impl,X,Y] , [impl,Y,X] | QQs].
                         
notnot(P,Qs,QQ):- 
	member([not,[not,X]],Qs,[],QQs),!,
        QQ is [X|QQs].

nand(P,Qs,E1,E2):- 
	member([not,[and,X,Y]],Qs,[],QQs),!,
	E1 is [[not,X]|QQs],
	E2 is [[not,Y]|QQs].

nor(P,Qs,QQ):- 
	member([not,[or,X,Y]],Qs,[],QQs),!,
        QQ is [[not,X],[not,Y]|QQs].

nimpl(P,Qs,QQ):- 
	member([not,[impl,X,Y]],Qs,[],QQs),!,
	QQ is [X,[not,Y]|QQs].

nekvi(P,Qs,E1,E2):- 
	member([not,[ekvi,X,Y]],Qs,[],QQs),!,
	E1 is [[not,[impl,X,Y]]|QQs],
	E2 is [[not,[impl,Y,X]]|QQs].

pc_solve(P,Qs):-
	indirect_proof(P,Qs,QQ),
	pc_solve_escqs(P,QQ,[]).

pc_solve_escqs(P,Qs,EQ):-
	(ecqs(P,Qs),!,loop(P,EQ));
	(nekvi(P,Qs,E1,E2),!,pc_solve_nimpl(1,P,E1,[E2|EQ]));
        pc_solve_nimpl(0,P,Qs,EQ).

pc_solve_nimpl(N,P,Qs,EQ):-
	(nimpl(P,Qs,QQ),!,pc_solve_nand((N+1),P,QQ,EQ)); 
        pc_solve_nand(N,P,Qs,EQ).

pc_solve_nand(N,P,Qs,EQ):-
	(nand(P,Qs,E1,E2),!,
         pc_solve_nor((N+1),P,E1,[E2|EQ]));
        pc_solve_nor(N,  P,Qs,EQ).
     
pc_solve_nor(N,P,Qs,EQ):-
	(nor(P,Qs,QQ),!,pc_solve_notnot((N+1),P,QQ,EQ));
        pc_solve_notnot(N,  P,Qs,EQ).

pc_solve_notnot(N,P,Qs,EQ):-
	(notnot(P,Qs,QQ),!,
        pc_solve_ekvi((N+1),P,QQ,EQ));
        pc_solve_ekvi(N,  P,Qs,EQ).

pc_solve_ekvi(N,P,Qs,EQ):-
	(ekvi(P,Qs,QQ),!,pc_solve_impl((N+1),P,QQ,EQ));
        pc_solve_impl(N,  P,Qs,EQ).

pc_solve_impl(N,P,Qs,EQ):-
	(impl(P,Qs,QQ),!,pc_solve_andl((N+1),P,QQ,EQ));
        pc_solve_andl(N,  P,Qs,EQ).

pc_solve_andl(N,P,Qs,EQ):-
	(impl(P,Qs,QQ),!,pc_solve_orl((N+1),P,QQ,EQ))
         ;pc_solve_orl(N,P,Qs,EQ).

pc_solve_orl(N,P,Qs,EQ):-
	(orl(P,Qs,E1,E2),!,
        pc_solve_escqs(P,E1,[E2|EQ]))
        ;(N>0, pc_solve_escqs(P,Qs,EQ)).
         

loop(P,[]).
loop(P,[Qs|EQ]) :- pc_solve_escqs(P,Qs,EQ).

run(P) :- P  = [ekvi,p,w],
          Qs = [[ekvi,p,q],
	        [ekvi,q,r],
		[ekvi,r,s],
                [ekvi,s,t],
                [ekvi,t,u],
		[ekvi,t,w]],
          pc_solve(P,Qs).

member(X,[X|Y],L,Q) :- sew(L,Y,Q).  
member(X,[Z|Y],L,Q) :- member(X,Y,[Z|L],Q).

member(X,[X|_]).
member(X,[_|L]) :- member(X,L).

sew([]   ,Q,Q).
sew([X|L],Y,Q) :- sew(L,[X|Y],Q).

