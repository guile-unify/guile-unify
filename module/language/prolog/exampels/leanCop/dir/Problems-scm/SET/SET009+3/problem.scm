#|
%--------------------------------------------------------------------------
% File     : SET009+3 : TPTP v5.1.0. Released v2.2.0.
% Domain   : Set Theory
% Problem  : If X is a subset of Y, then Z \ Y is a subset of Z \ X
% Version  : [Try90] axioms : Reduced > Incomplete.
% English  : If X is a subset of Y, then the difference of Z and Y is a
%            subset of the difference of Z and X.

% Refs     : [ILF] The ILF Group (1998), The ILF System: A Tool for the Int
%          : [Try90] Trybulec (1990), Tarski Grothendieck Set Theory
%          : [TS89]  Trybulec & Swieczkowska (1989), Boolean Properties of
% Source   : [ILF]
% Names    : BOOLE (47) [TS89]

% Status   : Theorem
% Rating   : 0.00 v4.0.1, 0.05 v3.7.0, 0.14 v3.5.0, 0.00 v3.1.0, 0.25 v2.7.0, 0.00 v2.2.1
% Syntax   : Number of formulae    :    4 (   1 unit)
%            Number of atoms       :    9 (   0 equality)
%            Maximal formula depth :    7 (   5 average)
%            Number of connectives :    6 (   1 ~  ;   0  |;   1  &)
%                                         (   2 <=>;   2 =>;   0 <=)
%                                         (   0 <~>;   0 ~|;   0 ~&)
%            Number of predicates  :    2 (   0 propositional; 2-2 arity)
%            Number of functors    :    1 (   0 constant; 2-2 arity)
%            Number of variables   :   10 (   0 singleton;  10 !;   0 ?)
%            Maximal term depth    :    2 (   1 average)
% SPC      : FOF_THM_RFO_NEQ
|#

(define-problem set009+3
  (axiom difference-defn
    (all [B C D]
      (<=> (member D (difference B C))
           (and (member D B) (~ (member D C))))))
  
  (axiom subset-defn
    (all [B C]
      (<=> (subset B C)
           (all (D)
             (=> (member D B)
                 (member D C))))))

  (axiom reflexivity-of-subset
    (all [B] (subset B B)))


  (conjecture subset-difference
    (all [B C D]
      (=> (subset B C)
          (subset (difference D C) (difference D B))))))
