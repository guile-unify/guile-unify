(define-module (language prolog exampels leanCop dir prover)
  #:use-module  (language prolog guile-log-ffi)
  #:use-module  (language prolog umatch-ffi)
  #:use-module  (language prolog postpone-ffi)
  #:use-module  (language prolog database-ffi)
  #:use-module  (srfi srfi-11)
  #:use-module  (ice-9 match-phd)
  #:use-module  (ice-9 pretty-print)
  #:export      (run-proof define-problem))

(define *pathlimit* 7)
(define *scut*      #f)
(define *cut*       #t)

(load "preprocess.scm")

(define (copy l)
  (umatch #:mode - (l)
          ((x . l) (u-cons (copy x) (copy l)))
          (x       (gp-lookup x))))

(define (read-prolog-structure nm)
  (define *vars* '())
  (define (evar? x)
    (and (symbol? x)
         (match (string->list (symbol->string x))
           ((a ... #\g #\g #\g)
            #t)
           (_ #f))))

  (define (var? x)
    (and (symbol? x)
         (eq? #\_ (car (string->list (symbol->string x))))))

  (define (get-var X)
    (define (f l)
      (match l
        (((,X . V) . _) V)
         ((_     . L) (f L))
         (()          
          (let ((V (gp-make-fluid)))
            (set! *vars* (cons (cons X V) *vars*))
            V))))
    (f *vars*))
  
  (define (term L)
    (match L
      (((? var? x) . l)
       (cons (get-var x) l))
      (((? evar? x) ('list a ...) . l)
       (term `((,x ,@a) ,@l)))
      (((? symbol? x) ('list a ...) . l)
       `(,x (list ,@a) ,@l))
      (((? symbol? x) (a ...) . l)
       (cons `(,x ,@(f a)) l))
      ((x . l) (cons (f x) l))
      (x x)))

  (define (term0 L)
    (match L
      ((t) t)
      (t   t)))

  (define (f X)
    (match X
      (('list . l)        
       (f l))
      
      (('- . l)
       (match (term l)
         ((t . l)  
          (cons `(- ,(term0 t)) (f l)))))

      (X (match (term X)
           ((t1 '= . l)
            (match (term l)
              ((t2 . l)
               (cons `(= ,t1 ,t2) (f l)))))
           ((t . l)
            (cons t (f l)))
           (X X)))))

  (let* ((S (open nm O_RDONLY))
         (R (f (read S))))
    (close S)
    R))



(define run-proof 
  (lambda x 
    (pretty-print
     (let ((R (read-prolog-structure "to-guile.scm")))      
       (pp R)
       (match R
         ([S M Set Proof2]
          (<run> 1 (Proof) (prove-compile S M Set Proof))))))))
    

(<define> (member/2 X L)
    (<match> (L)
      (()       (<cut> <fail>))
      ((,X . L)  <cc>)
      ((_  . L) (<cut> (member/2 X L)))
      (_        
       (<when> 
        (error 
         (format #f
           "(member ~a ~a)  got no list" (u-scm X) (u-scm L)))))))


(<define> (memb/2 X L)
    (<match> (L)
      ((? gp-var?) (<cut> <fail>))
      ((,X . L)    <cc>)
      ((_  . L)    (<cut> (memb/2 X L)))))

(<define> (membb/2 X L)
    (<match> (L)
      ((? gp-var? (,X . _) )     (<cut> <cc>)) 
      ((_  . L)                  (<cut> (membb/2 X L)))))
      


;;A port of the lean cop prolog solver (lean cop in the comments)
;prove(F,Proof) :- prove2(F,[cut,comp(7)],Proof).

(define neg* '(- *))
(define pos* '*)

(define (ground/1 P CC X)
  (if (let loop ((X (u-scm X)))
        (match X
          ((X . L) (or (loop X) (loop L)))
          ('<gp>   #t)
          (_       #f)))
      (u-abort P)
      (CC P)))

(define (pp X)
  (pretty-print (u-scm X))
  X)

(<define> (assert-clauses/2 L Set)
    (<match> (L)
      (()  <cc>)
      ((C . M)
       (<cut>
        (<var> (C1 G)
          (<if> (<and>
                 (<not> (<=> 'conj Set))
                 (<not> (member/2 `(- ,(u-var!)) C)))
                (<=> C1 (pos* . C))
                (<=> C1 C))
          (<if> (ground/1 C)
                (<=> G 'g)
                (<=> G 'n))
          (assert-clauses/3 C1 '() G)
          (assert-clauses/2 M Set))))))

(define (listify x)
  (define (f x)
    (define v '())
    (define (get-var x)
      (let ((w (assoc x v)))
        (if w
            (cdr w)
            (let ((w (gp-make-fluid)))
              (set! v (cons (cons x w) v))
              w))))
    (let loop ((x x))
      (umatch #:mode - (x)
              ((x . l) (cons (loop x) (loop l)))
              (x       (let ((w (gp-lookup x)))
                         (if (gp-var? w)
                             (get-var w)
                             w))))))

  (let loop ((x x))
    (umatch #:mode - (x)
      ((x . l) (cons (f x) (loop l)))
      (()      '()))))

(define *li*   '())
(define *start* #f)

(define (sort-l)
  (define (g l)
    (map cdr
         (sort 
          (map (lambda (x)
                 (let ((m (match x (('- x) x) (x `(- ,x)))))
                   (cons
                    (let loop ((l *li*) (n 0))
                      (match l
                        (((a . _) . u)
                         (let ((frame (gp-newframe)))
                           (if (gp-unify! a m)
                               (begin
                                 (gp-unwind frame)
                                 (loop u (+ n 1)))
                               (begin
                                 (gp-unwind frame)
                                 (loop u n)))))
                        (() n)))
                    x)))
               l)
          (lambda (x y) (< (car x) (car y))))))
                              
  (set! *li*
        (let loop ((l *li*))
          (match l
            (((a l G) . u)
             (cons `(,a ,(g l) ,G) (loop u)))
            (() '())))))

(define (assert-the-lit)
  (define *q* '())
  (define (find-companions x)
    (let loop ((a *li*) (res '()))
      (match a
        (((a l f) . u)
         (let ((frame (gp-newframe)))
           (if (gp-unify-raw! a x)
               (begin
                 (gp-unwind frame)
                 (loop u (cons f res)))
               (begin
                 (gp-unwind frame)

                 (loop u res)))))
        (()  (reverse! res)))))

  
  (define (mod-in-l l)
    (let loop ((l l) (res '()))
      (umatch #:mode - (l)
        ((('- x) . l)
         (let ((w (cons (find-companions x) res)))
           (set! *q* (cons w *q*))
           (loop l w)))
        ((x . l) 
         (let ((w (cons (find-companions `(- ,x)) res)))
           (set! *q* (cons w *q*))
           (loop l w)))
        (()
         (reverse! res)))))

  (define (mod-in-companions x)
    (let loop ((x x))
      (match x
        (((a l (f)) . u)
         (if (procedure? f)
             (f (mod-in-l l)))
         (loop u))
        (() 'ok))))

  (define (dbfy a)
    (let ((env (make-matcher-env)))
      (let loop ((a a))
        (match a
          ((((and q (vs x l))) . u)
           (env 'add x q)
           (loop u))

          (() (env 'make))))))


  (set! *li* (listify (cons `(start (,neg*) g) *li*)))
  (sort-l)
  ;(pp *li*)
  (let* ((v (map (lambda (x)
                   (match x
                     ((a l g) 
                      (list a l 
                            (let ((w (cons 1 '())))
                              (set-car! w
                                 (lambda (fs)
                                   (set-car! w
                                      (make-fpattern a
                                         `(,l ,g ,(lambda () fs))))))
                              w)))))
                 *li*)))
    (set! *li*  v)
    ;(pp *li*)
    (mod-in-companions v)

    (let loop ((x *q*))
      (match x
        ((x . u)
         (set-car! x (dbfy (car x)))
         (loop u))
        (() 'ok)))
             
    ;(pp *li*)
    (set! *start* (match *li* 
                    (((a l ((_ _ (_ _ f)))) . u) (f))))))
  

  
    
         

#|
assert_clauses([],_).
assert_clauses([C|M],Set) :-
    (Set\=conj, \+member(-_,C) -> C1=['<*>|C] ; C1=C),
    (ground(C) -> G=g ; G=n), assert_clauses2(C1,[],G),
    assert_clauses(M,Set).
|#

(<define> (append/3 A B C)
    (<match> (A C)
      (() ,B    
       (<cut> <cc>))
      ((A . AA) (A . CC)
       (<cut> (append/3 AA B CC)))
      (_ _ (error "wrong input to append/3"))))



;;in (a b c d e), g
;; a (b c d e) g
;; b (a c d e) g
;; c (a b d e) g
;; d (a b c e) g
;; e (a b c d) g
;; is stored in the database
(<define> (assert-clauses/3 U C1 G)
  ;;(<format> #t "assert-clause/3 ~a ~a ~a~%" U C1 G)
  (<match> (U)    
    (() 
     (<cut> <cc>))
    ((L . C)
     (<cut> 
      (<var> (C2 C3)
        (append/3 C1 C C2)
        (<code> (set! *li* (cons `(,L ,C2 ,G) *li*)))
        (append/3 C1 (list L) C3)
        (assert-clauses/3 C C3 G))))
    (M   (<and>
          (<pp> M)
          (<code> (error "assert clause"))))))

(define pathlim #f)
(let ((V #f))
  (set! pathlim 
        (case-lambda 
          ((x) (case x
                 ((assert)
                  (set! V #t)
                  #t)
                 ((retract)
                   (set! V #f)
                   #t)))

          (()     V))))
          
#;
(begin
  (<define> (prove2/4 S M Set Proof)
      (prove/3 1 Proof))
  (load "case.scm"))


(<define> (prove-compile S M Set Proof)
    (<and!> 
     (assert-clauses/2 M S))
    (<code> (assert-the-lit))
    (prove/3 1 5 Proof)
    #;(<pp> `(proved ,Proof)))

(<define> (prove/3 i PathLim Proof)
  (postpone-frame 1 1 250)
  (<pp> `(solving depth ,i))
  (<or>
   (<var> (Vs G Fs)
     (<db-lookup> *start* '* `(,Vs ,G ,Fs))
     (prove/6  Vs ((gp-lookup Fs)) 0 (list neg*) 
               1 0 1 0 '[] i Proof))
   (<and> 
    <fail>
    (<when> (<= i PathLim))
    (prove/3 (+ i 1) PathLim Proof))))
           
  

(define (potential N Ndepth Nbranch Nsize)
  (+ (* (gp-lookup N)            0.1)
     (* Nsize                    1.0)
     (* Nbranch                  0.1)
     (/ (random 1000) 100000.0)))

(define (u-length L)  
  (umatch #:mode - #:name 'u-length (L)
          ((X . L) (+ 1 (u-length L)))
          (()      0)))

(define (u-size L)
  (umatch #:mode - #:name 'u-size (L)
          ((X . L) (+ (u-size X) (u-size L)))
          (()      0)
          (_       1)))

(define (u-cplx l)
  (umatch #:mode - (l)
          (((n . _) . u)
           (+ (gp-lookup n) (u-cplx u)))
          (() 0)))

(define (prove/6 pr cc Lits Db c . x)
  (let ((Q (let loop ((L Lits) (D Db) (R '()))
             (umatch #:name 'prove/6 #:mode - (L D)
                     ((Lx . Ls) (Dx . Ds)
                      (let* ((frame (gp-newframe))
                             (pr2   (lambda () #f))
                             (ret   (apply prove/60 
                                           `(,pr2 ,cc ,L ,D 0 0 ,@x 'test))))
                        (gp-unwind frame)
                        ;;(pp `(tried ,Lx got ,ret))
                        (if ret
                            (loop Ls Ds (cons (cons ret (cons Lx Dx)) R))
                            #f)))
                     (_ () R)))))
    (if Q
        (let ((Q (sort Q (lambda (x y) (< (car x) (car y)))))
              (S (+ (let loop ((Q Q) (n 0))
                      (match Q
                        (((m . _) . l) (loop l (+ n m)))
                        (() n)))
                    c)))
          ;;(pp `(Q ,Q))
          (apply prove/60 `(,pr ,cc ,(map cadr Q) ,(map cddr Q) ,S ,c ,@x #f)))
        (u-abort pr))))


(<define> (prove/60 Lits Db c d Path PathLim 
                    Nbranch Nsize Ndepth Lem pl Proof test)
    ;(<pp> `(code ,Lits , Db))
    (<match> (Lits Proof Db)
      (() () () <cc>)
       
      ([Lit . Cla] _ [Db . Dbs]
       (<cut>
        (<and>
         ;(<pp> `(try ,Lit))
         
         (<var> (NegLit NegL Cla1 Db1 Proof1 Q Proof2 LitC LitP LitL G)
           ;;(<pp> `(try+ ,(length Path) ,(length Lem)))
           ;; Destructs the Proof into parts
           (<=> Proof [[[NegLit . Cla1] . Proof1] . Proof2])
           ;; search for LitC and LitP not matching
           (<not> (<and> (member/2 (cons Q LitC) Lits)
                         (member/2 LitP Path)
                         (<==> LitC LitP)))     

           (if (not test) (postpone Ndepth Nbranch) <cc>)
           (<set-cut>)

           (<if> (<or> (<=> ('- NegLit) Lit)
                       (<=> ('- Lit   ) NegLit))
                 (<and>
                  (<or> (<and>  ;the lemma pattern e.g. Lit known
                         (<and!>
                          (member/2 LitL Lem) 
                          (<==> Lit LitL))
                         ;(<pp> 'lemma)
                         (if test (<return> 0) <cc>)
                         (<=>  Cla1  '[])
                         (<=>  Proof1 '[]))
                        
                        (<and> ;Negl in path contradiction! 
                         (<and>
                          (member/2 NegL Path)
                          (<=> NegL  NegLit))
                         ;(<pp> `(negation ,NegL found in path))
                         (if test (<return> 0) <cc>)
                         (<=> Cla1      '[])
                         (<=> Proof1    '[]))
                        
                       
                        (<and> 
                         (if test 
                             (<let> ((r (length 
                                         (db-list (gp-lookup Db) NegLit))))
                               ;;(<pp> 'list)
                               (if (= r 0)
                                   (<return> #f)
                                   (<return> r)))
                             <cc>)
                                         
                         (<db-lookup> (gp-lookup Db) NegLit (list Cla1 G Db1))
                         #;(<pp-dyn> `(dive 
                                     ,Lit                  
                                     ,Cla1)
                                   `(backtrack ,Lit ,Cla1))
                         (<let> ((S (u-size Lit)))
                           (<when> (or #t (eq? (gp-lookup G) 'n)
                                       (< Ndepth pl)))
                           
                           (prove/6 Cla1 ((gp-lookup Db1)) c
                                    (cons Lit Path) PathLim
                                    (+ Nbranch (u-length Cla1))
                                    (+ Nsize S)
                                    (+ Ndepth  1)
                                    Lem pl Proof1))))
                  (if *cut* (<cut> <cc>) <cc>)
                  (prove/6 Cla Dbs d Path PathLim 
                           (- Nbranch 1) Nsize Ndepth
                           (cons Lit Lem) pl Proof2)))))))

      (_ _ _ (<cut> <fail>))))


(define (var->uvar x)
  (define table '())
  (define (get-var x)
    (let ((r (assoc x table)))
      (if r
          (cdr r)
          (let ((v (u-var!)))
            (set! table (cons (cons x v) table))
            v))))
            
  (let loop ((x x))
    (match x
      (('var! x) (get-var x))
      ((x . l)   (cons (loop x) (loop l)))
      (x x))))

(define-syntax define-problem
  (lambda (x)
    (define (get-axioms x)
      (syntax-case x (axiom conjecture)
        (((axiom n u) . l)
         #`(u #,@(get-axioms #'l)))
        (((conjecture . u) . l)
         (get-axioms #'l))
        (_ x)))

    (define (get-conjecture x)
      (syntax-case x (axiom conjecture)
        (((conjecture n u) . l)
         #`(u #,@(get-conjecture #'l)))
        ((_ . l)
         (get-conjecture #'l))
        (_ x)))

    (syntax-case x ()
     ((_ name . l)
      (with-syntax (((ax ...)  (get-axioms     #'l))
                    ((co ...)  (get-conjecture #'l)))        
        #'(define name           
            (let ((v '(ax ... (~ co) ...)))
              (lambda (set)
                (gp-clear)
                (let ((f (pp (map (lambda (x) 
                                    (var->uvar (make-matrix x set)))
                                  v)))
                      (s (u-var!)))
                  (<run> 1 (proof) (prove2/4 s f set proof)))))))))))
                
