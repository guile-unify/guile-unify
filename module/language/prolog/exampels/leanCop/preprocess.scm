;; dnf will push and down and move or up
(define (dnf x)
  (match x
    (('and ('or a b) c)
     `(or ,(dnf `(and ,a ,c))
          ,(dnf `(and ,b ,c))))
    
    (('and a ('or b c))
     `(or ,(dnf `(and ,a ,b))
          ,(dnf `(and ,a ,c))))

    (('and a b)
     (let ((a1 (dnf a))
           (b1 (dnf b)))
       (if (or (eq? (car a1) 'or)
               (eq? (car b1) 'or))
           (dnf `(and ,a1 ,b1))
           `(and ,a1 ,b1))))

    (('or a b)
     `('or ,(dnf a) ,(dnf b)))

    (a a)))



(define (skolem x v tr i set)
  (define (translate tr x)
    (let loop ((x x))
      (match x
        ((x . l) (cons (loop x) (loop l)))
        (x       (let ((v (assoc x tr)))
                   (if v
                       (cdr v)
                       x))))))

  (let loop ((s x))
    (match (pp s)
      ;; basic rewrite rules
      (('~ ('~ a)) 
       (loop a))

      (('~ ('all x f))
       (loop `(ex ,x (~ ,f))))

      (('~ ('ex x f))
       (loop `(all ,x (~ ,f))))
      
      (('~ ('or . l))
       (loop `(and ,@(map (lambda (x) `(~ ,x)) l))))

      (('~ ('and . l))
       (loop `(or ,@(map (lambda (x) `(~ ,x)) l))))
      
      (('and a)
       (loop a))

      (('and a b c . l)
       (loop `(and ,a (and ,b ,c ,@l))))

      (('or A)
       (loop A))

      (('or a b c . l)
       (loop `(or ,a (or ,b ,c ,@l))))
      
      (('=> a b)
       (loop `(or (~ ,a) ,b)))

      (('~ ('=> a b))
       (loop `(and ,a (~ ,b))))

      (('<=> a b)
       (if (eq? set 'def)
           (loop `(and (=> ,a ,b) (=> ,b ,a)))
           (loop `(or  (and ,a ,b) (and (~ ,a) (~ ,b))))))

      (('~ ('<=> a b))
       (loop `(or  (and ,a (~ ,b)) (and (~ ,a) ,b))))      
      
      (('ex (x) f)
       (loop `(ex ,x ,f)))

      (('ex (x . l) f)
       (loop `(ex ,x (ex ,l ,f))))

      (('ex x f)
       (let ((i2 (+ i 1))             
             (v2 (if (null? v) i `(,(gensym "g") ,@v))))
         (skolem f v (cons (cons x v2) tr) i2 set)))

      (('all (x) f)
       (loop `(all ,x ,f)))

      (('all (x . l) f)
       (loop `(all ,x (all ,l ,f))))

      (('all v1 f) 
       (let ((v2 `(var! ,(gensym "A"))))
         (skolem f (cons v2 v) (cons (cons v1 v2) tr) i set)))

      (('or a b)
       (let*-values
           (((nnf1 def1 paths1 i1) (skolem a v tr i  set))
            ((nnf2 def2 paths2 i2) (skolem b v tr i1 set)))
         (let ((def   (append def1 def2))
               (paths (* paths1 paths2))
               (nnf   (if (> paths1 paths2)
                          `(or ,nnf2 ,nnf1)
                          `(or ,nnf1 ,nnf2))))
           (values nnf def paths i2))))

      (('and a b)
       (let*-values
           (((nnf3 def3 paths1 i2) (skolem a v tr i set))
            ((def1 nnf1 i3)        (if (and (eq? (car nnf3) 'or)
                                            (eq? set 'def))
                                       (values
                                        (cons `(and (~ (^ ,i2 ,v)) ,nnf3)
                                              def3)
                                        `(^ ,i2 ,v)
                                        (+ i2 1))
                                       
                                       (values def3 nnf3 i2)))

            ((nnf4 def4 paths2 i4) (skolem b v tr i3 set))
            ((nnf2 def2 i1       ) (if (and (eq? (car nnf4) 'or)
                                            (eq? set 'def))
                                       
                                       (values
                                        (cons `(and (~ (^ ,i4 ,v)) ,nnf4)
                                              def4)
                                        `(^ ,i4 ,v)
                                        (+ i4 1))
                                       
                                       (values def4 nnf3 i4))))
         (values (if (> paths1 paths2)
                     `(and ,nnf2 ,nnf1) 
                     `(and ,nnf1 ,nnf2) )
                 (append def1 def2)
                 (+ paths1 paths2)
                 i1)))

      (lit
       (let ((lit (translate tr lit)))
         (values lit '() 1 i))))))


               
(define (def x y)
  (match x
    (() 
     y)

    ((('and a ('or b c)) . l) 
     (def `((and ,a ,b) (and ,a ,c) ,@l) y))

    ((a . l)
     (def l `(or ,a ,y)))))
    
(define (def-nnf x i set)
  (let-values (((nnf1 def1 n i) (skolem x '() '() i set)))
    (pp nnf1)
    (pp def1)
    (values (def def1 nnf1) i)))


(define (union2 x l)
  (match x
    (() 
     (match l 
       (() '()) 
       (_  (list l))))

    ((a . as)
     (if (member a l)
         (union2 as l)
         (let ((xn (match a 
                     (('- x) x) 
                     (_ `(- ,a)))))
           (if (member xn l)
               '()
               (union2 as (cons x l))))))))

(define (mat x)
  (match x
    (('or a b)
     (append (mat a) (mat b)))
    
    (('and a b)
     (union2 (mat a) (mat b)))

    (('~ a)  `(- ,a))
    (a        a)))

;leanCop's reo parameter is not supported
(define (make-matrix x set)
  (let ((x (if (and (member 'conj set)
                    (eq? (car x) '=>))
               `(=> (and ,(cadr x) <*>) (and <*> ,(caddr x)))
               x)))
    (mat (pp (if (member 'nodef set)
             (dnf (def-nnf x 1 'nnf))
             (if (and (not (member 'def set))
                      (eq? '=> (car x)))
                 (match x
                   ((_ b d)
                    (let*-values
                        (((i  df1) (def-nnf `(~ ,b) 1 'nnf))
                         ((i2 df2) (def-nnf  d      i 'def)))
                      `(or ,df2 ,(dnf df1)))))
                 (def-nnf x 1 'def)))))))
                         


                     

