
; :- op(1130, xfy, <=>). % equivalence
; :- op(1110, xfy, =>).  % implication
;                     % disjunction (;)
;                     % conjunction (,)
; :- op( 500, fy, ~).    % negation
; :- op( 500, fy, all).  % universal quantifier
; :- op( 500, fy, ex).   % existential quantifier
; :- op( 500, xfy, :).

;;Set up user operators in scheme !!
(set!-user-operator '<<=>> "<=>" 1130 ':infix  ':left)
(set!-user-operator '<=>>  "=>"  1110 ':infix  ':left)
(set!-user-operator '<not> "~"   500  ':prefix 'yes)
(set!-user-operator '<all> "all" 500  ':prefix 'yes)
(set!-user-operator '<ex>  "ex"  500  ':prefix 'yes)
(set!-user-operator '<:>   ":"   500  ':infix  ':left)
(arrange-opdata)
