(load "prolog-parser.scm")
(gp-set-postpone!)

(prolog-it "
prun() :- L = [[[5],2],1,[3,4]],
          X is 0,
          f(L,X).

f([H|L],N) :- N is N + 1,
              g(H,L,N).
f(H,N)     :- pk([H,N]), fail.
     
g(H,L,N)       :- f(H,N).
g(_,[H|L],N)   :- g(H,L,N).
g(_,[],_)      :- fail.
")
(run "prun()" #f)

(prolog-it "
prun() :- L = [[[5],2],1,[3,4]],
          postpone_frame,
          X is 0,
          f(L,X).

f([H|L],N) :- !, postpone,
              N is N + 1,
              g(H,L,N).
f(H,N)     :- pk([H,N]), fail.
     
g(H,L,N)       :- f(H,N).
g(_,[H|L],N)   :- g(H,L,N).
g(_,[],_)      :- fail.
")
(run "prun()" #f)

(prolog-it "
prun() :- L = [[[[[6,5],4],3],2],1],
          postpone_frame,
          X is 0,
          pk(X),
          f(L,X).

f([H|L],1) :- N is 0,
              g(H,L,N).
f([H|L],N) :- !, postpone,
              N is N + 1,
              g(H,L,N).

f(H,N)     :- pk([H,N]), fail.
     
g(H,L,N)       :- f(H,N).
g(_,[H|L],N)   :- g(H,L,N).
g(_,[],_)      :- fail.
")
(run "prun()" #f)

(prolog-it "
prun() :- L = [9,[9,[9,[9,[6,5],4],3],2],1],
          postpone_frame,
          X is 0,
          pk(X),
          f(L,X).

f([H|L],2) :- !, postpone_frame,
              N is 0,
              g(H,L,N).
f([H|L],N) :- !, postpone,
              N is N + 1,
              g(H,L,N).

f(H,N)     :- pk([H,N]), fail.
     
g(H,L,N)       :- f(H,N).
g(_,[H|L],N)   :- g(H,L,N).
g(_,[],_)      :- fail.
")
(run "prun()" #f)

(gp-use-token!)
(prolog-it "
prun() :- L = [9,[9,[9,[9,[6,5],4],3],2],1],
          postpone_frame__,
          X is 0,
          pk(X),
          f(L,X).

f([H|L],2) :- !, postpone_frame,
              N is 0,
              g(H,L,N).
f([H|L],N) :- !, postpone,
              N is N + 1,
              g(H,L,N).

f(H,N)     :- pk([H,N]), fail.
     
g(H,L,N)       :- f(H,N).
g(_,[H|L],N)   :- g(H,L,N).
g(_,[],_)      :- fail.
")
(run "prun()" #f)

(load "prolog-parser.scm")
(gp-set-postpone!)
(gp-use-token!)

(define (tree_max/2 L X Cut CC)
  (def f #:mode +
       ((X . L)   (max (f X) (f L)))
       (()        0)
       (X         (gp->scm X)))
  (gp-unify! X (f L))
  (CC Cut))

(prolog-it "
prun() :- L = [1,[3,[7,[15],8,[16]],
               4,[9,[17],10,[18]]],
               2,[5,[11,[19],12,[20]]],
               6,[13,[21],14,[22]]],
          postpone_frame_x(4,30,0.8),
          f(L).

f([H|L]) :- !,
            tree_max([H|L],X),
            postpone_x(X),
            g(H,L).

f(H)         :- postpone_x(H),pk(H), fail.

g(H,L)       :- f(H).
g(_,[H|L])   :- g(H,L).
g(_,[])      :- fail.
")
(run "prun()" #f)


(prolog-it "
prun() :- L = [1,[3,[7,[15],8,[16]],
               4,[9,[17],10,[18]]],
               2,[5,[11,[19],12,[20]]],
               6,[13,[21],14,[22]]],
          postpone_frame_minmax(30,0.8),
          f(L).

f([H|L]) :- !,
            tree_max([H|L],X),
            postpone(X),
            g(H,L).

f(H)         :- postpone(H),pk(H), fail.

g(H,L)       :- f(H).
g(_,[H|L])   :- g(H,L).
g(_,[])      :- fail.
")
(run "prun()" #f)


(prolog-it "
prun() :- L = [1,[3,[7,[15],8,[16]],
               4,[9,[17],10,[18]]],
               2,[5,[11,[19],12,[20]]],
               6,[13,[21],14,[22]]],
          postpone_frame_minmax(30,0.9),
          X is 0,
          f(L,X).

f([H|L],2) :- !,
              tree_max([H|L],X),
              postpone(X),
              N is 0,              
              g(H,L,N).

f([H|L],N) :- !, N is N + 1,
              g(H,L,N).

f(H,N)     :- pk([H,N]), fail.

g(H,L,N)       :- f(H,N).
g(_,[H|L],N)   :- g(H,L,N).
g(_,[],_)      :- fail.
")
(run "prun()" #f)



