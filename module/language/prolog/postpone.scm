(define-module (language prolog postpone)
  #:use-module (language prolog guile-log)
  #:use-module (language prolog umatch))

(include-from-path "language/prolog/postpone-code.scm")
