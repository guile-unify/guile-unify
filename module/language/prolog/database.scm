(define-module (language prolog database)
  #:use-module (language prolog guile-log)
  #:use-module (language prolog umatch))

(include-from-path "language/prolog/database-code.scm")
