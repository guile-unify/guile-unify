;;Term processing
(define (functor/3 Term Name Arity Cut CC)
  (if (gp-var? Term)
      (let* ((NA (gp->scm Arity))
	     (N  (string->symbol 
		  (string->append
		   (symbol->string (gp->scm Name))
		   '(#\/)
		   (number->string NA))))
	     (A  (list-tabulate N (lambda (N) (gp-var!)))))
	(umatch #:mode + Term ( ((':fkn ,N . A) (CC Cut))
				(_              (u-abort Cut)))))
      (umatch #:mode - Term Name
	      ( ((:fkn N . A) (+ N) (let ((N (gp->scm N)))
				      (if (gp-unify Arity (symbol-property
							   N ':arity))
					  (CC Cut)
				      (u-abort Cut))))
		((_ . _)      _      (error "not a atom or functor"))
		(A            (+ 0)  (if (gp-unify Name A)
					 (CC Cut)
					 (u-abort Cut)))))))


(def functor/2 #:mode +
     ((':fkn N . A) N2 Cut CC (if (gp-unify N N2)
				  (CC Cut)
				  (u-abort Cut)))
     ((_ . _)       _  _   _  (error "not a functor or atom"))
     ( N            N  Cut CC (CC Cut))
     ( _            _  Cut _  (u-abort Cut)))


(define (arg/3 N Term Arg Cut CC)
  (def f 
       ( 0    (+ (A . _)) (if (gp-unify A Arg)
			      (CC Cut)
			      (u-abort Cut)))
       ( N    (+ (A . L)) (f (- N 1) L)))
  (let ((X (gp->scm N)))
    (if (integer? X)
	(umatch #:mode + Term
		( ( (':fkn N . Args)
		    (f N Args))
		  ( (_ . _) (error "Term is not a functor"))
		  ( X       (if (eq? N 0)
				(CC Cut)
				(error "wrong number of arguments")))))
	(error "N in arg/3 is not an integer"))))

       
(define (copy_term/2 T1 T2 Cut CC)
  (def f
     ((- (X . L))  (+ (X2 . L2)) (begin (f X X2) (f L L2)))
     ((- (? var?))     _         'ok)
     ( X           (+  X)        'ok)
     ( _               _         (u-abort Cut)))
  (begin 
    (f T1 T2)
    (CC Cut)))

;this is gnu prolog!!
#;


(define (atom_length/2 Atm Len Cut CC)
  (if (gp-unify! Len (length (string->list (symbol->string (gp->scm Atm)))))
      (CC Cut)
      (u-abort Cut)))

(define (atom_concat/3 A B AB Cut CC)
  (if (gp-unify! AB (string->symbol 
		     (string-append 
		      (symbol->string (gp->scm A))
		      (symbol->string (gp->scm B)))))
      (CC Cut)
      (u-abort Cut)))


	 