(define-module (language prolog guile-log-pre)
  #:export (define-guile-log guile-log-macro? log-code-macro log-code-macro?))

(define (guile-log-macro? s)
  (and (symbol? s) (symbol-property s 'guile-log-macro?)))

(define (log-code-macro? s)
  (and (symbol? s) (symbol-property s 'log-code-macro?)))

(define (log-code-macro s)
  (set-symbol-property! s 'log-code-macro? #t))

(define-syntax define-guile-log
  (lambda (x)
    (syntax-case x ()
      ((_ n . l)
       #'(begin
           (set-symbol-property! 'n 'guile-log-macro? #t)
           (define-syntax n . l))))))
