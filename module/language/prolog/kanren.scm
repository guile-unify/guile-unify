(define-module  (language prolog kanren)
  #:use-module  (srfi srfi-9)
  #:use-module  (srfi srfi-11)
  #:use-module  (ice-9 pretty-print)
  #:export      (succeed fail sfail exists all all! all!! if-only 
                         any fails succeeds any-interleave any-union if-some 
                         all-interleave relation id-memv?? relation-head-let 
                         fact lift-to-relations let-gls == query promise-one-answer
                         solve solution project project/no-check predicate
                         nonvar! tarce-vars *equal? extend-relation-with-recur-limit empty-subst 
                         reify-subst trace-vars cout nl var? _
                         let-lv eigen occurs? ground? copy-term universalize unify
                         test-check extend-relation partially-eval-sgl intersect-relation))

(define errorf error)
(define (printf fmt . args) (apply format #t fmt args))

; like cout << arguments << args
; where argument can be any Scheme object. If it's a procedure
; (without args) it's executed rather than printed (like newline)

(define (cout . args)
  (for-each (lambda (x)
              (if (procedure? x) (x) (display x)))
            args))

;(define cerr cout)
(define (cerr . args)
  (for-each (lambda (x)
              (if (procedure? x) (x (console-output-port))
		(display x (console-output-port))))
            args))

(define nl (string #\newline))

(include-from-path "language/prolog/kanren/lib/scm-specific.scm")
(include-from-path "language/prolog/kanren/lib/term.scm")
(include-from-path "language/prolog/kanren/lib/kanren.ss")
