;;; PROLOG COMPILER

;; Copyright (C) 2001,2008,2009,2010 Free Software Foundation, Inc.

;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

;;;; Code:

(define-module (language prolog prolog-parser)
  #:use-module (language prolog umatch)
  #:use-module (language prolog parse-util)
  #:use-module (ice-9 pretty-print)
  #:use-module (ice-9 readline)
  #:use-module (srfi srfi-1)
  #:export     (run prolog-it load-prolog scheme-it parse print/1 g_pk/1 nextp u-continue))


(define union (lambda (x y) (lset-union      eq? x y)))
(define diff  (lambda (x y) (lset-difference eq? x y)))

(set! *pmax* 2000)
(reset-opdata)
(def-operators
  (<:->    ":-"  1200 :infix  :no    )	
  (<-->>   "-->" 1200 :infix  :no    )
  (<p:->   ":-"  1200 :prefix :no    )
  (<semi>  ";"   1100 :infix  :right )
  (<->>    "->"  1050 :infix  :right )
  (<comma> ","   1000 :infix  :right )
  (</+>    "\\+" 900  :prefix :yes   )
  (<:>     ":"   600  :infix  :right )

  (((<=>    "="  )
    (</=>  "\\="  )
    (<=..>  "=.." )
    (<==>   "=="  )
    (</==>  "\\==") 
    (<at<>  "@<"  )
    (<at=<> "@=<" )
    (<at>>  "@>"  )
    (<at>=> "@>=" ) 
    (<is>   "is"  )
    (<=:=>  "=:=" )
    (<=/=>  "=\\=")
    (<<>    "<"   )
    (<=<>   "=<"  )
    (<<=>   "<="  )
    (<>>    ">"   )
    (<>=>   ">="  )
    (<=>>   "=>"  ))
   700  :infix  :no    )
  
  ((( <o+> "+"   )
    ( <o-> "-"   )
    ( <A>  "/\\" )
    ( <V>  "\\/" ))
   500  :infix  :left)

  (((<o*>  "*"  )
    (<o/>  "/"  )
    (<o//> "//" ) 
    (<rem> "rem")
    (<mod> "mod")
    (<<<>  "<<")
    (<>>>  ">>"))
   400  :infix  :left)
  
  (((<**> "**")
    (<^>  "^" ))
   200  :infix  :right)

  (((<p+> "+")
    (<p-> "-")
    (<p/> "\\"))
   200  :prefix :yes))


(def-match-str <left>   "(")
(def-match-str <right>  ")")
(def-match-str <bleft>  "[")
(def-match-str <bright> "]")
(def-match-str <bar>    "|")
(def-match-str <dot>    ".")
(def-match-str <!>      "!")
(def-match-str <comma>  ",")
(def-match-str <_>      "_")
(def-match-str <fail>   "fail")
(def-match-str <true>   "true")

(define (sp?  x) (member x '(#\space #\tab #\newline)))
(def <sp> 
     ( ((? sp? X) . L) (cons X  L ))
     ( _               (cons #f #f)))

(def <spc>  
     (( (<> <*> <sp>) <com> . L) (<spc> L))
     (( (<> <*> <sp>)       . L) (transit <spc> <*>)))

(def <spc+> (( (<> <+> <sp>) . L) (transit <spc> <+>)))


(define (nw?  x) (ttok? x))
(def <nw> 
     ( ((? nw? X) . L) (cons X  L ))
     ( _               (cons #f #f)))

(def <stok>
     ((<stokJ> <stokL> . L) (cons (mk-var <stokH>.0 <stokL>.0) L))
     (_ (cons #f #f)))

(define (stk?  x) (member x '(#\space #\tab #\newline #\( #\) #\[ #\])))

(define (<stokH> X)
  (let ((H (car X)))
    (if (and (not (stk? H)) (not (tok? H)))
	X
	(cons #f #f))))

(define (<sk> X)
  (let ((H (car X)))
    (if (not (stk? H))
	X
	(cons #f #f))))

(def <stokL> 
     (( (<> <*> <sk>) . L)  (cons <*>.0 L))
     (_ (cons #f #f)))

(define (tok? x) (char-lower-case? x))
(def <tk> 
     ( ((? tok? X) . L) (cons X  L ))
     ( _                (cons #f #f)))


(define (ttok? x) (or (equal? x #\_) (char-is-both? x)  (char-numeric? x)))
(def <ttk> 
     ( ((? ttok? X) . L) (cons X  L ))
     ( _                (cons #f #f)))


(define (dig? x) (char-numeric? x))
(def <dig> 
     ( ((? dig? X) . L) (cons X  L ))
     ( _                         (cons #f #f)))



(def <nws>  (( (<> <*> <ttk>) . L) (cons <*>.0 L)))
(def <fkn>
     ((<tok> <spc> <left> <args> <right> . L)  
      (cons `(<fkn> ,(mk-fkn <tok>.0 <args>.0) ,<args>.0) L))
     ((<tok>                             . L)  
      (cons `(<fkn> ,(mk-fkn <tok>.0 '()) ())             L))        
     (_ (cons #f #f)))

(define (mk-fkn F A)  
    (let ((N (length A)))
      (if (> (length A) 0)
	  (string->symbol
	   (string-append 
	    (symbol->string F) "/" 
	    (number->string (length A))))
	  F)))
      

(define (concat x y)
  (string->symbol
   (string-append 
    (symbol->string x)
    (symbol->string y))))

(define (<noteoln> X)
  (if (equal? (car X) #\newline)
      (cons #f #f)
      (cons #t (cdr X))))
  
(def <com>
     ((#\% (<> <*> <noteoln>) . L)    (cons '<com> L))
     (_                               (cons #f #f)))

(def <args>
     ((<spc> <com>                             . L)  (<args> L))
     ((<spc> <term> <spc> <comma> <spc> <args> . L)  (cons (cons <term>.0 <args>.0) L))
     ((<spc> <term> <spc>                      . L)  (cons (cons <term>.0 '()     ) L))
     ((<spc>                                   . L)  (cons '() L)))

(def <arg>
     ( (#\s #\c #\m <sexp> . L) 
       (cons `(<scm> ,<sexp>.0) L))
     ( (#\f #\w #\h #\e #\n <sexp> . L)
       (cons `(<fkn> fwhen (,<sexp>.0) L)))
     ( (<var> . L)   (transit <var> <var>) )
     ( (<fkn> . L)   (stamp <fkn>)         )
     ( (<lis> . L)   (transit <lis> <lis>) )
     ( (<atm> . L)   (transit <atm> <atm>) )
     ( (<_>   . L)   (stamp <_>))
     (  _        (cons #f #f)          ))


(def <sexp>
     ((<spc> <left> <sexargs> <spc> <right> . L)
      (cons <sexargs>.0 L))
     (_ (cons #f #f)))
(def <sexargs>
     ((<spc> <right>           . L) (cons '() (cons #\) L)))
     ((<spc> <sterm> <sexargs> . L) (cons (cons <term>.0 <sexargs>.0) L))
     (_ (cons #f #f)))
     

(def <sterm>
     ( (<sexp> . L) (cons <sexp>.0           L))
     ( (<var>  . L) (cons `(u-deref <var>.0) L))
     ( (<stok> . L) (cons <tok>.0            L))     
     ( (<int>  . L) (cons <int>.0            L))
     ( (<str>  . L) (cons <str>.0            L))
     ( _ (cons #f #f)))
      

(define  (mk-var x) (string->symbol (list->string x)))
(def <var>
     (((? char-upper-case? H) <nws> . L)  (cons (mk-var (cons H <nws>.0)) L))
     (_                                   (cons #f #f)))

(def <lis>      
     ( (<bleft> <spc> <bright>              . L) (cons '(<atm> <eoln>) L))
     ( (<bleft> <spc> <lisb> <spc> <bright> . L) (cons <lisb>.0 L))
     ( _                                         (cons #f #f)))

(def <lisb>     
     (( <com>                            . L) (<lisb> L))
     (( <arg> <spc> <comma> <spc> <lisb> . L)  
      (cons `(cons ,<arg>.0 ,<lisb>.0) L))
     (( <arg> <spc> <bar> <spc> <arg>  . L)  
      (cons `(cons ,<arg>.0 ,<arg>.1)  L))
     (( <arg>              . L)  (cons `(cons ,<arg>.0 (<atm> <eoln>))       L))
     ( _                         (cons #f #f)))

(def <atm>
     ((<int> . L)    (transit <int>  <int> ))
     ((<str> . L)    (transit <str>  <str> ))
     ((<num> . L)    (transit <num>  <num> ))
     ((<tok> . L)    (transit <tok>  <tok> ))
     (_              (cons #f #f)))

(def <tok>
     (( <tk> (<> <*> <ttk>) . L) (cons (mk-var (cons <tk>.0 <*>.0)) L))
     (_                          (cons #f #f)))

(define (mk-num n) (string->number (list->string n)))
(def <int>
     ((   (<> <+> <dig>) . L)  (cons (mk-num <+>.0           ) L)) 
     (('- (<> <+> <dig>) . L)  (cons (mk-num (cons #\- <+>.0)) L))
     (_                        (cons #f #f)))

;;Not implemented yet
(def <num>  (_ (cons #f #f)))

(define (<strbody> X)
  (if (equal? (car X) #\')
      (cons #f #f)
      X))

(def <str>
     ((#\' (<> <*> <strbody>) #\' . L)   (cons `(<str> ,(list->string <*>.0)) L))
     (_                                  (cons #f #f)))

(def <stms> (((<> <+> <stm>) <spc>)  <+>.0))
       
(def <stm> 
     ((<spc> <com>                      . L)   (<stm> L))
     ((<spc> <term> <spc> <dot>         . L)   (cons  (cons <term>.0 '())               L))
     ((<spc> <term> <spc> <xy-op> <stm> . L)   (cons `(,<term>.0 ,<xy-op>.0 ,@<stm>.0)  L))
     (_ (cons #f #f)))

(def <stmo> 
     ((<spc> <com>                      . L)   (<stmo> L))
     ((<spc> <term> <spc> <right>        . L)   
      (cons  (cons <term>.0 '())                (cons #\) L)))
     ((<spc> <term> <spc> <xy-op> <stmo> . L)   
      (cons `(,<term>.0 ,<xy-op>.0 ,@<stmo>.0)    L          ))
     (_ (cons #f #f)))

(def mk-term
     ((Pr . L)  Pos     C        (mk-term L   Pos `(,Pr ,C)))
     (()       (Po . L) C        (mk-term '() L   `(,Po ,C)))
     (()       ()       C         C))

(def <x-> ((<spc> <x-op> . L) (stamp <x-op>)) (_ (cons #f #f)))
(def <-x> ((<spc> <op-x> . L) (stamp <op-x>)) (_ (cons #f #f)))
(def <term>
     (((<> <*> <x->) <spc> <arg> (<> <*> <-x>) . L) (cons (mk-term (reverse <*>.0) 
								   <*>.1 
								   `(:term ,<arg>.0))
							  L))
     ((<!>                                     . L) (cons '(:term <!>)       L))
     ((<fail>                                  . L) (cons '(:term <fail>)    L))
     ((<true>                                  . L) (cons '(:trem <true>)    L))
     ((<left> <stmo> <spc> <right>             . L) (cons `(:group ,<stmo>.0) L))
     (_ (cons #f #f)))

(define (parse x) (pretty-print (map treeify-ops (<stms> (string->list x)))))

;;Code do combine chunks of the same function into a groups preserving order.
(def sieve-out-H
     (H ((and X ((:term ('<fkn> H Arg)) . _)) . L)
	(cons X (sieve-out-H H L)))
     (H (X . L)                           (sieve-out-H H L))
     (H ()                               '()))

(def sieve-rem-H
     (H ((and X ((:term ('<fkn> H Arg)) . _)) . L)
	(sieve-rem-H H L))
     (H (X . L)                      (cons X (sieve-rem-H H L)))
     (H ()                          '()))

(def sieve 
     ((and X ( ((:term ('<fkn> H . _)) . _) . _))
      (cons (sieve-out-H H X)
	    (sieve (sieve-rem-H H X))))
     (() '()))

(define (compile X)
  (let* ((Tree   (<stms> X))
	 (Fkns   (sieve Tree)))
    (map generate-code Fkns)))

(define (gen-args N)
  (if (> N 0)  
      (cons (gensym "Arg") (gen-args (- N 1)))
      '()))

(def generate-code 
     ( (and Block (((:term ('<fkn> H _)) . _) . _))
       (let* ((Last? (map (lambda (X) #f) Block))
	      (Last? (reverse (cons #t (cdr Last?))))
	      (Code `(,@(map compile-stm Block Last?) ,(fin-stm (car Block))))
	      (Args  (gen-args (- (length (car Code)) 1))))	      
	 `(define (,H ,@Args Cut CC)
	    (umatch #:mode + ,@Args ,Code)))))

(def fin-stm
     (((:term ('<fkn> H Args)) . R) 
      `(,@(map (lambda (X) '_) Args) (u-abort Cut))))

(def comma-it
     (()        L   L)
     ((X Y . L) L  `((:term ,X) <comma> ,@(comma-it (cons Y L))))
     ((X      ) () `((:term ,X)))
     ((X      ) L  `((:term ,X) <comma> ,@L)))

(define rhs-vars '())
(def compile-stm
     (((:term ('<fkn> H Args)) . R) Last?
      (let* ((Stubs   (map sieve-out-stubs Args))     
	     (Rhs     (compile-rhs (if (pair? R) (cdr R) R)))
	     (Vars    (sieve-out-vars Args))
	     (RHSVars (sieve-out-vars R))
	     (FVars   (diff RHSVars Vars)))
	(set! rhs-vars RHSVars)
	`(,@Stubs ,(-: Last? FVars (treeify-ops (handle_ Rhs)))))))

(def handle_
     ( (X . L)  (cons (handle_ X) (handle_ L)))
     ( '_       '(u-var!))
     ( X        X))

(def <deep>
     ((:term ('<fkn> H Args)) (cons Args '()))
     ((Op <deep>)             (stamp <deep>))
     (_                       (cons #f #f)))

(def repl-deep
     ((:term ('<fkn> H Args)) Nargs `(:term (<fkn> ,H ,Nargs)))
     ((Op X)                  Nargs `(,Op (repl-deep X Nargs))))

(def compile-rhs
     ((and X ((<deep>) . L))  (let* ((Args  <deep>.0)
				     (Stubs (map sieve-out-stubs-rhs Args)))
				`(,(repl-deep (car X) Stubs) ,@(compile-rhs L))))
     (((:group X)    . L)   (cons `(:group ,(compile-rhs X)) (compile-rhs L)))
     (((Op A B)      . L)  `((,Op ,(compile-rhs A) ,(compile-rhs B)) 
                             ,@(compile-rhs L)))
     ((X             . L)   (cons X (compile-rhs L)))
     (()                   '()))

(def sieve-out-vars
     ((('<fkn> . L) . U) (union (sieve-out-vars L) (sieve-out-vars U)))
     ((('<lis>   L) . U) (union (sieve-out-vars L) (sieve-out-vars U)))
     (('cons A B)        (union (sieve-out-vars A) (sieve-out-vars B)))
     (('<var> V )        (cons V '()))
     (('<atm> A )       '())
     ((:group X)         (sieve-out-vars X))
     ((H . L)            (union (sieve-out-vars H) (sieve-out-vars L)))
     ('<_>              '())
     (_                 '()))   
      

(def consify_help
     ((H . L)  `(cons ,H ,(consify_help L)))
     (()       '()))

(def sieve-out-stubs
     (('<fkn> N Args)      (let ((Q (map sieve-out-stubs Args)))
			     (if (= (length Args) 0)
				 `(quote ,N)
				 (consify_help `((quote :fkn) (quote ,N) ,@Q)))))
     (('<lis> Args)        (sieve-out-stubs Args))
     (('<var>  V   )        V)
     ('<_>                  '_)
     ((:term X)            (sieve-out-stubs X))
     (('<atm> ('<tok> X)) `(quote ,X))
     (('<atm> (_     X))    X)
     (('<atm> '<eoln>)     '())
     (('cons A B)         `(cons ,(sieve-out-stubs A) ,(sieve-out-stubs B))))

(def sieve-out-stubs-rhs
     (('<fkn> N Args)      (let ((Q (map sieve-out-stubs-rhs Args)))
			     (if (= (length Args) 0)
				 `(quote ,N)
				  (consify_help `((quote :fkn) (quote ,N) ,@Q)))))
     ( '<_>                '_)
     (('<lis> Args)        (sieve-out-stubs-rhs Args))
     (('<var>  V   )        V)
     ((:term X)            (sieve-out-stubs-rhs X))
     (('<atm> ('<tok> X))  `(u-gp (quote ,X)))
     (('<atm> (_     X))   `(u-gp ,X ))
     (('<atm> '<eoln>)     '(u-gp '()))
     (('cons A B)          `(u-cons ,(sieve-out-stubs-rhs A) ,(sieve-out-stubs-rhs B))))


(define Next      (make-prompt-tag "gp-next"))
(define NextSemi  (make-prompt-tag "gp-next-semi"))
(define NextCatch (make-prompt-tag "gp-next-catch"))

(defmacro prompt (Pr Code)
  `(let ((,Prompt (u-prompt *gp-fi*)))
     ,Code))

(define (-: Last? Vars L)
  (let ((F  (lambda (Fi CC Nx) (if (eq? Fi 'first)
				   `(u-call *gp-fi* ,CC ,Nx)
				   `(,CC ,Nx))))
	(s  (gensym "state"))
	(Pr (gensym "Prompt")))
    (if (pair? Vars)
	`(let ,(map (lambda (V) `(,V (u-var!))) Vars)
	   ,(if Last?
		(tangle 'last 'CC 'Cut  L F)
		(tangle 'first 'CC '*gp-fi*-prompt L F)))
				     
	(if Last?
	    (tangle 'last 'CC 'Cut  L F)
	    (tangle 'first 'CC '*gp-fi*-prompt L F)))))
	

(define lam-true (lambda (x) #t))

(def tangle
     ;(Fi    CC Nx W              F  (begin (pk (cons Fi W)) (gp-next *gp-fi*)))
     (First CC Nx (:group X)   F
	    (tangle First CC Nx X F))

     (First CC Nx ('<comma> X Y) F     
	    (tangle First CC Nx X 
		    (lambda (Fi CC Nx) 
		      (tangle Fi CC Nx Y F))))
     
     (First CC Nx ('<semi>  X Y) F
	    (let* ((Pr  (gensym "prompt"))
		   (Prr (concat Pr '-prompt)))
	      `(umatch #:tag ,Pr
		       ( ( ,(tangle 'first CC Prr X F) ) 
			 ( ,(tangle 'first CC Nx Y F))))))

     (First CC Nx ('<->>   X Y) F 
	    (tangle First CC Nx X 
		    (lambda (Fi CC Nx2) 
		      (tangle Fi CC Nx Y F))))

     (First CC Nx ('<is>   X Y) F      
	    `(if (gp-unify! ,(tangle-left X) (u-gp ,(tangle-expr Y)))
		 ,(F First CC Nx)
		 (u-abort ,Nx)))

     (First CC Nx ('<=>    X Y) F      
	    (compile-unify (tangle-unify X) (tangle-unify Y)
			   `(u-abort ,Nx)
			   (lambda () (F First CC Nx))))

     (First CC Nx ('</=>   X Y) F      
	    (let ((fi (gensym "fi"))
		  (G  (gensym "F")))
	      `(let ((,fi (gp-newframe))
		     (,G  (lambda () ,(F 'last CC NX)))) 
		 ,(compile-unify (tangle-unify X) (tangle-unify Y))
		 (lambda () `(u-abort ,Nx))
		 '(,G))))

     (First CC Nx ('<=:=> X Y) F       
	    `(if (equal? ,(tangle-expr X) ,(tangle-expr Y))
		 ,(F First CC Nx)
		 (u-abort ,Nx)))

     (First CC Nx ('<=/=> X Y) F       
	    `(if (equal? ,(tangle-expr X) ,(tangle-expr Y))      
		 (u-abort ,Nx)
		 ,(F First CC Nx)))

     (First CC Nx ('<<>  X Y) F        
	    `(if (< ,(tangle-expr X) ,(tangle-expr Y))
		 ,(F First CC Nx)
		 (u-abort ,Nx)))

     (First CC Nx ('<=<>  X Y) F       
	    `(if (<= ,(tangle-expr X) ,(tangle-expr Y))
		 ,(F First CC Nx)
		 (u-abort ,Nx)))

     (First CC Nx ('<<=>  X Y) F       
	    `(if (<= ,(tangle-expr X) ,(tangle-expr Y))
		 ,(F First CC Nx)
		 (u-abort ,Nx)))

     (First CC Nx ('<>>  X Y) F        
	    `(if (> ,(tangle-expr X) ,(tangle-expr Y))
		 ,(F First CC Nx)
		 (u-abort ,Nx)))
 
     (First CC Nx ('<=>>  X Y) F       
	    `(if (>= ,(tangle-expr X) ,(tangle-expr Y))
		 ,(F First CC Nx)
		 (u-abort ,Nx)))

     (First CC Nx ('<>=>  X Y) F       
	    `(if (>= ,(tangle-expr X) ,(tangle-expr Y))
		 ,(F First CC Nx)
		 (u-abort ,Nx)))
          
     (First CC Nx ('</+> X)    F       
	    (let* ((Pr  (gensym "prompt"))
		   (Prr (concat Pr '-prompt)))
	      `(umatch #:tag ,Pr 
		       (((begin ,(tangle 'last lam-true Prr X 
					(lambda (Fi CC Nx) #t))
				(u-abort ,Nx)))
			(,(F First CC Nx))))))

     (First CC Nx (:term ('<fkn> atomic/1 (X))) F  
	    `(if (gp-atomic? X) 
		 ,(F First CC Nx)
		 (u-abort ,Nx)))

     (First CC Nx (:term ('<fkn> var/1 (X))) F  
	    `(if (gp-var? X) 
		 ,(F First CC Nx)
		 (u-abort ,Nx)))

     (First CC Nx (:term ('<fkn> nonvar/1 (X))) F  
	    `(if (not (gp-var? X))
		 ,(F First CC Nx)
		 (u-abort ,Nx)))
     
     (First CC Nx (:term ('<fkn> integer/1 (X))) F  
	    `(if (gp-integer? (u-atm? X))
		 ,(F First CC Nx)
		 (u-abort ,Nx)))
     
     (First CC Nx (:term ('<fkn> list/1 (X))) F  
	    `(if (gp-cons? X)
		 ,(F First CC Nx)
		 (u-abort ,Nx)))
     
     (First CC Nx (:term ('<fkn> fwhen (X))) F  
	    `(if ,X
		 ,(F First CC Nx)
		 (u-abort ,Nx)))
     
     (First CC Nx (:term ('<fkn> call (X))) F  
	    (let ((Fi (gensym "Fi")))
	      `(let ((,Fi (gp-newframe)))
		 (if ,(tangle 'last lam-true Nx X (lambda (Fi CC Nx) #t))
		     (begin (gp-unwind ,Fi) 
			    ,(F First CC Nx))
		     (u-abort ,Nx)))))

     (First CC Nx (:term ('<fkn> once (U))) F
	    (let ((Fi (gensym "Fi")))
	      `(let ((,Fi (gp-newframe)))
		 (if ,(tangle 'last lam-true Nx X (lambda (Fi CC Nx) #t))
		     (begin (gp-unwind ,Fi) 
			    ,(F First CC Cut))
		     (u-abort ,Nx)))))

     (First CC Nx (:term ('<fkn> catch (G C R))) F
	    `(let ((,Fi (gp-newframe)))
	       (cc-prompt NextCatch
		    (lambda ( ) ,(tangle 'last CC Nx G F))
		    (lambda (S) (let ((,Fi2 (gp-newframe)))
				  (if (S ,(tangle-left C))
				      (begin (gp-unwind ,Fi)
					     ,(tangle First CC Nx R F))
				      (begin (gp-enwind ,Fi2)
					     (S (u-abort NextCatch)))))))))

     (First CC Nx (:term ('<fkn> throw (U))) F
	    `(gp-unify (tangle-left U) (u-abort NextCatch)))


     ('first CC Nx (:term ('<fkn> H A))   F
	     (let ((Next (gensym "Next")))	       
	       `(u-call *gp-fi* ,H ,@(map tangle-left A) 
			,Nx (lambda (,Next) ,(F 'last CC Next)))))

     ('last CC Nx (:term ('<fkn> H A))   F    
	    (let ((Next (gensym "Next")))
	      `(,H ,@(map tangle-left A) 
		   ,Nx ,(lam-it Next F 'last CC))))
     
     (First CC Nx (:term '<!>        )   F    (F 'last CC 'Cut))
     (First CC Nx (:term '<fail>     )   F    `(u-abort ,Nx))
     (First CC Nx (:term '<true>     )   F    (F First CC Nx))
     (First CC Nx (X)                    F    (tangle First C Nx X F))
     (First CC Nx ()                     F    (F First CC Nx)))

(define (lam-it Next F Last CC)
  (let ((R (F Last CC Next)))
    (if (equal? R `(,CC ,Next))
	CC
	`(lambda (,Next) ,R))))

(def tangle-expr
     ( ('<o+> X Y) `(+ ,(tangle-expr X) ,(tangle-expr Y)))
     ( ('<o-> X Y) `(- ,(tangle-expr X) ,(tangle-expr Y)))
     ( ('<o*> X Y) `(* ,(tangle-expr X) ,(tangle-expr Y)))
     ( ('<o/> X Y) `(/ ,(tangle-expr X) ,(tangle-expr Y)))
     ( ('<p+> X)    (tangle-expr X))
     ( ('<p-> X)   `(- ,(tangle-expre X)))
     ( (:term X)   (tangle-expr X))
     ( ('<var> X)  `(u-scm ,X))
     ( ('<atm> (_  X))    X)
     ( ('<atm> '<eoln>) '())
     ( X            (error (format #f "~a not allowed in an expression!" X))))

(def tangle-left
     ;( X                 (begin (pk X) (gp-next *gp-fi*)))
     ( (:term X)         (tangle-left X))
     ( ('<fkn> N ())    `(quote ,N))
     ( ('<fkn> N A)      (tangle-left (consify_help `((<atm> (s :fkn)) 
						 (<atm> (s ,N  )) ,@A))))
     ( ('<var> X)        X)
     ( ('<atm> (_ X))   `(u-gp ,X))
     ( ('<atm> '<eoln>) '(u-gp '()))
     ( ('<lis> X)        (tangle-left X))
     ( ('cons X Y)      `(u-cons ,(tangle-left X) ,(tangle-left Y)))
     ( ()               '(u-gp '()))
     ( X                 X))

(def tangle-unify
     ;( X                 (begin (pk X) (gp-next *gp-fi*)))
     ( (:term X)         (tangle-unify X))
     ( ('<fkn> N ())    `(quote ,N))
     ( ('<fkn> N A)      (tangle-unify (consify_help 
					`((quote :fkn)
					  (quote ,N  ) 
					  ,@A))))
     ( ('<var> X)       `(uunquote ,X))
     ( ('u-scm  X)          X)   
     ( ('<atm> (_ X))    X)
     ( ('<atm> '<eoln>) '())
     ( ('<lis> X)        (tangle-unify X))
     ( ('cons X Y)      `(cons ,(tangle-unify X) ,(tangle-unify Y)))
     ( ()               '())
     ( X                 X))
 

(def compile-unify
     ;(X           Y  _    _       (begin (pk `(,X : ,Y)) (gp-next *gp-fi*)))
     (('uunquote  X)  Y  Next Cont   
      `(umatch #:tag   *gp-wii* 
	       #:mode  + 
	       #:raw
	       ,X ((,Y ,(Cont)) (_  ,Next))))

     (X  ('uunquote Y)  Next Cont   
	 `(umatch #:tag   *gp-wii* 
		  #:mode  +
		  #:raw 
		  ,Y ((,X ,(Cont)) (_  ,Next))))
     
     (('cons H1  L1) ('cons H2  L2) Next Cont
      (compile-unify H1 H2 Next (lambda () (compile-unify L1 L2 Next Cont))))
     (X           X  Next Cont    (Cont))
     (_           _  Next Cont    (error "unify always fails!")))
					      

(define (file->list fname)
  (let ((p (open-file fname "r")))
     (let read ((ret '())) 
      (let ((ch (read-char p)))
	(if (eof-object? ch)
	    (reverse ret)
	    (read (cons ch ret)))))))

(define-syntax load-prolog
  (lambda (x)
    (syntax-case x ()
      ((_ file)
       (with-syntax (((exp ...) (datum->syntax
                                 x 
                                 (compile (file->list (syntax->datum #'file))))))
	 #'(begin exp ...))))))

(define-syntax prolog-it
  (lambda (x)
    (syntax-case x ()
      ((_ str)
       (with-syntax (((exp ...) (datum->syntax
                                 x 
                                 (compile (string->list (syntax->datum #'str))))))
		    #'(begin exp ...))))))

(def mk-L
     ( (H . L) (if (eq? H '_)
		   (cons '(u-var!)     (mk-L L))
		   (cons `(scm->gp ,H) (mk-L L))))
     ( ()     '()))
  			    
;;*****************************  Utility ****************

(def g_pk/1 (X N CC (begin (pk (gp->scm X)) (CC N))))
     
     
(define (scheme-it str) (pretty-print (compile (string->list str))))


(define (symbol->list X) (string->list (symbol->string X)))


(define (mk-print L)
  (list->string
   (cons #\[
	 (if (null? L)
	     '(#\])
	     (let f ((X L))
	       (umatch X ( ( (H    )  (append (symbol->list H) '(#\])))
			   ( (H . L)  (append (symbol->list H) '(#\,) (f L))))))))))


(define (print/1 V N CC)
  (format #t "Data ~a~%" (gp->scm V))
  (CC N))


(define u-continue #f)

(define (nextp N CC)
  (if u-continue (u-abort N))
  (let ((C (lambda () (readline "more m, finish e, all a > "))))
    (let f ((Ret (C)))
      (cond  ((equal? Ret "m")  (u-abort N))
	     ((equal? Ret "e")  #t)
	     ((equal? Ret "a")  (begin (set! u-continue #t) (u-abort N)))
	     (#t                (f (C)))))))

  
(define (e Sexp c) (eval (macroexpand Sexp) (interaction-environment)))


(define (run_ s c)
  (let* ((A1 "start123  :- ")
	 (S1 (string-append A1 s ",nextp, start123.")))
    (e (car (compile (string->list S1))) c)
    (let* ((R (mk-print rhs-vars))
	   (S1 (string-append A1 
			      s 
			      (format #f ",print(~a), nextp." R))))
      (car (compile (string->list S1))))))

     
(defmacro run (x p) 
  `(begin 
     ,(run_ x (current-module))
     (gp-clear)
     (set! u-continue ,p)
     (umatch
      (
       ( (begin (start123 *gp-fi*-prompt (lambda (N) (u-abort N)))
		#t))
       ( #f )))))