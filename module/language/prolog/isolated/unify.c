/*
 	Copyright (C) 2009, 2010 Free Software Foundation, Inc.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/


#include<libguile.h>
#include<stdio.h>
#include "unify.h"


typedef scm_i_pthread_key_t scm_i_thread_key;

#  define SCM_I_CURRENT_THREAD                                          \
    ((scm_i_thread *) scm_i_pthread_getspecific (scm_i_thread_key))

# define scm_i_dynwinds()         (SCM_I_CURRENT_THREAD->dynwinds)


#define DB(X) 
#define DS(X)
#define gp_debug0(s)        DB(printf(s)      ; fflush(stdout))
#define gp_debug1(s,a)      DB(printf(s,a)    ; fflush(stdout))
#define gp_debug2(s,a,b)    DB(printf(s,a,b)  ; fflush(stdout))
#define gp_debug3(s,a,b,c)  DB(printf(s,a,b,c); fflush(stdout))
#define gp_debus0(s)        DS(printf(s)      ; fflush(stdout))
#define gp_debus1(s,a)      DS(printf(s,a)    ; fflush(stdout))
#define gp_debus2(s,a,b)    DS(printf(s,a,b)  ; fflush(stdout))
#define gp_debus3(s,a,b,c)  DS(printf(s,a,b,c); fflush(stdout))
/*
   000 - A ref to a new object
   010 - A cons ref
   110 - A cons rest
   100 - A rest

 111xx - unbounded
 101xx - eq
 011xx - val

  unbounded is a null reference aka X & 0b000  == 0
*/

/*
  We will work with three stacks.
  stack  - the variable stack
  cstack - storage stack to undo setted values
  wstack - frame stack in order to do batched backtracking
*/


typedef union gp_cstack_t
{
  SCM * id;
  SCM   val;
} gp_cstack_t;

typedef union gp_wstack_t
{
  gp_cstack_t* ci;
  SCM*      si;
} gp_wstack_t;
  
int  gp_nw      = 20000;
gp_wstack_t gp_wstack[20000];


int gp_nc      = 100000;
gp_cstack_t gp_cstack[100000];

int gp_ns      = 200000;
SCM gp_stack[200000];

SCM* gp_si;
gp_wstack_t *gp_wi;
gp_cstack_t *gp_ci;
gp_cstack_t *gp_nnc;
SCM *gp_nns;
gp_wstack_t *gp_nnw;

scm_t_bits gp_smob_t;

#define B(c) ((scm_t_bits) c)
#define GP_FF (~B(0))
#define GPM_UNBOUND B(0x1c)
#define GPI_EQ      B(0x18)
#define GPI_EQL     B(0x08)
#define GPI_VAL     B(0x10)
#define GPI_CONS     B(2)
#define GPM_EQ      (GP_FF ^ GPI_EQ)
#define GPM_EQL     (GP_FF ^ GPI_EQL)
#define GPM_VAL     (GP_FF ^ GPI_VAL)
#define GPM_CONS    (~ B(2))
#define GPM_POINTER B(0x6)
#define GP_TP_MASK   B(0x1e)
#define GP_UNBOUND(ref) (( SCM_UNPACK(ref) & GP_TP_MASK) == GPM_UNBOUND)
#define GP_POINTER(ref) (!(SCM_UNPACK(ref) & GPM_POINTER))
#define GP_CONS(ref)    (  SCM_UNPACK(ref) & GPI_CONS    )
#define GP_EQ(ref)      ( (SCM_UNPACK(ref) & GP_TP_MASK) == GPM_EQ)
#define GP_EQL(ref)     ( (SCM_UNPACK(ref) & GP_TP_MASK) == GPM_EQL)
#define GP_VAL(ref)     ( (SCM_UNPACK(ref) & GP_TP_MASK) == GPM_VAL)
#define GP_MK_FRAME_VAL(fr)  ((fr) & GPM_VAL)
#define GP_MK_FRAME_EQ(fr)   ((fr) & GPM_EQ)
#define GP_MK_FRAME_EQL(fr)  ((fr) & GPM_EQL)
#define GP_MK_FRAME_CONS(fr) ((fr) | GPI_CONS)
#define GP_UNMASK_CONSBIT(ref) SCM_PACK( (SCM_UNPACK(ref)) & GPM_CONS)

#define NIMP_SYMBOL(ref) (SCM_TYP7 (ref) == scm_tc7_symbol)
#define NIMP_CONSP(x)    ((1 & SCM_CELL_TYPE (x)) == 0)
#define GP_DFR  ((scm_t_bits) 0x20)
#define SCM_TO_FI(x) ( SCM_UNPACK(x) - 2 )
#define FI_TO_SCM(x) SCM_PACK((x) + 2)
#define FR_TO_WI(x)  (gp_wstack + (((x) & (~0x1f)) >> 4))
scm_t_bits gp_frame_id = GPM_UNBOUND;



/*
static inline SCM* GP_GETREF(SCM x)
{
  scm_t_bits up = SCM_UNPACK(x);
  gp_debug1("getref> unpacked %x\n",up);
  return (SCM *) up;
}

static inline SCM GP_UNREF(SCM *x)
{
  scm_t_bits up = (scm_t_bits) x;
  gp_debug1("unref> unpacked %x\n",up);
  return SCM_PACK(up);
  }
*/

#define GP_GETREF(x) ((SCM *) (x))

#define GP_UNREF(x)  ((SCM)   (x))


inline static SCM GP_IT(SCM* id)
{
  scm_t_bits v = (scm_t_bits) id;
  return SCM_PACK(v | B(2));
}


static SCM* UN_GP(SCM scm)
{
  scm_t_bits b = SCM_UNPACK(scm);
  return (SCM *) (b & (~B(2)));
}


/*
static inline int GP(SCM scm)
{
  return SCM_UNPACK(scm) & B(2);
}
*/

 /*
#define GP_IT(id) SCM_PACK(SCM_UNPACK(GP_UNREF(id)) + B(2))
#define UN_GP(scm) GP_GETREF(SCM_PACK(SCM_UNPACK(scm) - B(2)))
 */
#define GP(scm) (SCM_UNPACK(scm) & B(2))


SCM gp_unbound_sym;
SCM gp_unbound_str;
SCM gp_cons_sym;
SCM gp_cons_str;

static inline  scm_t_bits gp_get_frame(SCM *id)
{
  SCM ref = *id;
  SCM val = *(id+1);
  scm_t_bits ret;
  if (GP_POINTER(ref))
    ret = SCM_UNPACK(val);
  else if (GP_CONS(ref))    
    ret = SCM_UNPACK(GP_UNMASK_CONSBIT(ref));     
  else if(!GP_UNBOUND(ref))
    ret = SCM_UNPACK(val) | GP_TP_MASK;
  else
    ret = SCM_UNPACK(ref);

  gp_debug1("get_frame> %x\n",(int) ret);
  
  return ret;
}

#define GP_TEST_CSTACK if(gp_ci > gp_nnc) scm_out_of_range(NULL, SCM_I_MAKINUM(gp_nc))

static inline  scm_t_bits gp_store_var_2(SCM *id,scm_t_bits fr)
{
  GP_TEST_CSTACK;
  if(!(GP_CONS(*id)))
    {
      if(gp_frame_id > fr)
	{      
	  (*(gp_ci+0)).id  = id;
	  (*(gp_ci+1)).val = *(id+0);
	  (*(gp_ci+2)).val = *(id+1);
	  gp_ci += 3;
	}
    }
  
  return gp_frame_id;    
}


static inline void gp_set_val(SCM *id, SCM v)
{
  scm_t_bits fr;
  SCM val; 
  DS(if(!v) gp_debus0("setting 0 value\n");)
  fr = gp_get_frame(id);
  fr = gp_store_var_2(id,fr);
  val = SCM_PACK(GP_MK_FRAME_VAL(fr));
  gp_debug2("set-val> id = %x, val = %x\n",(int) id, (int) SCM_UNPACK(val));
  *(id + 0) = val;
  *(id + 1) = v;
}


static inline  void gp_set_ref(SCM *id, SCM ref)
{
  scm_t_bits fr = gp_get_frame(id);
  fr = gp_store_var_2(id,fr);
  gp_debug0("setref>\n");
  *(id + 0) = ref;
  *(id + 1) = SCM_PACK(fr);
}

static inline  void gp_set_eq(SCM *id, SCM v) 
{
  scm_t_bits fr = gp_get_frame(id);
  gp_debug1("got fr %x\n",fr);
    DS(if(!v) gp_debus0("setting 0 value\n");)
  fr = gp_store_var_2(id,fr);
  gp_debug0("handled storing\n");
  *(id + 0) = SCM_PACK(GP_MK_FRAME_EQ(fr));
  gp_debug2("set id = %x 0 val = %x\n",id,SCM_UNPACK(*id)) ;
  *(id + 1) = v;
  gp_debug2("set id = %x 1 val = %x\n",id,SCM_UNPACK(v));
}

static inline void gp_set_unbound(SCM *id, SCM v) 
{
  gp_debug0("set-unbound>\n");
  scm_t_bits fr = gp_get_frame(id);
  fr = gp_store_var_2(id,fr);
  *(id + 0) = SCM_PACK(fr);
  *(id + 1) = v;
}


struct gp_lookup
{
  SCM        *id;
  SCM         val;
  SCM         ref;
};

struct gp_lookup gp_ret;

/*
static void gp_lookup(SCM *id);

inline static void gp_lookup1(SCM *id)
{
  gp_ret.ref = *id;
  gp_debug1("lookup> id = %x\n",id);
  if(GP_POINTER(gp_ret.ref))
    {
      gp_debug0("lookup> pointer\n");
      gp_lookup(GP_GETREF(gp_ret.ref));
      gp_set_ref(id,GP_UNREF(gp_ret.id));
      return;
    }
  else 
    if(GP_UNBOUND(gp_ret.ref))
      {	
	gp_debug0("lookup> unbound\n");
	gp_ret.val = (SCM) 0;
      }
    else
      gp_ret.val = *(id + 1);
  if(GP_CONS(gp_ret.ref))
    {
      gp_debug0("looked a cons up!\n");
    }
  else
    {
      gp_debug0("looked a value up!\n");
    }
  gp_ret.id = id;
  return;
}

static void gp_lookup(SCM *id)
{
  gp_ret.ref = *id;
  gp_debug1("lookup> id = %x\n",id);
  if(GP_POINTER(gp_ret.ref))
    {
      gp_debug0("lookup> pointer\n");
      gp_lookup1(GP_GETREF(gp_ret.ref));
      gp_set_ref(id,GP_UNREF(gp_ret.id));
      return;
    }
  else 
    if(GP_UNBOUND(gp_ret.ref))
      {	
	gp_debug0("lookup> unbound\n");
	gp_ret.val = (SCM) 0;
      }
    else
      gp_ret.val = *(id + 1);
  if(GP_CONS(gp_ret.ref))
    {
      gp_debug0("looked a cons up!\n");
    }
  else
    {
      gp_debug0("looked a value up!\n");
    }
  gp_ret.id = id;
  return;
}
*/

static void gp_lookup(SCM *id)
{
  SCM ref, *stack[10];
  int sp;
  sp = 0;

 retry:
  ref = *id;  
  if(GP_POINTER(ref))
    {
      if(SCM_LIKELY(sp<10))
	{
	  stack[sp++] = id;
	  id = GP_GETREF(ref);
	  goto retry;
	}
      else
	{
	  gp_debus0("long chain\n");
	  gp_lookup(id);
	  while(sp>0) gp_set_ref(stack[--sp],GP_UNREF(gp_ret.id));
	  return;
	}
    }

  gp_ret.id  =  id;
  gp_ret.ref =  ref;
  if(GP_UNBOUND(ref))
    {	
      gp_ret.val = (SCM) 0;
    }
  else
    {
      gp_ret.val = *(id + 1);
      DS(if(!gp_ret.val) gp_debus0("looked up a value 0 - problem!!\n");)
    }

  while(sp>0) gp_set_ref(stack[--sp],GP_UNREF(id));
  return;
}




#define GP_TEST_WSTACK if(gp_wi > gp_nnw) scm_out_of_range(NULL, SCM_I_MAKINUM(gp_nw))


SCM_DEFINE (gp_gp_newframe, "gp-newframe",0,0,0,(),
	    "Created a prolog frame to backtrack from")
#define FUNC_NAME s_gp_gp_newframe
{
  SCM ret;

  GP_TEST_WSTACK;
  gp_debug0("newframe>\n");
  gp_debug2("newframe>  store fr = %x amd wi = %x\n",gp_frame_id,gp_wi);

  *(gp_wi+0) = (gp_wstack_t) gp_ci;
  *(gp_wi+1) = (gp_wstack_t) gp_si;
  gp_wi += 2;
  ret = FI_TO_SCM(gp_frame_id);
  gp_frame_id +=  ((scm_t_bits) 0x20);
  return ret;
}
#undef FUNC_NAME

static inline SCM gp_newframe()
{
  SCM ret;

  GP_TEST_WSTACK;
  *(gp_wi+0) = (gp_wstack_t) gp_ci;
  *(gp_wi+1) = (gp_wstack_t) gp_si;
  gp_wi += 2;
  ret = FI_TO_SCM(gp_frame_id);
  gp_frame_id +=  ((scm_t_bits) 0x20);
  return ret;
}


static inline gp_unwind(SCM fr)
{
  gp_cstack_t *ci_old;
  gp_cstack_t *i;   
  
  gp_debus0("unwind>\n");
  if(SCM_UNLIKELY( fr >= gp_frame_id))
    {
      gp_debus2("unwind> got the same fi : %x >= %x\n",fr,gp_frame_id);
      return SCM_BOOL_F;
    }
    
  gp_frame_id = SCM_TO_FI(fr);
  gp_wi       = FR_TO_WI(gp_frame_id);
  
  gp_debus2("unwind> got fr = %x amd wi = %x\n",gp_frame_id,gp_wi);fflush(stdout);
  
  ci_old = gp_ci;
  
  gp_ci =(gp_wi+0)-> ci;
  gp_si =(gp_wi+1)-> si;
  
  for(i = ci_old-3; i >= gp_ci; i-=3)
    {
      SCM *id;
      
      id      = (i+0)->id;
      *(id+0) = (i+1)->val;
      *(id+1) = (i+2)->val;
    }
  return SCM_BOOL_T;
}

SCM_DEFINE (gp_gp_unwind, "gp-unwind",1,0,0,(SCM fr),
	    "unwind the prolog stack")
#define FUNC_NAME s_gp_gp_unwind
{
  gp_unwind(fr);
}
#undef FUNC_NAME

#define GP_TEST_STACK if(gp_si > gp_nns) scm_out_of_range(NULL, SCM_I_MAKINUM(gp_ns))
static inline  SCM* gp_mk_var()
{
  SCM *ret;
  SCM ref;  

  GP_TEST_STACK;
  gp_debug1("test stack handled! %x\n",gp_si);
  ret  = gp_si;
  gp_si += 2;

  ref = SCM_PACK(gp_frame_id); 
  *ret = ref;
  *(ret + 1) = 13 << 2 + 2;
  gp_debug2("returning from mk_var %x, ref = %x\n",ret,SCM_UNPACK(ref));
  return ret;
}

SCM_DEFINE(gp_mkvar, "gp-var!", 0, 0, 0, (),
	   "makes a unbounded gp variable")
#define FUNC_NAME s_gp_mkvar
{
  return GP_IT(gp_mk_var());
}
#undef FUNC_NAME


static inline SCM gp_mk_cons() 
{
  SCM *ret;
  scm_t_bits fi = GP_MK_FRAME_CONS(gp_frame_id);
  GP_TEST_STACK;
  ret = gp_si;
  gp_si += 5;
  
  *(ret+0) = SCM_PACK(fi);
  *(ret+1) = SCM_PACK(gp_frame_id);
  *(ret+3) = SCM_PACK(gp_frame_id);
  return GP_UNREF(ret);
}

static inline  SCM *mk_gp_var(SCM *id,SCM data)
{
  if(SCM_IMP(data) || NIMP_SYMBOL(data))
    {
      gp_debug1("gp> set an eq! id = %x",id);
      gp_set_eq(id,data);
    }
  else
    {
      gp_debug1("gp> set a var!, id = %x",id);
      gp_set_val(id,data);
    }
  return id;
}
          
static void mk_gp_cons(SCM *id,SCM data)
{
  SCM car,cdr;
  car = SCM_CAR(data);
  cdr = SCM_CDR(data);
  gp_debug2("gp> (cons %x %x)\n",SCM_UNPACK(car),SCM_UNPACK(cdr));
  
  if(SCM_I_CONSP(car))
  {
    SCM* ref;

    ref = GP_GETREF(gp_mk_cons());

    gp_debug0("gp> car is a cons\n");
  
    mk_gp_cons(ref,car);
    gp_set_ref(id + 1,GP_UNREF(ref));
  } 
  else 
    {
      gp_debug0("gp> car is a mkvar\n");
      mk_gp_var(id + 1,car);
    }

  if(SCM_I_CONSP(cdr)) 
    {
      SCM* ref;
      gp_debug0("gp> cdr is a cons\n");
      ref = GP_GETREF(gp_mk_cons());
      mk_gp_cons(ref,cdr);
      gp_set_ref(id+3,GP_UNREF(ref));
    } 
  else 
    {
      gp_debug0("gp> cdr is a var\n");
      mk_gp_var(id + 3,cdr);  
    }

  return;
}

static inline  SCM *mk_gp(SCM data)
{
  gp_debug0("gp> Not a gp\n");
  if(SCM_I_CONSP(data))
    {
      SCM *ref;
      
      gp_debug0("gp> Got cons!\n");
      ref = GP_GETREF(gp_mk_cons());
      mk_gp_cons(ref,data);
      return ref;
    }
  else
    {
      SCM *ref;
      
      gp_debug0("gp> makes a variable!\n");
      ref = gp_mk_var();
      mk_gp_var(ref,data);
      return ref; 
    }
}


SCM_DEFINE(gp_mk, "scm->gp", 1, 0, 0, (SCM data),
	   "creates a gp object from scheme repr")
#define FUNC_NAME s_gp_mk
{
  return GP_IT(mk_gp(data));
}
#undef FUNC_NAME


static int gp_recurent(SCM *id1,SCM *id2)
{
  gp_lookup(id2);  
  gp_debug0("recurent> looked up data\n");
  if(id1 == gp_ret.id  )  
    {
      gp_debug0("recurent> Found a recurence!!\n");
      return 1;  
    }
  
  if(GP_CONS(gp_ret.ref))
    {
      gp_debug0("recurent> got a cons\n");
      id2 = gp_ret.id;
      return gp_recurent(id1,id2+1) || gp_recurent(id1,id2+3);
    }

  gp_debug0("recurent> atom!\n");
  return 0;
}

static SCM smob2scm_gp(SCM *id)
{
  gp_debus0("gp2scm> in gp part\n");
  
  gp_lookup(id);

  gp_debus0("gp2scm> lookup\n");

  if(gp_ret.val == 0)
    {
      gp_debus0("gp2scm> unbounded\n");
      return gp_unbound_sym;
    }
  
  if(GP_CONS(gp_ret.ref))
    {
      gp_debus0("gp2scm> cons!\n");
      id = gp_ret.id;
      return scm_cons(smob2scm_gp(id+1),smob2scm_gp(id+3));
    }
  gp_debus2("gp2scm> scm-value %x at id %x\n", SCM_UNPACK(gp_ret.val),gp_ret.id);
  return gp_ret.val;
}


SCM_DEFINE( smob2scm, "gp->scm", 1, 0, 0, (SCM scm),
	    "creates a scheme representation of a gp object")
#define FUNC_NAME s_smob2scm
{
  gp_debus0("gp2scm> start!\n");
  if(GP(scm))
    {
      SCM *id;

      gp_debus0("gp2scm> a gp!\n");
      id = UN_GP(scm);
      gp_lookup(id);

      if(gp_ret.val == 0)
	{
	  gp_debus0("gp2scm> unbounded\n");
	  return gp_unbound_sym;
	}

      if(GP_CONS(gp_ret.ref))
	{
	  gp_debus0("gp2scm> cons! at main\n");
	  id = gp_ret.id;
	  return scm_cons(smob2scm_gp(id+1),smob2scm_gp(id+3));
	}      
      return smob2scm_gp(gp_ret.id);
    }
  else 
    return scm;
}
#undef FUNC_NAME

static int gp_unify(SCM *id1, SCM *id2)
{ 
  SCM * stack[20];
  int   sp;
  struct gp_lookup ret2;
  
  sp = 0;
  
#define U_NEXT					\
  {						\
  if(SCM_UNLIKELY(sp==0))			\
    {						\
      return 1;					\
    }						\
  else						\
    {						\
      id2 = stack[--sp];			\
      id1 = stack[--sp];			\
      goto retry;				\
    }						\
}
    
 retry:
  gp_lookup(id2);
  ret2 = gp_ret;
  gp_lookup(id1);
  gp_debug0("unify> looked up with\n");

  if(gp_ret .val == 0) 
    {
      if(ret2.val == 0 && gp_ret.id == ret2.id)
	{
	  gp_debug0("unify> var == var");
	  U_NEXT;
	}
      else
	goto  unbound1;
    }

  if(ret2.val == 0) 
    {
      if(gp_ret .val == 0 && gp_ret.id == ret2.id)
	{
	  gp_debug0("unify> var == var");
	  U_NEXT;
	}
      else
	goto  unbound2;
    }

  //conses!!
#define DO_CONS						\
  {							\
    gp_debug0("unify> cons\n");				\
    id1 = gp_ret.id;					\
    id2 = ret2.id;					\
    if(SCM_UNLIKELY(sp >= 18))				\
      {							\
	if(!gp_unify(id1 + 3, id2 + 3)) return 0;	\
	id1 = id1 + 1;					\
	id2 = id2 + 1;					\
	goto retry;					\
      }							\
    else						\
      {							\
	stack[sp++] = id1 + 3;				\
	stack[sp++] = id2 + 3;				\
	id1 = id1 + 1;					\
	id2 = id2 + 1;					\
	goto retry;					\
      }							\
  }


  if(GP_CONS(gp_ret.ref))
    {
      if(GP_CONS(ret2.ref))
	{
	  DO_CONS;
	}
      else
	return 0;
    }

  if(GP_CONS(ret2.ref))
    {
      if(GP_CONS(gp_ret.ref))
	{
	  DO_CONS;
	}
      else
	return 0;
    }

  if(GP_EQ(gp_ret.ref) || GP_EQ(ret2.ref))
    {
      gp_debug2("unify> (eq ~x ~x)\n",gp_ret.val,ret2.val);
      if(gp_ret.val == ret2.val)
	{U_NEXT;}
      else
	return 0;
    }
    
  gp_debug2("unify> (equal ~x ~x)\n",gp_ret.val,ret2.val);
  if(scm_is_true(scm_equal_p(gp_ret.val,ret2.val)))
    {U_NEXT;}
  else
    return 0;
 
 unbound1: 
  gp_debug0("unify> unbound1\n");
  id1 = gp_ret.id;
  id2 = ret2.id;
  if(GP_CONS(ret2.ref) && gp_recurent(id1,id2)) return 0;    
  gp_set_ref(id1,GP_UNREF(id2));
  U_NEXT;

 unbound2: 
  gp_debug0("unify> unbound2\n");
  id1 = gp_ret.id;
  id2 = ret2.id;
  if(GP_CONS(gp_ret.ref) && gp_recurent(id2,id1)) return 0;    
  gp_set_ref(id2,GP_UNREF(id1));
  U_NEXT;
}

static int gp_unify_raw(SCM *id1, SCM *id2)
{ 
  SCM * stack[20];
  int   sp;
  struct gp_lookup ret2;
  
  sp = 0;
  
#define U_NEXT					\
  {						\
  if(SCM_UNLIKELY(sp==0))			\
    {						\
      return 1;					\
    }						\
  else						\
    {						\
      id2 = stack[--sp];			\
      id1 = stack[--sp];			\
      goto retry;				\
    }						\
}
    
 retry:
  gp_lookup(id2);
  ret2 = gp_ret;
  gp_lookup(id1);
  gp_debug0("unify> looked up with\n");

  if(gp_ret .val == 0) 
    {
      if(ret2.val == 0 && gp_ret.id == ret2.id)
	{
	  gp_debug0("unify> var == var");
	  U_NEXT;
	}
      else
	goto  unbound1;
    }

  if(ret2.val == 0) 
    {
      if(gp_ret .val == 0 && gp_ret.id == ret2.id)
	{
	  gp_debug0("unify> var == var");
	  U_NEXT;
	}
      else
	goto  unbound2;
    }

  //conses!!
#define DO_CONS						\
  {							\
    gp_debug0("unify> cons\n");				\
    id1 = gp_ret.id;					\
    id2 = ret2.id;					\
    if(SCM_UNLIKELY(sp >= 18))				\
      {							\
	if(!gp_unify_raw(id1 + 3, id2 + 3)) return 0;	\
	id1 = id1 + 1;					\
	id2 = id2 + 1;					\
	goto retry;					\
      }							\
    else						\
      {							\
	stack[sp++] = id1 + 3;				\
	stack[sp++] = id2 + 3;				\
	id1 = id1 + 1;					\
	id2 = id2 + 1;					\
	goto retry;					\
      }							\
  }


  if(GP_CONS(gp_ret.ref))
    {
      if(GP_CONS(ret2.ref))
	{
	  DO_CONS;
	}
      else
	return 0;
    }

  if(GP_CONS(ret2.ref))
    {
      if(GP_CONS(gp_ret.ref))
	{
	  DO_CONS;
	}
      else
	return 0;
    }

  if(GP_EQ(gp_ret.ref) || GP_EQ(ret2.ref))
    {
      gp_debug2("unify> (eq ~x ~x)\n",gp_ret.val,ret2.val);
      if(gp_ret.val == ret2.val)
	{U_NEXT;}
      else
	return 0;
    }
    
  gp_debug2("unify> (equal ~x ~x)\n",gp_ret.val,ret2.val);
  if(scm_is_true(scm_equal_p(gp_ret.val,ret2.val)))
    {U_NEXT;}
  else
    return 0;
 
 unbound1: 
  gp_debug0("unify> unbound1\n");
  id1 = gp_ret.id;
  id2 = ret2.id;
  gp_set_ref(id1,GP_UNREF(id2));
  U_NEXT;

 unbound2: 
  gp_debug0("unify> unbound2\n");
  id1 = gp_ret.id;
  id2 = ret2.id;
  gp_set_ref(id2,GP_UNREF(id1));
  U_NEXT;
}

SCM_DEFINE(gp_gp_unify,"gp-unify!",2,0,0,(SCM v1, SCM v2),
	   "unifies two gp variables")
#define FUNC_NAME s_gp_gp_unify
{
  SCM *vv1, *vv2;
  
  vv1 = UN_GP(v1);
  vv2 = UN_GP(v2);
  return gp_unify(vv1,vv2) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_unify_raw,"gp-unify-raw!",2,0,0,(SCM v1, SCM v2),
	   "unifies two gp variables, no recurrent check")
#define FUNC_NAME s_gp_gp_unify_raw
{
  SCM *vv1, *vv2;
  
  vv1 = UN_GP(v1);
  vv2 = UN_GP(v2);
  return gp_unify_raw(vv1,vv2) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(gp_varp,"gp-var?",1,0,0,(SCM x),
	   "Test for an unbound variable.")
#define FUNC_NAME s_gp_varp
{
  
  gp_lookup(UN_GP(x));
  return GP_UNBOUND(gp_ret.ref) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(gp_atomicp,"gp-atomic?",1,0,0,(SCM x),
	   "Test for a atomic gp")
#define FUNC_NAME s_gp_atomicp
{
  gp_lookup(UN_GP(x));
  return (!GP_UNBOUND(gp_ret.ref) && !GP_CONS(gp_ret.ref)) 
    ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_consp,"gp-cons?",1,0,0,(SCM x),
	   "Test for a cons gp")
#define FUNC_NAME s_gp_consp
{
  gp_debug0("gp-cons?>\n");
  gp_lookup(UN_GP(x));
  return GP_CONS(gp_ret.ref) ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_lookup, "gp-lookup", 1, 0, 0, (SCM id),
	   "lookup gp variable content")
#define FUNC_NAME s_gp_gp_lookup
{
  gp_lookup(UN_GP(id));
  return GP_IT(gp_ret.id);
}
#undef FUNC_NAME

SCM_DEFINE(gp_cons_plus, "gp-cons-+?", 2, 0, 0, (SCM x, SCM e),
	   "Check to see if a cons, if var make a cons.")
#define FUNC_NAME s_gp_cons_plus
{
  gp_debus0("cons plus\n");
  SCM ret;
  gp_lookup(UN_GP(x));
  if(GP_CONS(gp_ret.ref))
    return e;
  if(gp_ret.val == 0)
    {
      if(SCM_UNPACK(e) == 2)
	{
	  int ret = gp_newframe();
	  SCM ref = gp_mk_cons();
	  gp_set_ref(gp_ret.id,ref);
	  return SCM_PACK(ret);
	}
      else 
	{
	  SCM ref = gp_mk_cons();
	  gp_set_ref(gp_ret.id,ref);
	  return e;
	}
    }
  return SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_cons_minus, "gp-cons--?", 2, 0, 0, (SCM x),
	   "Check to see if a cons")
#define FUNC_NAME s_gp_cons_minus
{
  SCM ret;
  gp_lookup(UN_GP(x));
  if(GP_CONS(gp_ret.ref))
    return SCM_BOOL_T;
  return SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_car, "gp-car", 1, 0, 0, (SCM x),
	   "Take car of cons cell")
#define FUNC_NAME s_gp_car
{
  gp_lookup(UN_GP(x));
  if(GP_CONS(gp_ret.ref))
    return GP_IT(gp_ret.id+1);
  printf("TODO, make me a exception, car ofnoncons object\n");
}

SCM_DEFINE(gp_cdr, "gp-cdr", 1, 0, 0, (SCM x),
	   "Take cdr of cons cell")
#define FUNC_NAME s_gp_cdr
{
  gp_lookup(UN_GP(x));
  if(GP_CONS(gp_ret.ref))
    return GP_IT(gp_ret.id+3);
  printf("TODO, make me a exception, car ofnoncons object\n");
}
#undef FUNC_NAME

#undef FUNC_NAME

SCM_DEFINE(gp_equal_minus, "gp-equal--?", 2, 0, 0, (SCM x, SCM u),
	   "Check for equal equality")
#define FUNC_NAME s_gp_equal-minus
{
  gp_lookup(UN_GP(u));
  if(GP_EQ(gp_ret.ref))
    return (gp_ret.val == x) ? SCM_BOOL_T : SCM_BOOL_F;
  return scm_equal_p(gp_ret.val,x);
}
#undef FUNC_NAME

SCM_DEFINE(gp_gp_cons, "gp-cons", 2, 0, 0, (SCM x, SCM y),
	   "unify consing two variables")
#define FUNC_NAME s_gp_gp_cons
{
  SCM *cons   = GP_GETREF(gp_mk_cons());
  gp_set_ref(cons + 1,UN_GP(x));
  gp_set_ref(cons + 3,UN_GP(y));
  return GP_IT(GP_UNREF(cons));
}
#undef FUNC_NAME

SCM_DEFINE(gp_equal_plus, "gp-equal-+?", 3, 0, 0, (SCM x, SCM u, SCM e),
	   "Check for equal equality")
#define FUNC_NAME s_gp_equal-minus
{
  gp_lookup(UN_GP(u));
  if(gp_ret.val == 0)
    {
      if(SCM_UNPACK(e) == 2)
	{
	  int ret = gp_newframe();
	  mk_gp_var(gp_ret.id,x);
	  return SCM_PACK(ret);
	}
      else
	{
	  mk_gp_var(gp_ret.id,x);
	  return e;
	}
    }
  
  if(GP_EQ(gp_ret.ref))
    return (gp_ret.val == x) ? e : SCM_BOOL_F;
  if(GP_CONS(gp_ret.ref)) return SCM_BOOL_F;
  scm_is_true(scm_equal_p(gp_ret.val,x)) ? e : SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(gp_clear, "gp-clear", 0, 0, 0, (),
	   "resets the unifyier stack")
#define FUNC_NAME s_gp_clear
{
  gp_si  = gp_stack;
  gp_wi  = gp_wstack;
  gp_ci  = gp_cstack;
  gp_frame_id = GPM_UNBOUND;
  return SCM_BOOL_T;
}
#undef FUNC_NAME


SCM_DEFINE(gp_i_load, "<load>", 0, 0, 0, (), "")
#define FUNC_NAME s_gp_i_load
{
  return SCM_PACK(i_load);
}
#undef FUNC_NAME

SCM_DEFINE(gp_i_scheme, "<scheme>", 0, 0, 0, (), "")
#define FUNC_NAME s_gp_i_scheme
{
  return SCM_PACK(i_scheme);
}
#undef FUNC_NAME


SCM_DEFINE(gp_i_match, "<match>", 0, 0, 0, (), "")
#define FUNC_NAME s_gp_i_match
{
  return SCM_PACK(i_match);
}
#undef FUNC_NAME


SCM_DEFINE(gp_i_unify, "<+>", 0, 0, 0, (), "")
#define FUNC_NAME s_gp_i_unify
{
  return SCM_PACK(i_unify);
}
#undef FUNC_NAME



SCM_DEFINE(gp_i_pop, "<pop>", 0, 0, 0, (), "")
#define FUNC_NAME s_gp_i_pop
{
  return SCM_PACK(i_pop);
}
#undef FUNC_NAME

SCM_DEFINE(gp_i_ins, "<insert>", 0, 0, 0, (), "")
#define FUNC_NAME s_gp_i_ins
{
  return SCM_PACK(i_insert);
}
#undef FUNC_NAME


SCM_DEFINE(gp_i_eq, "<eq>", 0, 0, 0, (), "")
#define FUNC_NAME s_gp_i_eq
{
  return SCM_PACK(i_eq);
}
#undef FUNC_NAME


SCM_DEFINE(gp_i_var, "<var>", 0, 0, 0, (), "")
#define FUNC_NAME s_gp_i_var
{
  return SCM_PACK(i_var);
}

#undef FUNC_NAME
SCM_DEFINE(gp_i_arbr, "<_>", 0, 0, 0, (), "")
#define FUNC_NAME s_gp_i_arbr
{
  return SCM_PACK(i_arbr);
}
#undef FUNC_NAME


SCM_DEFINE(gp_i_end, "<end>", 0, 0, 0, (), "")
#define FUNC_NAME s_gp_i_end
{
  return SCM_PACK(i_end);
}
#undef FUNC_NAME

SCM_DEFINE(gp_i_cons, "<cons>", 0, 0, 0, (), "")
#define FUNC_NAME s_gp_i_cons
{
  return SCM_PACK(i_cons);
}
#undef FUNC_NAME

static void gp_init()
{
  /* stack initializations */
  gp_clear();
  gp_nns = gp_stack  + gp_ns - 10;
  gp_nnw = gp_wstack + gp_nw - 10;
  gp_nnc = gp_cstack + gp_nc - 10;
  gp_unbound_str = scm_from_locale_string ("<gp>");
  gp_unbound_sym = scm_string_to_symbol (gp_unbound_str);
  gp_cons_str    = scm_from_locale_string ("<gp-cons>");
  gp_cons_sym    = scm_string_to_symbol (gp_cons_str);
} 


void init_unify()
{
  gp_init();
#ifndef SCM_MAGIC_SNARFER
     #include "unify.x"
#endif
}

/*
  Local Variables:
  c-file-style: "gnu"
  End:
*/

