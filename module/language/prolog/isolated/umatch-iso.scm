;;; UNIFY MATCH MATCHER COMPILER

;; Copyright (C) 2001,2008,2009,2010 Free Software Foundation, Inc.

;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

;;; Code:
(define-module (language prolog umatch)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:export     (gp-clear gp-unify! gp-newframe gp-unwind gp-var! 
			 gp->scm scm->gp gp-atom gp-cons fast-match def 
                         fun umatch *gp-fi* g-member g-iright u-prompt
                         u-abort u-var! u-call u-deref gp-atomic?
                         gp-var? gp-cons?
                         <umatch>))

(defmacro u-prompt (x) `(<gp> (gp-prompt   ,x)))
(defmacro u-abort  (x) `(<gp> (gp-unprompt ,x)))
(defmacro u-var!   ()  `(<gp> (g-var!        )))
(defmacro u-call   x   `(<gp> (g-call     ,@x)))
(defmacro u-scm    (x) `(<gp> (gp-scm     ,x)))

(load-extension (string-append "libguile-" (effective-version))
                "scm_init_vm")

(define *vars* '())
(define *n*     0)

(define (before? x)
  (letrec ((pred (lambda (y) (eq? x y)))
	   (f    (lambda (v)
		   (match v
			  ( (((? pred q) . u) . l) #t    )
			  ( (x                . l) (f l) )
			  ( ()                     #f    )))))
    (f *vars*)))


(define (mk-var! x)
  (let ((ret `(,x ,*n*))
	(i    *n*))
    (set! *n*    (+ 1 *n*))
    (set! *vars* (cons ret *vars*))
    `(,i 'ok)))


(define *ins* '())
    
(define (var? x)
  (if (symbol? x)
      (let ((H (car (string->list (symbol->string x)))))
	(char-upper-case? H))
      #f))

(define (match? x)
  (if (symbol? x)
      (if (not (eq? x '<>))
	  (let* ((L (string->list (symbol->string x)))
		 (H (car L))
		 (R (car (reverse L))))
	    (and (equal? H #\<)
		 (equal? R #\>)))
	  #f)
      #f))
	    
(define (next-sym x)
  (let ((w (reverse(string->list (symbol->string x)))))
    (match w
	   ((#\9 . l) (set! w `(#\0 #\9 ,@l)))
	   ((#\8 . l) (set! w (cons #\9 l)))
	   ((#\7 . l) (set! w (cons #\8 l)))
	   ((#\6 . l) (set! w (cons #\7 l)))
	   ((#\5 . l) (set! w (cons #\6 l)))
	   ((#\4 . l) (set! w (cons #\5 l)))
	   ((#\3 . l) (set! w (cons #\4 l)))
	   ((#\2 . l) (set! w (cons #\3 l)))
	   ((#\1 . l) (set! w (cons #\2 l)))
	   ((#\0 . l) (set! w (cons #\1 l)))
	   ( X        (set! w `(#\0 #\. ,@X))))
    (string->symbol (list->string (reverse w)))))

(define (mk-nrest x) (string->symbol (string-append (symbol->string x) "...")))
(define (mode? x)    (member x '(+ - *)))

(define *int* '())

(define (concat x y) (string->symbol 
                      (string->append 
                       (symbol->string x)
                       (symbol->string y))))
(define (mkenv n)
  (list (concat n '-stack)
        (concat n '-prompt)
        (concat n '-gp)))
(define (stack  e) (car   e))
(define (prompt e) (cadr  e))
(define (gp     e) (caddr e))
(define (init-env e)
  `((,(stack  e) '())
    (,(prompt e) #f)
    (,(gp     e) -1)))

;here follows a model how we make use of prompts in order to save
;stack space in ordinary guile scheme, using prompts are a bit slow but this
;is expected to change.
(define *prompt* (make-prompt-tag "*prompt*"))

;model
#;
(let ((stack  '())
      (prompt #f)
      (gp     -1))
  (call-with-prompt 
   *prompt*
   (lambda () (set! prompt *prompt*)
           ,@code1)
   (lambda (s)
     (set! stack '())
     (gp-unwind   gp)
     (set! gp -1)
     (call-with-prompt 
      *prompt*
      (lambda () (set! prompt *prompt*)
              ,@code2)
                     
      and-so-on...))))

(define (compile-pattern pat mode recur env)
  (letrec 
   ((make-0 
     (lambda (pat m env cc)
	(define (make-1 pat m cc)
	  (let ((ret (if (eq? pat '())
			 (make-0 pat m cc)    ;(make-0 cc m '<end-it>)
			 (make-0 pat m cc))))
	    (if (> (length ret) 0)
		`((begin ,@ret))
		'())))

	(define (make-box LL Fs Lo m cc)
	  (if (pair? Fs)
	      `((<u-label> ',(car LL))
		(<u-goto>  ',(cadr LL))
                (<u-dup>)
		,@(make-0 (car Fs) m '<end-it>)
                (<u-skip>)
		(<u-goto> ',Lo)
		,@(make-box (cdr LL) (cdr Fs) Lo m cc))
	      '()))

        (define (m-id m) (match m ('* 2) ('- 3) ('+ 4)))
        (define (ui r) (if r `(u-insert ,env) `(uu-insert ,env)))

        (define (u-push e x) `(set! ,(stack e) (cons ,x ,(stack e))))
        (define (u-pop  e  ) `(let ((*q* (car ,(stack e))))
                                (set! ,(stack e) (cdr ,(stack e)))
                                *q*))
        (define (u-skip e)   `(set! ,(stack e) (cdr ,(stack e))))
        (define (u-dup  e  ) `(set! ,(stack e) 
                                    (cons (car ,(stack e)) ,(stack e))))
        (define (u-eq   e m x c) 
          `(if (gp-equal? x ,(u-pop e) ,(m-id m))
               (begin ,@c)
               (abort-to-prompt ,(prompt e))))

        (define (u-unquote e m x c)
          `(if (,(if recur 'gp-unify 'gp-unify-raw)
                ,(u-pop e) ,x)
               (begin ,@c)
               (abort-to-prompt ,(prompt e))))

        (define (u-? e m c)
          `(if *q* (begin ,@c) (abort-to-prompt ,(prompt e))))

        (define (or-it M Eold Env F L)
          (match L 
            ((H1 H2 . L)    
             `(call-with-prompt 
               *prompt*
               (lambda () 
                 (set! ,(prompt Env) *prompt*)
                 ,@(make-0 H N Env `(closure ,F)))
               (lambda (,(gensym "s"))
                 (set! ,(stack Env) '())
                 (gp-unwind ,(gp Env))
                 ,(or-it M Eold Ebv 'last F (cons H2 L)))))

            ((H)    
           `(call-with-prompt 
             *prompt*
             (lambda () 
               (set! ,(prompt Env) *prompt*)
               ,@(make-0 H N Env `(closure ,F)))
             (lambda (,(gensym "s"))
               (abort-to-prompt ,(prompt Eold)))))))
     
	(match pat
               ((('+1+  x  p))      (begin 
				      (mk-var! x) 
				      `(,(u-push env x) ,@(make-0 p m env cc))))
	       ((('+1+  x  p) . l)  (begin 
				      (mk-var! x) 
				      `(,(u-push env x) ,@(make-0 p m env l))))

	       ('_                 `(,(u-skip env) 
                                     ,@(make-0 cc m env '<end-it>)))
	       (((? mode? m2) x)   (if (eq? m2 m)
				       (make-0 x m env cc)
				       (make-0 x m2 env `(,m ,m ,cc))))

	       (((? mode? m1) _ (('+1+ x ((? mode? m2) p)) . l))
		(make-0 `((+1+ ,x (,m1 ,m1 (,m2 ,p))) ,@l) m env cc))

	       (((? mode? m1) _ ((? mode? m2) x)) (make-0 `(,m2 ,x) m env
                                                          `(,m1 ,m1 ,cc)))
	       (((? mode? m1) _ x)  (if (eq? m1 m)
					(make-0 x m  env cc)
					(make-0 x m1 env cc)))
	       (('and x y . l)     `((begin (u-dup ,env)
                                            (begin ,@(make-0 x m env 
                                                             '<end-it>)) 
                                            (begin ,@(make-0 `(and ,y ,@l)
                                                             m env cc)))))
	       (('and x      )      (make-0 x m env cc))
	       
	       (('quote   x)      `((u-eq ,env ,m ',x 
                                          ,(make-0 cc m env '<end-it>))))
	       
               ((or ('unquote x) ('uunquote x))
                (begin (set! *ins* 
                             (lset-union eq? *ins* `(,x)))
                       `(u-unquote ,env ,m ,x
                                   ,@(make-0 cc m env
                                             '<end-it>))))

	       (('? x p)            (make-0 `(and (? ,x) ,p) m env cc))
	       (('? 'var?)         `((u-? ,env ',m var?) 
                                     ,@(make-0 cc m env '<end-it>)))
	       (('? x    )          `(let ((*q* (,@x *q*)))
                                       (u-? env m (make-0 cc m env 
                                                          '<end-it>))))

	       (('or . l)           (let* ((e    (gensym "state"))
                                           (nenv (mkenv e))
                                           (ie   (init-env nenv))
                                           (F    (gensym "F")))
                                      `(let ,(cons `(,F (lambda () 
                                                          (make-0 cc m env
                                                                  '<end-it>)))
                                                   ,(or-it M env nenv F l)))))

               (('closure F)        `(,F))

	       (((? match? x)       . l)  (make-0 `((<> ,x    ) ,@l) m env cc))
	       ((('<> x . r)        . l)
                (let f ((y (next-sym x)))
                  (if (before? y)
                      (f (next-sym y))
                      (begin
                        (mk-var! y)
                        (set! *int* (cons y *int*))
                        (let ((z  (mk-nrest y)))
                          (mk-var! z)
                          (set! *int* (cons z *int*))
                          `(let ((*q* (,x ,@r ,(u-pop env))))
                             (let ((y (car *q*)))
                               (if ,y
                                 (let ((,z (cdr *q*)))
                                   ,(u-push env z)
                                   ,@(make-0 l m env cc))
                                 (abort-to-prompt ,(prompt env))))))))))

	       ((or ('cons x y) (x . y))
                `(if (u-cons x y ,(stack env))
                     (begin 
                       ,@(make-0 x m '<end-it>)
                       ,@(make-0 y m cc))
                     (abort-to-prompt ,(stack env))))

	       ('<end-it>           (if (eq? cc '<end-it>) 
                                        '() 
                                        (make-0 cc m env '<end-it>)))
               
	       (()                  (u-eq env m '() 
                                          (make-0 cc m env '<end-it>)))
	       ((? var? x)          
                (if (before? x)
                    (u-unquote env m x
                               (make-0 cc m env
                                       '<end-it>))
                    (begin (mk-var! x) 
                              `(let ((,x ,(u-pop env)))
                                ,@(make-0 cc m '<end-it>)))))

               (x                 (u-eq env m x (make-0 cc m '<end-it>)))
	       ))))
   `(,@(make-0 pat mode '<end-it>))))





(define (prepare args body)
  (match body
	 ((line . lines) (let* ((r    (reverse line))
				(code (car r))
				(l    (cdr r))
				(pat  (map (lambda (a p) `(+1+ ,a ,p)) args (reverse l))))
			   (cons `(,pat ,code) (prepare args lines))))
	 (()             '())))

(define (rem-vs y x)
  (match y 
	 (((v i) . b) (if (member v x)
			  (rem-vs b x)
			  (cons (list v i) (rem-vs b x))))
	 ( x x)))

;;After this
(define (handle name mode tag recur args code oldv first)
  (match code
	 (((pat code) . v)
	  (begin
	    (set! *vars* '())
	    (set! *int*  '())
	    (set! *n*     0)
	    (set! *ins*  '())
	    (let* ((env       (mkenv tag))
                   (compiled  (if (null? pat)
                                  '()
                                  (compile-pattern pat mode recur)))
                   
		   (lv        (map car *vars*))
		   (mode-id   (match mode ('* 2) ('- 3) ('+ 4)))

		   (next      (handle name mode tag recur args v (lset-union eq? *vars* (cons tag lv)) #f))

		   (defs      (fold-right (lambda (x a)
					    (match x 
						   ((var id) 
						    (if #f
							a
							(if (member var args)
							    a
							    (cons `(,var #f) a))))))
					  '()
					  (rem-vs *vars* oldv)))
		   (defs      (if (null? oldv)
				  `((,tag    #f) 
				    (*gp-mo* #f)
				    (*gp-jp* #f)
				    (*gp-sp* #f) ,@defs) 
				  defs))

		   (args      (fold-right (lambda (x l)
					    (match x ((var id) `(',var ,var ,@l))))
					  '() *vars*)))
	      `(let ,(init-env env)
                 (call-with-prompt 
                  *prompt*
                  (lambda () 
                    (set! ,(prompt env) *prompt*)
                    ,@compiled)
                  (lambda (,(gensym "s"))
                    (if (> ,(gp env) 0) (gp-unwind ,(gp env)))
                    (set! ,(stack env) '())
                    ,next))))))
	 (()  `(error ,(format #f "no match in ~a" name)))))


;(def f (a b b) (a b b))
(define (mk-vars-again x)
  (if (null? x)
      '()
      (let ((V (car x)))
	`(',V ,V ,@(mk-vars-again (cdr x))))))

(define (skip-args v args)
  (if (null? v)
      '()
      (if (member (car v) args)
	  (skip-args (cdr v) args)
	  (cons (car v) (skip-args (cdr v) args)))))

(define (hhandle name mode tag recur args code)
  (let* ((a     (handle name mode tag recur args code '() #t)))
    `(<umatch> ,a)))


(defmacro def (n . data)
  (let ((mode  '*       )
        (recur #t       )
        (tag   '*gp-fi* ))
   (let keys ((data data))
     (match data
            (('#:tag  t . l)  (begin (set! tag  t) (keys l)))   
            (('#:raw    . l)  (begin (set! recur #f) (keys l)))          
	    (('#:mode o . l)  (begin (set! mode o) (keys l)))
	    (data             (let* ((args   (map (lambda (x) (gensym "arg"))
						  (cdr (match data ((p . l) p)))))
				     (rew    (prepare args data)))
				`(define ,n (lambda (,@args)  ,(hhandle n mode tag recur args rew)))))))))


(defmacro fun data
 (let ((name "anon")
       (tag   '*gp-fi* )
       (recur #t       )
       (mode '*))
   (let keys ((data data))
     (match data
	    (('#:name n . l)  (begin (set! name n) (keys l)))
            (('#:tag  t . l)  (begin (set! tag  t) (keys l)))   
            (('#:raw    . l)  (begin (set! recur #f) (keys l)))          
	    (('#:mode o . l)  (begin (set! mode o) (keys l)))             
	    (data             (let* ((args   (map (lambda (x) (gensym "arg"))
						  (cdr (match data ((p . l) p)))))
				     (rew    (prepare args data)))
				`(lambda (,@args) ,(hhandle name mode tag recur args rew))))))))

(defmacro umatch data
  (let ((name  "anon"   )
        (tag   '*gp-fi* )
        (recur #t       )
	(mode  '*       ))
    
    (let keys ((data data))
      (match data
	     (('#:name n . l)  (begin (set! name   n) (keys l)))
	     (('#:mode o . l)  (begin (set! mode   o) (keys l)))
             (('#:raw    . l)  (begin (set! recur #f) (keys l)))
	     (('#:tag  t . l)  (begin (set! tag    t) (keys l)))             
	     (data             (let* ((rev   (reverse data))
				      (pats  (car rev))
				      (args  (reverse (cdr rev)))
				      (rew   (prepare args pats)))   
				 `,(hhandle name mode tag recur args rew)))))))
