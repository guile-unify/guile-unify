;;; PARSER UTILITIES OPERATOR COMPILER

;; Copyright (C) 2001,2008,2009,2010 Free Software Foundation, Inc.

;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

;;; Code:
(define-module (language prolog parse-util)
  #:use-module (language prolog umatch)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 pretty-print)
  #:export     (def-operators def-optypes reset-opdata <xy-op> <x-op> <op-x> def-match-str <+> <*> 
                 stamp transit <pk> <pr> op->str op->prio treeify-ops set!-operator *infix* *prefix* *postfix* 
                 *pmax* arrange-opdata))

(define (pp x) (pk (pretty-print x)) x)

;;******************** UTILITIES ****************
;; Simple operator parser.

;; ((1)) + ((2)  * (3))  one need to go through the code and insert
;; + -> 2 + 2
;; * -> 1 + 1 
;; plus insert numbers at the edges.

(define (treeify-ops x) (if (pair? x)
                            (car (treeify-ops0 (pre-tree-and-validate x)))
                            x))

(define (abs x) (if (< x 0) (- x) x))
(define (sgn x) (if (< x 0) -1 1))

(def treeify-ops0
     (()                      '())
     ((N1 (? pair? X) N2    )  (cons (may-tree X) #f))
     ((N1 (? pair? X) N2 . L)  (let ((E1 (sgn N1))
                                     (E2 (sgn N2))
                                     (N1 (abs N1))
                                     (N2 (abs N2)))
                                 (if (and (eq? N1 N2) (eq? E1 -1)) (pk (set! N1 (- N1 1/2))))
                                 (if (>= N1 N2)  
                                     (let ((Res (treeify-ops0 (cons X (cons N2 L)))))
                                       (if (cdr Res) (treeify-ops0 (cons N1 Res)) Res))
                                     (cons (may-tree X) (cons N2 L)))))

     ((X N Op . L)             (let* ((R   (treeify-ops0 L))
				      (Y   (car R))
				      (Z   (cdr R)))
				 (if Z
                                     (cons `(,Op ,(may-tree X) ,Y) Z)
                                     (cons `(,Op ,(may-tree X) ,Y) #f)))))

(def may-tree
     ((:term  X)    `(:term ,X))
     ((:group X)    `(:group ,(car (treeify-ops0 X))))
     ((Op X)        `(,Op ,(may-tree X)))
     (X              X))
				
(define *pmax* 0)
(def pre-tree-and-validate 
     ( ()   '())
     ( Y    `(,*pmax* ,@(pre-tree-and-validate0 Y) ,*pmax*)))

(def pre-tree-and-validate0
     ( (Term1 Op1 Term2 Op2 . L) (if (or (associative? Op1) (associative? Op2) (not (eq? (op->prio Op1) (op->prio Op2))))
				     (gp-next *gp-fi*)
				     (error (format #f "These Two Operators are not allowed in sequence ~a ~a" 
						    (op->str Op1) (op->str Op2)))))
     ( (Term  Op            . L) (let* ((N (op->prio Op))
                                        (N (if (eq? (op->lr Op) ':l) (- N) N)))
				   `(,(validate-term Term) ,N ,Op ,N 
                                     ,@(pre-tree-and-validate0 L))))
     
     ( (Term)                    `(,(validate-term Term))))

(def validate-term
     ((and It (:term  X))    It)
     ((and It (:group X))   `(:group ,(pre-tree-and-validate X)))
     ((Op1 (and It (Op2 X))) (if (or (associative? Op1) (associative? Op2))
                                 `(,Op1 ,(validate-term It))
     (error (format #f "These Two Operators are not allowed in sequence ~a ~a" 
                    (op->str Op1) (op->str Op2)))))
     ((Op1 X)               `(,Op1 ,(validate-term X))))


;;Test cases left-right right-left can easilly be handled as well     

;( (1) * (2))  + ((3))	
#;			     
(treeify-ops '(2 (<num> 1) 1 * 1 (<num> 2) 2 + 2 (<num> 3) 2))

;((1)) + ((2)  * (3))
#;
(treeify-ops '(2 (<num> 1) 2 + 2 (<num> 2) 1 * 1 (<num> 3) 2))

;;simple matcher generator to match a string.
(defmacro def-match-str (name str)
  `(def ,name ((,@(string->list str) . L) (cons ',name L))
        ( _                               (cons #f #f))))

(define (...it x) (string->symbol (string-append (symbol->string x) ".0...")))
(define (.it   x) (string->symbol (string-append (symbol->string x) ".0")))

(defmacro transit (tag e)
  (let ((ref.   (  .it e))
	(ref... (...it e)))
    `(cons (cons ',tag (cons ,ref. '())) ,ref...)))

(defmacro stamp (e)
  (let ((ref.   (  .it e))
	(ref... (...it e)))
    `(cons ,ref. ,ref...)))


;;Greedy operator tail call version aka * and + in regexps!!
(define (<*> F L) (<.0.> '() F L))
(def <.0.>
     (U F ((<> F) . L)    (<.0.> (cons F.0 U) F L))
     (U F           L     (cons (reverse U) L)))

(def <+> 
  ( F ((<> F) . L)   (<.0.> (cons F.0 '()) F L))
  ( F _              (cons #f #f)))
	
(define (<pk> L  ) (cons 'print (pk L)))
(define (<pr> F L) (begin (pk F) (cons F L)))

;;-------------------------------------------------------------------------------------------

(define (op->str      op) (symbol-property op ':str))
(define (op->prio     op) (symbol-property op ':prio))	     
(define (op->lr       op) (let ((LR (symbol-property op ':lr)))
			    (if (eq? LR ':left)
				':l
				':r)))			    
(define (associative? op) (not (eq? (symbol-property op ':lr) ':no)))


(define (set!-operator Id Str N Fix LR)
  (set-symbol-property! Id ':fixtype  Fix )
  (set-symbol-property! Id ':lr       LR  )
  (set-symbol-property! Id ':prio     N   )
  (set-symbol-property! Id ':str      Str )
  (cond ((eq? Fix ':infix  )  
         (set! *infix*   (append *infix*   `((,Id ,@(string->list Str))))))
        ((eq? Fix ':prefix )  
         (set! *prefix*  (append *prefix*  `((,Id ,@(string->list Str))))))
        ((eq? Fix ':postfix)  
         (set! *postfix* (append *postfix* `((,Id ,@(string->list Str))))))))

(def parse-defop
     (((X . L) N Fix LR)   
      `(begin ,@(map (lambda (X) 
                       (parse-defop `(,(car X) ,(cadr X) ,N ,Fix ,LR))) 
                     (cons X L))))
     ((Id Str  N Fix LR)   `(set!-operator ',Id ,Str ,N ',Fix ',LR)))

(define (simple Nm) `( (<spc> ,Nm <spc> . L)  (cons ',Nm L)))

(define *infix*   '())
(define *prefix*  '())
(define *postfix* '())

(define (max-length X)
  (let f ((Q (cdr X)) (N (length (cdar X))))
    (if (null? Q)
        N
        (f (cdr Q) (max (length (cdar Q)) N)))))


(define (take-n X N)
  (if (pair? X)
      (if (eq? (length (cdar X)) N)
          (cons (car X) (take-n (cdr X) N))
          (take-n (cdr X) N))
      '()))


(define (rem-n X N)
  (if (pair? X)
      (if (eq? (length (cdar X)) N)          
          (rem-n (cdr X) N)
          (cons (car X) (rem-n (cdr X) N)))
      '()))


(define (sort-it X)
  (if (pair? X)
      (let* ((N (max-length X))
             (Y (take-n X N))
             (L (rem-n  X N)))
        (append Y (sort-it L)))
      '()))

(define (arrange-opdata)
  (set! *infix*   (sort-it *infix*  ))
  (set! *prefix*  (sort-it *prefix* ))
  (set! *postfix* (sort-it *postfix*)))
              
(define (reset-opdata)
  (begin (set! *infix*   '())
         (set! *prefix*  '())
         (set! *postfix* '())))

(defmacro def-operators xs `(begin ,@(map parse-defop xs) (arrange-opdata)))

(def <seq>
     ( (X . L)  (X . U)  (<seq> L U))
     ( ()       L        (cons #t L))
     ( _        _        (cons #f #f)))

(def <sseq>
     (((X . Str) . L) ((<> <seq> Str) . L2) (cons X L2))
     (( X        . L) U                     (<sseq> L U))
     (()                _                   (cons #f #f)))

(define (<xy-op> L) (<sseq> *infix*   L))
(define (<x-op>  L) (<sseq> *prefix*  L))
(define (<op-x>  L) (<sseq> *postfix* L))
