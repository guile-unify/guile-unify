/*
 	Copyright (C) 2009, 2010 Free Software Foundation, Inc.
 
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
*/

#define Q(x) ((((scm_t_bits) x) << 2) + 2)
#define Ix   (SCM_UNPACK(*pat))
#define i_end    Q(0)
#define i_cons   Q(1)
#define i_var    Q(2)
#define i_eq     Q(3)
#define i_pop    Q(4)
#define i_insert Q(5)
#define i_unify  Q(6)
#define i_match  Q(7)
#define i_scheme Q(8)
#define i_load   Q(9)
#define i_arbr   Q(10)

SCM_API SCM gp_i_ins();
SCM_API SCM gp_i_end();
SCM_API SCM gp_i_cons();
SCM_API SCM gp_i_var();
SCM_API SCM gp_i_eq();
SCM_API SCM gp_i_pop();
SCM_API SCM gp_i_unify();
SCM_API SCM gp_i_match();
SCM_API SCM gp_i_scheme();
SCM_API SCM gp_i_load();
SCM_API SCM gp_i_arbr();

SCM_API SCM  gp_varp(SCM x);
SCM_API SCM  gp_atomicp(SCM x);
SCM_API SCM  gp_consp(SCM x);
SCM_API SCM  gp_cons_plus(SCM x, SCM e);
SCM_API SCM  gp_cons_minus(SCM x);
SCM_API SCM  gp_car(SCM x);
SCM_API SCM  gp_cdr(SCM x);
SCM_API SCM  gp_equal_minus(SCM x, SCM u);
SCM_API SCM  gp_equal_plus(SCM x, SCM u, SCM e);
SCM_API SCM  gp_gp_cons(SCM x, SCM y);

SCM_API SCM  gp_clear();
SCM_API SCM  gp_gp_newframe();
SCM_API SCM  gp_gp_unwind(SCM fr);
SCM_API SCM  gp_mkvar();
SCM_API SCM  gp_mk();
SCM_API SCM  smob2scm(SCM scm);
SCM_API SCM  gp_gp_unify(SCM scm1, SCM scm2);
SCM_API SCM  gp_gp_unify_raw(SCM scm1, SCM scm2);
SCM_API SCM  g_member(SCM scm1, SCM scm2, SCM scm3);
SCM_API SCM  g_iright(SCM scm1, SCM scm2, SCM scm3, SCM scm4);
SCM_API SCM  gp_gp_lookup(SCM scm);

SCM_API SCM  scm_at_unify_abort(SCM tag, SCM id, SCM args);
SCM_API SCM  prompt_generation_id(SCM tag);
