;;; PARSER UTILITIES OPERATOR COMPILER

;; Copyright (C) 2001,2008,2009,2010 Free Software Foundation, Inc.

;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

;;; Code:
(define-module (language prolog parse-util)
  #:use-module (language prolog umatch)
  #:use-module (language prolog umatch-phd)
  #:use-module (ice-9 match-phd)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 pretty-print)
  #:export     (def-operators def-optypes reset-opdata <xy-op> <x-op> <op-x> def-match-str <+> <*> 
                 <pk> <pr> op->str op->prio treeify-ops set!-operator set!-user-operator *infix* 
                 *prefix* *postfix* *pmax* arrange-opdata *expression-context*))


;;debug utility
(define (pp x) (pk (pretty-print x)) x)





;;******************** UTILITIES ****************
;; Simple operator parser.

;; ((1)) + ((2)  * (3))  one need to go through the code and insert
;; + -> 2 + 2
;; * -> 1 + 1 
;; plus insert numbers at the edges.

(define (treeify-ops x) (if (pair? x)
                            (car (treeify-ops0 (pre-tree-and-validate x)))
                            x))

(define (abs x) (if (< x 0) (- x) x))
(define (sgn x) (if (< x 0) -1 1))


(define (treeify-ops0 X)
  (match X
         ([]                      '())
         ([N1 (? pair? X) N2    ]  (cons (may-tree X) #f))
         ([N1 (? pair? X) N2 . L]  (let ((E1 (sgn N1))
                                         (E2 (sgn N2))
                                         (N1 (abs N1))
                                         (N2 (abs N2)))
                                     (if (and (eq? N1 N2) (eq? E1 -1)) (set! N1 (- N1 1/2)))
                                     (if (>= N1 N2)  
                                         (let ((Res (treeify-ops0 (cons X (cons N2 L)))))
                                           (if (cdr Res) (treeify-ops0 (cons N1 Res)) Res))
                                         (cons (may-tree X) (cons N2 L)))))

         ([X N Op . L]             (let* ((R   (treeify-ops0 L))
                                          (Y   (car R))
                                          (Z   (cdr R)))
                                     (if Z
                                         (cons `(,Op ,(may-tree X) ,Y) Z)
                                         (cons `(,Op ,(may-tree X) ,Y) #f))))))




(define (may-tree X)
  (match X
         ([':term  X]   `(:term ,X))
         ([':group X]   `(:group ,X))
         ([Op     X]    `(,Op ,(may-tree X)))
         (X              X)))
				
(define *pmax* 0)

(define  (pre-tree-and-validate X)
  (match X
         ( ()   '())
         ( Y    `(,*pmax* ,@(pre-tree-and-validate0 Y) ,*pmax*))))

(define (pre-tree-and-validate0 X)
  (match X 
         ( (Term1 Op1 Term2 Op2 . L) (=> next)
                                        (if (or (associative? Op1) 
                                                (associative? Op2) 
                                                (not (eq? Op1 Op2)))
                                            (next)
                                            (error (format #f "These Two Operators are not allowed in sequence ~a ~a" 
                                                           (op->str Op1) (op->str Op2)))))
         ( (Term  Op            . L) (let* ((N (op->prio Op))
                                            (N (if (eq? (op->lr Op) ':l) (- N) N)))
                                       `(,(validate-term Term) ,N ,Op ,N 
                                         ,@(pre-tree-and-validate0 L))))
     
         ( (Term)                    `(,(validate-term Term)))))



(define (validate-term X)
  (match X
         ((and It (':term  X))    It)
         ((and It (':group X))    It)
         ((Op1 (and It (Op2 X))) (if (or (associative? Op1) (associative? Op2))
                                     `(,Op1 ,(validate-term It))
                                     (error (format #f "These Two Operators are not allowed in sequence ~a ~a" 
                                                    (op->str Op1) (op->str Op2)))))
         ((Op1 X)               `(,Op1 ,(validate-term X)))))


;;Test cases left-right right-left can easilly be handled as well     

;( (1) * (2))  + ((3))	
#;			     
(treeify-ops '(2 (<num> 1) 1 * 1 (<num> 2) 2 + 2 (<num> 3) 2))

;((1)) + ((2)  * (3))
#;
(treeify-ops '(2 (<num> 1) 2 + 2 (<num> 2) 1 * 1 (<num> 3) 2))

;;simple matcher generator to match a string.

(define-syntax def-match-str
  (lambda (x)
    (syntax-case x ()
      ((def name str)
       (with-syntax ((li (datum->syntax (syntax def) (string->list (syntax->datum (syntax str))))))
                    (syntax (define (name X)                              
                              (match-prolog-syntax X
                                     ([,@'li . L] (cons 'name L))
                                     ( _         #f)))))))))



;;Greedy operator tail call version aka * and + in regexps!!
(define (<*> <F>)
  (define (f U X)
    (match-prolog-syntax X
           ([(<> <F> F) . L]   (f (cons F U) L))
           (L                  (cons (reverse U) L))))
  (lambda (X) (f '() X)))

(define (<+> <F>)
  (lambda (X)
    (match-prolog-syntax X
           ([(<>  <F>      Fh) 
             (<> (<*> <F>) Fs) . L]  (cons (cons Fh Fs) L))
           (_                          #f))))

(define (<pk> L) (cons 'pk (pk L)))
(define (<pr> F) (lambda (L) (begin (pk `(,F ,L)) (cons F L))))

;;-------------------------------------------------------------------------------------------

(define *expression-context* (make-fluid))
(fluid-set! *expression-context* #f)

(define (get-prop op sym)
  (let* ((R (symbol-property op sym))
         (A (if (pair? R) (car R) R ))
         (B (if (pair? R) (cdr R) #f)))       
    (if (fluid-ref *expression-context*)
        (if B B (error (format #f "(prop ~a) operator ~a have no expression context" sym op)))
        (if A A (error (format #f "(prop ~a) operator ~a have no statement context"  sym op))))))

(define (set-prop! op sym val)
  (let* ((R (symbol-property op sym))
         (A (if (pair? R) (car R) #f))
         (B (if (pair? R) (cdr R) #f)))
    (if (fluid-ref *expression-context*)
        (set-symbol-property! op sym (cons A val))
        (set-symbol-property! op sym (cons val B)))))

(define (op->str      op) (get-prop op ':str))
(define (op->prio     op) (get-prop op ':prio))	     
(define (op->lr       op) (let ((LR (get-prop op ':lr)))
			    (if (eq? LR ':left)
				':l
				':r)))			    
(define (associative? op) (not (eq? (get-prop op ':lr) ':no)))



(define (set!-user-operator a b c d e)
  (with-fluids ((*expression-context* #t))
      (set!-operator a b c d e)))

(define (set!-operator Id Str N Fix LR)
  (define (find-id L)
    (match L 
           (((X . ,Str) . L) X)
           ((_          . L) (find-id L))
           (_                #f)))
  
  (if (not (fluid-ref *expression-context*))
      (set!-user-operator Id Str N Fix LR))

  (let* ((Id2 (cond ((eq? Fix ':infix  )  (find-id *infix*))
                    ((eq? Fix ':prefix )  (find-id *prefix*))
                    ((eq? Fix ':postfix)  (find-id *postfix*))))
         (Id  (if Id2 Id2 Id)))
    
    (set-prop! Id ':fixtype  Fix )
    (set-prop! Id ':lr       LR  )
    (set-prop! Id ':prio     N   )
    (set-prop! Id ':str      Str )
    (if (not Id2)
        (cond ((eq? Fix ':infix  )  (set! *infix*   (insert *infix*   `(,Id ,@(string->list Str)))))
              ((eq? Fix ':prefix )  (set! *prefix*  (insert *prefix*  `(,Id ,@(string->list Str)))))
              ((eq? Fix ':postfix)  (set! *postfix* (insert *postfix* `(,Id ,@(string->list Str)))))))))

(define (parse-defop X)
  (match X
         ([(X . L) N Fix LR]
          `(begin ,@(map (lambda (X) (parse-defop `(,(car X) ,(cadr X) ,N ,Fix ,LR))) (cons X L))))
         ([Id Str  N Fix LR]   
          `(set!-operator ',Id ,Str ,N ',Fix ',LR))))

(define *infix*   '())
(define *prefix*  '())
(define *postfix* '())

(define (max-length X)
  (let f ((Q (cdr X)) (N  (length (cdar X))))
    (if (null? Q)
        N
        (f (cdr Q) (max (length (cdar Q)) N)))))


(define (insert L X)
  (define (find X L)
    (match L
           (((_ . ,X) . _)   #t       )
           ((_        . L)  (find X L))
           (_                #f       )))
           
  (if (find (cdr X) L) L (cons X L)))
     
(define (take-n X N)
  (if (pair? X)
      (if (eq? (length (cdar X)) N)
          (cons (car X) (take-n (cdr X) N))
          (take-n (cdr X) N))
      '()))


(define (rem-n X N)
  (if (pair? X)
      (if (eq? (length (cdar X)) N)          
          (rem-n (cdr X) N)
          (cons (car X) (rem-n (cdr X) N)))
      '()))


(define (sort-it X)
  (if (pair? X)
      (let* ((N (max-length X))
             (Y (take-n X N))
             (L (rem-n  X N)))
        (append Y (sort-it L)))
      '()))

(define (arrange-opdata)
      (begin
        (set! *infix*   (sort-it *infix*  ))
        (set! *prefix*  (sort-it *prefix* ))
        (set! *postfix* (sort-it *postfix*))))
              
(define (reset-opdata)
  (begin (set! *infix*   '())
         (set! *prefix*  '())
         (set! *postfix* '())))

(define (sort< X Y) (char<? (car (cdr X)) (car (cdr Y))))

(define (treeish L)
  (define (u Ch L)
    (define rest '())
    (define (uu L)
      (match L 
             (((X)             . L) (begin (set! rest L)  X))
             (((X . (,Ch . U)) . L) (cons (cons X U) (uu L)))
             (L                     (begin (set! rest L) '()))))
    (let* ((C (uu L)))
      (cons (cons Ch C) rest)))

  (define (t L)
    (let* ((Ch (cdr (car L)))
           (HT (u Ch L))
           (H  (car HT))
           (T  (cdr HT)))
      (cons (cons Ch (treeish H)) T)))
      
  (define (g L)
    (if (pair? L)
        (let* ((HT (t L))
               (H  (car HT))
               (T  (cdr HT)))
          (cons H (g T)))
        '()))
  (if (= (length L) 1)
      L
      (g (sort L sort<))))

         
    

(define-syntax def-operators
  (lambda (x)
    (syntax-case x ()
      ((def . xs)
       (with-syntax ((nxs (datum->syntax (syntax def)
                                         (cons 'begin
                                               (map parse-defop 
                                                    (syntax->datum 
                                                     (syntax xs)))))))
                    (syntax (begin nxs (arrange-opdata))))))))

#;
(with-fluids ((*expression-context* #t))
             (begin nxs (arrange-opdata)))

(define (context? X)
  (let ((R (symbol-property X ':lr))
        (F (symbol-property X ':fixtype)))
    (if (fluid-ref *expression-context*)
        (if (pair? R) (and (eq? (cdr F) ':infix) (cdr R)) #f)
        (if (pair? R) (and (eq? (car F) ':infix) (car R)) #f))))

(define (<sseq> V)
  (define (f V X)
    (match-prolog-syntax `(,V ,X)
     ((- ((X . Str) . L) (+ ,@Str . L2)) (cons X L2))
     ((- (X         . L)  U            ) (f L U))
     ((- ()               _            ) #f)))
  (lambda (X) #;(pk `(sseq: ,V ,X)) (f V X)))

(define (<xy-op> L) ((<sseq> *infix*   ) L))
(define (<x-op>  L) ((<sseq> *prefix*  ) L))
(define (<op-x>  L) ((<sseq> *postfix* ) L))
