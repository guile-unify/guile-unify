(define gp-token-fluid (gp-unwind-fluid))

(define *vars* '())
(define *n*     0)

(define (before? x)
  (letrec ((pred (lambda (y) (eq? (syntax->datum x) (syntax->datum y))))
	   (f    (lambda (v)
		   (match v
			  ( (((? pred q) . u) . l) #t    )
			  ( (x                . l) (f l) )
			  ( ()                     #f    )))))
    (f *vars*)))


(define (mk-var! x)
  (let ((ret `(,x ,*n*))
	(i    *n*))
    (set! *n*    (+ 1 *n*))
    (set! *vars* (cons ret *vars*))
    `(,i 'ok)))


(define *ins* '())
    
(define (var? x)
  (let ((x (syntax->datum x)))
    (if (symbol? x)
        (let ((H (car (string->list (symbol->string x)))))
          (char-upper-case? H))
        #f)))

(define (match? x)
  (let ((x (syntax->datum x)))
    (if (symbol? x)
        (if (not (eq? x '<>))
            (let* ((L (string->list (symbol->string x)))
                   (H (car L))
                   (R (car (reverse L))))
              (and (equal? H #\<)
                   (equal? R #\>)))
            #f)
        #f)))
	    
(define (next-sym x)
  (let ((w (reverse (string->list (symbol->string x)))))
    (match w
	   ((#\9 . l) (set! w `(#\0 #\9 ,@l)))
	   ((#\8 . l) (set! w (cons #\9 l)))
	   ((#\7 . l) (set! w (cons #\8 l)))
	   ((#\6 . l) (set! w (cons #\7 l)))
	   ((#\5 . l) (set! w (cons #\6 l)))
	   ((#\4 . l) (set! w (cons #\5 l)))
	   ((#\3 . l) (set! w (cons #\4 l)))
	   ((#\2 . l) (set! w (cons #\3 l)))
	   ((#\1 . l) (set! w (cons #\2 l)))
	   ((#\0 . l) (set! w (cons #\1 l)))
	   ( X        (set! w `(#\0 #\. ,@X))))
    (string->symbol (list->string (reverse w)))))

(define (mk-nrest x) (string->symbol (string-append (symbol->string x) "...")))
(define (mode? x)    (member (syntax->datum x) '(+ - *)))

(define *int* '())

(define (compile-pattern pat mode tag recur)
  (letrec 
   ((make-0 
      (lambda (pat m cc)
	(define (make-1 pat m cc)
	  (let ((ret (if (eq? pat '())
			 (make-0 pat m cc)    ;(make-0 cc m '<end-it>)
			 (make-0 pat m cc))))
	    (if (> (length ret) 0)
		`((begin ,@ret))
		'())))

	(define (make-box LL Fs Lo m cc)
	  (if (pair? Fs)
	      `((<u-label> ',(car LL))
		(<u-goto>  ',(cadr LL))
                (<u-dup>)
		,@(make-0 (car Fs) m '<end-it>)
                (<u-skip>)
		(<u-goto> ',Lo)
		,@(make-box (cdr LL) (cdr Fs) Lo m cc))
	      '()))

        (define (m-id m) (match m ('* 2) ('- 3) ('+ 4)))
        (define (ui r) (if r '<u-insert> '<uu-insert>))
       
	(match-syntax-hack pat
	       ((('+1+  x  p))      (begin 
				      (mk-var! x)
				      `((<u-load> ,x) ,@(make-0 p m cc))))
	       ((('+1+  x  p) . l)  (begin 
				      (mk-var! x)
				      `((<u-load> ,x) ,@(make-0 p m l))))
	       ('_                 `((<u-skip>) ,@(make-0 cc m '<end-it>)))
	       (((? mode? m2) x)   (if (eq? (syntax->datum m2) (syntax->datum m))
				       (make-0 x m cc)
				       `((<mode> ,(m-id m2)) ,@(make-0 x m2 `(,m ,m ,cc)))))

	       (((? mode? m1) _ (('+1+ x ((? mode? m2) p)) . l))
		(make-0 `((+1+ ,x (,m1 ,m1 (,m2 ,p))) ,@l) m cc))

	       (((? mode? m1) _ ((? mode? m2) x)) (make-0 `(,m2 ,x) m `(,m1 ,m1 ,cc)))
	       (((? mode? m1) _ x)  (if (eq? (syntax->datum m1) (syntax->datum m))
					(make-0 x m cc)
					`((<mode> ,(m-id m1)) ,@(make-0 x m1 cc))))
	       (('and x y . l)     `((<u-and> (begin ,@(make-0 x m '<end-it>)) 
					      (begin ,@(make-0 `(and ,y ,@l) m cc)))))
	       (('and x      )      (make-0 x m cc))
	       
	       (('quote   x)       `((<u-eq> ',x) ,@(make-0 cc m '<end-it>)))
	       (('unquote x)       (begin (set! *ins* 
                                                (lset-union eq? *ins* `(,x)))
					  `((,(ui recur) ,x)
                                            ,@(make-0 cc m '<end-it>))))
	       (('uunquote x)       (begin (set! *ins* 
                                                 (lset-union eq? *ins* `(,x)))
                                           `((,(ui recur) ,x) 
                                             ,@(make-0 cc m '<end-it>))))

	       (('? x p)            (make-0 `(and (? ,x) ,p) m cc))
	       (('? 'var?)         `((<?> ',m var?) ,@(make-0 cc m '<end-it>)))
	       (('? x    )          (if (symbol? (syntax->datum x))
					`((<?> ',m (let ((*q* #f)) 
                                                     (<gp> (<u-push> (,x  *q*)))
                                                     (<gp> (<u-base> ,tag))))
					  ,@(make-0 cc m '<end-it>))
					`((<?> ',m (let ((*q* #f)) 
                                                     (<gp> (<u-push> (,@x *q*)))
                                                     (<gp> (<u-base> ,tag))))
					  ,@(make-0 cc m '<end-it>))))

	       (('or . l)           (let* ((LL (map (lambda (X) (gensym ":L")) l))
					   (La (cadr (reverse LL)))
					   (L1 (car LL))
					   (R  (reverse l))
					   (Lo (gensym ":L"))
					   (Q  (car R))
					   (mo (m-id m))
					   (Fs (reverse (cdr R))))
				      `((<u-box> (let ((*gp-fi* #f)
						       (*gp-mo* #f)
						       (*gp-jp* #f)
						       (*gp-sp* #f))
						   (<u-mute> *gp-fi* *gp-mo* 
							     *gp-jp* *gp-sp*)
						   (<u-init> *gp-fi* ,mo ',L1)
                                                   (<u-dup>)
						   ,@(make-0 (car Fs) m '<end-it>)
                                                   (<u-skip>)
						   (<u-goto> ',Lo)
						   ,@(make-box LL (cdr Fs) Lo m cc))
						 (begin
						   (<u-label> ',La)
						   (<u-base> *gp-fi*)
						   ,@(make-0 Q m cc)
						   (<u-label> ',Lo))))))
				      
					      

	       (((? match? x)       . l)  (make-0 `((<> ,x    ) ,@l) m cc))
	       ((('<> x . r)        . l)  (let f ((y (next-sym x)))
					    (if (before? y)
						(f (next-sym y))
						(begin
						  (mk-var! y)
						  (set! *int* (cons y *int*))
						  (let ((z   (mk-nrest y)))
						    (mk-var! z)
						    (set! *int* (cons z *int*))
						    `((<match>  
						       (let ((*q* #f)) 
							 (<u-push> (,x ,@r *q*))
							 (<u-base> ,tag)
							 (<u-cons>)
							 (<u-pop-ref> 
							  ,y 
							  ,z
							  (begin
							    (if (not ,y)
								(gp-next ,tag)
                                                                (<gp>
                                                                 (begin 
                                                                   (<u-push> ,z)
                                                                   ,@(make-1 l m cc))))))))))))))
	       (('cons x y)        `((<u-cons>) ,@(make-0 x m '<end-it>) ,@(make-0 y m cc)))
               ((x . (y . z))      `((<u-cons>) ,@(make-0 x m '<end-it>) ,@(make-0 (cons y z) m cc)))
	       ((x . y)            `((<u-cons>) ,@(make-0 x m '<end-it>) ,@(make-0 y m cc)))
	       ('<end-it>           (if (eq? cc '<end-it>) '() (make-0 cc m '<end-it>)))
	       (()                 `((<u-eq> '()) ,@(make-0 cc m '<end-it>)))
	       ((? var? x)          (if (symbol? (syntax->datum x))
					(if (before? x)
					    `((<u-insert> ,x) ,@(make-0 cc m '<end-it>))
					    (begin (mk-var! x)
                                                   `((<u-var> ,x) 
                                                     ,@(make-0 cc m '<end-it>))))
					`((<u-eq> ,x) ,@(make-0 cc m '<end-it>))))
	       (x                  `((<u-eq> ',x)  ,@(make-0 cc m '<end-it>)))
	       ))))
   `(,@(make-0 pat mode '<end-it>))))





(define (prepare args body)
  (match body
	 ((line . lines) (let* ((r    (reverse line))
				(code (car r))
				(l    (cdr r))
				(pat  (map (lambda (a p) `(+1+ ,a ,p)) args (reverse l))))
			   (cons `(,pat ,code) (prepare args lines))))
	 (()             '())))

(define (rem-vs y x)
  (match y 
	 (((v i) . b) (if (member v x)
			  (rem-vs b x)
			  (cons (list v i) (rem-vs b x))))
	 ( x x)))

;;After this
(define (handle name mode tag recur args code oldv first)
  (match code
	 (((pat code) . v)
	  (begin
	    (set! *vars* '())
	    (set! *int*  '())
	    (set! *n*     0)
	    (set! *ins*  '())
	    (let* ((compiled  (if (null? pat)
                                  '()
                                  (compile-pattern pat mode tag recur)))
		   (*vars*    *vars*)
		   (lv        (map car *vars*))
		   (mode-id   (match mode ('* 2) ('- 3) ('+ 4)))
				     
		   (*ins*     *ins*)
		   (next      (handle name mode tag recur args v (lset-union eq? *vars* (cons tag lv)) #f))

		   (defs      (fold-right (lambda (x a)
					    (match x 
						   ((var id) 
						    (if #f
							a
							(if (member var args)
							    a
							    (cons `(,var #f) a))))))
					  '()
					  *vars*))

		   (defs*     (if (null? oldv)
				  `((,tag    #f) 
				    (*gp-mo* #f)
				    (*gp-jp* #f)
				    (*gp-sp* #f))
                                  '()))

		   (args      (fold-right (lambda (x l)
					    (match x ((var id) `(',var ,var ,@l))))
					  '() *vars*)))
              (if first
                  `(let 
                    ,defs* ,(if (null? defs)
                                `(x ,@compiled (<code> ,code))
                                `(let
                                  ,defs ,@compiled (<code> ,code)))
                                ,next
                                ,mode-id
                                (begin ,tag *gp-mo* *gp-jp* *gp-sp*))
                  `(begin ,(if (null? defs)
                               `(x ,@compiled (<code> ,code))
                               `(let
                                 ,defs ,@compiled (<code> ,code)))
                          ,next)))))
         (()  `(error ,(format #f "no match in ~a" name)))))


;(def f (a b b) (a b b))
(define (mk-vars-again x)
  (if (null? x)
      '()
      (let ((V (car x)))
	`(',V ,V ,@(mk-vars-again (cdr x))))))

(define (skip-args v args)
  (if (null? v)
      '()
      (if (member (car v) args)
	  (skip-args (cdr v) args)
	  (cons (car v) (skip-args (cdr v) args)))))

;;cannot be used because defmacro sucks, you need define-syntax all the way
;;to be safe!
(define-syntax let-alias 
   (syntax-rules () 
     ((_ ((id alias) ...) body ...) 
      (let-syntax ((helper (syntax-rules () 
                             ((_ id ...) (begin body ...))))) 
        (helper alias ...))))) 

;;this does not work FIXME use define-syntax all the way!!
#;
(define (simplify code)
  (match code
    ((('<u-load> x) ('<u-skip>  ) . l) (simplify l))
    ((('<u-load> x) ('<u-var>  v) . l) `((let-alias ((,v ,x)) 
                                                    (begin ,@(simplify l)))))
    ((x . l)                           (cons (simplify x) (simplify l)))
    (x                                 x)))

(define (simplify code)
  (match code
    ((('<u-load> x) ('<u-skip>  ) . l) (simplify l))
    ((x . l)                           (cons (simplify x) (simplify l)))
    (x                                 x)))



(define (hhandle name mode tag recur args code)
  (let* ((a     (simplify (handle name mode tag recur args code '() #t))))
    `(<umatch> ,a)))


(defmacro def (n . data)
  (let ((mode  '*       )
        (recur #t       )
        (tag   '*gp-fi* ))
   (let keys ((data data))
     (match data
            (('#:tag  t . l)  (begin (set! tag  t) (keys l)))   
            (('#:raw    . l)  (begin (set! recur #f) (keys l)))          
	    (('#:mode o . l)  (begin (set! mode o) (keys l)))
	    (data             (let* ((args   (map (lambda (x) (gensym "arg"))
						  (cdr (match data ((p . l) p)))))
				     (rew    (prepare args data)))
				`(define ,n (lambda (,@args)  ,(hhandle n mode tag recur args rew)))))))))


(defmacro fun data
 (let ((name "anon"    )
       (tag   '*gp-fi* )
       (recur #t       )
       (mode '*        ))
   (let keys ((data data))
     (match data
	    (('#:name n . l)  (begin (set! name n) (keys l)))
            (('#:tag  t . l)  (begin (set! tag  t) (keys l)))   
            (('#:raw    . l)  (begin (set! recur #f) (keys l)))          
	    (('#:mode o . l)  (begin (set! mode o) (keys l)))             
	    (data             (let* ((args   (map (lambda (x) (gensym "arg"))
						  (cdr (match data ((p . l) p)))))
				     (rew    (prepare args data)))
				`(lambda (,@args) ,(hhandle name mode tag recur args rew))))))))
