;;; UNIFY MATCH MATCHER COMPILER

;; Copyright (C) 2001,2008,2009,2010 Free Software Foundation, Inc.

;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

;;; Code:

(define-module (language prolog umatch-ffi)
  #:use-module (ice-9 match-phd-lookup)
  #:use-module (ice-9 pretty-print)
  #:use-module (ice-9 syntax-matcher)
  #:use-module (srfi srfi-1))



;;need to add modded,   


(define gp-module-init #f)
(define gp?            #f)
(define gp-pair?       #f)
(define gp-car         #f)
(define gp-cdr         #f)

(load-extension (string-append "libguile-" (effective-version))
                "scm_init_vm")

(include-from-path "language/prolog/umatch-export.scm")

;;prompts will be just a continuation lambda

(define-syntax u-prompt
  (syntax-rules ()
    ((_ x)    x)))

(define-syntax u-abort
  (syntax-rules ()
    ((_ x)    (x))))

(define-syntax u-call
  (syntax-rules ()
    ((_ g f ...) (f ...))))

(define-syntax u-var!
  (syntax-rules ()
    ((_) (gp-var!))))

(define-syntax u-scm
  (syntax-rules ()
    ((_ x) (gp->scm x))))

(define-syntax u-set!
  (syntax-rules ()
    ((_ x y) (gp-set! x y))))

(define-syntax u-cons
  (syntax-rules ()
    ((_ x y) (gp-cons! x y))))

(define-syntax u-unify!
  (syntax-rules ()
    ((_ x y) (gp-unify! x y))))

(define-syntax u-unify-raw!
  (syntax-rules ()
    ((_ x y) (gp-unify-raw! x y))))


(define-syntax u-modded
  (syntax-rules ()
    ((_) #t)))

 
(define (id x) x)

(make-phd-matcher umatch0
  (        (gp-car gp-cdr gp-pair!? gp-null!?   gp-unify!     gp-lookup)
    (  (+  (gp-car gp-cdr gp-pair!? gp-null!?   gp-unify!     gp-lookup))
       (++ (gp-car gp-cdr gp-pair!? gp-null!?   gp-unify-raw! gp-lookup))
       (-  (gp-car gp-cdr gp-pair?  gp-null?    gp-m-unify!   gp-lookup))
       (*  ( car  cdr  pair?  null?  equal? id)))))

(define-syntax umatch
  (lambda (x)
    (syntax-case x ()
      ((umatch . l) 
       (with-syntax ((w (datum->syntax (syntax l) '*gp-fi*)))
         (syntax (umatch* "anon" w #f * . l)))))))

;;unsyntax construct that works quite ok
(define (pp x) (pretty-print x) x)


(define-syntax umatch* 
  (lambda (x)
    (syntax-case x ()
      ((umatch* nn tt rr mm #:name   n . l)
       (syntax (umatch*  n tt rr mm . l)))

      ((umatch* nn tt rr mm #:tag    t . l)
       (syntax (umatch*  nn t rr mm . l)))

      ((umatch* nn tt rr  mm #:raw     . l)
       (syntax (umatch*  nn tt #t mm . l)))

      ((umatch* nn tt rr mm #:mode   m . l)
       (syntax (umatch*  nn tt rr m . l)))

      ((umatch* n t r m args a ...) 
       (syntax (umatch** (a ...) () args (n t r m)))))))


(define-syntax umatch**
  (lambda (x)
    (syntax-case x ()
      ((_ . l) 
       ;(pk `(umatch** ,@(syntax->datum #'l)))
       #'(umatch**+ . l)))))

(define-syntax umatch**+
  (syntax-rules ()
    ((_ ((code) ...)  a . l)             (umatch*** (code ...) a . l))

    ((_ ((a as ...) ...) () . l)   
     (umatch** ((as ...) ...) ((a) ...) . l))

    ((_ ((a as ...) ...) ((b ...) ...) . l)   
     (umatch** ((as ...) ...) ((b ... a) ...) . l))))


(define-syntax umatch***
  (lambda (x)
    (syntax-case x ()
      ((_ . l) 
       ;(pk `(umatch*** ,@(syntax->datum #'l)))
       #'(umatch***+ . l)))))

(define (ppq a x)
  ;(pk `(,a ,x))
  x)

(define-syntax mk-failure
  (syntax-rules ()
    ((_ fr code)
     (letrec ((base (case-lambda
                      (()  
                       (gp-unwind fr) 
                       code)

                      ((x) 
                       (gp-unwind fr)
                       (let ((s (gp-store-state)))
                         (letrec ((self (case-lambda 
                                          (() (base))
                                          ((x) self))))
                           self))))))
       base))))

(define-syntax umatch***+
  (syntax-rules (+)
    ((_ (code ...) () () (n t _ _))
     (let ((frame (ppq 'new (gp-newframe))))
       (umatch0 (#:args) 
                ((arguments) (-> t (mk-failure frame)) 
                 code)
                ...
                (_ (error (format #f "umatch ~a did not match" n))))))

    ((_ (code ...) ((a ...) ...) arg (n t #t +))
     (let ((frame (ppq 'new (gp-newframe))))
       (umatch0 (#:args . arg) 
                ((arguments (++ ++ a) ...) 
                 (-> t  (mk-failure frame))
                   
                 code)
                ...
                (_ (error (format #f "umatch ~a did not match" n))))))

    ((_ (code ...) ((a ...) ...) arg (n t r m))
     (let ((frame (ppq 'new (gp-newframe))))
       (umatch0 (#:args . arg) 
                ((arguments (m m a) ...) 
                 (-> t (mk-failure frame))
                 code)
                ...
                (_ (error (format #f "umatch ~a did not match" n))))))))
