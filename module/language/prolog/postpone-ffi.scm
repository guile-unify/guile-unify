(define-module (language prolog postpone-ffi)
  #:use-module (language prolog guile-log-ffi)
  #:use-module (language prolog umatch-ffi))

(include-from-path "language/prolog/postpone-code.scm")
