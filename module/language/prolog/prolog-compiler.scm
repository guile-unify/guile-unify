(define (pp x) (pk (pretty-print x)) x)

;;Code do combine chunks of the same function into a groups preserving order.
(define (sieve-out-H H X)
  (match X
         (((and X ((':term ('<fkn> H Arg)) . _)) . L)
          (cons X (sieve-out-H H L)))

         ((X . L)                           (sieve-out-H H L))
         (()                               '())))

(define (sieve-rem-H H X)
  (match X
         (((and X ((':term ('<fkn> H Arg)) . _)) . L)
          (sieve-rem-H H L))
         ((X . L)                      (cons X (sieve-rem-H H L)))
         (()                          '())))

(define (sieve X)
  (match X
         ((and X (((':term ('<fkn> H . _)) . _) . _))
          (cons (sieve-out-H H X)
                (sieve (sieve-rem-H H X))))
         (() '())))

;;---------------------------------------------------------------------------

;;main compile function stub inserted by macros
(define (prolog-compile X . l)
  (set! maxmatch '((0 0) ()))
  (let ((Bin (if (null? l)
                 '(ram)
                 l)))
    (let* ((Tree   (check-error (<stms> `(,X 1 1 0 ,Bin))))
           (Fkns   (sieve Tree)))
      (map generate-code Fkns))))

  
    

(define (gen-args N)
  (if (> N 0)  
      (cons (gensym "Arg") (gen-args (- N 1)))
      '()))

(define (generate-code X)
  (match X
         ( (and Block (((':term ('<fkn> H _)) . _) . _))
           (let* ((Last? (map (lambda (X) #f) Block))
                  (Last? (reverse (cons #t (cdr Last?))))
                  (Code `(,@(map compile-stm Block Last?) ,(fin-stm (car Block))))
                  (Args  (gen-args (- (length (car Code)) 1))))	      
             (set-symbol-property! H ':arity (length Args))
             `(define (,H ,@Args <Cut> <CC>)
                (umatch #:mode + ,@Args ,Code))))))

(define (fin-stm X)
  (match X
         (((':term ('<fkn> H Args)) . R) 
          `(,@(map (lambda (X) '_) Args) (u-abort <Cut>)))))


(define rhs-vars '())
(define (compile-stm X Last?)
  (match X
         (((':term ('<fkn> H Args)) . R)
          (let* ((Stubs   (clean-term '() 
                           (with-fluids ((*expression-context* #t)) 
                                        (map sieve-out-stubs Args))))
                 (Rhs     (qtreeify (compile-rhs (if (pair? R) (cdr R) R))))
                 (Vars    (sieve-out-vars-head Args))
                 (RHSVars (sieve-out-vars Rhs))
                 (FVars   (generate-all-vars Vars '() RHSVars)))
            (set! rhs-vars RHSVars)
            `(,@Stubs ,(-:: Last? FVars Rhs))))))

;;simple routine for consifying head signatures
(define (fix-grouping X)
  (match X
         (('cons A B)           `(cons (fix-grouping A) (fix-grouping B)))
         ((':term (':group A  )) (fix-grouping (qtreeify A)))
         ((':term ('<fkn>  F A)) (fix-grouping (cons F A)))
         ((':term ('<lis>  A  )) (fix-grouping A))         
         ((and A (':term _))      A)
         ((A . B)               `(cons (fix-grouping A) (fix-grouping B)))
         (A                       A)))
         

(define (handle-lambda X)  
  (define (f X)
    (match X
           (('<fin> ('<:->  ('</.> (':term (':group A))) B) Y) 
            (cons `(,A ,B) (f Y)))
           (('<:->  ('</.> (':term (':group A))) B)            
            `((,A ,B)))))
  (let ((Ret `(<lam> ,(f (qtreeify X)))))
    `(handle-lambda ,Ret)
    Ret))


     
(define (clean-term Vb X)
  (define (f X)
    ;;(pp `(clean ,X))
    (match X 
           (('<fkn> F ()) (f F))
           (('<fkn> F A)  (f (cons F A)))  
           (('<var> X)    (if (member X Vb) `(uunquote ,X) X))
           (('<lam> X)    (error "lambdas cannot be defined in head"))
           (('<top> X)   `(uunquote ,X))
           ((':term X)    (f X))
           (('cons X Y)  `(cons ,(f X) ,(f Y)))
           ((A . B)       (cons (f A) (f B)))
           ('_            '_)
           ((? symbol? X) `(quote ,X))
           (A             A)))
  (f X))
         
(define (qtreeify X)
  ;;(pp `(qtreeify ,X))
  (match X
         ((and A (cond (':term _) 
                       (_) 
                       () 
                       (_ _) 
                       ('cons . _))) 
          A)

         (A                          (treeify A))))

(define (deep X)
  (match X
         ((and A (':term _)       )     A      )
         (('cons . _              )     #f     )
         ((Op (= deep .deep.)     )     .deep. )
         (_                             #f     )))

(define (repl-deep X Y)
  ;(pp `(repl ,X ,Y))
  (match `(,X ,Y)
     (((':term _)     (cond (':term Nargs) Nargs))  `(:term ,Nargs))
     (((Op X . L)     Nargs)                        `(,Op ,(repl-deep X Nargs)))))

(define (compile-rhs X)
  ;(pp `(:rhs ,X))
  (match   X      
     ((and A ('<lam> L))       (handle-lambda A))
     ((and A (':group L))      (sieve-out-stubs A))
     ((X (? expression-op? Op) Y . L)      
      (begin        
        (with-fluids ((*expression-context* #t))
                     (set! X (car (compile-rhs `(,X))))
                     (set! Y (car (compile-rhs `(,Y)))))
        `(,X ,Op ,Y ,@(compile-rhs L))))
 
     ((and X (= deep .deep.))       (=> next)
      (if .deep.
          (let ((Stubs (sieve-out-stubs .deep.)))
            (repl-deep X Stubs))
          (next)))

     ((and X ((= deep .deep.) . L)) (=> next) 
      (if .deep.
          (let ((Stubs (sieve-out-stubs .deep.)))
            `(,(repl-deep (car X) Stubs) ,@(compile-rhs L)))
          (next)))
      
     ((X             . L)   (cons (compile-rhs X) (compile-rhs L)))
     (()                   '())
     (X                      X)))
  


;;Build variable dependency structure for efficient introduction of variables.
(define (sieve-out-vars X)
  (define *vars* '())

  (define (reverse-vars X) (if (pair? X) (reverse (map reverse-vars X)) X))
  
  (define (soft-args X)
    ;;(pp `(soft-args ,X))
    (match X
           (((A V) . L) (let ((Vs *vars*)
                              (As (sieve-out-vars-head A)))
                          (set! *vars* '())
                          (sieve-out-vars-tail V)
                          (set! *vars* `((,*vars* ,As) ,@Vs))
                          (soft-args L)))
           (()          '())))

  (define (sieve-out-vars-tail X)                           
    ;;(pp `(sov-tail ,X))
    (match X
           (('<fkn> . L)  
            (let ((Vs *vars*))
              (set! *vars* '())
              (sieve-out-vars-tail L)
              (set! *vars* (cons `(,*vars* <fkn>) Vs))))
           (('<lis>   L)  (begin (sieve-out-vars-tail L) (sieve-out-vars-tail U)))
           (('cons A B)   (begin (sieve-out-vars-tail A) (sieve-out-vars-tail B)))
           (('<var> V )   (set! *vars* (cons V *vars*)))
           (('<lam> A)    (begin
                            (let ((Vs *vars*))
                              (set! *vars* '())
                              (soft-args A)
                              (set! *vars* (cons `( ,*vars* <lam>) Vs)))))                                       
           (('<top> _ )   '())
           ((':group X)   (sieve-out-vars-tail X))
           ((':term  X)   (sieve-out-vars-tail X))
           ((':atm . _)  '())
           (((and H (cond '<comma> '</=> '<=> '<is> '<semi> '<->>)) X Y)
            (let ((Vs *vars*)
                  (Vx #f))
              (set! *vars* '()) 
              (sieve-out-vars-tail X)
              (set! Vx *vars*)
              (set! *vars* '())
              (sieve-out-vars-tail Y)
              (set! *vars* (cons `(,*vars* ,Vx ,H) Vs))))

           (('</+> X)
            (let ((Vs *vars*))
              (set! *vars* '())
              (sieve-out-vars-tail X)
              (set! *vars* (cons `(,*vars* </+>) Vs))))
     
           ((H . L)            (begin (sieve-out-vars-tail H) (sieve-out-vars-tail L)))
           (X                 '())))
  
  (sieve-out-vars-tail X)
  ;(pp `(sv ,*vars*))
  (reverse-vars *vars*))
      

(define  (sieve-out-vars-head X)
  ;(pp `(sovh ,X))
  (match X
         (('<fkn> . L) (sieve-out-vars-head L)) 
         (('<lis>  L)  (sieve-out-vars-head L))
         (('cons A B)  (union (sieve-out-vars-head A) (sieve-out-vars-head B)))
         (('<var> V )  (cons V '()))
         (('<top> V)   '())
         ((':group X)  (sieve-out-vars-head X))
         (('<atm> . _) '())
         ((':term X)   (sieve-out-vars-head X))
         ((H . L)      (union (sieve-out-vars-head H) (sieve-out-vars-head L)))
         (X           '())))


(define (pvar? X)
  (and (symbol? X) 
       (char-upper-case? (car (string->list (symbol->string X))))))

;;structure to handle variable instantiation at correct time
;;this can be speeded with memoizing!
(define (generate-all-vars Vb Vf X)
  (define (all-vars Vs X)
    ;(pp `(all-vars ,Vs ,X))
    (match X
           (('<->>    X Y)   (all-vars Vs Y))
           (('</+>    X  )   (all-vars Vs X))
           ((X .        L)   (union (all-vars Vs X) (all-vars Vs L)))           
           (()              '())
           (X                (if (and (symbol? X) 
                                      (char-upper-case? (car (string->list (symbol->string X)))))
                                 (if (member X Vs) '() (cons X '()))
                                 '()))))

                                      
  (define (all-shallow Vs X)
    ;(pp `(all-shallow ,Vs ,X))
    (match X 
           (((_ . _) . L) (all-shallow Vs L))
           ((X       . L) (if (and (symbol? X) 
                                   (char-upper-case? (car (string->list (symbol->string X)))))
                              (if (member X Vs) 
                                  (all-shallow Vs L) 
                                  (union (all-shallow Vs L) (cons X '())))
                              (all-shallow Vs L)))
           ('()          '())))

  (define (lam-vars X)
    (match X
           ((A B) (let ((VVb (union (all-vars Vb A) Vb)))
                    `(<item> ,Vb ,VVb ,(generate-all-vars VVb '() B))))
           (()    '())))
                        
                    

  ;;(pp `(gav ,Vb ,Vf , X))
  (match -abs ((<syms> <.syms.>)) X
         (('<lam> A) `(<lam> ,(map lam-vars A)))
         (('<semi> X Y)
          (let* ((Vx   (all-vars Vf X))
                 (Vy   (all-vars Vf Y))
                 (Vs   (in-all (union Vx Vy) Vf))
                 (Vb   (union Vb Vs)))
            `(<let-semi> ,Vs 
                         ,(generate-all-vars Vb Vf X) 
                         ,(generate-all-vars Vb Vf Y))))
                 
         (('<->> X Y)
          `(<->> ,(generate-all-vars Vb '() X) 
                 ,(generate-all-vars Vb Vf Y)))

         (('<fkn> A)
          (let* ((Vs (in-all (all-vars Vb A) Vf))
                 (Vb (union Vb Vs)))
          `(<fkn> ,Vs ,Vb ,Vf)))

         (('<comma> X Y)
          (let ((Vy (all-vars Vb Y))
                (Vx (all-vars Vb X)))            
            `(<comma> ,(generate-all-vars Vb (union Vy Vf) X)
                      ,(generate-all-vars (union Vb Vx) Vf Y))))

         (('<is> X Y)
          (let* ((Vx  (all-vars Vb X))
                 (Vy  (all-vars Vb Y))
                 (Vs  (diff (in-all Vx Vy) Vf))
                 (Vx  (diff Vx Vs))
                 (Vb1 (union Vs Vb))
                 (Vb2 (union Vx Vb1)))             
            (if (pair? Vs)                
                (error "is form is a self referential initial")
                `(<is> ,Vx 
                       ,(generate-all-vars Vb2  Vf Y)))))

         (('<=> X Y)
          (let* ((Vx  (all-vars Vb X))
                 (Vy  (all-vars Vb Y))
                 (Vs  (in-all Vx Vy)))               
            
            `(<let=> ,Vs ,(union Vb Vs) ,Vf)))

         (('</=> X Y)           
          `(<let/=> ,(diff (union (all-vars Vb X) (all-vars Vb Y)) Vb)
                    ,(generate-all-vars Vb '() X)
                    ,(generate-all-vars Vb '() Y)))

         (('</+> X)
          `(</+> ,(generate-all-vars Vb '() X)))
         ((<syms> . L) 
          (if (null? <.syms.>) (error "Bugg, generate-all-vars should not let zeroe elements"))
          (let* ((Vf2 (union (all-vars Vb L) Vf))
                 (Vg  (fold (lambda (X R) 
                              (if (and (not (member X Vb))
                                       (member X Vf2))
                                  (cons X R)
                                 R))
                           '() <.syms.>))
                 (Vl (fold (lambda (X R) 
                             (if (and (not (member X Vb))
                                      (not (member X Vf2)))
                                 (cons X R)
                                 R))
                           '() <.syms.>)))
            (if (and (null? Vg) (null? Vl))
                (generate-all-vars Vb Vf L)
                `(let ,Vg ,Vl ,(generate-all-vars (union (union Vg Vl) Vb) Vf L)))))

         ((X  . L)  (let* ((Vf2 (union (all-vars Vb L) Vf   ))
                           (Vb2 (union (all-vars Vb X) Vb   ))
                           (L   (generate-all-vars Vb2 Vf L )))
                      (cons (generate-all-vars Vb  Vf2 X)
                            (match L
                                   (((? pvar?) . _) L)
                                   (X               (cons X '()))))))
                     
         (() '())
         (X   (error (format #f "match errror in generate-all-vars with ~a" X)))))
          

(define (<syms> X)
  (define (syms X)
    (if (pair? X)
        (if (symbol? (car X))
            (let ((R (syms (cdr X))))
              (cons (cons (car X) (car R)) (cdr R)))
            (cons '() X))
        (cons '() X)))
                  
  (if (pair? X)
      (if (symbol? (car X))
          (let ((R (syms (cdr X))))
            (cons (cons (car X) (car R)) (cdr R)))
          #f)
      #f))

(define (consify_help X)
  (match X
         ((':group X)           X)
         ((and A ('quote _))    A)           
         ((and A ('cons . _))   A)
         ((H . L)               `(cons ,H ,(consify_help L)))
         (X                     X)))

(define (sieve-out-stubs X)
  (match X
     (('cons A B)          `(cons ,(sieve-out-stubs A) ,(sieve-out-stubs B)))
     ((':term (and A ('cons X Y))) A)
     ((':term  X)           (sieve-out-stubs2 X (lambda (X) `(:term ,X))))
     (('<lam> X)            (handle-lambda X))
     ((':group ((? expression-op? Op) X Y))
      (if (fluid-ref *expression-context*) (gp-next *gp-fi*))          
      (with-fluids ((*expression-context* #t))
          (let ((X (sieve-out-stubs (qtreeify (compile-rhs X))))
                (Y (sieve-out-stubs (qtreeify (compile-rhs Y)))))
            `(:term (,Op ,X ,Y)))))
            
     ((':group X)          (match (qtreeify (compile-rhs X))
                                  ((and A (X))  A)
                                  (X           `(:group ,X))))
     ((Op P)               (consify-help `(,Op ,(sieve-out-stubs P))))))

(define (consify-help X)
  (if (fluid-ref *expression-context*)
      (consify_help X)
      X))
(define (id X) X)

(define (sieve-out-stubs2 X F)
;  (pp `(:stubs2 ,X))
  (match `(,X ,F)         
     ((('<var>  V   )      F)  (F `(<var> ,V)))
     (( '<_>               F)  (F '<_>))
     ((('quote X)          F)  (F `(quote ,X)))
     (( '<!>               F)  (F '<!>))
     ((('<atm> ('<tok> X)) F)  (F `(quote ,X)))
     ((('<atm> ('<top> X)) F)  (F `(<top> ,X)))
     ((('<atm> (_     X))  F)  (F X))
     ((('<atm> '<eoln>)    F)  (F '()))
     ((('<fkn> F ())       G)  (G `(quote ,F)))
     (((':term X)          G)  `(:term ,X))
     ((('<fkn> F Args)     G)
      (let ((Args (consify-help
                   (with-fluids ((*expression-context* #t))
                                (map arg-handle Args)))))
        (G (consify-help `(<fkn> ,F ,Args)))))
     ((('<lis> L)          F)
      (with-fluids ((*expression-context* #t))
         (F (sieve-out-stubs L))))
     ((('<lam> L)          F) (handle-lambda L))
     (((':group L)         F)
      (consify-help (qtreeify (compile-rhs L))))))

(define (arg-handle X)
  (define (arg-handle0 X)
    (match X
       ((':term  X) (arg-handle2 X))
       (('<lam> X)  (handle-lambda X))
       ((':group X) (consify-help (qtreeify (compile-rhs X))))
       (X           (consify-help (qtreeify (compile-rhs X))))))
  ;(pp `(:handle ,X))
  (arg-handle0 X))

(define (arg-handle2 X)
  (match X
     (('<lam> L)           (handle-lambda L))
     ((and L ('<fkn> F A)) (consify_help (sieve-out-stubs2 L id)))
     ((and L (':group V))  (consify_help `(:group ,(sieve-out-stubs2 L id))))
     (L                    (sieve-out-stubs2 L 
                                             (lambda (X) 
                                               (match X
                                                      ((cond (':term X) X) `(:term ,X))))))))
     


  
 


(define Next      (make-prompt-tag "gp-next"))
(define NextSemi  (make-prompt-tag "gp-next-semi"))
(define NextCatch (make-prompt-tag "gp-next-catch"))

(define (-:: Last? Vars L) (-: Last? Vars L))
(define (-:  Last? Vars L)
  (let ((F  (lambda (Fi <CC> Nx) (if (eq? Fi 'last)
				   `(,<CC> ,Nx)
				   `(u-call *gp-fi* ,<CC> ,Nx))))

	(s  (gensym "state"))
	(Pr (gensym "Prompt")))

    (if Last?
        (tangle Vars 'last '<CC> '<Cut>  L F)
        `(let ((,Pr (u-prompt *gp-fi*)))
           ,(tangle Vars '*gp-fi* '<CC> Pr L F)))))
	

;;A very simple implementation of iso =.. e.g. no error modes checked
;;just a simple backtrack!
(define (=../2 X L Cut CC)
  (umatch #:mode + 
          (X L Cut CC)
          ((':fkn . List) List <Cut> <CC> (<CC> <Cut>))
          (L              L    <Cut> <CC> (<CC> <Cut>))
          ( _             _    <Cut> _  (u-abort <Cut>))))


(define fun-ops '((<=..> =../2)))

(define (get-fun-ops X)
  (define (f X)
    (match X
           (((,X . V) . _) V)
           ((_        . L) (f L))
           ( _             #f)))
  (f fun-ops))
(define (fun-ops? X) (get-fun-ops X))

(define lam-true (lambda (x) #t))
(define (let-it Vs Code) (if (pair? Vs) `(let ,(map (lambda (X) `(,X (u-var!))) Vs) ,Code) Code))

;;Vs     : is variable let structure
;;Fi     : ('first u-calls! 'last tail-calls!),
;;<CC>   : Next closure function!
;;Nx     : <Cut> prompt
;;Code   
;;F      : compilation continuation

(define (tangle Vs Fi <CC> Nx W F)
  ;;(pp `(tangle ,Vs ,Fi ,W))
  (match `(,Vs ,W)
    (((X ())          Q)  (tangle X Fi <CC> Nx Q F))
    ((('let V Vg X)   W)  (let-it V (tangle X Fi <CC> Nx W F)))

    ((Vs              (':term (':group X)))  (tangle Vs Fi <CC> Nx X F))
    ((Vs              (':group X         ))  (tangle Vs Fi <CC> Nx X F))

    (((cond (('<comma> Vx Vy) . _) ('<comma> Vx Vy))  ('<comma> X Y))
     (tangle Vx Fi <CC> Nx X 
             (lambda (Fi <CC> Nx) 
               (tangle Vy Fi <CC> Nx Y F))))               
        
    ;;(X ; Y) if X is true, continue else if Y is true continue! (warning! exponential code generation can happen)
    ((('<let-semi> Vs Vx Vy)      ('<semi>  X Y))
     (let ((Pr   (gensym "prompt"))
           (Cont (gensym "cont"))
           (Qr   (gensym "Pr"))
           (Tag  (gensym "tag"   )))        
       (let-it Vs 
               (match (F 'last <CC> Qr)
                      (((? symbol? C))                          
                       `(umatch #:tag ,Tag
                                ( ( (let ((,Pr (u-prompt  ,Tag)))
                                      ,(tangle Vx Tag <CC> Pr X (lambda (A B Cq) `(,C ,Cq))) ) )
                                  ( ,(tangle Vy Fi <CC> Nx Y (lambda (A B Cq) `(,C ,Cq)))))))
                      (FF
                       `(let ((,Cont (lambda (Qr) ,FF)))
                          (umatch #:tag ,Tag
                                  ( ( (let ((,Pr (u-prompt ,Tag)))
                                        ,(tangle Vx Tag <CC> Pr X (lambda (A B C) `(,Cont ,C))) ) )
                                    ( ,(tangle Vy Fi <CC> Nx Y (lambda (A B C) `(,Cont ,C))))))))))))

     ;;(X -> Y)  if X is true undo and continue with Y else fail
     ((('<->> Vx Vy)       ('<->>   X Y))
      (let ((Pr  (gensym "prompt"))
            (Tag (gensym "tag")))
        `(umatch #:tag ,Tag
                 ( ( (let ((,Pr (u-prompt ,Tag)))
                       ,(tangle Vx Tag `(lambda (X) (u-abort ,Pr)) Nx X F)))
                   ( ,(tangle Vy Fi <CC> Nx Y F))))))


     ((('<is> V Vy)        ('<is>   X Y))
      (let ((X (match X 
                      ((':term ('<var> X)) X)
                      (X                   (error "expect variable in lhs in is form")))))
        (if (null? V)
            `(if (u-unify-raw! ,X ,(tangle-expr Vy Y))
                 ,(F Fi <CC> Nx)
                 (u-abort ,Nx))
            `(let ((,X ,(tangle-expr Vy Y)))
               ,(F Fi <CC> Nx)))))
     
     ;;Todo make these work with variable settings!! later on e.g. robustify they
     ;;should always refer to a valid already defined variable,
     ((Vs    ('<ref-is>   X Y))
      `(if (gp-ref-set! ,(tangle-left X) ,(tangle-expr Y))
           ,(F Fi <CC> Nx)
           (error "tries to set a non gp variable")))

     ((Vs    ('<attr-is>   X Y))
      `(if (gp-force-attr! ,(tangle-left X) ,(tangle-expr Y))
           ,(F Fi <CC> Nx)
           (error "tries to set a non gp variable")))

     ((Vs    ('<force-is>   X Y))
     `(if (u-set! ,(tangle-left X) ,(tangle-expr Y))
          ,(F Fi <CC> Nx)
          (error "tries to set a non gp variable")))

     ;;unification equality
     ((('<let=> Vs Vb Vf)      (':term ('<=> X Y)))
      (let-it Vs
              (compile-unify Vb Vf X Y
                             Nx
                             (lambda () (F Fi <CC> Nx)))))

     ;;Todo please fix this bitrotted part of the program
     ((('</=> Vx Vy)           ('</=>   X Y))
      (let ((fi (gensym "fi"))
            (G  (gensym "F")))
       `(let ((,fi (gp-newframe))
              (,G  (lambda () ,(F 'last <CC> NX))))
          ,(compile-unify '() '() (tangle-= Vx X) (tangle-= Vy Y)
                          (lambda () `(u-abort ,Nx))
                          '(,G)))))
    

     ((Vs                      ('<=:=> X Y))
      `(if (equal? ,(tangle-expr X) ,(tangle-expr Y))
           ,(F Fi <CC> Nx)
           (u-abort ,Nx)))

     ((Vs                      ('<==> X Y))
      `(if (equal? ,(tangle-expr X) ,(tangle-expr Y))
           ,(F Fi <CC> Nx)
           (u-abort ,Nx)))

     ((Vs                      ('<=/=> X Y))
      `(if (equal? ,(tangle-expr X) ,(tangle-expr Y))      
           (u-abort ,Nx)
           ,(F Fi <CC> Nx)))

     ((Vs                       ('</==> X Y))
     `(if (equal? ,(tangle-expr X) ,(tangle-expr Y))      
          (u-abort ,Nx)
          ,(F Fi <CC> Nx)))

     ((Vs                       ('<<>  X Y))
      `(if (< ,(tangle-expr X) ,(tangle-expr Y))
           ,(F Fi <CC> Nx)
           (u-abort ,Nx)))

     ((Vs                       ('<=<>  X Y))
      `(if (<= ,(tangle-expr X) ,(tangle-expr Y))
           ,(F Fi <CC> Nx)
           (u-abort ,Nx)))

     ((Vs                       ('<<=>  X Y))
      `(if (<= ,(tangle-expr X) ,(tangle-expr Y))
           ,(F Fi <CC> Nx)
           (u-abort ,Nx)))

     ((Vs                       ('<>>   X Y))
      `(if (> ,(tangle-expr X) ,(tangle-expr Y))
           ,(F Fi <CC> Nx)
           (u-abort ,Nx)))

     ((Vs                       ('<=>>  X Y))
      `(if (>= ,(tangle-expr X) ,(tangle-expr Y))
           ,(F Fi <CC> Nx)
           (u-abort ,Nx)))

     ((Vs                       ('<>=>  X Y))
      `(if (>= ,(tangle-expr X) ,(tangle-expr Y))
           ,(F Fi <CC> Nx)
           (u-abort ,Nx)))

     ((('</+> Vx)               ('</+> X))
      (let ((Pr  (gensym "prompt"))
            (Tag (gensym "tag"   )))
        `(umatch #:tag ,Tag
                 ( ((let ((,Pr (u-prompt ,Tag)))
                      ,(tangle Vx Tag `(lambda (x) (u-abort ,Nx)) Pr X 
                               (lambda (Fi <CC> Nxx) `(u-abort ,Nx)))))
                   (,(F Fi <CC> Nx))))))


     ((Vs                       (':term Term)) (=> next)
      (match Term
             ((and U (':group L))           (tangle Fi <CC> Nx U F))
                          
             (('<fkn> 'atomic/1 (X))          `(if (gp-atomic? X) 
                                                  ,(F Fi <CC> Nx)
                                                  (u-abort ,Nx)))

             (('<fkn> 'var/1 (X))    `(if (gp-var? X) 
                                                  ,(F Fi <CC> Nx)
                                                  (u-abort ,Nx)))

             (('<fkn> 'nonvar/1 (X)) `(if (not (gp-var? X))
                                                  ,(F Fi <CC> Nx)
                                                  (u-abort ,Nx)))
     
             (('<fkn> 'integer/1 (X))
              `(if (gp-integer? (u-atm? X))
                   ,(F Fi <CC> Nx)
                   (u-abort ,Nx)))
     
             (('<fkn> 'list/1 (X))
              `(if (gp-cons? X)
                   ,(F Fi <CC> Nx)
                   (u-abort ,Nx)))

             (('<fkn> 'fwhen (X))
              `(if ,X
                   ,(F Fi <CC> Nx)
                   (u-abort ,Nx)))
             
             (('<fkn> 'newframe/1 (P))
              `(begin (u-set! P (u-newframe))
                      ,(F Fi <CC> Nx)))

             (('<fkn> 'unwind/2 (P F))
              `(begin (u-unwind P) (F Nx <CC>)))

             ;;bitrotted
             (('<fkn> 'once (U))
              (let ((Fi (gensym "Fi")))
                `(let ((,Fi (gp-newframe)))
                   (if ,(tangle 'last lam-true Nx X (lambda (Fi <CC> Nx) #t))
                       (begin (gp-unwind ,Fi) 
                              ,(F Fi <CC> <Cut>))
                       (abort-to-prompt ,Nx)))))
    
             ;;bitrotted
             (('<fkn> 'catch (G C R))
              `(let ((,Fi (gp-newframe)))
                 (call-with-prompt NextCatch
                                   (lambda ( ) ,(tangle 'last <CC> Nx G F))
                                   (lambda (S) (let ((,Fi2 (gp-newframe)))
                                                 (if (S ,(tangle-left C))
                                                     (begin (gp-unwind ,Fi)
                                                            ,(tangle Fi <CC> Nx R F))
                                                     (begin (gp-enwind ,Fi2)
                                                            (S (abort-to-prompt NextCatch)))))))))

             ;;bitrotted
             (('<fkn> 'throw (U))
              `(u-unify! (tangle-left U) (abort-to-prompt NextCatch)))

             ('<!>         (F 'last <CC> '<Cut>))
             ('<fail>     `(u-abort ,Nx))
             ('<true>      (F Fi <CC> Nx))

             (('quote H)
              (let ((Next (gensym "Next")))
                (if (eq? Fi 'last)
                    `(,H ,Nx ,(lam-it Next F 'last <CC>))
                    `(u-call ,Fi ,H ,Nx ,(lam-it Next F 'last <CC>)))))     
             
             (_ (next))))

     ((('<fkn> Vs Vb Vf)      (':term ('<fkn> H A)))
      (let* ((Next     (gensym "Next"))
             (u-lam    (lambda (X) (cunify Vb Vf X)))             
             (code-lam (lambda (FF Args E)
                        (if (eq? Fi 'last)
                            (let-it Vs `(,FF ,@Args 
                                            ,Nx ,(lam-it Next F 
                                                         'last <CC>) ,@E))
                            (let-it Vs `(u-call ,Fi ,FF ,@Args
                                                ,Nx ,(lam-it Next F 
                                                             'last <CC>) ,@E)))))
             (AAA       (if (pair? A) (uncons (car A)) A)))
        (pp AAA)
        (match H
                ('mark/2      (code-lam (car A)
                                        (append (map u-lam
                                                     (uncons (cadr A)))
                                                '(#t))))
                ('mark/1      (code-lam (car A)
                                        (append (map u-lam
                                                     (uncons (cadr A)))
                                                '(#t))))
                ('dloga/2     (code-lam (car A)
                                        (append (map u-lam
                                                     (uncons (cadr A)))
                                                '(a))))
                ('dlogz/2     (code-lam (car A)
                                        (append (map u-lam
                                                     (uncons (cadr AA)))
                                                '(z))))

                ('asserta/1   (match AAA
                                     (('<:-> H Code)
                                      (match (uncons H)
                                             (('<fkn> H A)
                                              (code-lam H
                                                        (map u-lam (uncons Args))
                                                        `('(a ,Code))))))
                                     (('<fkn> H Args)
                                      (code-lam H 
                                                (map u-lam (uncons Args))
                                                '('(a))))))
                

                ('assertz/1   (match AAA
                                     (('<:-> H Code)
                                      (match (uncons H)
                                             (('<fkn> H A)
                                              (code-lam H
                                                        (map u-lam (uncons Args))
                                                        `('(z ,Code))))))
                                     (('<fkn> H Args)
                                      (code-lam H 
                                                (map u-lam (uncons Args))
                                                '('(z))))))
                                       
                (_ (code-lam (mk-f H) 
                             (map (lambda(X) (cunify Vb Vf X)) A)
                             '())))))
     

     ((Vs                      ((? fun-ops? X) . A))
      (let ((H    (get-fun-ops X))
            (Next (gensym "Next")))
        (if (eq? Fi 'last)
            `(,H ,@(map tangle-left A) 
                 ,Nx ,(lam-it Next F 'last <CC>))

            `(u-call ,Fi ,H ,@(map tangle-left A) 
                     ,Nx ,(lam-it Next F 'last <CC>)))))



     ((Vs   (and A ('cons . _)))   A                            )
     (((Vs) (L))                   (tangle Vs Fi <CC> Nx L F))
     ((Vs   (L))                   (tangle Vs Fi <CC> Nx L F))
     ((Vs    ())                   (F Fi <CC> Nx)            )
     (((Vs)  L)                    (tangle Vs Fi <CC> Nx L F))
     ((Vs  (':term X))             (tangle Vs Fi <CC> Nx X F))))

(define (mk-f F)
  (if (pvar? F)
      `(u-scm ,F)
      F))
      
(define (g? X)
  (equal? #\G (car (string->list (symbol->string X)))))

(define (lam-it Next F Last <CC>)
    (define (f X)
      (match X
             ((,<CC> ,Next)       <CC>)
             (('let (((? g? G) V)) W) 
              `(let ((,G ,V)) (begin (u-context)
                                     (lambda (,Next) ,W))))
             (W                 `(begin (u-context)
                                        (lambda (,Next) ,W)))))
    (f  (F Last <CC> Next)))

(define (uncons X)
  (match X
         (('cons X Y) (cons X (uncons Y)))
         (X           X)))
         

(define (tangle-= X)
  (match X
     ( (':term X)         (tangle-= X))
     ( ('<lis> X)         (tangle-= X))
     ( ('<fkn> N ())     `(quote ,N))
     ( ('<fkn> N A)       (csf `(<fkn> ,N ,(csf (map tangle-= A)))))   
     ( ('<lis> X)          (tangle-= X))  
     ( ('<var> X)           X)
     ( ('<top> X)           X)
     ( ('<gp>  (gp-atom X)) X)   
     ( ('<atm> (_ X))       X)
     ( ('<atm> '<eoln>)  ''())
     ( '<_>                '_)
     ( ('cons X Y)        `(cons ,(tangle-user X) ,(tangle-user Y)))
     ( (Op X)             (consify_help `(',Op ,(tangle-user X))))
     ( (':group L)        (tangle-user L))
     ( ()                ''())
     ( X                  X)))

(define (tangle-user X)
  (match X
     ( (':term X)         (tangle-user X))
     ( ('<lis> X)         (tangle-user X))
     ( ('<fkn> N ())     `(quote ,N))
     ( ('<fkn> N A)       (csf `(<fkn> ,N ,(csf (map tangle-user A)))))
     ( ('<var> X)           X)
     ( ('<top> X)           X)
     ( ('<gp>  (gp-atom X)) X)   
     ( ('<atm> (_ X))       X)
     ( ('<atm> '<eoln>)  ''())
     ( '()               ''())
     ( '<_>                '_)
     ( ('cons X Y)       `(cons ,(tangle-user X) ,(tangle-user Y)))
     ( (':group L)        (consify_help `(:group ,(tangle-user L))))
     ( (Op X)             (consify_help `(',Op ,(tangle-user X))))
     ( (Op X Y)           (consify_help `(',Op ,(tangle-user X) ,(tangle-user Y))))
     ( X                  X)))

(define (tangle-expr . L)
  (let ((L (if (eq? (length L) 1) (cons '() L) L)))
    ;;(pp `(texpr ,L))
    (match L
           ((V ('<lam> A  ) ) (tangle-lambda V A))
           ((V ('<o+>  X Y) ) `(+        ,(tangle-expr V X)  ,(tangle-expr V Y)))
           ((V ('<o//> X Y) ) `(remainder ,(tangle-expr V X)  ,(tangle-expr V Y)))
           ((V ('<o->  X Y) ) `(-        ,(tangle-expr V X)  ,(tangle-expr V Y)))
           ((V ('<o*>  X Y) ) `(*        ,(tangle-expr V X)  ,(tangle-expr V Y)))
           ((V ('<o/>  X Y) ) `(/        ,(tangle-expr V X)  ,(tangle-expr V Y)))
           ((V ('<mod> X Y) ) `(modulo   ,(tangle-expr V X)  ,(tangle-expr V Y)))
           ((V ('<rem> X Y) ) `(modulo   ,(tangle-expr V X)  ,(tangle-expr V Y)))
           ((V ('<A>   X Y) ) `(logand   ,(tangle-expr V X)  ,(tangle-expr V Y)))
           ((V ('<V>   X Y) ) `(logior   ,(tangle-expr V X)  ,(tangle-expr V Y)))
           ((V ('<^>   X Y) ) `(logxor   ,(tangle-expr V X)  ,(tangle-expr V Y)))
           ((V ('<<<>  X Y) ) `(ash      ,(tangle-expr V X)  ,(tangle-expr V Y)))
           ((V ('<>>>  X Y) ) `(ash (-   ,(tangle-expr V X)) ,(tangle-expr V Y)))
           ((V ('<**>  X Y) ) `(expt     ,(tangle-expr V X)  ,(tangle-expr V Y)))
           ((V ('<p/> X)    ) `(lognot ,(tangle-expr V X)))
           ((V ('<p+> X)    ) (tangle-expr V X))
           ((V ('<p-> X)    ) `(- ,(tangle-expre V X)))
           ((V ('cons X Y)  ) `(cons ,(tangle-expr V X) ,(tangle-expr V Y)))
           ((V ()           )''())
           ((V (':term X)   ) (tangle-expr V X))
           ((V (':group X)  ) (tangle-expr V X))
           ((V ('<lis> X)   ) (tangle-expr V X))
           ((V ('<fkn> N ())) `(quote ,N))
           ((V ('<fkn> N A) ) (tangle-expr V (consify_help 
                                              `((quote :fkn)
                                                (quote ,N  ) 
                                                ,@A))))

           ((V ('cons X Y)) `(u-cons ,(tangle-expr V X) ,(tangle-expr V Y)))
           ((V ('<var> X) ) `(u-scm ,X))
           ((V ('<top> X) ) `(u-scm ,X))
           ((V ('<atm> (_  X)))    X)
           ((V ('<atm> '<eoln>)) ''())
           ((V (and X ('<gp> _))) X) 
           ((V X)                 X))))


(define (tangle-lambda Vx X)
  (define (h X)
    (match X
           (('<comma> X Y) (cons X (h Y)))
           (X              `(,X))))

  (define (f X)
    (let ((Code (qtreeify X)))
      (h Code)))

  (define (compile-stm-lam X Vx Last?)
    ;;(pp `(cstmlam ,X ,Vx))
    (match `(,X ,Vx)
           (((A B) ('<item> Vb Vbb Vx))
            (let* ((Stubs (clean-term Vb (f A))))
              `(,@Stubs ,(-:: Last? Vx B))))))

  ;;(pp `(tlam ,X ,Vx))
  (match X
         (((A B) . _)
          (let* ((Args  (gen-args (length A)))
                 (Last? (map (lambda (X) #f) X))
                 (Last? (reverse (cons #t (cdr Last?))))
                 (Code `(,@(map compile-stm-lam X (cadar Vx) Last?) 
                         (,@(map (lambda (X) '_) Args) (u-abort <Cut>)))))

            `(lambda (,@Args <Cut> <CC>)
               (umatch #:mode + ,@Args ,Code))))))
                       

(define (tangle-left X)
  ;;(pp `(t-left X))
  (match X
         ( ('<o+>  X Y) `(+ ,(tangle-expr X) ,(tangle-expr Y)))
         ( ('<o->  X Y) `(- ,(tangle-expr X) ,(tangle-expr Y)))
         ( ('<o*>  X Y) `(* ,(tangle-expr X) ,(tangle-expr Y)))
         ( ('<o/>  X Y) `(/ ,(tangle-expr X) ,(tangle-expr Y)))
         ( ('<o//> X Y) `(truncate (/ ,(tangle-expr X) ,(tangle-expr Y)))) 
         ( ('<p+> X)    (tangle-expr X))
         ( ('<p-> X)   `(- ,(tangle-expre X)))
         ( (':term X)        (tangle-left X))
         ( (':group X)       (tangle-left X))     
         ( ('<fkn> N ())    `(quote ,N))
         ( ('<fkn> N A)      (tangle-left (consify_help `((<atm> (s :fkn)) 
                                                      (<atm> (s ,N  )) ,@A))))
         ( ('<var> X)         X)
         ( ('<top> X)         X)
         ( ('<atm> (_ X))     X)
         ( ('<atm> '<eoln>) ''())
         ( ('<lis> X)        (tangle-left X))
         ( ('cons X Y)      `(u-cons ,(tangle-left X) ,(tangle-left Y)))
         ( ()               ''())
         ( X                 X)))
                                               
(define (cunify Vb Vf X) (compile-unify Vb Vf #f X #f #f))

(define (compile-unify Vbegin Vfinal X Y Next Cont)  
  (define *vars* '())

  (define (compile-unify-1 X)
    ;;(pp `(cunify-1 ,X))
    (match X
           ((':term X)    (compile-unify-1 X))
           (('<top> X)    X)
           (('<var> X)    (cond 
                           ((member X Vfinal)
                            (set! *vars* (cons X *vars*))
                            X)
                           ((member X Vbegin) X)
                           (#t '(u-var!))))
           (('cons X Y)   `(u-cons ,(compile-unify-1 X)
                                   ,(compile-unify-1 Y)))
           ('<_>          '_)
           (('quote X)    `(quote ,X))
           ((? symbol? X) `(quote ,X))
           (()           ''())
           (X              X)))

  (define (compile-unify-2 X)
    ;(pp `(cunify-2 ,X))
    (match X
           ((':term X)   (compile-unify-2 X))
           (('<top> X)  `(uunquote X))
           (('<var> X)   (cond 
                          ((member X Vfinal) X                )
                          ((member X Vbegin) `(uunquote ,X)   )
                          (#t                '_               )))
           (('cons X Y)  `(cons ,(compile-unify-2 X)
                                ,(compile-unify-2 Y)))
           ('<_>          '(u-var!))
           (('quote X)    `(quote ,X))
           ((? symbol? X) `(quote ,X))
           (()            ''())
           (X             X)))

  (let f ((X X) (Y Y))
    ;;(pp `(compile-unify ,Vbegin ,Vfinal ,X ,Y))
  (match `(,X ,Y)         
     (( #f          Y         )  (compile-unify-1 Y))
     (( (':term X)  Y         )  (f X Y))
     (( X          (':term Y) )  (f X Y))

     ((('<top> X) ('<top> Y))
      (compile-unify (cons X (cons Y Vbegin)) Vfinal X Y Next Cont))
     ((Y ('<top> X)) (compile-unify (cons X Vbegin) Vfinal Y X Next Cont))
     ((('<top> X) Y) (compile-unify (cons X Vbegin) Vfinal Y X Next Cont))

     ((('<var>  X)  ('<var> Y))
      (cond
       ((member X Vfinal)
        (cond 
         ((member Y Vfinal)
          `(let* ((,X (u-var!))
                  (,Y ,X))
             ,(Cont)))
         ((member Y Vbegin)
          `(let ((,X ,Y))
             ,(Cont)))
         (#t 
          `(let ((,X (u-var!)))
             ,(Cont)))))
       ((member X Vbegin)
        (cond
         ((member Y Vfinal)
          `(let ((,Y ,X)) 
             ,(Cont)))
         ((member Y Vbegin)
          `(if (u-unify-raw! ,X ,Y)
               ,(Cont)
               (u-abort ,Next)))
         (#t (Cont))))
       (#t
        (cond
         ((member Y Vfinal)
          `(let ((,Y (u-var!))) ,(Cont)))
         ((member Y Vbegin) (Cont))
         (#t                (Cont))))))

     ((('<var> X) (and A (not ('quote _)) ('cons H L)))
      (cond 
       ((member X Vfinal)
        (set! *vars* '())
        (let ((U (compile-unify-1 A))
              (V *vars*))
          (let-it V
                  `(let ((,X ,U))
                     ,(Cont)))))

       ((member X Vbegin)
        `(umatch #:tag   *gp-wii* 
                 #:mode  + 
                 #:raw
                 ,X 
                 ( (,(compile-unify-2 A)
                    (begin ,(Cont)))                          
                   (_  (u-abort ,Next)))))
       
       (#t (Cont))))

     (((and A (not ('quote _)) ('cons H L)) ('<var> X))
      (cond 
       ((member X Vfinal)
        (set! *vars* '())
        (let ((U (compile-unify-1 A))
              (V *vars*))
          (let-it V
                  `(let ((,X ,U))
                     ,(Cont)))))

        ((member X Vbegin)
        `(umatch #:tag   *gp-wii* 
                 #:mode  + 
                 #:raw
                 ,X 
                 ( (,(compile-unify-2 A)
                    (begin ,(Cont)))                          
                   (_  (u-abort ,Next)))))

        (#t (Cont))))

      ((('<var> X) Y)
              (cond
               ((member X Vfinal)
                `(let ((,X ,(compile-unify-1 Y))) ,(Cont)))
               ((member X Vbegin)
                `(if (u-unify-raw! ,X ,(compile-unify-1 Y))
                    ,(Cont)
                    (u-abort ,Next)))
               (#t  (Cont))))

      ((Y ('<var> X))
              (cond
               ((member X Vfinal)
                `(let ((,X ,(compile-unify-1 Y))) ,(Cont)))
               ((member X Vbegin)
                `(if (u-unify-raw! ,X ,(compile-unify Y))
                     ,(Cont)
                     (u-abort ,Next)))
               (#t  (Cont))))
     
      (((and (not ('quote _)) ('cons H1 L1))
        (and (not ('quote _)) ('cons H2 L2)))
       (compile-unify Vbegin Vfinal
                      H1 H2 Next 
                      (lambda () 
                        (compile-unify Vbegin Vfinal
                                       L1 L2 Next Cont))))

       ((X           X)  (Cont))
       (_               (begin
                          (warn "unification always fail")
                          `(u-abort ,Next))))))

(define (file->list fname)
  (let ((p (open-file fname "r")))
     (let read ((ret '())) 
      (let ((ch (read-char p)))
	(if (eof-object? ch)
	    (reverse ret)
	    (read (cons ch ret)))))))

(define-syntax load-prolog
  (lambda (x)
    (syntax-case x ()
      ((_ file)
       (with-syntax (((exp ...) (datum->syntax
                                 x 
                                 (prolog-compile (file->list (syntax->datum #'file))))))
	 #'(begin exp ...))))))

(define-syntax prolog-it
  (lambda (x)
    (syntax-case x ()
      ((_ str)
       (with-syntax (((exp ...) (datum->syntax
                                 x 
                                 (prolog-compile (string->list (syntax->datum #'str))))))
		    #'(begin exp ...))))))

(define (mk-L X)
  (match X
     ( (H . L) (if (eq? H '_)
		   (cons '(u-var!)     (mk-L L))
		   (cons `(scm->gp ,H) (mk-L L))))
     ( ()     '())))

;;***************************** Redo facilities ***************************
;; in order to do generational breath first kind of searches initiate
;; a redo-frame with redo-frame/0 then one can use postpone/0 to postpone
;; that branch to the next generation of search.


;;default unwind creation
(define (unwind-mute X Y) #f)

;;min/max interval
(define (unwind-interval X Y)
  (cons (min (car X) (car Y)) 
        (max (cdr X) (cdr Y))))

;; This is used when want to track individual continuations
(define (unwind-token X Y)
  (let ((Ret '(parent X Y)))
    (set-car! X Ret)
    (set-car! Y Ret)
    Ret))


(define (postpone_frame <Cut> <CC>)
  (define (postpone_frame_ <Cut> <CC>)
    (define (f X <Cut>)
      (let ((S (gp-search-branch X)))
        (cond ((null? S) (u-abort <Cut>))
              ((pair? S) (umatch ( ((let ((Pr (u-prompt *gp-fi*)))
                                      (u-call *gp-fi* f (car S) Pr)))
                                   ((f (cdr S) <Cut>)))))
              (else      (S <Cut>)))))
           
    (define (g)
      (if (gp-has-postpones?)
          (let ((S (gp-postpone-start))
                (F (gp-postpone-frame)))
            (gp-postpone-newframe)
            (umatch ( ((let ((Pr (u-prompt *gp-fi*)))
                         (u-call *gp-fi* f S Pr)))
                      ((if (gp-has-postpones?)
                           (begin (gp-cleanup-move F)
                                  (g))
                           (begin (u-abort <Cut>)))))))
          (u-abort <Cut>)))
                               

    (gp-postpone-newframe)
    (umatch ( ((let ((Pr (u-prompt *gp-fi*)))
                 (u-call *gp-fi* <CC> Pr))) 
              ((g)))))
  
  (with-fluids ((gp-token-fluid unwind-mute))
               (umatch ( ((let ((Pr (u-prompt *gp-fi*)))
                            (u-call *gp-fi* postpone_frame_ Pr <CC>)))
                         ((begin               
                            (gp-postpone-unwind)
                            (u-abort <Cut>)))))))


;;This is a tool to use potentials about goodness of branches.
;;It's a bit expensive if we need to take to many levels
(define *postpone-level* #f)
(define *postpone-minlevel* -1)
(define (postpone_frame_minmax/2 Level Fraction <Cut> <CC>)  
  (define (postpone_frame_ Level Fraction <Cut> <CC>)
    (define (f X <Cut>)
      (let ((S (gp-search-branch X)))
        (cond ((null? S) (u-abort <Cut>))
              ((pair? S) (umatch ( ((let ((Pr (u-prompt *gp-fi*)))
                                      (u-call *gp-fi* f (cdar S) Pr)))
                                   ((f (cddr S) <Cut>)))))
              (else      (S <Cut>)))))
           
    (define (g Level)
      (if (gp-has-postpones?)
          (let* ((Tok   (gp-get-token))
                 (Level (* Fraction (cdr Tok)))
                 (S     (gp-postpone-start))
                 (F     (gp-postpone-frame)))
            (set! *postpone-level* Level)
            (gp-postpone-newframe)
            (umatch ( ((let ((Pr (u-prompt *gp-fi*)))
                         (u-call *gp-fi* f S Pr)))
                      ((if (gp-has-postpones?)
                           (begin (gp-cleanup-move F)
                                  (g Level))
                           (begin (u-abort <Cut>)))))))
          (u-abort <Cut>)))
                               

    (gp-postpone-newframe)
    (set! *postpone-level* Level)    
    (umatch ( ((let ((Pr (u-prompt *gp-fi*)))
                 (u-call *gp-fi* <CC> Pr))) 
              ((g Level)))))
  
(with-fluids ((gp-token-fluid unwind-interval))
               (umatch ( ((let ((Pr (u-prompt *gp-fi*)))
                            (u-call *gp-fi* 
                                    postpone_frame_
                                    (gp->scm Level   )
                                    (gp->scm Fraction)
                                     Pr <CC>)))
                         ((begin               
                            (gp-postpone-unwind)
                            (u-abort <Cut>)))))))


(define (postpone_frame_x/3 Min-level Max-level Fraction <Cut> <CC>)  
  (define (postpone_frame_ Min-Level Max-Level Fraction <Cut> <CC>)
    (define (f X <Cut>)
      (let ((S (gp-search-branch-kill X)))        
        (umatch S ( (()                   (u-abort <Cut>))
                    (((A . X) . (B . Y))  (if (< (cdr B) *postpone-level*)
                                              (if (< (cdr A) *postpone-level*)
                                                  (u-abort <Cut>)
                                                  (f X <Cut>))
                                              (if (< (cdr A) *postpone-level*)
                                                  (f Y <Cut>)
                                                  (umatch ( ((let ((Pr (u-prompt *gp-fi*)))
                                                               (u-call *gp-fi* f Y Pr)))
                                                            ((f X <Cut>)))))))
                    (Lam                  (begin
                                            (gp-postpone-newframe)
                                            (umatch ( ((let ((Pr (u-prompt *gp-fi*)))                       
                                                         (u-call *gp-fi* Lam Pr)))
                                                      ((begin
                                                         (gp-postpones-to-prev-frame)
                                                         (u-abort <Cut>)))))))))))
    

    (define (g Level S)
      (if (gp-has-postpones?)
          (let* ((Tok   (gp-get-token))
                 (Level (* Fraction (min Level (cdr Tok))))
                 (F     (gp-postpone-frame)))
            (set! *postpone-level* Level)
            (let ((K (gp-postpone-search-ptr)))
              (umatch ( ((let ((Pr (u-prompt *gp-fi*)))
                           (u-call *gp-fi* f S Pr)))
                        ((if (gp-has-postpones?)
                             (begin
                               (gp-postpone-search-unwind K)
                               (g Level S))
                             (begin (u-abort <Cut>))))))))
          (u-abort <Cut>)))
                               

    (gp-postpone-newframe)
    (set! *postpone-minlevel* Min-level)
    (set! *postpone-level*    Max-level)
    (umatch ( ((let ((Pr (u-prompt *gp-fi*)))
                 (u-call *gp-fi* <CC> Pr))) 
              ((g Max-level (gp-postpone-start))))))
  
  (with-fluids ((gp-token-fluid unwind-interval))
               (umatch ( ((let ((Pr (u-prompt *gp-fi*)))
                            (u-call *gp-fi* 
                                    postpone_frame_
                                    (gp->scm Min-level)
                                    (gp->scm Max-level)
                                    (gp->scm Fraction)
                                    Pr <CC>)))
                         ((begin               
                            (gp-postpone-unwind)
                            (u-abort <Cut>)))))))

(define (postpone <Cut> <CC>)
  (gp-postpone <CC>)
  (u-abort <Cut>))

(define (postpone/1 X <Cut> <CC>)
  (let* ((Tok (gp->scm X))
         (Q   (cons Tok Tok)))
    (if (< Tok *postpone-level*)
        (begin
          (gp-postpone-token <Cut> Q)
          (u-abort <Cut>))
        (<CC> <Cut>))))

(define (postpone_x/1 X <Cut> <CC>)
  (let* ((Tok (gp->scm X))
         (Q   (cons Tok Tok)))
    (if (< Tok *postpone-minlevel*)
        (u-abort <Cut>)
        (if (< Tok *postpone-level*)
            (begin
              (gp-postpone-token <CC> Q)
              (u-abort <Cut>))
            (<CC> <Cut>)))))
  
  			    
;;*****************************  Utility ****************

(define (pk/1 X N <CC>) (begin (pk (gp->scm X)) (<CC> N)))
     
(define (scheme-it str) (pretty-print (prolog-compile (string->list str))))


(define (symbol->list X) (string->list (symbol->string X)))


(define (mk-print L)
  (list->string
   (cons #\[
	 (if (null? L)
	     '(#\])
	     (let f ((X L))
	       (umatch X ( ( (H    )  (append (symbol->list H) '(#\])))
			   ( (H . L)  (append (symbol->list H) '(#\,) (f L))))))))))


(define (print/1 V N <CC>)
  (format #t "Data ~a~%" (gp->scm V))
  (<CC> N))


(define u-continue #f)

(define just-one #f)
(define (nextp N <CC>)
  (if just-one 
      #t
      (begin 
      (if u-continue (u-abort N))
      (let ((C (lambda () (readline "more m, finish e, all a > "))))
        (let f ((Ret (C)))
          (cond  ((equal? Ret "m")  (u-abort N))
                 ((equal? Ret "e")  #t)
                 ((equal? Ret "a")  (begin (set! u-continue #t) (u-abort N)))
                 (#t                (f (C)))))))))

  
(define (e Sexp c) (eval (macroexpand Sexp) (interaction-environment)))


(define (run_ s c)
  (define (f rhs)
    (if (pair? rhs)
        (union (f (car rhs)) (f (cdr rhs)))
        (if (and (symbol? rhs)   
                 (char-upper-case? (car (string->list (symbol->string rhs)))))
            `(,rhs)
            '())))
           
  (let* ((A1 "start123  :- ")
	 (S1 (string-append A1 s ",nextp, start123.")))
    (e (car (prolog-compile (string->list S1))) c)
    (let* ((R (mk-print (f rhs-vars)))
	   (S1 (string-append A1 
			      s 
			      (format #f ",print(~a), nextp." R))))
      (car (prolog-compile (string->list S1))))))

     
(define n-run 1)
(define (nrun)
  (define (f)
    (umatch #:tag next ()            
            ( (let ((Next (u-prompt next)) ) 
               (start123 Next (lambda (N) (u-abort N))))
              #t )
            ( #f )))
  (let g ((n n-run))
    (if (eq? n 0)
        #t
        (begin
          (gp-clear)
          (f)
          (g (- n 1))))))

(define-syntax
 (syntax-rules ()
   ((run x p)
    (begin
      (run_ x (current-module))
      (set! u-continue ,p)
      (nrun)))))


(define (find_all/3 X F L <Cut> <CC>)
  (let ((Pr (gp-newframe)))
    (let loop ((Q '()))
      (gp-unwind Pr)
      (<semi> (Pr2) 
              (F Pr2 (lambda (P) (loop (cons (gp-scm X) Q))))
              (begin (u-unify! Q L) (<CC> <Cut>))))))

;;;Evaluator

;;If we get a lambda, just call it directly else start the evaluator
(define (call/1 Code <Cut> <CC>)
  (umatch Code 
          ( ((X . L) (eval/1 Code <Cut> <CC>))
            ((F      ((u-scm F) <Cut> <CC>))))))

;;currently supports , ; and ->
(define (eval/1 Code <Cut> <CC>)
  (umatch #:mode -
          ( (('<comma> X Y)  (eval/1 X <Cut> (lambda (Pr) (eval/1 Y Pr <CC>))))
            (('<semi>  X Y)  (umatch #:tag w
                                     ( ((let ((Pr (u-prompt w)))
                                          (eval/1 X Pr <CC>)))
                                       ((eval/1 X <Cut> <CC>)))))
            (('<->>    X Y)  (umatch #:tag w
                                     ( ((let ((Pr (u-prompt w)))
                                          (eval/1 X <Cut> (lambda (P) (u-abort Pr)))))
                                       ((eval/1 Y <Cut> <CC>)))))
            (('<fkn> F  L)   (eval `(F ,@L <Cut> <CC>) (interaction-environment))))))
            

;;after
(define *after* '())
(define (set_after/1 F <Cut> <CC>)
  (set! *after* (cons F *after*)))
(define (run_after <Cut> <CC>)
  (if (null? *after*)
      (<CC> <Cut>)
      (begin
        (let ((F (car *after*)))
          (set! *after* (cdr *after*))
          (F)))))


;; ASSERTS!!  database handling
;; Simple datalog like asserts
(define-syntax mk-db
  (syntax-rules ()
    ((_ name args ...)
     (let ((data '()))
       (define (name args ... <Cut> <CC>)
         (case-lambda
           ((A)
            ;;Assert = add data to database!
            (if (null? data)
                ;;At first use enter a dynamic wind so that we clean up 
                ;;at exceptions
                (dynamic-wind
                    ;;in guard
                    (lambda () '())
                    ;;thunk
                    (lambda ()
                      (begin
                        (if (eq? A 'a)
                            (set! data (cons `(,args ...) data))
                            (set! data (append data `((,args ...)))))
                        (<CC> <Cut>)))
                    ;;out-guard
                    (lambda ()
                      (set! data '())))
                (begin              
                  (if (eq? A 'a)
                      (set! data (cons `(,args ...) data))
                      (set! data (append data `((,args ...)))))
                  (<CC> <Cut>))))
           
           (()
             ;;Just equating the database
             (for loop ((d data))
                  (if (pair? d)
                      (umatch (car d)
                              ((,args ...) 
                               ((let ((Pr (u-prompt *gp-fi*)))
                                  (u-call *gp-fi* <CC> Pr))))
                              (_ (loop (cdr d))))
                      (u-abort <Cut>))))))))))

;;asserta asssertz iso predicates!!
(define-syntax mk-dbb
  (syntax-rules ()
    ((_ name args ...)
     (let ((data '()))
       (define (name args ... E <Cut> <CC>)
         (case-lambda
         ((A)
          ;;Assert = add data to database!
          (if (null? data)
              ;;At first use enter a dynamic wind so that we clean up 
              ;;at exceptions
              (dynamic-wind
                  ;;in guard
                  (lambda () '())
                  ;;thunk
                  (lambda ()
                    (begin
                      (if (eq? (car A) 'a)
                          (set! data (cons `(((,args ...) ,(cadr A))) data))
                          (set! data (append data `(((,args ...) (cadr A))))))
                      (<CC> <Cut>)))
                  ;;out-guard
                  (lambda ()
                    (set! data '())))
              (begin              
                (if (eq? (car A) 'a)
                    (set! data (cons `((,args ...) ,(cadr E)) data))
                    (set! data (append data `((,args ...) ,(cadr E)))))
                (<CC> <Cut>))))

             ;;Just equating the database
         (()
          (for loop ((d data))
               (if (pair? d)
                   (umatch (car d)
                           (((,args ...) E)
                            ((let ((Pr (u-prompt *gp-fi*)))
                               (if (null? E)
                                   (u-call *gp-fi           <CC> Pr)
                                   (u-call *gp-fi* eval/1 E <CC> Pr)))))
                           (_ (loop (cdr d))))
                   (u-abort <Cut>))))))))))


;;Faster lookup e.g. symbol -> data pointers only one reference value
(define-syntax mk-s-db
  (syntax-rules ()
    ((_ name sym args ...)
     (define (name sym args ... <Cut> <CC>)       
       (let ((A (symbol-property sym key)))
         ;;Assert = add data to database!
         (if (fluid-ref! *assert*)             
             (if (not A)
                 ;;we use dynamic winds so that we clean up 
                 ;;at exceptions
                 (dynamic-wind
                     ;;in guard
                     (lambda () '())
                     ;;thunk
                     (lambda ()
                       (begin
                         (set-symbol-property! sym key `(,args ...))
                         (<CC> <Cut>)))
                     ;;out-guard
                     (lambda ()
                       (symbol-property-remove! sym key #f)))
                 ;;if we been here fail!
                 (u-abort <Cut>)))

         ;;Just equating the database
         (umatch A
                 ((,args ...)
                  ((let ((Pr (u-prompt *gp-fi*)))
                     (u-call *gp-fi* <CC> Pr))))
                 ((u-abort <Cut>))))))))

;;One can make symbol -> list of data
;;Symbol x Symbol and keep marginal data etc.
     
