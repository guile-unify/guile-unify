;;macros needed, e.g. 

(define (make-c-fkn fname nargs code)
(format #f
"
#define LOCAL_REF(i)     fp[i]
#define LOCAL_SET(i,v)   fp[i] = (v)
void ~a(SCM *fp, SCM **spp)
{
 /* setup of environment */
 SCM *objects, *free, program, *sp, sss[100];
 sp = sss;
 program = fp[-1];
 objects = SCM_I_VECTOR_WELTS (SCM_PROGRAM_OBJTABLE (program));    
 free    = SCM_PROGRAM_FREE_VARIABLES(program);
 /* compiled section */
 ~a
}
" fname code))


(define (mk-c-init f fn)
 (define (f1 x) (format #f "SCM ~a_sym;" x))
 (define (f2 x) (format #f "~a_sym = scm_from_locale_symbol(\"~a\");" x x))
 (define (f3 x) (format #f "printf(\"loading fkn %s as adres %x.\\n\",\"~a\",&~a);" x x))
 (define (f4 x) (format #f "scm_define(~a_sym,SCM_PACK(((scm_t_bits) &~a) | 2));" x x))
 (format #f "
void init_~a()
{
   ~{~a~%   ~}

   ~{~a~%   ~}

   ~{~a~%   ~}

   ~{~a~%   ~}
}
" f (map f1 fn) (map f2 fn) (map f3 fn) (map f4 fn)))
	

(define (gen-makefile name)
  (format #f "libguile-~a.so : ~a.c~%~/gcc -shared -O2 -o libguile-~a.so -fPIC `pkg-config --cflags guile-2.0` ~a.c~%" name name name name))


(define (object->toc x)
  (cond ((eq? x #t)   "SCM_BOOL_T"    )
	((eq? x #f)   "SCM_BOOL_F"    )
        ((eq? x #nil) "SCM_ELISP_NIL" )
	((null? x)    "SCM_EOL"       )
	((and (integer? x) (exact? x))
	 (format #f "SCM_I_MAKINUM(~a)" x))
	((char? x)
	 (format #f "SCM_MAKE_CHAR(~a)" (char->integer x)))
	(else #f)))

(define (gen-topref   i) (format #f "TOPLEVEL_REF(~a);" i    ))
(define (gen-topset   i) (format #f "TOPLEVEL_SET(~a);" i    ))
(define (make-fix index) (format #f "MAKE_FIX(~a)    ;" index))
(define (scm-comp-str n) (format #f "SCM_COMP_STR(~a);" n    ))


(define (glil->toc glil toplevel? bindings
		   source-alist label-alist object-alist arities addr)  
  
  (define (skip-first X)
    (define (replace-space X)
      (if (pair? X)
	  (cons (if (eq? (car X) #\space) #\_  (car X)) (replace-space (cdr X)))
	  '()))
    (let ((L (string->list (symbol->string X))))
      (string->symbol 
       (list->string
	(replace-space
	 (if (eq? (car L) #\:) (cdr L) L))))))

  (pk  `(glil ,glil))

  (record-case glil
    ((<glil-program> meta body)  (error "toc is not for <glil-program>")) 
    ((<glil-std-prelude> nreq nlocs else-label)
                                 (error "toc is not for <glil-std-prelude>")) 
    ((<glil-opt-prelude> nreq nopt rest nlocs else-label)
                                 (error "toc is not for <glil-opt-prelude>")) 
    ((<glil-kw-prelude>  nreq nopt rest kw allow-other-keys? nlocs else-label)
                                 (error "toc is not for <glil-kw-prelude>")) 
    ((<glil-bind>    vars      ) (error "toc is not for <glil-bind>"))
    ((<glil-mv-bind> vars  rest) (error "toc is not for <glil-mv-bind>"))
    ((<glil-unbind>            ) (error "toc is not for <glil-unbind>"))
    ((<glil-source>  props     ) (error "toc is not for <glil-source>"))

    ((<glil-void>) '())

    ((<glil-mode> mode)
     (error "not implemented toc"))

    ;;basically (PUSH 1) and (PUSH (OBJ-REF N)) assumed that complex objects are already loded into function structure
    ((<glil-const> obj)
     (cond
      ((object->toc obj)
       => (lambda (code)
            `(,(format #f "sp++; *sp = ~a;" code))))
      ((not object-alist)
       (error "this object should have been already loaded into object memory for spedup function to use!!"))
      (else
       (receive (i object-alist)
		(object-index-and-alist obj object-alist)
	    (if (eq? obj (string->symbol "<c-code-tag>"))
		'("")
		`(,(format "sp++; *sp = OBJECT_REF(~a);" i)))))))

    ;;Non checking primitives, make sure to use checkable version later on
    ((<glil-lexical> local? boxed? op index)
     (if local?
	 (case op
	   ((ref) (if boxed?
		      `(,(format #f "sp++; *sp = VARIABLE_REF(~a);" index))
		      `(,(format #f "sp++; *sp = LOCAL_REF(~a);"    index))))
	   ((set) (if boxed?
		      `(,(format #f "VARIABLE_SET(LOCAL_REF(~a),*sp); sp--;" index))
		      `(,(format #f "LOCAL_SET(~a,*sp)              ; sp--;" index))))
	   ((box)       `("SYNC_BEFORE_GC();"
			  ,(format #f  "LOCAL_SET(~a,scm_cell (scm_tc7_variable, SCM_UNPACK(*sp))); sp--;" index)))
	   ((empty-box) `("SYNC_BEFORE_GC();"
			  ,(format #f  "LOCAL_SET(~a,scm_cell (scm_tc7_variable, SCM_UNPACK(SCM_UNDEFINED));" index)))
	   
	   ((fix)       (make-fix index))
	   
	   ((bound?) (if boxed?
			 `(,(format #f "sp++; if(VARIABLE_BOUNDP(LOCAL_REF(~a)) *sp = SCM_BOOL_T; else *sp = SCM_BOOL_F;" index))
			 `(,(format #f "sp++; if(LOCAL_REF(~a))                 *sp = SCM_BOOL_T; else *sp = SCM_BOOL_F;" index))))

	   (else (error (format #f "what ~a" op))))
	 (case op
	   ((ref) (if boxed?
		      `(,(format #f "sp++; *sp = VARIABLE_REF(FREE_VARIABLE_REF(~a));" index))
		      `(,(format #f "sp++; *sp = FREE_VARIABLE_REF(~a);"               index))))
	   ((set) (if boxed? 
		      `(,(format #f "VARIABLE_SET(FREE_VARIABLE_REF(~a),*sp);" index))
		      (error "what." glil)))
	   (else (error (format #f "what ~a" op))))))
    
    ((<glil-toplevel> op name)
     (case op
       ((ref set)
        (cond
         ((not object-alist)
	  (error "toc needs and object-alist in <glil-toplevel>"))
         (else
	  (if (eq? name '<c-dummy>) 
	      '("")	      
	      (receive (i object-alist)
		       (object-index-and-alist (make-variable-cache-cell name)
					       object-alist)
		       (case op
			 ((ref) `(,(gen-topref i)))
			 ((set) `(,(gen-topset i)))))))))
       ((define)
	(error "define op not possible for toc at <glil-toplevel>"))
       (else
        (error "unknown toplevel var kind" op name))))

    ((<glil-module> op mod name public?)
     (let ((key (list mod name public?)))
       (case op
         ((ref set)
          (cond
           ((not object-alist)
            `(,@(dump-object key addr)
	      (link-now)
	      ,(case op 
		 ((ref) '(variable-ref))
		 ((set) '(variable-set)))))
           (else
            (receive (i object-alist)
                (object-index-and-alist (make-variable-cache-cell key)
                                        object-alist)
		(case op
		  ((ref) `((toplevel-ref ,i)))
		  ((set) `((toplevel-set ,i))))
		object-alist))))
         (else
          (error "unknown module var kind" op key)))))


    ((<glil-label> label)
     `(,(format #f "~a:" (skip-first label))))

    ((<glil-branch> inst label)
     (let ((label (skip-first label)))
       `(,(case inst
	  ((br)             (format #f "goto ~a;" (skip-first label)))
	  ((br-if)          (format #f "sp-=1; if(scm_is_true (sp[1]))     goto ~a;" (skip-first label)))
	  ((br-if-not)      (format #f "sp-=1; if(scm_is_false(sp[1]))     goto ~a;" (skip-first label)))
	  ((br-if-eq)       (format #f "sp-=2; if(scm_is_eq(sp[1],sp[2]))  goto ~a;" (skip-first label)))
	  ((br-if-not-eq)   (format #f "sp-=2; if(!scm_is_eq(sp[1],sp[2])) goto ~a;" (skip-first label)))
	  ((br-if-null)     (format #f "sp-=1; if(scm_is_null(sp[1]))      goto ~a;" (skip-first label)))
	  ((br-if-not-null) (format #f "sp-=1; if(!scm_is_null(sp[1]))     goto ~a;" (skip-first label)))))))

    ;; nargs is number of stack args to insn. probably should rename.
    ((<glil-call> inst nargs)
     (if (not (instruction? inst))
         (error "Unknown instruction:" inst))
     (let ((pops (instruction-pops inst)))
       (cond ((< pops 0)
	      `(,(case inst
		   ((tail-call)     (format #f "TAIL_CALL(~a)   ;" nargs)) 
		   ((make-closure)  (format #f "MAKE_CLOSURE(~a);" nargs))
		   (else        (error (format #f "instruction ~a not suported to translate to C" inst))))))
             ((= pops nargs)
	     `(
	     ,(case inst
		((not)       "       *sp = scm_from_bool( scm_is_false (*sp)        );")
		((not-not)   "       *sp = scm_from_bool(!scm_is_false (*sp)        );")
		((eq?)       "sp--;  *sp = scm_from_bool( scm_is_eq    (sp[0], sp[1]);")
		((not-eq?)   "sp--;  *sp = scm_from_bool(!scm_is_eq    (sp[0], sp[1]);")
		((null?)     "       *sp = scm_from_bool( scm_is_null  (*sp         );")
		((not-null?) "       *sp = scm_from_bool(!scm_is_null  (*sp         );")
		((eqv?)      (scm-comp-str 'scm_eqv_p))
                ((equal?)    (scm-comp-str 'scm_equal_p))
		((pair?)     "       *sp = scm_from_bool(scm_is_pair(*sp)    ) ;")
		((list?)     "       *sp = scm_from_bool(scm_ilength(*sp) >=0) ;")
		((cons)      "sp--;  *sp = CONS(sp[0],sp[0],sp[1])             ;")
		((car)       "       *sp = SCM_CAR(x)                          ;")
		((cdr)       "       *sp = SCM_CDR(x)                          ;")
		((set_car)   "sp-=2;       SCM_SETCAR(sp[1],sp[2])             ;")
		((set_car)   "sp-=2;       SCM_SETCDR(sp[1],sp[2])             ;")
		((ee?)       "REL(==, scm_num_eq_p)                            ;")
		((lt?)       "REL(< , scm_less_p)                              ;")
		((le?)       "REL(<=, scm_leq_p)                               ;")
		((gt?)       "REL(> , scm_gr_p)                                ;")
		((ge?)       "REL(>=, scm_geq_p)                               ;")
		((add)       "FUNC2(+, scm_sum)                                ;")
		((sub)       "FUNC2(-, scm_diffrence)                          ;")
		((mul)       "sp--; *sp = scm_product  (sp[0],sp[1])           ;")
		((div)       "sp--; *sp = scm_divide   (sp[0],sp[1])           ;")
		((quo)       "sp--; *sp = scm_quotient (sp[0],sp[1])           ;")
		((rem)       "sp--; *sp = scm_remainder(sp[0],sp[1])           ;")
		((mod)       "sp--; *sp = scm_modulo   (sp[0],sp[1])           ;")
		((logand)    "FUNC2(&, scm_logand)                             ;")
		((logior)    "FUNC2(|, scm_logior)                             ;")
		((logxor)    "FUNC2(^, scm_logxor)                             ;")
		((return)    "RETURN;"                                           ) 
		((ash)       "ASH;"                                              )
		((add1)      "ADD1;"                                             )
		((sub1)      "SUB1;"                                             )
		(else        (error (format #f "instruction ~a not suported to translate to C" inst))))))
             (else
              (error "Wrong number of stack arguments to instruction:" inst nargs)))))

    ((<glil-ucall> nargs)
     (error "<glil-ucall> not supported for toc"))

    ((<glil-mv-call> nargs ra)
     (error "<glil-mv-call> not supported for toc"))

    ((<glil-prompt> label escape-only?)
     (error "<glil-prompt> not supported for toc"))))
