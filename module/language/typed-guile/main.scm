(define-module (language typed-guile main)
  #:use-module (srfi srfi-11)
  #:use-module (language prolog typecheck solver)
  #:use-module (language prolog typecheck register)
  #:use-module (language tree-il)
  #:use-module (language prolog umatch-ffi)
  #:use-module (ice-9 pretty-print)
  #:use-module (ice-9 match)
  #:export (tdefine type declare subtype define-tfn))


(define-syntax declare
  (syntax-rules (:)
     ((_ f : l)  (register-fkn-signature 'f f `l))))

(define-syntax subtype
  (syntax-rules (is-a)
    ((_ x is-a y) (add-subtype-relation=> `x `y))))

(define-syntax define-tfn
  (syntax-rules ()
    ((_ def res) (define-parameter-type def res))))

(define (pp X)
  (pk (pretty-print X))
  X)

(define *add-type-information* (make-fluid))
(fluid-set! *add-type-information* #f)

(define-syntax type
  (lambda (x)
    (syntax-case x ()
      ((_ tp code ...)
       (if (fluid-ref *add-type-information*)
           #'(let ((F (lambda () code ...)))
               (type-string 'tp)
               (F))
           #'(begin code ...))))))

(define (prepare-code X)
  (match X
    (('define x code)
     (prepare-code code))

    (('lambda (a ...) 
       ('let ((F ('lambda () code ...)))
         ('begin
           ('type-string ('quote Tp))
           Appl)))
     (match (homogenize-type-symbols Tp)
       (('lambda (t-in ...) t-out)
        (let ((args (map (lambda (t x) (list x ': t)) t-in a)))
          `(lambda ,args : ,t-out ,(prepare-code `(begin ,@code)))))))

    
    (('case-lambda 
       ((A ...) ('let  ((F ('lambda () Code ...)))
                  ('begin
                    ('type-string ('quote Tp))
                    Appl)))
       ...)     
     `(case-lambda 
        ,(map (lambda (Tp a code)
                (match (homogenize-type-symbols Tp)
                  (('lambda (t-in ...) t-out)
                   (let ((args (map (lambda (t x) (list x ': t)) t-in a)))
                     `(lambda ,args : ,t-out 
                              ,(prepare-code `(begin ,@code)))))))
              Tp A Code)))

    (('letrec ((f lam) ...) code ...)
     `(letrec ,f ,(map (lambda (f lam) `(,f ,(prepare-code lam))) f lam)
              ,(prepare-code `(begin ,@code))))          
          

    (('let ((F ('lambda () code)))
       ('begin
         ('type-string ('quote Tp))
         Appl))
     `(type ,Tp ,(prepare-code code)))

    (('begin . l)
     `(begin ,@(map prepare-code l)))
    
    (('let () . code)
     (prepare-code `(begin ,@code)))
    
    (('let ((a v) . l) . code)
     `(let ,a ,(prepare-code v) ,(prepare-code `(let ,l ,@code))))

    (('if P X Y)
     `(if ,(prepare-code P) ,(prepare-code X) ,(prepare-code Y)))

    (('set! A B)
     `(set! ,A ,(prepare-code B)))

    (('quote a) `(quote ,a))

    ((and A ('@@ path sym)) A)

    ((f . l)   `(apply ,(prepare-code f) ,(map prepare-code l)))
    (x x)))


(define (clip x) (cadr x))

(define (un-arg x)
  (match x
    (('lambda ('arg . l) z) `(lambda ,l ,z))
    (x x)))

(define-syntax tdefine 
  (lambda (x)
    (syntax-case x ()
      ((_ (f . a) code ...)
       #'(begin
           (define (f . a) code ...)
           (with-fluids ((*add-type-information* #t))
             (let* ((MCode (tree-il->scheme 
                            (macroexpand 
                             '(define (f . a) code ...))))
                    (SCode (prepare-code MCode)))
               (let ((tp (clip (code->solver SCode))))             
                 (register-fkn-signature 'f f (un-arg tp)))
               'f))))
      ((_ f dat)
       #'(begin
           (define f dat)
           (with-fluids ((*add-type-information* #t))
             (let* ((MCode (tree-il->scheme 
                            (macroexpand 
                             '(define f dat))))
                    (SCode (prepare-code MCode)))
               (let ((tp (clip (code->solver SCode))))             
                 (register-fkn-signature 'f f (un-arg tp)))
               'f)))))))
      


              

    
