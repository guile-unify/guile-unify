;;; TREE-IL umatch stub to GLIL compiler

;; Copyright (C) 2001,2008,2009,2010 Free Software Foundation, Inc.

;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

;;; Code:
(defmacro make-ucompile ()
'(begin

(define (reg-uvars src names vars)
  (emit-bindings src names vars allocation self emit-code)
  (for-each (lambda (v)
              (pmatch (hashq-ref (hashq-ref allocation v) self)
                      ((#t #f . ,n)
                       (make-glil-lexical #t #f 'set n))
                      ((#t #t . ,n)
                       (make-glil-lexical #t #t 'box n))
                      (,loc (error "badness" x loc))))
            (reverse vars)))


(define (set-loc src v)
  (pmatch (hashq-ref (hashq-ref allocation v) self)
          ((#t #f . ,n)
           (emit-code src (make-glil-lexical #t #f 'set n)))
          ((#t #t . ,n)
           (emit-code src (make-glil-lexical #t #t 'box n)))
          (,loc (error "badness" x loc))))


(define (handle-xlet -let-)
  (record-case -let-
               ((<let> src names gensyms body)
                (reg-uvars src names gensyms)
                (match body
                       (($ sequence? s (pat next mode ($ sequence? ss (fi . l))))
                        (let ((L  (make-label)))
                          (comp-push   fi)
                          (comp-push   mode)
                          (emit-branch src 'br L)
                          (with-fluids ((next-fluid L)) (handle-pat pat))
                          (emit-label L)
                          (handle-x src next)
                          (emit-code #f (make-glil-unbind))))))))

(define (handle-pat pat)
  (match pat
     (($ sequence? s (($ let? src names gensyms vals ($ sequence? ss pat))))
      (reg-uvars src names gensyms)
      (handle-pat-pat pat)
      (emit-code #f (make-glil-unbind)))
     (($ application? s x pat)
      (handle-pat-pat pat))))


(define (handle-pat-pat pat)
  (match pat
         ((instr ... code)
          (let f ((is instr))
            (if (pair? is)
                (begin
                  (u-compile (car is))
                  (f (cdr is)))
                (u-compile code))))))
         
                
(define (handle-x src cur)
  (match cur
       (($ sequence? s (pat next))
        (let ((L  (make-label)))
          (emit-branch src 'br L)
          (with-fluids ((next-fluid L)) (handle-pat pat))
          (emit-label L)
          (handle-x src next)))
       (X (comp-tail X))))

;;(use-modules (language prolog umatch))

(define (u-compile tree-il-code)
  ;(pk tree-il-code)
     (match tree-il-code
            (($ sequence? s data) (map u-compile data))

            (($ application? src ($ module-ref? s _ '<gp> _) (code))
             (u-compile code))

            (($ application? src ($ module-ref? s ('language 'prolog 'umatch) '<umatch> _) args)
             (emit-code #f (make-glil-call 'unify-init 0))
             (let f ((args (sequence-exps (car args))))
               (if (const? (car args))
                   (f (cdr args))
                   (handle-xlet (car args)))))

            (($ application? src ($ module-ref? _ _ 'g-call _) (fi f . args))
             (emit-code #f (make-glil-call 'new-frame 0))
             (for-each comp-push (cons f args))
             (emit-code #f (make-glil-call 'u-tail-call (length args)))
             (comp-push fi)
             (emit-code #f (make-glil-call 'return 1)))
             
            (($ application? src ($ module-ref? _ _ 'g-var! _) ())
             (emit-code #f (make-glil-call 'unify-var! 0))
             (maybe-emit-return))             

            (($ application? src ($ module-ref? _ _ 'g-unify! _) (X Y))
             (comp-push X)
             (comp-push Y)
             (emit-code #f (make-glil-unify 0))
             (maybe-emit-return))

            (($ application? src ($ module-ref? _ _ 'g-unify-raw! _) (X Y))
             (comp-push X)
             (comp-push Y)
             (emit-code #f (make-glil-unify 1))
             (maybe-emit-return))


            (($ application? src ($ module-ref? _ _ 'g-set! _) (var val))
             (comp-push var)
             (comp-push val)
             (emit-code #f (make-glil-call 'unify-set! 0)))

            (($ application? src ($ module-ref? _ _ 'g-context _) ())
             (emit-code #f (make-glil-call 'unify-alloc-context 0)))
            (($ application? src ($ module-ref? _ _ 'g-modded _) ())
             (emit-code #f (make-glil-call 'unify-modded 0)))

            (($ application? src ($ module-ref? _ _ 'gp-prompt _) (frame-tagg))
             (emit-code #f (make-glil-call 'unify-prompt 0))
             (comp-push frame-tagg))
            
            (($ application? src ($ module-ref? _ _ 'gp-unprompt _) (prompt))
             (comp-push prompt)
             (emit-code #f (make-glil-call 'unify-unprompt 0)))
            
            (($ application? src ($ module-ref? _ _ 'gp-next _) (frame-tag))
             (emit-code #f (make-glil-call 'unify-next 0))
             (comp-push frame-tag)
             (emit-branch src 'br (fluid-ref next-fluid)))
            
            (($ application? src ($ module-ref? _ _ '<u-box> _) (-let- b))
             (let f ((args (sequence-exps -let-)))
               (if (const? (car args))
                   (f (cdr args))
                (record-case (car args)
                             ((<let> src names gensyms body)
                              (reg-uvars src names gensyms)
                              (u-compile body)))))
             (u-compile b))
	 
            (($ application? src ($ module-ref? _ _ '<mode> _)  (m))
             (emit-code #f (make-glil-mode (const-exp m))))

            (($ application? src ($ module-ref? _ _ '<u-init> _) 
                (mode fr-match ($ const? s label)))
             (emit-code #f (make-glil-call 'unify-init 0))
             (comp-push mode     )
             (comp-push fr-match )
             (emit-branch src 'br label))

            (($ application? src ($ module-ref? _ _ '<code> _) (code))
             (comp-tail code))
                
            (($ application? src ($ module-ref? _ _ '<u-label> _) 
                (($ const? s label)))
             (emit-label label))
            
            (($ application? src ($ module-ref? _ _ 'gp-atom _) 
                (X))
             (comp-push X)
             (emit-code #f (make-glil-call 'unify-atom 0))
             (maybe-emit-return))
           
            (($ application? src ($ module-ref? _ _ 'gp-scm _) 
                (X))
             (comp-push X)
             (emit-code #f (make-glil-call 'unify-scm 0))
             (maybe-emit-return))
            
            (($ application? src ($ module-ref? _ _ 'gp-cons _) 
                (Car Cdr))             
             (comp-push Car)
             (comp-push Cdr)
             (emit-code #f (make-glil-call 'gp-cons 0))
             (maybe-emit-return))
            
            (($ application? src ($ module-ref? _ _ '<u-goto> _) 
                (($ const? s label)))
             (emit-branch src 'br label))
    
            (($ application? src ($ module-ref? _ _ '<u-load> _) args)
             (comp-push (car args)))
	
            (($ application? src ($ module-ref? _ _ '<u-skip> _) args)
             (emit-code #f (make-glil-call 'drop 1)))

            (($ application? src ($ module-ref? _ _ '<u-dup> _) args)
             (emit-code #f (make-glil-call 'dup 0)))

            (($ application? src ($ module-ref? _ _ '<u-mute> _) args)
             'ok)

            (($ application? src ($ module-ref? _ _ '<u-eq> _) (data))
             (comp-push data)
             (emit-code #f (make-glil-call 'unify-eq 0)))
    
            (($ application? src ($ module-ref? _ _ '<u-insert> _) (data))
             (comp-push data)
             (emit-code #f (make-glil-call 'unify-insert 0)))

            (($ application? src ($ module-ref? _ _ '<uu-insert> _) (data))
             (comp-push data)
             (emit-code #f (make-glil-call 'unify-insert-raw 0)))

            
            (($ application? src ($ module-ref? _ _ '<u-nop> _) ()) #f)
            (($ application? src ($ module-ref? _ _ '<u-f?> _)  ())
             (emit-code #f (make-glil-call 'unify-f? 0)))

            (($ application? src ($ module-ref? _ _ '<u-cons> _) args)
             (emit-code #f (make-glil-call 'unify-cons 0)))

            (($ application? src ($ module-ref? _ _ '<u-pop-ref> _) 
                (($ lexical-ref? src1 name1 g1)
                 ($ lexical-ref? src2 name2 g2)
                 code))
             (set-loc src1 g1)
             (set-loc src2 g2)
             (u-compile code))


            (($ application? src ($ module-ref? _ _ '<u-and> _) (a b))
             (emit-code #f (make-glil-call 'dup 0))
             (u-compile a)
             (u-compile b))
	
            #;
            (($ application? src ($ module-ref? _ _ '<?> _) args)
             (cond ((and (module-ref? _ _ 'var? _))
                    (set! args (cdr args))
                    (emit-code #f (make-glil-call 'unify-val 0))
                    (comp-push (make-const s '<gp>))
                    (emit-code #f (make-glil-call 'eq? 0))
                    (emit-code #f (make-glil-call 'unify-ifnext 0)))
                   (#t
                    (if (member (const-exp (car args)) '(+ -))
                        (emit-code #f (make-glil-call 'unify-val 0)))
                    (set! args (cdr args))
                    (record-case (car (sequence-exps (car args)))
                                 ((<let> src names gensyms body)
                                  (reg-uvars src names gensyms)		
                                  (set-loc src (car gensyms))
                                  (comp-push body)
                                  (emit-code #f (make-glil-call 'unify-ifnext 0)))))))
         
            (($ application? src ($ module-ref? _ _ '<match> _) args)
             (record-case (car (sequence-exps (car args)))
                          ((<let> src names gensyms body)
                           (reg-uvars src names gensyms)		
                           (set-loc src (car gensyms))
                           (u-compile body))))
    
            (($ application? src ($ module-ref? _ _ '<u-push> _) (data))
             (comp-push data))
         
            (($ application? src ($ module-ref? _ _ '<u-base> _) (match-fr))
             (emit-code #f (make-glil-call 'unify-base 0))
             (comp-push match-fr))
	 

            (($ application? src ($ module-ref? _ _ '<u-var> _) 
                (($ lexical-ref? s name gensym)))
             (pmatch (hashq-ref (hashq-ref allocation gensym) self)
                     ((#t #f . ,n)
                      (emit-code src (make-glil-lexical #t #f 'set n)))
                     ((#t #t . ,n)
                      (emit-code src (make-glil-lexical #t #t 'box n)))
                     (,loc (error "badness" x loc))))

            (Code (comp-push Code))))))
