(define-module (language tree-il match-tree-il)
  #:use-module (ice-9 match-phd)
  #:use-module (language tree-il)
  #:use-module (ice-9 pretty-print)
  #:export     (match-tree-il tree-il-struct? tree-il->tree pp-expand))

(make-phd-matcher match-tree-il
     ( (tree-il-car tree-il-cdr tree-il-pair? tree-il-null? tree-il-equal?)
       (  (+ (tree-il-car tree-il-cdr tree-il-pair? tree-il-null? tree-il-equal?))
          (- ( car  cdr  pair?  null?  equal?)))))

(define (tree-il-car x)
  (match x
         ((x . l)                  x               )
         ((? void?            x)  '<void>          )
         ((? const?           x)  '<const>         )
         ((? primitive-ref?   x)  '<primitive-ref> )
         ((? lexical-ref?     x)  '<lexical-ref>   )
         ((? lexical-set?     x)  '<lexical-set>   )
         ((? module-ref?      x)  '<module-ref>    )
         ((? module-set?      x)  '<module-set>    )
         ((? toplevel-ref?    x)  '<toplevel-ref>  )
         ((? toplevel-set?    x)  '<toplevel-set>  )
         ((? toplevel-define? x)  '<toplevel-define>)
         ((? conditional?     x)  '<conditional>   )
         ((? application?     x)  '<application>   )
         ((? sequence?        x)  '<sequence>      )
         ((? lambda?          x)  '<lambda>        )
         ((? lambda-case?     x)  '<lambda-case>   ) 
         ((? let?             x)  '<let>           )
         ((? letrec?          x)  '<letrec>        )
         ((? fix?             x)  '<fix>           )
         ((? let-values?      x)  '<let-values>    )
         ((? dynwind?         x)  '<dynwind>       )
         ((? dynlet?          x)  '<dynlet>        )
         ((? dynref?          x)  '<dynref>        )
         ((? dynset?          x)  '<dynset>        )
         ((? prompt?          x)  '<prompt>        )
         ((? abort?           x)  '<abort>         )))

(define (tree-il-cdr x)
  (match x
         ((x . l)          l)
         (($ void?            x        )  `(,x            ))
         (($ const?           x y      )  `(,x ,y         ))
         (($ primitive-ref?   x y z    )  `(,x ,y ,z      ))
         (($ lexical-ref?     x y z    )  `(,x ,y ,z      ))
         (($ lexical-set?     x y z w  )  `(,x ,y ,z ,w   ))
         (($ module-ref?      x y z w  )  `(,x ,y ,z ,w   ))
         (($ module-set?      x y z w v)  `(,x ,y ,z ,w ,v))
         (($ toplevel-ref?    x y      )  `(,x ,y         ))
         (($ toplevel-set?    x y z    )  `(,x ,y ,z      ))
         (($ toplevel-define? x y z    )  `(,x ,y ,z      ))
         (($ conditional?     x y z w  )  `(,x ,y ,z ,w   ))
         (($ application?     x y z    )  `(,x ,y ,z      ))
         (($ sequence?        x y      )  `(,x ,y         ))
         (($ lambda?          x y z    )  `(,x ,y ,z      ))
         (($ lambda-case?     x y z w v u m n o) `(,x ,y ,z ,w ,v ,u ,m ,n ,o))
         (($ let?             x y z w v)  `(,x ,y ,z ,w ,v))
         (($ letrec?          x y z w v u)  `(,x ,y ,z ,w ,v ,u))
         (($ fix?             x y z w v u)  `(,x ,y ,z ,w ,v ,u))
         (($ let-values?      x y z    )  `(,x ,y ,z      ))
         (($ dynwind?         x y z w  )  `(,x ,y ,z ,w   ))
         (($ dynlet?          x y z w  )  `(,x ,y ,z ,w   ))
         (($ dynref?          x y      )  `(,x ,y         ))
         (($ dynset?          x y z    )  `(,x ,y ,z      ))
         (($ prompt?          x y z w  )  `(,x ,y ,z ,w   ))
         (($ abort?           x y z w  )  `(,x ,y ,z ,w   ))))

(define *tree-il-struct-predicates*  (list void?
                                      const?
                                      primitive-ref?
                                      lexical-ref?
                                      lexical-set?
                                      module-ref?
                                      module-set?
                                      toplevel-ref?
                                      toplevel-set?
                                      toplevel-define?
                                      conditional?
                                      application?
                                      sequence?
                                      lambda?
                                      lambda-case?
                                      let?
                                      letrec?
                                      fix?
                                      let-values?
                                      dynwind?
                                      dynlet?
                                      dynref?
                                      dynset?
                                      prompt?
                                      abort?))


(define (tree-il-struct? x)
  (let loop ((pred *tree-il-struct-predicates*))
    (if (pair? pred)
        (if ((car pred) x)
            #t
            (loop (cdr pred)))
        #f)))

(define (tree-il-pair?  x)   (or (pair? x) (tree-il-struct? x)))
(define (tree-il-null?  x)   (null?  x))
(define (tree-il-equal? x y) (equal? x y))

(define (tree-il->tree x)
  (define (f x)
    (match-tree-il x 
                   ((x . l) (cons (f x) (f l)))
                   (f x)))
  (f x))


(define (pp-expand x)
  (pretty-print (tree-il->tree (macroexpand x))))







  
