#include "header.h"

SCM scm_num18706;

SCM scm_num18416;


SCM validate__int (SCM x) {
     {
        SCM ret32788;
         {
            
            if (!(SCM_BOOL_F == scm_leq_p(x, scm_num18416))
                  && !(SCM_BOOL_F == scm_geq_p(x, scm_num18706))) {
                ret32788 = SCM_BOOL_T;
            } else {
                ret32788 = ERROR("qp problem sizes is not a valid integer");
            }
        }
        return ret32788;
    }
}

SCM validate__num (SCM x) {
     {
        SCM ret32791;
         {
            
            if (!(SCM_BOOL_F == scm_number_p(x))) {
                ret32791 = SCM_BOOL_T;
            } else {
                ret32791 = ERROR("qp data element are not a number");
            }
        }
        return ret32791;
    }
}

void set__vector (gsl_vector * d___gQ, SCM Q, int n) {
     {
        
         {
            int i32794;
            i32794 = 0;
             {
                SCM Q32795;
                Q32795 = Q;
                 {
                    
                  loop__i32797:
                     {
                        
                        if (i32794 < n) {
                             {
                                SCM work32798[2];
                                
                                 {
                                    SCM w1989032799;
                                    w1989032799 = Q32795;
                                     {
                                        
                                        
                                        
                                        while (1) {
                                             {
                                                
                                                 {
                                                    
                                                    
                                                    
                                                     {
                                                        
                                                        AREF(work32798, 0)
                                                          = w1989032799;
                                                        
                                                         {
                                                            int pred32801;
                                                            pred32801
                                                              = SCM_CONSP(AREF(work32798,
                                                                               0));
                                                            if (pred32801) {
                                                                 {
                                                                    
                                                                    AREF(work32798,
                                                                         1)
                                                                      = SCM_CAR(AREF(work32798,
                                                                                     0));
                                                                    
                                                                    AREF(work32798,
                                                                         0)
                                                                      = SCM_CDR(AREF(work32798,
                                                                                     0));
                                                                    
                                                                     {
                                                                        SCM Q32800;
                                                                        Q32800
                                                                          = AREF(work32798,
                                                                                 1);
                                                                         {
                                                                            
                                                                            
                                                                            
                                                                             {
                                                                                SCM l32802;
                                                                                l32802
                                                                                  = AREF(work32798,
                                                                                         0);
                                                                                 {
                                                                                    
                                                                                    
                                                                                    
                                                                                     {
                                                                                         {
                                                                                            
                                                                                            validate__num(Q32800);
                                                                                            
                                                                                            gsl_vector_set(d___gQ,
                                                                                                           i32794,
                                                                                                           scm_to_double(Q32800));
                                                                                            
                                                                                             {
                                                                                                i32794
                                                                                                  = i32794
                                                                                                      + 1;
                                                                                                Q32795
                                                                                                  = l32802;
                                                                                                goto loop__i32797;
                                                                                            }
                                                                                        }
                                                                                        break;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                goto next19894;
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                                 {
                                                    
                                                    
                                                  next19894:
                                                    
                                                     {
                                                        
                                                        AREF(work32798, 0)
                                                          = w1989032799;
                                                        
                                                         {
                                                            ERROR("number of V elements does not match spec");
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

void set__matrix (gsl_matrix * d___gQ, SCM Q, int m, int n) {
     {
        
         {
            int i32805;
            i32805 = 0;
             {
                SCM Q32806;
                Q32806 = Q;
                 {
                    
                  loop__i32808:
                     {
                        
                        if (i32805 < m) {
                             {
                                SCM work32809[2];
                                
                                 {
                                    SCM w2097532810;
                                    w2097532810 = Q32806;
                                     {
                                        
                                        
                                        
                                        while (1) {
                                             {
                                                
                                                 {
                                                    
                                                    
                                                    
                                                     {
                                                        
                                                        AREF(work32809, 0)
                                                          = w2097532810;
                                                        
                                                         {
                                                            int pred32812;
                                                            pred32812
                                                              = SCM_CONSP(AREF(work32809,
                                                                               0));
                                                            if (pred32812) {
                                                                 {
                                                                    
                                                                    AREF(work32809,
                                                                         1)
                                                                      = SCM_CAR(AREF(work32809,
                                                                                     0));
                                                                    
                                                                    AREF(work32809,
                                                                         0)
                                                                      = SCM_CDR(AREF(work32809,
                                                                                     0));
                                                                    
                                                                     {
                                                                        SCM Q32811;
                                                                        Q32811
                                                                          = AREF(work32809,
                                                                                 1);
                                                                         {
                                                                            
                                                                            
                                                                            
                                                                             {
                                                                                SCM l32813;
                                                                                l32813
                                                                                  = AREF(work32809,
                                                                                         0);
                                                                                 {
                                                                                    
                                                                                    
                                                                                    
                                                                                     {
                                                                                         {
                                                                                            int j32814;
                                                                                            j32814
                                                                                              = 0;
                                                                                             {
                                                                                                SCM Q32815;
                                                                                                Q32815
                                                                                                  = Q32811;
                                                                                                 {
                                                                                                    
                                                                                                  loop__j32816:
                                                                                                     {
                                                                                                        
                                                                                                        if (j32814
                                                                                                              < n) {
                                                                                                             {
                                                                                                                SCM work32817[2];
                                                                                                                
                                                                                                                 {
                                                                                                                    SCM w2136532818;
                                                                                                                    w2136532818
                                                                                                                      = Q32815;
                                                                                                                     {
                                                                                                                        
                                                                                                                        
                                                                                                                        
                                                                                                                        while (1) {
                                                                                                                             {
                                                                                                                                
                                                                                                                                 {
                                                                                                                                    
                                                                                                                                    
                                                                                                                                    
                                                                                                                                     {
                                                                                                                                        
                                                                                                                                        AREF(work32817,
                                                                                                                                             0)
                                                                                                                                          = w2136532818;
                                                                                                                                        
                                                                                                                                         {
                                                                                                                                            int pred32820;
                                                                                                                                            pred32820
                                                                                                                                              = SCM_CONSP(AREF(work32817,
                                                                                                                                                               0));
                                                                                                                                            if (pred32820) {
                                                                                                                                                 {
                                                                                                                                                    
                                                                                                                                                    AREF(work32817,
                                                                                                                                                         1)
                                                                                                                                                      = SCM_CAR(AREF(work32817,
                                                                                                                                                                     0));
                                                                                                                                                    
                                                                                                                                                    AREF(work32817,
                                                                                                                                                         0)
                                                                                                                                                      = SCM_CDR(AREF(work32817,
                                                                                                                                                                     0));
                                                                                                                                                    
                                                                                                                                                     {
                                                                                                                                                        SCM Q32819;
                                                                                                                                                        Q32819
                                                                                                                                                          = AREF(work32817,
                                                                                                                                                                 1);
                                                                                                                                                         {
                                                                                                                                                            
                                                                                                                                                            
                                                                                                                                                            
                                                                                                                                                             {
                                                                                                                                                                SCM l32821;
                                                                                                                                                                l32821
                                                                                                                                                                  = AREF(work32817,
                                                                                                                                                                         0);
                                                                                                                                                                 {
                                                                                                                                                                    
                                                                                                                                                                    
                                                                                                                                                                    
                                                                                                                                                                     {
                                                                                                                                                                         {
                                                                                                                                                                            
                                                                                                                                                                            validate__num(Q32819);
                                                                                                                                                                            
                                                                                                                                                                            gsl_matrix_set(d___gQ,
                                                                                                                                                                                           i32805,
                                                                                                                                                                                           j32814,
                                                                                                                                                                                           scm_to_double(Q32819));
                                                                                                                                                                            
                                                                                                                                                                             {
                                                                                                                                                                                j32814
                                                                                                                                                                                  = j32814
                                                                                                                                                                                      + 1;
                                                                                                                                                                                Q32815
                                                                                                                                                                                  = l32821;
                                                                                                                                                                                goto loop__j32816;
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                        break;
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            } else {
                                                                                                                                                goto next21369;
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                
                                                                                                                                 {
                                                                                                                                    
                                                                                                                                    
                                                                                                                                  next21369:
                                                                                                                                    
                                                                                                                                     {
                                                                                                                                        
                                                                                                                                        AREF(work32817,
                                                                                                                                             0)
                                                                                                                                          = w2136532818;
                                                                                                                                        
                                                                                                                                         {
                                                                                                                                            ERROR("number of M col elements does not match spec");
                                                                                                                                            break;
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        } else {
                                                                                                             {
                                                                                                                i32805
                                                                                                                  = i32805
                                                                                                                      + 1;
                                                                                                                Q32806
                                                                                                                  = l32813;
                                                                                                                goto loop__i32808;
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                        break;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                goto next20979;
                                                            }
                                                        }
                                                    }
                                                }
                                                
                                                 {
                                                    
                                                    
                                                  next20979:
                                                    
                                                     {
                                                        
                                                        AREF(work32809, 0)
                                                          = w2097532810;
                                                        
                                                         {
                                                            ERROR("number of M row elements does not match spec");
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

void fill__in__data (gsl_cqp_data * da, SCM l, int n, int me, int mi) {
     {
        
        da->Q = gsl_matrix_alloc(n, n);
        
        da->q = gsl_vector_alloc(n);
        
        da->A = gsl_matrix_alloc(me, n);
        
        da->b = gsl_vector_alloc(me);
        
        da->C = gsl_matrix_calloc(mi, n);
        
        da->d = gsl_vector_calloc(mi);
        
         {
            SCM work32827[2];
            
             {
                SCM w2371732828;
                w2371732828 = l;
                 {
                    
                    
                    
                    while (1) {
                         {
                            
                             {
                                
                                
                                
                                 {
                                    
                                    AREF(work32827, 0) = w2371732828;
                                    
                                     {
                                        int pred32830;
                                        pred32830
                                          = SCM_CONSP(AREF(work32827, 0));
                                        if (pred32830) {
                                             {
                                                
                                                AREF(work32827, 1)
                                                  = SCM_CAR(AREF(work32827, 0));
                                                
                                                AREF(work32827, 0)
                                                  = SCM_CDR(AREF(work32827, 0));
                                                
                                                 {
                                                    SCM Q32829;
                                                    Q32829
                                                      = AREF(work32827, 1);
                                                     {
                                                        
                                                        
                                                        
                                                         {
                                                            int pred32832;
                                                            pred32832
                                                              = SCM_CONSP(AREF(work32827,
                                                                               0));
                                                            if (pred32832) {
                                                                 {
                                                                    
                                                                    AREF(work32827,
                                                                         1)
                                                                      = SCM_CAR(AREF(work32827,
                                                                                     0));
                                                                    
                                                                    AREF(work32827,
                                                                         0)
                                                                      = SCM_CDR(AREF(work32827,
                                                                                     0));
                                                                    
                                                                     {
                                                                        SCM q32831;
                                                                        q32831
                                                                          = AREF(work32827,
                                                                                 1);
                                                                         {
                                                                            
                                                                            
                                                                            
                                                                             {
                                                                                int pred32834;
                                                                                pred32834
                                                                                  = SCM_CONSP(AREF(work32827,
                                                                                                   0));
                                                                                if (pred32834) {
                                                                                     {
                                                                                        
                                                                                        AREF(work32827,
                                                                                             1)
                                                                                          = SCM_CAR(AREF(work32827,
                                                                                                         0));
                                                                                        
                                                                                        AREF(work32827,
                                                                                             0)
                                                                                          = SCM_CDR(AREF(work32827,
                                                                                                         0));
                                                                                        
                                                                                         {
                                                                                            SCM A32833;
                                                                                            A32833
                                                                                              = AREF(work32827,
                                                                                                     1);
                                                                                             {
                                                                                                
                                                                                                
                                                                                                
                                                                                                 {
                                                                                                    int pred32836;
                                                                                                    pred32836
                                                                                                      = SCM_CONSP(AREF(work32827,
                                                                                                                       0));
                                                                                                    if (pred32836) {
                                                                                                         {
                                                                                                            
                                                                                                            AREF(work32827,
                                                                                                                 1)
                                                                                                              = SCM_CAR(AREF(work32827,
                                                                                                                             0));
                                                                                                            
                                                                                                            AREF(work32827,
                                                                                                                 0)
                                                                                                              = SCM_CDR(AREF(work32827,
                                                                                                                             0));
                                                                                                            
                                                                                                             {
                                                                                                                SCM b32835;
                                                                                                                b32835
                                                                                                                  = AREF(work32827,
                                                                                                                         1);
                                                                                                                 {
                                                                                                                    
                                                                                                                    
                                                                                                                    
                                                                                                                     {
                                                                                                                        int pred32838;
                                                                                                                        pred32838
                                                                                                                          = SCM_CONSP(AREF(work32827,
                                                                                                                                           0));
                                                                                                                        if (pred32838) {
                                                                                                                             {
                                                                                                                                
                                                                                                                                AREF(work32827,
                                                                                                                                     1)
                                                                                                                                  = SCM_CAR(AREF(work32827,
                                                                                                                                                 0));
                                                                                                                                
                                                                                                                                AREF(work32827,
                                                                                                                                     0)
                                                                                                                                  = SCM_CDR(AREF(work32827,
                                                                                                                                                 0));
                                                                                                                                
                                                                                                                                 {
                                                                                                                                    SCM C32837;
                                                                                                                                    C32837
                                                                                                                                      = AREF(work32827,
                                                                                                                                             1);
                                                                                                                                     {
                                                                                                                                        
                                                                                                                                        
                                                                                                                                        
                                                                                                                                         {
                                                                                                                                            int pred32840;
                                                                                                                                            pred32840
                                                                                                                                              = SCM_CONSP(AREF(work32827,
                                                                                                                                                               0));
                                                                                                                                            if (pred32840) {
                                                                                                                                                 {
                                                                                                                                                    
                                                                                                                                                    AREF(work32827,
                                                                                                                                                         1)
                                                                                                                                                      = SCM_CAR(AREF(work32827,
                                                                                                                                                                     0));
                                                                                                                                                    
                                                                                                                                                    AREF(work32827,
                                                                                                                                                         0)
                                                                                                                                                      = SCM_CDR(AREF(work32827,
                                                                                                                                                                     0));
                                                                                                                                                    
                                                                                                                                                     {
                                                                                                                                                        SCM d32839;
                                                                                                                                                        d32839
                                                                                                                                                          = AREF(work32827,
                                                                                                                                                                 1);
                                                                                                                                                         {
                                                                                                                                                            
                                                                                                                                                            
                                                                                                                                                            
                                                                                                                                                            if (SCM_EOL
                                                                                                                                                                  == AREF(work32827,
                                                                                                                                                                          0)) {
                                                                                                                                                                 {
                                                                                                                                                                     {
                                                                                                                                                                        
                                                                                                                                                                        set__matrix((da->Q),
                                                                                                                                                                                    Q32829,
                                                                                                                                                                                    n,
                                                                                                                                                                                    n);
                                                                                                                                                                        
                                                                                                                                                                        set__vector((da->q),
                                                                                                                                                                                    q32831,
                                                                                                                                                                                    n);
                                                                                                                                                                        
                                                                                                                                                                        set__matrix((da->A),
                                                                                                                                                                                    A32833,
                                                                                                                                                                                    me,
                                                                                                                                                                                    n);
                                                                                                                                                                        
                                                                                                                                                                        set__vector((da->b),
                                                                                                                                                                                    b32835,
                                                                                                                                                                                    me);
                                                                                                                                                                        
                                                                                                                                                                        set__matrix((da->C),
                                                                                                                                                                                    C32837,
                                                                                                                                                                                    mi,
                                                                                                                                                                                    n);
                                                                                                                                                                        
                                                                                                                                                                        set__vector((da->d),
                                                                                                                                                                                    d32839,
                                                                                                                                                                                    mi);
                                                                                                                                                                    }
                                                                                                                                                                    break;
                                                                                                                                                                }
                                                                                                                                                            } else {
                                                                                                                                                                goto next23721;
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            } else {
                                                                                                                                                goto next23721;
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                    }
                                                                                                                                }
                                                                                                                            }
                                                                                                                        } else {
                                                                                                                            goto next23721;
                                                                                                                        }
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    } else {
                                                                                                        goto next23721;
                                                                                                    }
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    goto next23721;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                goto next23721;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            goto next23721;
                                        }
                                    }
                                }
                            }
                            
                             {
                                
                                
                              next23721:
                                
                                 {
                                    
                                    AREF(work32827, 0) = w2371732828;
                                    
                                     {
                                        ERROR("wrong number of linalg structures (Q q A b C d)");
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

SCM solve (SCM Problem) {
     {
        SCM ret32841;
         {
            
             {
                SCM work32842[3];
                
                 {
                    SCM w2568332843;
                    w2568332843 = Problem;
                     {
                        
                        
                        
                        while (1) {
                             {
                                
                                 {
                                    
                                    
                                    
                                     {
                                        
                                        AREF(work32842, 0) = w2568332843;
                                        
                                         {
                                            int pred32845;
                                            pred32845
                                              = SCM_CONSP(AREF(work32842, 0));
                                            if (pred32845) {
                                                 {
                                                    
                                                    AREF(work32842, 1)
                                                      = SCM_CAR(AREF(work32842,
                                                                     0));
                                                    
                                                    AREF(work32842, 0)
                                                      = SCM_CDR(AREF(work32842,
                                                                     0));
                                                    
                                                     {
                                                        int pred32846;
                                                        pred32846
                                                          = SCM_CONSP(AREF(work32842,
                                                                           1));
                                                        if (pred32846) {
                                                             {
                                                                
                                                                AREF(work32842,
                                                                     2)
                                                                  = SCM_CAR(AREF(work32842,
                                                                                 1));
                                                                
                                                                AREF(work32842,
                                                                     1)
                                                                  = SCM_CDR(AREF(work32842,
                                                                                 1));
                                                                
                                                                 {
                                                                    SCM n32844;
                                                                    n32844
                                                                      = AREF(work32842,
                                                                             2);
                                                                     {
                                                                        
                                                                        
                                                                        
                                                                         {
                                                                            int pred32848;
                                                                            pred32848
                                                                              = SCM_CONSP(AREF(work32842,
                                                                                               1));
                                                                            if (pred32848) {
                                                                                 {
                                                                                    
                                                                                    AREF(work32842,
                                                                                         2)
                                                                                      = SCM_CAR(AREF(work32842,
                                                                                                     1));
                                                                                    
                                                                                    AREF(work32842,
                                                                                         1)
                                                                                      = SCM_CDR(AREF(work32842,
                                                                                                     1));
                                                                                    
                                                                                     {
                                                                                        SCM me32847;
                                                                                        me32847
                                                                                          = AREF(work32842,
                                                                                                 2);
                                                                                         {
                                                                                            
                                                                                            
                                                                                            
                                                                                             {
                                                                                                int pred32850;
                                                                                                pred32850
                                                                                                  = SCM_CONSP(AREF(work32842,
                                                                                                                   1));
                                                                                                if (pred32850) {
                                                                                                     {
                                                                                                        
                                                                                                        AREF(work32842,
                                                                                                             2)
                                                                                                          = SCM_CAR(AREF(work32842,
                                                                                                                         1));
                                                                                                        
                                                                                                        AREF(work32842,
                                                                                                             1)
                                                                                                          = SCM_CDR(AREF(work32842,
                                                                                                                         1));
                                                                                                        
                                                                                                         {
                                                                                                            SCM mi32849;
                                                                                                            mi32849
                                                                                                              = AREF(work32842,
                                                                                                                     2);
                                                                                                             {
                                                                                                                
                                                                                                                
                                                                                                                
                                                                                                                if (SCM_EOL
                                                                                                                      == AREF(work32842,
                                                                                                                              1)) {
                                                                                                                     {
                                                                                                                        SCM l32851;
                                                                                                                        l32851
                                                                                                                          = AREF(work32842,
                                                                                                                                 0);
                                                                                                                         {
                                                                                                                            
                                                                                                                            
                                                                                                                            
                                                                                                                             {
                                                                                                                                 {
                                                                                                                                    
                                                                                                                                    validate__int(n32844);
                                                                                                                                    
                                                                                                                                    validate__int(me32847);
                                                                                                                                    
                                                                                                                                    validate__int(mi32849);
                                                                                                                                    
                                                                                                                                     {
                                                                                                                                        size_t max__iter32852;
                                                                                                                                        int status32853;
                                                                                                                                        gsl_cqpminimizer_type * T32854;
                                                                                                                                        gsl_cqpminimizer * s32855;
                                                                                                                                        gsl_cqp_data data32856;
                                                                                                                                        SCM ret32857;
                                                                                                                                        int n__i32858;
                                                                                                                                        int me__i32859;
                                                                                                                                        int mi__i32860;
                                                                                                                                        max__iter32852
                                                                                                                                          = 1000;
                                                                                                                                        
                                                                                                                                        
                                                                                                                                        
                                                                                                                                        
                                                                                                                                        
                                                                                                                                        n__i32858
                                                                                                                                          = scm_to_int(n32844);
                                                                                                                                        me__i32859
                                                                                                                                          = scm_to_int(me32847);
                                                                                                                                        mi__i32860
                                                                                                                                          = scm_to_int(mi32849);
                                                                                                                                        
                                                                                                                                        
                                                                                                                                        
                                                                                                                                        T32854
                                                                                                                                          = gsl_cqpminimizer_mg_pdip;
                                                                                                                                        
                                                                                                                                        s32855
                                                                                                                                          = gsl_cqpminimizer_alloc(T32854,
                                                                                                                                                                   n__i32858,
                                                                                                                                                                   me__i32859,
                                                                                                                                                                   mi__i32860);
                                                                                                                                        
                                                                                                                                        fill__in__data(&(data32856),
                                                                                                                                                       l32851,
                                                                                                                                                       n__i32858,
                                                                                                                                                       me__i32859,
                                                                                                                                                       mi__i32860);
                                                                                                                                        
                                                                                                                                        status32853
                                                                                                                                          = gsl_cqpminimizer_set(s32855,
                                                                                                                                                                 &(data32856));
                                                                                                                                        
                                                                                                                                         {
                                                                                                                                            int i32861;
                                                                                                                                            i32861
                                                                                                                                              = 1;
                                                                                                                                             {
                                                                                                                                                
                                                                                                                                              loop32862:
                                                                                                                                                 {
                                                                                                                                                    
                                                                                                                                                    status32853
                                                                                                                                                      = gsl_cqpminimizer_iterate(s32855);
                                                                                                                                                }
                                                                                                                                                 {
                                                                                                                                                    
                                                                                                                                                    status32853
                                                                                                                                                      = gsl_cqpminimizer_test_convergence(s32855,
                                                                                                                                                                                          1.0e-10,
                                                                                                                                                                                          1.0e-10);
                                                                                                                                                }
                                                                                                                                                 {
                                                                                                                                                    
                                                                                                                                                    if (status32853
                                                                                                                                                          == GSL_SUCCESS) {
                                                                                                                                                         {
                                                                                                                                                            SCM a32875;
                                                                                                                                                            SCM a32876;
                                                                                                                                                             {
                                                                                                                                                                int j32863;
                                                                                                                                                                j32863
                                                                                                                                                                  = 0;
                                                                                                                                                                 {
                                                                                                                                                                    SCM ret32864;
                                                                                                                                                                    ret32864
                                                                                                                                                                      = SCM_EOL;
                                                                                                                                                                     {
                                                                                                                                                                        
                                                                                                                                                                      pr__loop32877:
                                                                                                                                                                         {
                                                                                                                                                                            
                                                                                                                                                                            if (j32863
                                                                                                                                                                                  < gsl_cqpminimizer_x(s32855)->size) {
                                                                                                                                                                                 {
                                                                                                                                                                                    SCM res32878;
                                                                                                                                                                                     {
                                                                                                                                                                                        SCM a32879;
                                                                                                                                                                                        SCM a32880;
                                                                                                                                                                                        a32879
                                                                                                                                                                                          = scm_from_double(gsl_vector_get(gsl_cqpminimizer_x(s32855),
                                                                                                                                                                                                                           j32863));
                                                                                                                                                                                        a32880
                                                                                                                                                                                          = ret32864;
                                                                                                                                                                                        res32878
                                                                                                                                                                                          = scm_cons(a32879,
                                                                                                                                                                                                     a32880);
                                                                                                                                                                                    }
                                                                                                                                                                                    
                                                                                                                                                                                    
                                                                                                                                                                                    
                                                                                                                                                                                     {
                                                                                                                                                                                        j32863
                                                                                                                                                                                          = j32863
                                                                                                                                                                                              + 1;
                                                                                                                                                                                        ret32864
                                                                                                                                                                                          = res32878;
                                                                                                                                                                                        goto pr__loop32877;
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            } else {
                                                                                                                                                                                a32875
                                                                                                                                                                                  = scm_reverse(ret32864);
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                            }
                                                                                                                                                             {
                                                                                                                                                                SCM a32887;
                                                                                                                                                                SCM a32888;
                                                                                                                                                                 {
                                                                                                                                                                    int j32865;
                                                                                                                                                                    j32865
                                                                                                                                                                      = 0;
                                                                                                                                                                     {
                                                                                                                                                                        SCM ret32866;
                                                                                                                                                                        ret32866
                                                                                                                                                                          = SCM_EOL;
                                                                                                                                                                         {
                                                                                                                                                                            
                                                                                                                                                                          pr__loop32889:
                                                                                                                                                                             {
                                                                                                                                                                                
                                                                                                                                                                                if (j32865
                                                                                                                                                                                      < gsl_cqpminimizer_lm_eq(s32855)->size) {
                                                                                                                                                                                     {
                                                                                                                                                                                        SCM res32890;
                                                                                                                                                                                         {
                                                                                                                                                                                            SCM a32891;
                                                                                                                                                                                            SCM a32892;
                                                                                                                                                                                            a32891
                                                                                                                                                                                              = scm_from_double(gsl_vector_get(gsl_cqpminimizer_lm_eq(s32855),
                                                                                                                                                                                                                               j32865));
                                                                                                                                                                                            a32892
                                                                                                                                                                                              = ret32866;
                                                                                                                                                                                            res32890
                                                                                                                                                                                              = scm_cons(a32891,
                                                                                                                                                                                                         a32892);
                                                                                                                                                                                        }
                                                                                                                                                                                        
                                                                                                                                                                                        
                                                                                                                                                                                        
                                                                                                                                                                                         {
                                                                                                                                                                                            j32865
                                                                                                                                                                                              = j32865
                                                                                                                                                                                                  + 1;
                                                                                                                                                                                            ret32866
                                                                                                                                                                                              = res32890;
                                                                                                                                                                                            goto pr__loop32889;
                                                                                                                                                                                        }
                                                                                                                                                                                    }
                                                                                                                                                                                } else {
                                                                                                                                                                                    a32887
                                                                                                                                                                                      = scm_reverse(ret32866);
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                }
                                                                                                                                                                 {
                                                                                                                                                                    SCM a32897;
                                                                                                                                                                    SCM a32898;
                                                                                                                                                                     {
                                                                                                                                                                        int j32867;
                                                                                                                                                                        j32867
                                                                                                                                                                          = 0;
                                                                                                                                                                         {
                                                                                                                                                                            SCM ret32868;
                                                                                                                                                                            ret32868
                                                                                                                                                                              = SCM_EOL;
                                                                                                                                                                             {
                                                                                                                                                                                
                                                                                                                                                                              pr__loop32899:
                                                                                                                                                                                 {
                                                                                                                                                                                    
                                                                                                                                                                                    if (j32867
                                                                                                                                                                                          < gsl_cqpminimizer_lm_ineq(s32855)->size) {
                                                                                                                                                                                         {
                                                                                                                                                                                            SCM res32900;
                                                                                                                                                                                             {
                                                                                                                                                                                                SCM a32901;
                                                                                                                                                                                                SCM a32902;
                                                                                                                                                                                                a32901
                                                                                                                                                                                                  = scm_from_double(gsl_vector_get(gsl_cqpminimizer_lm_ineq(s32855),
                                                                                                                                                                                                                                   j32867));
                                                                                                                                                                                                a32902
                                                                                                                                                                                                  = ret32868;
                                                                                                                                                                                                res32900
                                                                                                                                                                                                  = scm_cons(a32901,
                                                                                                                                                                                                             a32902);
                                                                                                                                                                                            }
                                                                                                                                                                                            
                                                                                                                                                                                            
                                                                                                                                                                                            
                                                                                                                                                                                             {
                                                                                                                                                                                                j32867
                                                                                                                                                                                                  = j32867
                                                                                                                                                                                                      + 1;
                                                                                                                                                                                                ret32868
                                                                                                                                                                                                  = res32900;
                                                                                                                                                                                                goto pr__loop32899;
                                                                                                                                                                                            }
                                                                                                                                                                                        }
                                                                                                                                                                                    } else {
                                                                                                                                                                                        a32897
                                                                                                                                                                                          = scm_reverse(ret32868);
                                                                                                                                                                                    }
                                                                                                                                                                                }
                                                                                                                                                                            }
                                                                                                                                                                        }
                                                                                                                                                                    }
                                                                                                                                                                    a32898
                                                                                                                                                                      = SCM_EOL;
                                                                                                                                                                    a32888
                                                                                                                                                                      = scm_cons(a32897,
                                                                                                                                                                                 a32898);
                                                                                                                                                                }
                                                                                                                                                                a32876
                                                                                                                                                                  = scm_cons(a32887,
                                                                                                                                                                             a32888);
                                                                                                                                                            }
                                                                                                                                                            ret32857
                                                                                                                                                              = scm_cons(a32875,
                                                                                                                                                                         a32876);
                                                                                                                                                        }
                                                                                                                                                    } else {
                                                                                                                                                        if (status32853
                                                                                                                                                              == GSL_CONTINUE
                                                                                                                                                              && i32861
                                                                                                                                                                   <= max__iter32852) {
                                                                                                                                                             {
                                                                                                                                                                i32861
                                                                                                                                                                  = i32861
                                                                                                                                                                      + 1;
                                                                                                                                                                goto loop32862;
                                                                                                                                                            }
                                                                                                                                                        }
                                                                                                                                                    }
                                                                                                                                                }
                                                                                                                                            }
                                                                                                                                        }
                                                                                                                                        
                                                                                                                                        gsl_cqpminimizer_free(s32855);
                                                                                                                                        
                                                                                                                                        ret32841
                                                                                                                                          = ret32857;
                                                                                                                                    }
                                                                                                                                }
                                                                                                                                break;
                                                                                                                            }
                                                                                                                        }
                                                                                                                    }
                                                                                                                } else {
                                                                                                                    goto next25687;
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                } else {
                                                                                                    goto next25687;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            } else {
                                                                                goto next25687;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            goto next25687;
                                                        }
                                                    }
                                                }
                                            } else {
                                                goto next25687;
                                            }
                                        }
                                    }
                                }
                                
                                 {
                                    
                                    
                                  next25687:
                                    
                                     {
                                        
                                        AREF(work32842, 0) = w2568332843;
                                        
                                         {
                                            ret32841
                                              = ERROR("wrong problem description ((n me mi) . l)");
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return ret32841;
    }
}

void init () {
     {
        
         {
            scm_num18706 = scm_c_locale_stringn_to_number("0", 1, 10);
            scm_num18416
              = scm_c_locale_stringn_to_number("1000000000", 10, 10);
            scm_c_define_gsubr("solve", 1, 0, 0, solve);
            
        }
    }
}

