/***************************************************************************
 *   Copyright (C) 2004 by Ewgenij Hbner                                  *
 *   huebner@uni-trier.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#include "test_examples.h"

int
betts(gsl_cqp_data * betts_cqp)
{	
	betts_cqp->Q = gsl_matrix_calloc(2, 2);
	betts_cqp->q = gsl_vector_calloc(2);
	
			
	betts_cqp->A = gsl_matrix_alloc(1,2);
	betts_cqp->b = gsl_vector_alloc(1);
	betts_cqp->C = gsl_matrix_calloc(4, 2);
	betts_cqp->d = gsl_vector_calloc(4);
	
	/* Q */
	gsl_matrix_set(betts_cqp->Q,0,0,0.02); gsl_matrix_set(betts_cqp->Q,1,1,2.0);
	/* q */
	/*gsl_vector_set(betts_cqp->q,0,0.0); gsl_vector_set(betts_cqp->q,1,0.0);*/
	/* A */
	gsl_matrix_set(betts_cqp->A,0,0,10.0); gsl_matrix_set(betts_cqp->A,0,1,-1.0);
	/* b */
	gsl_vector_set(betts_cqp->b,0,20.0);
	/* C */
	gsl_matrix_set(betts_cqp->C,0,0,1.0); gsl_matrix_set(betts_cqp->C,1,1,1.0);
	gsl_matrix_set(betts_cqp->C,2,0,-1.0); gsl_matrix_set(betts_cqp->C,3,1,-11.0);
	/* d */
	gsl_vector_set(betts_cqp->d,0,2.0); gsl_vector_set(betts_cqp->d,1,-50.0);
	gsl_vector_set(betts_cqp->d,2,-50.0); gsl_vector_set(betts_cqp->d,3,-50.0);
	
	
	return GSL_SUCCESS;
}

