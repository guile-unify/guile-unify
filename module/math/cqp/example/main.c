
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#include <time.h>

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

/*#include "gsl_cqp.h"*/
#include "test_exampels.h"

typedef struct
{
	const char * name;
	size_t n;
	size_t me;
	size_t mi;
	gsl_cqp_data *cqp;
	double opt_value;
}
test_problem;

int test_problems_init(test_problem *tp);
int test_problems_free(test_problem *tp, size_t n);

int solve(test_problem *tp);

int main(void)
{	
	const size_t n_pr = 3;
		
	int status;
	
	size_t i;
	
	test_problem tp[n_pr];
	
	/*  Initialize test problems */
	status = test_problems_init(tp);
	
	for(i=0; i< n_pr; i++)
		status = solve(&tp[i]);
	
	status = test_problems_free(tp, n_pr);
	
	printf("\nTime: %.6f (Seconds)\n",(clock()/(double)CLOCKS_PER_SEC));
	
	return EXIT_SUCCESS;
	
	
	
}	


int test_problems_init(test_problem *tp)
{
	int status;
	
	size_t pn =0;
	
	gsl_cqp_data *cqp_data; 
	
	
	/* Goulds's problem */
	cqp_data = malloc(sizeof(gsl_cqp_data));
	status = gould(cqp_data);
	
	tp[pn].name 		= "Goulds's problem";
	tp[pn].n 			= 2;
	tp[pn].me			= 1;
	tp[pn].mi			= 2;
	tp[pn].cqp			= cqp_data; 
	tp[pn].opt_value	= 0.0;
	
	/* Betts's problem */
	pn++;
	cqp_data = malloc(sizeof(gsl_cqp_data));
	status = betts(cqp_data);
	
	tp[pn].name 		= "Betts's problem";
	tp[pn].n 			= 2;
	tp[pn].me			= 1;
	tp[pn].mi			= 4;
	tp[pn].cqp			= cqp_data; 
	tp[pn].opt_value	= 0.04;
	
	/* Beale's problem */
	pn++;
	cqp_data = malloc(sizeof(gsl_cqp_data));
	status = beale(cqp_data);
	
	tp[pn].name 		= "Beale's problem";
	tp[pn].n 			= 3;
	tp[pn].me			= 1;
	tp[pn].mi			= 3;
	tp[pn].cqp			= cqp_data; 
	tp[pn].opt_value	= 9.0+1.0/9.0;
	
	
  return EXIT_SUCCESS;
}

int solve(test_problem *tp)
{
	const size_t max_iter = 1000;
	
	size_t iter=1;
	
	int status;
	
	const gsl_cqpminimizer_type * T;
	gsl_cqpminimizer *s;
	
	T = gsl_cqpminimizer_mg_pdip;
	s = gsl_cqpminimizer_alloc(T, tp->n, tp->me, tp->mi);	
	
	status = gsl_cqpminimizer_set(s, tp->cqp);
	
	printf("********************  %s  ********************\n\n",tp->name);
		
	printf("== Itn ======= f ======== ||gap|| ==== ||residual||\n\n");
	
	do
	{
		
		status = gsl_cqpminimizer_iterate(s);
		status = gsl_cqpminimizer_test_convergence(s, 1e-10, 1e-10);
		  
		printf("%4d   %14.8f  %13.6e  %13.6e\n", iter, gsl_cqpminimizer_f(s), gsl_cqpminimizer_gap(s), gsl_cqpminimizer_residuals_norm(s));
		  
		if(status == GSL_SUCCESS)
    	{
			size_t j;
			printf("\nMinimum is found at\n");
        	for(j=0; j<gsl_cqpminimizer_x(s)->size; j++)
				printf("%9.6f ",gsl_vector_get(gsl_cqpminimizer_x(s), j));
			printf("\n\n");
			
			printf("\nLagrange-multiplier for Ax=b\n");
        	for(j=0; j<gsl_cqpminimizer_lm_eq(s)->size; j++)
				printf("%9.6f ",gsl_vector_get(gsl_cqpminimizer_lm_eq(s), j));
			printf("\n\n");
			
			printf("\nLagrange-multiplier for Cx>=d\n");
        	for(j=0; j<gsl_cqpminimizer_lm_ineq(s)->size; j++)
				printf("%9.6f ",gsl_vector_get(gsl_cqpminimizer_lm_ineq(s), j));
			printf("\n\n");
    	}  
		else
    	{
			iter++;
    	}  
	
		}
	while(status == GSL_CONTINUE && iter<=max_iter);
	
	gsl_cqpminimizer_free(s);
	
	return GSL_SUCCESS;
}

int test_problems_free(test_problem *tp, size_t n)
{
	
	size_t i;
	
	for(i=0; i<n; i++)
	{
		gsl_matrix_free(tp[i].cqp->Q);
		gsl_vector_free(tp[i].cqp->q);
		
		gsl_matrix_free(tp[i].cqp->A);
		gsl_vector_free(tp[i].cqp->b);
		
		gsl_matrix_free(tp[i].cqp->C);
		gsl_vector_free(tp[i].cqp->d);
		
		free(tp[i].cqp);
	}
	
	return GSL_SUCCESS;
}
