/***************************************************************************
 *   Copyright (C) 2004 by Ewgenij Hbner                                  *
 *   huebner@uni-trier.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#include "test_examples.h"

int
beale(gsl_cqp_data * beale_cqp)
{	
	beale_cqp->Q = gsl_matrix_alloc(3, 3);
	beale_cqp->q = gsl_vector_alloc(3);
	
			
	beale_cqp->A = gsl_matrix_alloc(1,3);
	beale_cqp->b = gsl_vector_calloc(1);
	beale_cqp->C = gsl_matrix_calloc(3, 3);
	beale_cqp->d = gsl_vector_calloc(3);
	
	/* Q */
	gsl_matrix_set(beale_cqp->Q,0,0,4.0); gsl_matrix_set(beale_cqp->Q,0,1,2.0); gsl_matrix_set(beale_cqp->Q,0,2,2.0);
	gsl_matrix_set(beale_cqp->Q,1,0,2.0); gsl_matrix_set(beale_cqp->Q,1,1,4.0); gsl_matrix_set(beale_cqp->Q,1,2,0.0);
	gsl_matrix_set(beale_cqp->Q,2,0,2.0); gsl_matrix_set(beale_cqp->Q,2,1,0.0); gsl_matrix_set(beale_cqp->Q,2,2,2.0);
	/* q */
	gsl_vector_set(beale_cqp->q,0,-8.0); gsl_vector_set(beale_cqp->q,1,-6.0); gsl_vector_set(beale_cqp->q,2,-4.0);
	/* A */
	gsl_matrix_set(beale_cqp->A,0,0,-1.0); gsl_matrix_set(beale_cqp->A,0,1,-1.0); ; gsl_matrix_set(beale_cqp->A,0,2,-2.0);
	/* b */
	gsl_vector_set(beale_cqp->b,0,-3.0);
	/* C */
	gsl_matrix_set_identity(beale_cqp->C);
	/* d */
	
	
	return GSL_SUCCESS;
}

