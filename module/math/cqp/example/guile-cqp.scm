(use-modules (language clambda clambda))
(use-modules (language clambda scm))

#|
This is a wrapper around QP Convex programming library ontop

Note, this links to GPL v2 (v3) data so that the overall license when
linking with gsl and the QP library are essentially GPL

License   : LGPL, 
Copyright : Stefan Israelsson Tampe
|#

;;Prefix for seting up the clambda->c compilation
(init-clambda-scm)

(clambda-add (cpp-include "header.h"))

(auto-defs)

;;***********************************************

(define-syntax get-result
  (syntax-rules ()
    ((_ fkn s)
     (<recur> pr-loop ((int j (<c> 0)) (SCM ret (<scm> '())))
       (<if> (q< j (-> (<icall> fkn s) size))		       
             (<let> ((SCM res (<cons>
                               (<double->number>
                                (<icall> 'gsl_vector_get 
                                         (<icall> fkn s) j))
                               ret)))
               (<next> pr-loop (<+> j (<c> 1))		     
                       res))
	     (<reverse> ret))))))


(<define> validate-int (x)
  (<<if>> (<<and>> (<integer?> x)
		   (s<= x (<scm> 1000000000))
		   (s>= x (<scm> 0)))
	  (<scm> #t)
	  (<error> "qp problem sizes is not a valid integer")))


(<define> validate-num (x)
  (<<if>> (<number?> x)
          (<scm> #t)
          (<error> "qp data element are not a number")))

(<define> void set-vector (((gsl_vector *) d->Q) (SCM Q) (int n))
  (<recur> loop-i ((int i (<c> 0)) (Q Q))
    (<if> (q< i n)
          (<match> (Q)
            ((,Q . ,l)
             (<begin>
              (<call> validate-num Q)
              (<icall> 'gsl_vector_set
                       d->Q
                       i (<number->double> Q))
              (<next> loop-i (<+> i (<c> 1)) l)))
            (_ (<error> 
                "number of V elements does not match spec"))))))
	
(<define> void set-matrix (((gsl_matrix *) d->Q)
                           (SCM Q) (int m) (int n))
  (<recur> loop-i ((int i (<c> 0)) (Q Q))
    (<if> (q< i m)
          (<match> (Q)
            ((,Q . ,l)
             (<recur> loop-j ((int j (<c> 0)) (Q Q))
               (<if> (q< j n)
                     (<match> (Q)
                       ((,Q . ,l)
                        (<begin>
                         (<call> validate-num Q)
                         (<icall> 'gsl_matrix_set
                                  d->Q
                                  i j (<number->double> Q))
                         (<next> loop-j (<+> j (<c> 1)) l)))
                       (_ (<error> 
                           "number of M col elements does not match spec")))
                     (<next> loop-i (<+> i (<c> 1)) l))))
            (_ (<error> 
                "number of M row elements does not match spec"))))))


(define-syntax new-matrix-m
  (syntax-rules ()
    ((_ m n) (<icall> 'gsl_matrix_alloc m n))))
(define-syntax new-matrix-c
  (syntax-rules ()
    ((_ m n) (<icall> 'gsl_matrix_calloc m n))))
(define-syntax new-vector-m
  (syntax-rules ()
    ((_ m) (<icall> 'gsl_vector_alloc m))))
(define-syntax new-vector-c
  (syntax-rules ()
    ((_ m) (<icall> 'gsl_vector_calloc m))))
     

(<define> void fill-in-data (((gsl_cqp_data *) da) 
			     (SCM              l ) 
			     (int              n ) 
			     (int              me) 
			     (int              mi))

  ;;Allocation of data from the heap!!
  (<=> (-> da Q)  (new-matrix-m n n))
  (<=> (-> da q)  (new-vector-m n))
  (<=> (-> da A)  (new-matrix-m me n))
  (<=> (-> da b)  (new-vector-m me))
  (<=> (-> da C)  (new-matrix-c mi n))
  (<=> (-> da d)  (new-vector-c mi))
  
  
  (<match> (l)
    ((,Q ,q ,A ,b ,C ,d)
     (<begin>      
      (<call> set-matrix (-> da Q) Q n  n)      
      (<call> set-vector (-> da q) q n)
      (<call> set-matrix (-> da A) A me n)
      (<call> set-vector (-> da b) b me)
      (<call> set-matrix (-> da C) C mi n)
      (<call> set-vector (-> da d) d mi)))
    (_ (<error> "wrong number of linalg structures (Q q A b C d)"))))
	     

(<scm-ext>
(<define> solve (Problem)	  
  (<match> (Problem)
    (((,n ,me ,mi) . ,l)
     (<begin>
      (<call> validate-int n )
      (<call> validate-int me)
      (<call> validate-int mi)
      (<let> ((size_t                     max-iter (<c> 1000))
              (int                        status   *)
              ((gsl_cqpminimizer_type *)  T        *)
              ((gsl_cqpminimizer      *)  s        *)
              (gsl_cqp_data               data     *)
              (SCM                        ret      *)
              (int                        n-i      (<fixnum->int> n))
              (int                        me-i     (<fixnum->int> me))
              (int                        mi-i     (<fixnum->int> mi)))

        (<=> T      (<lit> 'gsl_cqpminimizer_mg_pdip))
	(<=> s      (<icall> 'gsl_cqpminimizer_alloc 
			     T 
			     n-i
			     me-i
			     mi-i))
	(<call> fill-in-data (<addr> data) l n-i me-i mi-i)
	(<=> status (<icall> 'gsl_cqpminimizer_set s (<addr> data)))
	 	
	(<=> ret 
	     (<recur> loop ((int i (<c> 1)))
	       (<=> status  (<icall> 'gsl_cqpminimizer_iterate s))
	       (<=> status  (<icall> 'gsl_cqpminimizer_test_convergence
				     s (<c> 1e-10) (<c> 1e-10)))
	       (<if> (<==> status (<lit> 'GSL_SUCCESS))
		     (<scm>
		      `(,(get-result 'gsl_cqpminimizer_x       s)
			,(get-result 'gsl_cqpminimizer_lm_eq   s) 
			,(get-result 'gsl_cqpminimizer_lm_ineq s)))
		     (<if> (<and> (<==> status (<lit> 'GSL_CONTINUE))
				  (q<= i max-iter))
			   (<next> loop (<+> i (<c> 1)))))))
	(<icall> 'gsl_cqpminimizer_free s)
	ret)))
    (_ (<error> "wrong problem description ((n me mi) . l)")))))

;;POSFIX
(<define> void init()
  (auto-inits))

#;
(<define> int main ()
  (<call> init)
  (<c> 0))

(clambda->c "guile-cqp.c")
;; *****************************************
