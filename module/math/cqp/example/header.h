#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#include "../include/gsl_cqp.h"
#include "../../../../libguile.h"

#define AREF(w,i) (w)[i]
SCM ERROR(const char *w) 
{
  scm_misc_error("clambda error", w, SCM_EOL);
  return SCM_BOOL_F;
}

typedef struct gsl_matrix s_matrix;
typedef struct gsl_vector s_vector;

//#include "header.h"
