/***************************************************************************
 *   Copyright (C) 2004 by Ewgenij Hbner                                  *
 *   huebner@uni-trier.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#include "test_examples.h"

int
gould(gsl_cqp_data * gould_cqp)
{	
	gould_cqp->Q = gsl_matrix_alloc(2, 2);
	gould_cqp->q = gsl_vector_alloc(2);
	
			
	gould_cqp->A = gsl_matrix_alloc(1,2);
	gould_cqp->b = gsl_vector_alloc(1);
	gould_cqp->C = gsl_matrix_calloc(2, 2);
	gould_cqp->d = gsl_vector_calloc(2);
	
	/* Q */
	gsl_matrix_set_identity(gould_cqp->Q);
	/* q */
	gsl_vector_set(gould_cqp->q,0,-2.0); gsl_vector_set(gould_cqp->q,1,-1.0);
	/* A */
	gsl_matrix_set(gould_cqp->A,0,0,3.0); gsl_matrix_set(gould_cqp->A,0,1,1.0);
	/* b */
	gsl_vector_set(gould_cqp->b,0,1.5);
	/* C */
	gsl_matrix_set(gould_cqp->C,0,0,1.0); gsl_matrix_set(gould_cqp->C,1,1,1.0);
	/* d=0 */
	
	
	return GSL_SUCCESS;
}

