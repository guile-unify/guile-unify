;; d (y - x - p) ** 2
;; x <= y

(define I '())
(define (reset-layout) (set! I '()))

(define mk-var
  (let ((x (gensym "x")))
    (set! I (cons x I))
    (lambda () x)))

(define (x=y x y)
  (lambda (v)
    (match v
      ('xy  '())
      ('x   '())
      ('1   '())
      ('=   '(0 (-1 ,(x)) (1 ,(y))))
      ('<=  '()))))

(define (x->y x y d)
  (define (pxy d) (* -1 2 d))
  (define (px q d p) (* -1 2 q d p))
  (lambda (v)
    (match v
      ('xy `((,(pxy d) ,(x) ,(y)) (,d ,(x) ,(x)) (,d ,(y) ,(y))))
      ('x  `((,(px 1 d p) ,(x)) (,(px -1 d p) ,(y))))
      ('1   (* p p))
      ('=   '())
      ('<= `(0 (`(-1 ,(x)) `(1 ,(y))))))))

(define (union . l)
  (lambda (v)
    (fold append '() (map (lambda (f) (f v)) l))))

(define strong 100)
(define medium 10)
(define weak   1)

(define Qxy
  (lambda (dpx)
    (let ((xmin (mk-var)) (xmax (mk-var)))
      (lambda (dpy)
        (let ((ymin (mk-var)) (ymax (mk-var)))
          (lambda (v)
            (match v
              ('f (list xmin xmax ymin ymax))
              (_  ((union (x->y xmin xmax dpx strong)
                          (x->y ymin ymax dpy strong))
                   v)))))))))

(define Qyx
  (lambda (dpy)
    (let ((ymin (mk-var)) (ymax (mk-var)))
      (lambda (dpx)
        (let ((xmin (mk-var)) (xmax (mk-var)))
          (lambda (v)
            (match v
              ('f (list xmin xmax ymin ymax))
              (_  ((union (x->y xmin xmax dpx strong)
                          (x->y ymin ymax dpy strong))
                   v)))))))))
            
(define Spxy
  (lambda (dpx)
    (let ((xmin (mk-var)) (xmax (mk-var)))
      (lambda ()
        (let ((ymin (mk-var)) (ymax (mk-var)))
          (lambda (v)
            (match v
              ('f (list xmin xmax ymin ymax))
              (_  ((x->y xmin xmax dpx strong) v)))))))))

(define Spyx
  (lambda ()
    (let ((ymin (mk-var)) (ymax (mk-var)))
      (lambda (dpx)
        (let ((xmin (mk-var)) (xmax (mk-var)))
          (lambda (v)
            (match v
              ('f (list xmin xmax ymin ymax))
              (_  ((x->y xmin xmax dpx strong) v)))))))))
             

(define (row . a)
  (let ((xmin (mk-var)) 
        (xmax (mk-var)) 
        (ymin (mk-var)) 
        (ymax (mk-var)))
    (match a
      ((a) a)
      ((a b ... c)
       (let ((link (map (lambda (x x+1)
                          (match (x 'f)
                            ((_ xmax _ _)
                             (match (x+1 'f)
                               ((xmin _ _ _)
                                (x=y xmax xmin))))))
                        (cons a b)
                        (append b (list c))))
             (l1    (match (x 'f)
                      ((x-mi . _)
                       (list (x=y xmin x-mi)))))
             (ln    (match (c 'f)
                      ((_ x-ma . _)
                       (list (x=y x-ma xmax)))))
             (lmax  (map (lambda (c)
                           (match (c 'f)
                             ((_ _ _ yma)
                              (x->y yma ymax 0 strong))))
                         a))
             (lmin  (map (lambda (c)
                           (match (c 'f)
                             ((_ _ ymi _)
                              (x->y ymin ymi 0 strong))))
                         a))
             (append link l1 ln lmax lmin)))))))

   
    
(define (make-qp-data L)
  (define (mk-H s)
    (define H '())
    (define (add p x y)
      (let loop ((h H))
        (match h
          (((and a (q ,x ,y)) . _)
           (set-car a (+ p q)))
          (((and a (q ,y ,x)) . _)
           (set-car a (+ p q)))
          ((_ . l)
           (loop l))
          (() 
           (set! H (cons (list x y p) H))))))
                         
    (let loop ((L L))
      (match L
        ((X . L)
         (begin
           (match (X s)
             (((p vx vy) ...)
              (map (lambda (p vx vy)
                     (add p vx vy))
                   p vx vy)))
           (loop L)))
        (() H))))

  (define (mk-v s)
    (define H '())
    (define (add p x)
      (let loop ((h H))
        (match h
          (((and a (q ,x)) . _)
           (set-car a (+ p q)))
          ((_ . l)
           (loop l))
          (() 
           (set! H (cons (list x y p) H))))))
    
    (let loop ((L L))
      (match L
        ((X . L)
         (begin
           (match (X s)
             (((p vx) ...)
              (map (lambda (p vx)
                     (add p vx))
                   p vx)))
           (loop L)))
        (() H))))

  (define (mk-a s)
    (let loop ((L L))
      (match L
        ((X . L)
         (append (X s) (loop L)))
        () '())))

  (define (collect A H)
    (define (coll x H)
      (if (member x H)
          H
          (cons x H)))
    (let loop ((X A) (H H))
      (((p x y) . l)
       (loop l (coll x (coll y H))))
      (((p x)   . l)
       (loop l (coll x H)))
      () H))

  (define (add-i R)
    (let loop ((R R) (I 0))
      (match R
        ((x . l)
         (cons (cons x I)
               (loop l (+ I 1))))
        (() '()))))

  (define (ar2li Ar N)
    (let loop ((I 0))
      (if (eq? I N)
          '()
          (cons
           (let loop2 ((J 0))
             (if (eq? J N)
                 '()
                 (cons
                  (array-ref Ar I J)
                  (loop2 (+ J 1)))))
           (loop (+ I 1))))))

  (define (v2li v N)
    (let loop ((I 0))
      (if (eq? I N)
          '()
          (cons (array-ref v I) (loop (+ I 1))))))
             
  (let* ((H   (mk-H 'xy))
         (v   (mk-v 'x))
         (Ae* (mk-a '=))
         (ve  (map car Ae*))
         (Ae  (map cdr Ae*))
         (Ai* (mk_a '<=))
         (vi  (map car Ai*))
         (Ai  (map cdr Ai*))
         (R   (collect
               (fold append '() Ai)
               (collect
                (fold append '() Ae)
                (collect 
                 v 
                 (collect H '())))))
         (RI   (add-i R))
         (N    (length RI))         
         (arH  (make-array 0 N N))
         (arV  (make-array 0 N))
         (Ne   (length Ae))
         (arAe (make-array 0 Ne N))
         (arve (make-array 0 Ne))
         (Ni   (length Ai))
         (arAi (make-array 0 Ni N))
         (arvi (make-array 0 Ni)))

    ;;*********** Building up the arrays
    (let loop ((H H))
      (match H
        (((p x y) . l)
         (if (eq? x y)
             (array-set! arH p (get-i x) (get-i y))
             (begin
               (array-set! arH (/ p 2) (get-i x) (get-i y))
               (array-set! arH (/ p 2) (get-i y) (get-i x))))
         (loop l))
        (() 'ok)))

    (let loop ((v v))
      (match v
        (((p x) . l)
         (array-set! arV p (get-i x)))
        (() 'ok)))

    (let loop ((I 0) (Ai Ai) (vi vi))
      (match Ai
        ((x . l)
         (let loop2 ((x x))
           (match x
             (((p x) . l)
              (array-set! arAi p I (get-i x))
              (loop2 l))             
             (() 'ok)))
         (array-set! arvi (car vi) I)
         (loop (+ I 1) l (cdr vi))
        (() 'ok))))

    (let loop ((I 0) (Ae Ae) (ve ve))
      (match Ae
        ((x . l)
         (let loop2 ((x x))
           (match x
             (((p x) . l)
              (array-set! arAe p I (get-i x))
              (loop2 l))             
             (() 'ok)))
         (array-set! arve (car ve) I)
         (loop (+ I 1) l (cdr ve))
        (() 'ok))))
    
    ;; *array builder finished, output again to lists and yield
    ;; A final structure
    `((,N ,Mi ,Me)
      ,(ar2li arH N N)
      ,(v2li  arV N)
      ,(ar2li arMi Mi N)
      ,(v2li  arvi Mi)
      ,(ar2li arMe Me N)
      ,(v2li  arve Me))))


  
