#|
Syntax parse examples
|#
(use-modules (parse syntax-parse))
(use-modules (parse match-parse))


(define <list2> 
  (<parse-list> 
   #t                           ;; signal error if  #t else record error if #f

   ;; error msg if not a list
   "in let decl, (symbol val) malformed"    

   ;; error msg in case of wrong number of elements
   "list must have length 2"    

   2))                          ;; the length of the list


(define <arg2> 
  (<parse-list..> 
   #t                          
   "not a list Bug! args are a list"    
   "either * and/or code1 is missing in:~%   (let * code1 code ...)"
   3))


(define <list> 
  (<parse-list..> 
   #t                           
   "declaration form in let is not a list"                 
   "Bug if this is printed"     
   ))                          

;; here is an example of designing a matcher out of pieces

(define <arg2..>
  (lambda (x)
    (match-parse -abs ((<arg2> x) (<list> y))  ;; matchers needs declaration
                 x

          ;; if 2 args followed by a list of 0 ... elements then success
          ((<arg2> <list>) (list #t))             
          (_               (error "Bug in let macro")))))
           
;; here is a simple checked let form

;; Using the symbol tool
(define <sym> (<symbol> "variable decl in let"))

(define-syntax my-let
  (lambda (x)
    (syntax-parse x () 
      ((_ (((s : <sym>) v : <list2>) ... : <list>) 
          code2 ... : <arg2..> ) 
       #'(let ((s v) ...) code2 ...)))))




